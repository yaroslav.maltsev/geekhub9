[![codecov](https://codecov.io/gl/yaroslav.maltsev/geekhub9/branch/master/graph/badge.svg?token=wVufVvbLzN)](https://codecov.io/gl/yaroslav.maltsev/geekhub9)

# GeekHub9

Java for Web project for GeekHub, Season 9

##########################################

|Lesson|Date|Topic|
|:----:|:--:|:----|
| 1|October  2, 2019|Intro|
| 2|October 10, 2019|Built-In Classes|
| 3|October 16, 2019|Object-Oriented Programming|
| 4|October 23, 2019|Generics. Collections Framework - List|
| 5|October 30, 2019|Collections Framework - Set, Map|
| 6|November 5, 2019|Practice|
| 7|November 13, 2019|Code Testing (Unit Testing)|
| 8|November 18, 2019|Functional Programming|
| 9|November 25, 2019|Stream API|
| 10|December 2, 2019|Exceptions|
| 11|December 9, 2019|Input/Output|
| 12|December 16, 2019|Reflection API|
| 13|December 23, 2019|Servlet API (Web)|
| 14|January 08, 2020|JDBC|
| 15|January 13, 2020|Practice|
| 16|January 20, 2020|Spring IoC|
| 17|January 27, 2020|Spring MVC, Boot|
| 18|February 03, 2020|Spring Security|
| 19|February 10, 2020|REST, Swagger|
| 20|February 17, 2020|Practice|
| 21|February 24, 2020|Spring JDBC, Flyway|
| 22|March 02, 2020|Gradle|
| 23|March 09, 2020|Integration Testing|
| 24|March 16, 2020|Concurrency|
| 25|March 23, 2020|Course works pre-review|
| 26|March 30, 2020|Hibernate, Spring Data|
| 27|April 06, 2020|Practice|
| 28|April 13, 2020|Course works final review|
