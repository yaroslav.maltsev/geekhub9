package org.geekhub.socialnetwork;

import org.geekhub.socialnetwork.manager.comment.CommentManager;
import org.geekhub.socialnetwork.manager.history.HistoryManager;
import org.geekhub.socialnetwork.manager.like.LikeManager;
import org.geekhub.socialnetwork.manager.post.PostManager;
import org.geekhub.socialnetwork.persistance.comment.CommentRecord;
import org.geekhub.socialnetwork.persistance.history.HistoryRecord;
import org.geekhub.socialnetwork.persistance.history.RecordType;
import org.geekhub.socialnetwork.persistance.like.LikeRecord;
import org.geekhub.socialnetwork.persistance.post.PostRecord;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class HistoryReaderTest {
    private PostManager postManager;
    private LikeManager likeManager;
    private CommentManager commentManager;
    private HistoryManager historyManager;
    private HistoryReader historyReader;

    @BeforeMethod
    public void setUp() {
        historyManager = mock(HistoryManager.class);
        postManager = mock(PostManager.class);
        likeManager = mock(LikeManager.class);
        commentManager = mock(CommentManager.class);

        historyReader = new HistoryReader(historyManager, postManager, likeManager, commentManager);
    }

    @Test
    public void test_getPostsHistory_when_history_is_empty_is_success() {
        when(historyManager.getAllRecordsOfType(RecordType.POST)).thenReturn(new ArrayList<>());
        Map<LocalDateTime, PostRecord> postsHistory = historyReader.getGlobalPostHistory();
        assertEquals(postsHistory.size(), 0);
    }

    @Test
    public void test_getPostsHistory_when_history_has_one_record_is_success() {
        int id = 1;
        PostRecord record = new PostRecord();
        LocalDateTime date = LocalDateTime.now();
        record.setDate(date);
        record.setAuthorId(1);
        record.setId(id);

        when(historyManager.getAllRecordsOfType(RecordType.POST)).thenReturn(
                Collections.singletonList(new HistoryRecord(record))
        );
        when(postManager.getPostById(id)).thenReturn(record);

        Map<LocalDateTime, PostRecord> postsHistory = historyReader.getGlobalPostHistory();
        assertEquals(postsHistory.size(), 1);
        assertEquals(postsHistory.get(date).getId(), 1);
    }

    @Test
    public void test_getPostsHistory_when_history_has_more_than_one_record_is_success() {
        List<HistoryRecord> historyElements = new ArrayList<>();
        LocalDateTime now = LocalDateTime.now();

        PostRecord firstHistoryRecord = new PostRecord();
        firstHistoryRecord.setDate(now);
        firstHistoryRecord.setId(1);
        PostRecord secondHistoryRecord = new PostRecord();
        secondHistoryRecord.setDate(now.plusDays(1));
        secondHistoryRecord.setId(2);

        historyElements.add(new HistoryRecord(firstHistoryRecord));
        historyElements.add(new HistoryRecord(secondHistoryRecord));

        PostRecord firstPostRecord = new PostRecord();
        firstPostRecord.setId(1);
        PostRecord secondPostRecord = new PostRecord();
        secondPostRecord.setId(2);

        when(historyManager.getAllRecordsOfType(RecordType.POST)).thenReturn(historyElements);
        when(postManager.getPostById(1)).thenReturn(firstPostRecord);
        when(postManager.getPostById(2)).thenReturn(secondPostRecord);

        Map<LocalDateTime, PostRecord> postsHistory = historyReader.getGlobalPostHistory();
        assertEquals(postsHistory.size(), 2);
        assertEquals(postsHistory.get(now).getId(), 1);
        assertEquals(postsHistory.get(now.plusDays(1)).getId(), 2);
    }


    @Test
    public void test_getPostHistoryForUser_when_history_is_empty_is_success() {
        int userId = 1;
        when(historyManager.getAllUserRecordsOfType(userId, RecordType.POST)).thenReturn(new ArrayList<>());
        Map<LocalDateTime, PostRecord> postsHistory = historyReader.getPostHistoryForUser(userId);
        assertEquals(postsHistory.size(), 0);
    }

    @Test
    public void test_getPostHistoryForUser_when_history_has_one_record_is_success() {
        int userId = 2;
        int id = 1;

        PostRecord record = new PostRecord();
        LocalDateTime date = LocalDateTime.now();
        record.setDate(date);
        record.setAuthorId(1);
        record.setId(id);

        when(historyManager.getAllUserRecordsOfType(userId, RecordType.POST)).thenReturn(
                Collections.singletonList(new HistoryRecord(record))
        );
        when(postManager.getPostById(id)).thenReturn(record);

        Map<LocalDateTime, PostRecord> postsHistory = historyReader.getPostHistoryForUser(userId);
        assertEquals(postsHistory.size(), 1);
        assertEquals(postsHistory.get(date).getId(), 1);
    }

    @Test
    public void test_getPostHistoryForUser_when_history_has_more_than_one_record_is_success() {
        int userId = 1;

        List<HistoryRecord> historyElements = new ArrayList<>();
        LocalDateTime now = LocalDateTime.now();

        PostRecord firstHistoryRecord = new PostRecord();
        firstHistoryRecord.setDate(now);
        firstHistoryRecord.setId(1);
        PostRecord secondHistoryRecord = new PostRecord();
        secondHistoryRecord.setDate(now.plusDays(1));
        secondHistoryRecord.setId(2);

        historyElements.add(new HistoryRecord(firstHistoryRecord));
        historyElements.add(new HistoryRecord(secondHistoryRecord));

        PostRecord firstPostRecord = new PostRecord();
        firstPostRecord.setId(1);
        PostRecord secondPostRecord = new PostRecord();
        secondPostRecord.setId(2);

        when(historyManager.getAllUserRecordsOfType(userId, RecordType.POST)).thenReturn(historyElements);
        when(postManager.getPostById(1)).thenReturn(firstPostRecord);
        when(postManager.getPostById(2)).thenReturn(secondPostRecord);

        Map<LocalDateTime, PostRecord> postsHistory = historyReader.getPostHistoryForUser(userId);
        assertEquals(postsHistory.size(), 2);
        assertEquals(postsHistory.get(now).getId(), 1);
        assertEquals(postsHistory.get(now.plusDays(1)).getId(), 2);
    }


    @Test
    public void test_getGlobalLikeHistory_when_history_is_empty_is_success() {
        when(historyManager.getAllRecordsOfType(RecordType.LIKE)).thenReturn(new ArrayList<>());
        Map<LocalDateTime, LikeRecord> history = historyReader.getGlobalLikeHistory();
        assertEquals(history.size(), 0);
    }

    @Test
    public void test_getGlobalLikeHistory_when_history_has_one_record_is_success() {
        LikeRecord historyRecord = new LikeRecord();
        LocalDateTime date = LocalDateTime.now();
        historyRecord.setDate(date);
        historyRecord.setId(1);

        when(historyManager.getAllRecordsOfType(RecordType.LIKE)).thenReturn(
                Collections.singletonList(new HistoryRecord(historyRecord))
        );
        LikeRecord record = new LikeRecord();
        record.setId(1);
        when(likeManager.getLikeById(1)).thenReturn(record);

        Map<LocalDateTime, LikeRecord> history = historyReader.getGlobalLikeHistory();
        assertEquals(history.size(), 1);
        assertEquals(history.get(date).getId(), 1);
    }

    @Test
    public void test_getGlobalLikeHistory_when_history_has_more_than_one_record_is_success() {
        List<HistoryRecord> historyElements = new ArrayList<>();
        LocalDateTime now = LocalDateTime.now();

        LikeRecord firstHistoryRecord = new LikeRecord();
        firstHistoryRecord.setDate(now);
        firstHistoryRecord.setId(1);

        LikeRecord secondHistoryRecord = new LikeRecord();
        secondHistoryRecord.setDate(now.plusDays(1));
        secondHistoryRecord.setId(2);

        historyElements.add(new HistoryRecord(firstHistoryRecord));
        historyElements.add(new HistoryRecord(secondHistoryRecord));

        when(historyManager.getAllRecordsOfType(RecordType.LIKE)).thenReturn(historyElements);
        when(likeManager.getLikeById(1)).thenReturn(firstHistoryRecord);
        when(likeManager.getLikeById(2)).thenReturn(secondHistoryRecord);

        Map<LocalDateTime, LikeRecord> history = historyReader.getGlobalLikeHistory();
        assertEquals(history.size(), historyElements.size());
        assertEquals(history.get(now).getId(), 1);
        assertEquals(history.get(now.plusDays(1)).getId(), 2);
    }


    @Test
    public void test_getLikeHistoryForUser_when_history_is_empty_is_success() {
        int userId = 1;
        when(historyManager.getAllUserRecordsOfType(userId, RecordType.LIKE)).thenReturn(new ArrayList<>());
        Map<LocalDateTime, LikeRecord> history = historyReader.getLikeHistoryForUser(userId);
        assertEquals(history.size(), 0);
    }

    @Test
    public void test_getLikeHistoryForUser_when_history_has_one_record_is_success() {
        int userId = 1;

        LikeRecord historyRecord = new LikeRecord();
        LocalDateTime date = LocalDateTime.now();
        historyRecord.setDate(date);
        historyRecord.setId(1);

        when(historyManager.getAllUserRecordsOfType(userId, RecordType.LIKE)).thenReturn(
                Collections.singletonList(new HistoryRecord(historyRecord))
        );
        LikeRecord record = new LikeRecord();
        record.setId(1);
        when(likeManager.getLikeById(1)).thenReturn(record);

        Map<LocalDateTime, LikeRecord> history = historyReader.getLikeHistoryForUser(userId);
        assertEquals(history.size(), 1);
        assertEquals(history.get(date).getId(), 1);
    }

    @Test
    public void test_getLikeHistoryForUser_when_history_has_more_than_one_record_is_success() {
        int userId = 1;

        List<HistoryRecord> historyElements = new ArrayList<>();
        LocalDateTime now = LocalDateTime.now();

        LikeRecord firstHistoryRecord = new LikeRecord();
        firstHistoryRecord.setDate(now);
        firstHistoryRecord.setId(1);

        LikeRecord secondHistoryRecord = new LikeRecord();
        secondHistoryRecord.setDate(now.plusDays(1));
        secondHistoryRecord.setId(2);

        historyElements.add(new HistoryRecord(firstHistoryRecord));
        historyElements.add(new HistoryRecord(secondHistoryRecord));

        when(historyManager.getAllUserRecordsOfType(userId, RecordType.LIKE)).thenReturn(historyElements);
        when(likeManager.getLikeById(1)).thenReturn(firstHistoryRecord);
        when(likeManager.getLikeById(2)).thenReturn(secondHistoryRecord);

        Map<LocalDateTime, LikeRecord> history = historyReader.getLikeHistoryForUser(userId);
        assertEquals(history.size(), historyElements.size());
        assertEquals(history.get(now).getId(), 1);
        assertEquals(history.get(now.plusDays(1)).getId(), 2);
    }



    @Test
    public void test_getGlobalCommentHistory_when_history_is_empty_is_success() {
        when(historyManager.getAllRecordsOfType(RecordType.COMMENT)).thenReturn(new ArrayList<>());
        Map<LocalDateTime, CommentRecord> history = historyReader.getGlobalCommentRecordHistory();
        assertEquals(history.size(), 0);
    }

    @Test
    public void test_getGlobalCommentHistory_when_history_has_one_record_is_success() {
        CommentRecord historyRecord = new CommentRecord();
        LocalDateTime date = LocalDateTime.now();
        historyRecord.setDate(date);
        historyRecord.setId(1);

        when(historyManager.getAllRecordsOfType(RecordType.COMMENT)).thenReturn(
                Collections.singletonList(new HistoryRecord(historyRecord))
        );
        CommentRecord record = new CommentRecord();
        record.setId(1);
        when(commentManager.getCommentById(1)).thenReturn(record);

        Map<LocalDateTime, CommentRecord> history = historyReader.getGlobalCommentRecordHistory();
        assertEquals(history.size(), 1);
        assertEquals(history.get(date).getId(), 1);
    }

    @Test
    public void test_getGlobalCommentHistory_when_history_has_more_than_one_record_is_success() {
        List<HistoryRecord> historyElements = new ArrayList<>();
        LocalDateTime now = LocalDateTime.now();

        CommentRecord firstHistoryRecord = new CommentRecord();
        firstHistoryRecord.setDate(now);
        firstHistoryRecord.setId(1);

        CommentRecord secondHistoryRecord = new CommentRecord();
        secondHistoryRecord.setDate(now.plusDays(1));
        secondHistoryRecord.setId(2);

        historyElements.add(new HistoryRecord(firstHistoryRecord));
        historyElements.add(new HistoryRecord(secondHistoryRecord));

        when(historyManager.getAllRecordsOfType(RecordType.COMMENT)).thenReturn(historyElements);
        when(commentManager.getCommentById(1)).thenReturn(firstHistoryRecord);
        when(commentManager.getCommentById(2)).thenReturn(secondHistoryRecord);

        Map<LocalDateTime, CommentRecord> history = historyReader.getGlobalCommentRecordHistory();
        assertEquals(history.size(), historyElements.size());
        assertEquals(history.get(now).getId(), 1);
        assertEquals(history.get(now.plusDays(1)).getId(), 2);
    }


    @Test
    public void test_getCommentHistoryForUser_when_history_is_empty_is_success() {
        int userId = 1;
        when(historyManager.getAllUserRecordsOfType(userId, RecordType.COMMENT)).thenReturn(new ArrayList<>());
        Map<LocalDateTime, CommentRecord> history = historyReader.getCommentHistoryForUser(userId);
        assertEquals(history.size(), 0);
    }

    @Test
    public void test_getCommentHistoryForUser_when_history_has_one_record_is_success() {
        int userId = 1;
        CommentRecord historyRecord = new CommentRecord();
        LocalDateTime date = LocalDateTime.now();
        historyRecord.setDate(date);
        historyRecord.setId(1);

        when(historyManager.getAllUserRecordsOfType(userId, RecordType.COMMENT)).thenReturn(
                Collections.singletonList(new HistoryRecord(historyRecord))
        );
        CommentRecord record = new CommentRecord();
        record.setId(1);
        when(commentManager.getCommentById(1)).thenReturn(record);

        Map<LocalDateTime, CommentRecord> history = historyReader.getCommentHistoryForUser(userId);
        assertEquals(history.size(), 1);
        assertEquals(history.get(date).getId(), 1);
    }

    @Test
    public void test_getCommentHistoryForUser_when_history_has_more_than_one_record_is_success() {
        int userId = 1;

        List<HistoryRecord> historyElements = new ArrayList<>();
        LocalDateTime now = LocalDateTime.now();

        CommentRecord firstHistoryRecord = new CommentRecord();
        firstHistoryRecord.setDate(now);
        firstHistoryRecord.setId(1);

        CommentRecord secondHistoryRecord = new CommentRecord();
        secondHistoryRecord.setDate(now.plusDays(1));
        secondHistoryRecord.setId(2);

        historyElements.add(new HistoryRecord(firstHistoryRecord));
        historyElements.add(new HistoryRecord(secondHistoryRecord));

        when(historyManager.getAllUserRecordsOfType(userId, RecordType.COMMENT)).thenReturn(historyElements);
        when(commentManager.getCommentById(1)).thenReturn(firstHistoryRecord);
        when(commentManager.getCommentById(2)).thenReturn(secondHistoryRecord);

        Map<LocalDateTime, CommentRecord> history = historyReader.getCommentHistoryForUser(userId);
        assertEquals(history.size(), historyElements.size());
        assertEquals(history.get(now).getId(), 1);
        assertEquals(history.get(now.plusDays(1)).getId(), 2);
    }
}
