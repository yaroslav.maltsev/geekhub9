package org.geekhub.socialnetwork;

import org.geekhub.socialnetwork.manager.comment.CommentManager;
import org.geekhub.socialnetwork.manager.history.HistoryManager;
import org.geekhub.socialnetwork.manager.like.LikeManager;
import org.geekhub.socialnetwork.manager.post.PostManager;
import org.geekhub.socialnetwork.persistance.comment.CommentRecord;
import org.geekhub.socialnetwork.persistance.history.HistoryRecord;
import org.geekhub.socialnetwork.persistance.history.RecordType;
import org.geekhub.socialnetwork.persistance.like.LikeRecord;
import org.geekhub.socialnetwork.persistance.post.PostRecord;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.IntFunction;

@Service
public class HistoryReader {
    private final HistoryManager historyManager;
    private final PostManager postManager;
    private final LikeManager likeManager;
    private final CommentManager commentManager;

    public HistoryReader(
            HistoryManager historyManager,
            PostManager postManager,
            LikeManager likeManager,
            CommentManager commentManager
    ) {
        this.historyManager = historyManager;
        this.postManager = postManager;
        this.likeManager = likeManager;
        this.commentManager = commentManager;
    }

    public Map<LocalDateTime, PostRecord> getPostHistoryForUser(int userId) {
        List<HistoryRecord> historyElements = historyManager.getAllUserRecordsOfType(userId, RecordType.POST);
        return getHistoryOfSpecificRecordsType(historyElements, postManager::getPostById);
    }

    public Map<LocalDateTime, PostRecord> getGlobalPostHistory() {
        List<HistoryRecord> historyElements = historyManager.getAllRecordsOfType(RecordType.POST);
        return getHistoryOfSpecificRecordsType(historyElements, postManager::getPostById);
    }

    public Map<LocalDateTime, LikeRecord> getLikeHistoryForUser(int userId) {
        List<HistoryRecord> historyElements = historyManager.getAllUserRecordsOfType(userId, RecordType.LIKE);
        return getHistoryOfSpecificRecordsType(historyElements, likeManager::getLikeById);
    }

    public Map<LocalDateTime, LikeRecord> getGlobalLikeHistory() {
        List<HistoryRecord> historyElements = historyManager.getAllRecordsOfType(RecordType.LIKE);
        return getHistoryOfSpecificRecordsType(historyElements, likeManager::getLikeById);
    }

    public Map<LocalDateTime, CommentRecord> getCommentHistoryForUser(int userId) {
        List<HistoryRecord> historyElements = historyManager.getAllUserRecordsOfType(userId, RecordType.COMMENT);
        return getHistoryOfSpecificRecordsType(historyElements, commentManager::getCommentById);
    }

    public Map<LocalDateTime, CommentRecord> getGlobalCommentRecordHistory() {
        List<HistoryRecord> historyElements = historyManager.getAllRecordsOfType(RecordType.COMMENT);
        return getHistoryOfSpecificRecordsType(historyElements, commentManager::getCommentById);
    }


    private <T> Map<LocalDateTime, T> getHistoryOfSpecificRecordsType(
            List<HistoryRecord> historyElements,
            IntFunction<T> func
    ) {
        Map<LocalDateTime, T> recordsMap = new HashMap<>();
        for (HistoryRecord historyRecord : historyElements) {
            T record = func.apply(historyRecord.getRecordId());
            recordsMap.put(historyRecord.getDateOfAction(), record);
        }
        return recordsMap;
    }
}
