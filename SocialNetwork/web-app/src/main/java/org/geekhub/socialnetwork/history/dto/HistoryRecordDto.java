package org.geekhub.socialnetwork.history.dto;

import org.geekhub.socialnetwork.persistance.history.HistoryElement;

import java.time.LocalDateTime;

public class HistoryRecordDto<T extends HistoryElement> {
    private LocalDateTime date;
    private T historyElement;

    public HistoryRecordDto(LocalDateTime date, T historyElement) {
        this.date = date;
        this.historyElement = historyElement;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public HistoryElement getHistoryElement() {
        return historyElement;
    }

    public void setHistoryElement(T historyElement) {
        this.historyElement = historyElement;
    }
}
