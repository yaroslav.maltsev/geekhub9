package org.geekhub.socialnetwork.converter.dto.posts;

import org.geekhub.socialnetwork.persistance.post.PostRecord;
import org.geekhub.socialnetwork.posts.dto.UserPostInfoDto;

import java.util.ArrayList;
import java.util.List;

public class PostDtoConverter {
    private PostDtoConverter() {
    }

    public static List<UserPostInfoDto> convertRecordsInDtos(List<PostRecord> userPosts) {
        List<UserPostInfoDto> postInfoDtos = new ArrayList<>();
        for (PostRecord post : userPosts) {
            UserPostInfoDto infoDto = new UserPostInfoDto(
                    post.getId(),
                    post.getDate(),
                    post.getDescription(),
                    post.getAuthorId(),
                    post.getAuthorUsername()
            );
            postInfoDtos.add(infoDto);
        }
        return postInfoDtos;
    }
}
