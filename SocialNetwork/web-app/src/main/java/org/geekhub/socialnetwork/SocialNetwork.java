package org.geekhub.socialnetwork;

import org.apache.log4j.BasicConfigurator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableWebSecurity
@EnableWebMvc
@EnableScheduling
public class SocialNetwork {
    public static void main(String[] args) {
        BasicConfigurator.configure();
        SpringApplication.run(SocialNetwork.class, args);
    }
}
