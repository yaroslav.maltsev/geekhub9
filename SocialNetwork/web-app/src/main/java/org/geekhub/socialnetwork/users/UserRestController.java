package org.geekhub.socialnetwork.users;

import io.swagger.annotations.ApiParam;
import org.geekhub.socialnetwork.CustomUserDetails;
import org.geekhub.socialnetwork.converter.enums.GenderEnumConverter;
import org.geekhub.socialnetwork.friends.dto.FriendDto;
import org.geekhub.socialnetwork.manager.post.PostManager;
import org.geekhub.socialnetwork.manager.user.UsersManager;
import org.geekhub.socialnetwork.persistance.user.UserRecord;
import org.geekhub.socialnetwork.users.dto.UsersInfoDto;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

import static org.geekhub.socialnetwork.converter.dto.friend.FriendDtoConverter.convertRecordsInDto;
import static org.geekhub.socialnetwork.converter.dto.posts.PostDtoConverter.convertRecordsInDtos;

@RestController
public class UserRestController {
    private final UsersManager usersManager;
    private final PostManager postManager;
    private final PasswordEncoder passwordEncoder;
    private final GenderEnumConverter genderEnumConverter = new GenderEnumConverter();

    public UserRestController(
            UsersManager usersManager,
            PostManager postManager, PasswordEncoder passwordEncoder
    ) {
        this.usersManager = usersManager;
        this.postManager = postManager;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/users/{userId}/info/")
    public UsersInfoDto getUserInfo(@PathVariable int userId) {
            UserRecord userRecord = usersManager.getUserById(userId);
            return new UsersInfoDto(
                    userRecord,
                    convertRecordsInDtos(postManager.getAllUserPosts(userId))
            );
    }

    @GetMapping("/users/me")
    public UsersInfoDto getInfoAboutMe( @ApiParam(hidden = true) @AuthenticationPrincipal CustomUserDetails user) {
        UserRecord userRecord = user.getUserRecord();
        return new UsersInfoDto(
                userRecord,
                convertRecordsInDtos(postManager.getAllUserPosts(userRecord.getId()))
        );
    }

    @DeleteMapping("/users/me/delete/")
    public void deleteMyAccount(
            @AuthenticationPrincipal CustomUserDetails user,
            HttpServletRequest request
    ) throws ServletException {
        int userId = user.getUserRecord().getId();
        usersManager.deleteUser(userId);
        request.logout();
    }

    @DeleteMapping("/users/{userId}/delete")
    public void deleteUserAccount(
            @PathVariable int userId,
            Authentication auth
    ) {
        String userEmail = "";
        if (auth != null) {
            userEmail = auth.getName();
        }
        String role = usersManager.getUserByEmail(userEmail).getRole();
        if (role.equals("ROLE_ADMIN") || role.equals("ROLE_SUPER_ADMIN")) {
            usersManager.deleteUser(userId);
        } else {
            throw new IllegalArgumentException("Only user with role admin can delete another user!");
        }
    }

    @PostMapping("/users/{userId}/update/role")
    public void updateUserRole(
            @PathVariable int userId,
            @RequestParam String role,
            @ApiParam(hidden = true) @AuthenticationPrincipal CustomUserDetails user
    ) {

        if (Objects.isNull(role)) {
            throw new IllegalArgumentException("Role must be not null!");
        } else if ("ROLE_SUPER_ADMIN".equals(role)) {
            throw new IllegalArgumentException("Can exist only one super-admin in app!");
        }

        UserRecord userRecord = user.getUserRecord();
        if (userRecord.getId() == userId) {
            throw new IllegalArgumentException("Super admin can't change his role");
        }

        UserRecord userForUpdate = usersManager.getUserById(userId);
        if (role.equals(userForUpdate.getRole())) {
            return;
        }

        if ("ROLE_ADMIN".equals(role) || "ROLE_USER".equals(role)) {
            userForUpdate.setRole(role);
        } else {
            throw new IllegalArgumentException("Role " + role + " not exist!");
        }

        usersManager.updateUser(userForUpdate);
    }

    @PostMapping("/users/me/update/")
    public void updateUserInfo(
            @RequestParam String username,
            @RequestParam String password,
            @RequestParam String gender,
            @ApiParam(hidden = true) @AuthenticationPrincipal CustomUserDetails user
    ) {
        if (username.isBlank() || password.isBlank() || gender.isBlank()) {
            throw new IllegalArgumentException("Parameters must be not null!");
        }

        UserRecord userRecord = user.getUserRecord();

        userRecord.setUsername(username);
        userRecord.setEncodedPassword(passwordEncoder.encode(password));
        userRecord.setGender(genderEnumConverter.convert(gender));

        usersManager.updateUser(userRecord);
    }

    @GetMapping("/users/{userId}/friends/confirmed")
    public List<FriendDto> getConfirmedUserFriends(@PathVariable int userId) {
        return convertRecordsInDto(usersManager.getConfirmedUserFriends(userId));
    }

    @GetMapping("/users/{userId}/friends/notConfirmed")
    public List<FriendDto> getNotConfirmedUserFriends(@PathVariable int userId) {
        return convertRecordsInDto(usersManager.getNotConfirmedUserFriends(userId));
    }

    @GetMapping("/users/{userId}/friends/requests")
    public List<FriendDto> getUserRequestsToFriends(@PathVariable int userId) {
        return convertRecordsInDto(usersManager.getFriendshipRequestForUser(userId));
    }
}
