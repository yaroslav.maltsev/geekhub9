package org.geekhub.socialnetwork.advice;

import org.apache.log4j.Logger;
import org.geekhub.socialnetwork.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class ExceptionAdvice extends ResponseEntityExceptionHandler {
    private final Logger logger = Logger.getLogger(ExceptionAdvice.class);

    @ExceptionHandler(PasswordNotMatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handlePasswordNotMatchException(PasswordNotMatchException e) {
        String message = "Passwords are not equals!";
        logger.error(message, e);
        return new ErrorDto(message);
    }

    @ExceptionHandler(UserAlreadyExistException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ErrorDto handleUserAlreadyExistException(UserAlreadyExistException e) {
        String message = "User with email " + e.getEmail() + " already exist!";
        logger.error(message, e);
        return new ErrorDto(message);
    }

    @ExceptionHandler(RelationAlreadyExistException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ErrorDto handleRelationAlreadyExistException(RelationAlreadyExistException e) {
        String message = String.format(
                "Friendship with users with ids %s and %s already exist!", e.getUserId(), e.getFriendId()
        );
        logger.error(message, e);
        return new ErrorDto(message);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handleIllegalArgumentException(IllegalArgumentException e) {
        String message = "It's impossible to handle request";
        logger.error(message, e);
        return new ErrorDto(message);
    }

    @ExceptionHandler(FailedAutoLoginException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorDto handleFailedAutoLoginException(FailedAutoLoginException e) {
        String message = "Auto login was failed for user with email " + e.getEmail();
        logger.error(message, e);
        return new ErrorDto(message);
    }

    @ExceptionHandler(AccountNotActivatedException.class)
    @ResponseStatus(HttpStatus.LOCKED)
    public ErrorDto handleAccountNotActivatedException(
            AccountNotActivatedException e,
            @Value("${spring.root.path}") String rootPath
            ) {
        String pathForRepeatingSending = rootPath + "/activate/" + e.getAccountId() + "/repeate";
        String message = "You must to confirm account activation in your email. " +
                "Click on the link to send code one more: \n" +
                pathForRepeatingSending;

        logger.error(message, e);
        return new ErrorDto(message);
    }

    @ExceptionHandler(RecordNotExistException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handleRecordNotExistException(RecordNotExistException e) {
        String message = "Record not exist!";
        logger.error(message, e);
        return new ErrorDto(message);
    }
}
