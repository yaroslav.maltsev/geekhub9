package org.geekhub.socialnetwork;

import org.geekhub.socialnetwork.registration.UserRegistrationDto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class FrontendPagesController {
    @GetMapping("/register")
    public String getRegister(Model model) {
        model.addAttribute("userRegistrationDto", new UserRegistrationDto());
        return "register";
    }

    @GetMapping("/login")
    public String getLogin() {
        return "login";
    }
}
