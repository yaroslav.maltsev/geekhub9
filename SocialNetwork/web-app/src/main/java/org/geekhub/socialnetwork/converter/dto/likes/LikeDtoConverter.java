package org.geekhub.socialnetwork.converter.dto.likes;

import org.geekhub.socialnetwork.persistance.like.LikeRecord;
import org.geekhub.socialnetwork.likes.dto.LikeInfoDto;

import java.util.ArrayList;
import java.util.List;

public class LikeDtoConverter {
    private LikeDtoConverter() {
    }

    public static List<LikeInfoDto> convertLikeRecordListInDtoList(List<LikeRecord> allPostLikes) {
        List<LikeInfoDto> likeInfoDtos = new ArrayList<>();
        for (LikeRecord likeRecord : allPostLikes) {
            LikeInfoDto infoDto = new LikeInfoDto(likeRecord.getDate(), likeRecord.getAuthorId(), likeRecord.getAuthorUsername());
            likeInfoDtos.add(infoDto);
        }
        return likeInfoDtos;
    }
}
