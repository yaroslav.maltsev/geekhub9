package org.geekhub.socialnetwork.converter.dto.analytics;

import org.geekhub.socialnetwork.analytics.dto.HistoryAnalyticsDto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HistoryAnalyticsDtoConverter {
    private HistoryAnalyticsDtoConverter() {
    }

    public static List<HistoryAnalyticsDto> convertMapInDtoList(Map<LocalDateTime, Long> analyticResult) {
        List<HistoryAnalyticsDto> resultInDto = new ArrayList<>();

        for (Map.Entry<LocalDateTime, Long> entry: analyticResult.entrySet()) {
            HistoryAnalyticsDto dto = new HistoryAnalyticsDto(entry.getKey(), entry.getValue());
            resultInDto.add(dto);
        }

        return resultInDto;
    }
}
