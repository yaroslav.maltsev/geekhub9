package org.geekhub.socialnetwork.converter.dto.history;

import org.geekhub.socialnetwork.history.dto.HistoryRecordDto;
import org.geekhub.socialnetwork.persistance.history.HistoryElement;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HistoryDtoConverter {
    private HistoryDtoConverter(){}


    public static <T extends HistoryElement> List<HistoryRecordDto<T>> getHistoryMapInDtoList(Map<LocalDateTime, T> globalLikeHistory) {
        List<HistoryRecordDto<T>> dtoList = new ArrayList<>();
        for (Map.Entry<LocalDateTime, T> entry : globalLikeHistory.entrySet()) {
            HistoryRecordDto<T> dto = new HistoryRecordDto<>(entry.getKey(), entry.getValue());
            dtoList.add(dto);
        }
        return dtoList;
    }
}
