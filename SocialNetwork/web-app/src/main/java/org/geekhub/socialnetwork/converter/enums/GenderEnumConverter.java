package org.geekhub.socialnetwork.converter.enums;

import org.geekhub.socialnetwork.persistance.user.Gender;
import org.springframework.core.convert.converter.Converter;

public class GenderEnumConverter implements Converter<String, Gender> {
    @Override
    public Gender convert(String source) {
        try {
            return Gender.valueOf(source.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Gender with name \"" + source + "\" not exist", e);
        }
    }
}
