package org.geekhub.socialnetwork.converter.dto.friend;

import org.geekhub.socialnetwork.friends.dto.FriendDto;
import org.geekhub.socialnetwork.persistance.user.UserRecord;

import java.util.ArrayList;
import java.util.List;

public class FriendDtoConverter {
    private FriendDtoConverter() {
    }

    public static List<FriendDto> convertRecordsInDto(List<UserRecord> list) {
        List<FriendDto> dtoList = new ArrayList<>();
        for (UserRecord record : list) {
            dtoList.add(new FriendDto(record.getId(), record.getUsername()));
        }
        return dtoList;
    }
}
