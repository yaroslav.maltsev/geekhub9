package org.geekhub.socialnetwork.comments;

import io.swagger.annotations.ApiParam;
import org.geekhub.socialnetwork.CustomUserDetails;
import org.geekhub.socialnetwork.comments.dto.CommentRecordDto;
import org.geekhub.socialnetwork.manager.comment.CommentManager;
import org.geekhub.socialnetwork.manager.post.PostManager;
import org.geekhub.socialnetwork.persistance.comment.CommentRecord;
import org.geekhub.socialnetwork.persistance.post.PostRecord;
import org.geekhub.socialnetwork.persistance.user.UserRecord;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.geekhub.socialnetwork.converter.dto.comments.CommentsDtoConverter.convertCommentRecordListInDtoList;

@RequestMapping("/users/{userId}/posts/{postId}/comments")
@RestController
public class CommentsRestController {
    private final PostManager postManager;
    private final CommentManager commentsManager;

    public CommentsRestController(
            PostManager postManager,
            CommentManager commentsManager
    ) {
        this.postManager = postManager;
        this.commentsManager = commentsManager;
    }

    @GetMapping
    public List<CommentRecordDto> getPostComments(@PathVariable int userId, @PathVariable int postId) {
        PostRecord post = postManager.getPostById(postId);

        checkIfAuthorAndUserInPathAreIdentical(post, userId);

        List<CommentRecord> allPostComments = commentsManager.getAllPostComments(post.getId());
        return convertCommentRecordListInDtoList(allPostComments);
    }

    @ResponseBody
    @PostMapping("/add")
    public void addCommentToPost(
            @PathVariable int userId,
            @PathVariable int postId,
            @RequestParam String text,
            @ApiParam(hidden = true) @AuthenticationPrincipal CustomUserDetails user
    ) {
        PostRecord post = postManager.getPostById(postId);

        checkIfAuthorAndUserInPathAreIdentical(post, userId);

        UserRecord userRecord = user.getUserRecord();
        int authorId = userRecord.getId();
        CommentRecord commentRecord = new CommentRecord();
        commentRecord.setAuthorUsername(userRecord.getUsername());
        commentRecord.setPostId(postId);
        commentRecord.setAuthorId(authorId);
        commentRecord.setText(text);
        commentRecord.setDate(LocalDateTime.now().truncatedTo(ChronoUnit.MILLIS));

        commentsManager.addComment(commentRecord);
    }

    @ResponseBody
    @DeleteMapping("/{commentId}/delete")
    public void deleteComment(
            @PathVariable int userId,
            @PathVariable int postId,
            @PathVariable int commentId,
            @ApiParam(hidden = true) @AuthenticationPrincipal CustomUserDetails user
    ) {

        PostRecord post = postManager.getPostById(postId);

        checkIfAuthorAndUserInPathAreIdentical(post, userId);

        CommentRecord commentRecord = commentsManager.getCommentById(commentId);
        int authorId = user.getUserRecord().getId();

        if (commentRecord.getPostId() != postId) {
            throw new IllegalArgumentException("Comment postId and postId in path must be identical!");
        } else {
            if (commentRecord.getAuthorId() != authorId) {
                throw new IllegalArgumentException("Delete comment can only author of comment!");
            }
        }

        commentsManager.deleteComment(commentId);
    }

    @ResponseBody
    @PostMapping("/{commentId}/edit")
    public void editComment(
            @PathVariable int userId,
            @PathVariable int postId,
            @PathVariable int commentId,
            @RequestParam String editedText,
            @ApiParam(hidden = true) @AuthenticationPrincipal CustomUserDetails user
    ) {

        PostRecord post = postManager.getPostById(postId);
        checkIfAuthorAndUserInPathAreIdentical(post, userId);

        CommentRecord commentRecord = commentsManager.getCommentById(commentId);
        int authorId = user.getUserRecord().getId();

        if (commentRecord.getPostId() != postId) {
            throw new IllegalArgumentException("Comment postId and postId in path must be identical!");
        } else {
            if (commentRecord.getAuthorId() != authorId) {
                throw new IllegalArgumentException("Edit comment can only author of comment!");
            }
        }

        commentRecord.setText(editedText);

        commentsManager.editComment(commentRecord);
    }

    private void checkIfAuthorAndUserInPathAreIdentical(PostRecord post, int userId) {
        if (post.getAuthorId() != userId) {
            throw new IllegalArgumentException("Author of post and user in path must be equal!");
        }
    }
}
