package org.geekhub.socialnetwork.likes;


import io.swagger.annotations.ApiParam;
import org.geekhub.socialnetwork.CustomUserDetails;
import org.geekhub.socialnetwork.manager.like.LikeManager;
import org.geekhub.socialnetwork.manager.post.PostManager;
import org.geekhub.socialnetwork.persistance.like.LikeRecord;
import org.geekhub.socialnetwork.persistance.post.PostRecord;
import org.geekhub.socialnetwork.likes.dto.LikeInfoDto;
import org.geekhub.socialnetwork.persistance.user.UserRecord;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

import static org.geekhub.socialnetwork.converter.dto.likes.LikeDtoConverter.convertLikeRecordListInDtoList;

@RequestMapping("/users/{userId}/posts/{postId}/likes")
@RestController
public class LikesRestController {
    private final PostManager postManager;
    private final LikeManager likeManager;

    public LikesRestController(
            PostManager postManager,
            LikeManager likeManager
    ) {
        this.postManager = postManager;
        this.likeManager = likeManager;
    }

    @GetMapping
    public List<LikeInfoDto> getPostLikes(@PathVariable int userId, @PathVariable int postId) {
        checkIfAuthorAndUserInPathAreIdentical(userId, postId);

        PostRecord post = postManager.getPostById(postId);

        List<LikeRecord> allPostLikes = likeManager.getAllPostLikes(post.getId());

        return convertLikeRecordListInDtoList(allPostLikes);
    }

    @PostMapping("/add")
    public void addLikeToPost(
            @PathVariable int userId,
            @PathVariable int postId,
            @ApiParam(hidden = true) @AuthenticationPrincipal CustomUserDetails user
    ) {

        checkIfAuthorAndUserInPathAreIdentical(userId, postId);

        UserRecord userRecord = user.getUserRecord();
        int authorId = userRecord.getId();
        LikeRecord like = new LikeRecord();
        like.setAuthorUsername(userRecord.getUsername());
        like.setPostId(postId);
        like.setAuthorId(authorId);
        like.setDate(LocalDateTime.now());

        likeManager.addLike(like);
    }

    @ResponseBody
    @DeleteMapping("{likeId}/delete")
    public void deleteLike(
            @PathVariable int userId,
            @PathVariable int postId,
            @PathVariable int likeId,
            @ApiParam(hidden = true) @AuthenticationPrincipal CustomUserDetails user
    ) {
        checkIfAuthorAndUserInPathAreIdentical(userId, postId);

        LikeRecord likeRecord = likeManager.getLikeById(likeId);
        int authenticatedUserId = user.getUserRecord().getId();

        if (likeRecord.getPostId() != postId) {
            throw new IllegalArgumentException("Like postId and postId in path must be identical!");
        } else {
            if (likeRecord.getAuthorId() != authenticatedUserId) {
                throw new IllegalArgumentException("Delete like can only author of like!");
            }
        }

        likeManager.deleteLike(likeId);
    }


    private void checkIfAuthorAndUserInPathAreIdentical(int userId, int postId) {
        PostRecord post = postManager.getPostById(postId);
        if (post == null) {
            throw new IllegalArgumentException(String.format("Post with id %s not exist!", postId));
        } else if (post.getAuthorId() != userId) {
            throw new IllegalArgumentException("Author of post and user in path must be equal!");
        }
    }
}
