package org.geekhub.socialnetwork.main;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class MainPageController {
    @GetMapping("/")
    public void getFriendsPosts(
            HttpServletResponse response
    ) throws IOException {
        response.sendRedirect("/users/me");
    }
}
