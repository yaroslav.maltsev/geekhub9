package org.geekhub.socialnetwork.history;

import org.geekhub.socialnetwork.HistoryReader;
import org.geekhub.socialnetwork.history.dto.HistoryRecordDto;
import org.geekhub.socialnetwork.persistance.comment.CommentRecord;
import org.geekhub.socialnetwork.persistance.like.LikeRecord;
import org.geekhub.socialnetwork.persistance.post.PostRecord;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.geekhub.socialnetwork.converter.dto.history.HistoryDtoConverter.getHistoryMapInDtoList;

@RequestMapping("/history/")
@RestController
public class HistoryRestController {
    private final HistoryReader historyReader;

    public HistoryRestController(HistoryReader historyReader) {
        this.historyReader = historyReader;
    }

    @GetMapping("global/likes/")
    public List<HistoryRecordDto<LikeRecord>> getGlobalLikesHistory() {
        return getHistoryMapInDtoList(historyReader.getGlobalLikeHistory());
    }

    @GetMapping("/likes/{userId}")
    public List<HistoryRecordDto<LikeRecord>> getUserLikesHistory(
            @PathVariable int userId
    ) {
        return getHistoryMapInDtoList(historyReader.getLikeHistoryForUser(userId));
    }


    @GetMapping("global/comments/")
    public List<HistoryRecordDto<CommentRecord>> getGlobalCommentsHistory() {
        return getHistoryMapInDtoList(historyReader.getGlobalCommentRecordHistory());
    }

    @GetMapping("/comments/{userId}")
    public List<HistoryRecordDto<CommentRecord>> getUserCommentsHistory(
            @PathVariable int userId
    ) {
        return getHistoryMapInDtoList(historyReader.getCommentHistoryForUser(userId));
    }


    @GetMapping("global/posts/")
    public List<HistoryRecordDto<PostRecord>> getGlobalPostHistory() {
        return getHistoryMapInDtoList(historyReader.getGlobalPostHistory());
    }

    @GetMapping("/posts/{userId}")
    public List<HistoryRecordDto<PostRecord>> getPostHistoryForUser(
            @PathVariable int userId
    ) {
        return getHistoryMapInDtoList(historyReader.getPostHistoryForUser(userId));
    }
}
