package org.geekhub.socialnetwork.analytics;

import org.geekhub.socialnetwork.HistoryAudit;
import org.geekhub.socialnetwork.analytics.dto.HistoryAnalyticsDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static org.geekhub.socialnetwork.converter.dto.analytics.HistoryAnalyticsDtoConverter.convertMapInDtoList;

@RequestMapping("/analytics/history/")
@RestController
public class HistoryAnalyticsRestController {
    private final HistoryAudit historyAudit;

    public HistoryAnalyticsRestController(HistoryAudit historyAudit) {
        this.historyAudit = historyAudit;
    }

    @GetMapping("posts/{daysAmount}")
    public List<HistoryAnalyticsDto> getCountOfAllPosts(
            @PathVariable int daysAmount
    ) {
        Map<LocalDateTime, Long> analyticResult = historyAudit.getCountOfPostsPerLastDays(daysAmount);
        return convertMapInDtoList(analyticResult);
    }

    @GetMapping("users/{userId}/posts/{daysAmount}")
    public List<HistoryAnalyticsDto> getCountOfUserPosts(
            @PathVariable int userId,
            @PathVariable int daysAmount
    ) {
        Map<LocalDateTime, Long> analyticResult = historyAudit.getCountOfUserPostsPerLastDays(userId, daysAmount);
        return convertMapInDtoList(analyticResult);
    }


    @GetMapping("likes/{daysAmount}")
    public List<HistoryAnalyticsDto> getCountOfAllLikes(
            @PathVariable int daysAmount
    ) {
        Map<LocalDateTime, Long> analyticResult = historyAudit.getCountOfLikesPerLastDays(daysAmount);
        return convertMapInDtoList(analyticResult);
    }

    @GetMapping("users/{userId}/likes/{daysAmount}")
    public List<HistoryAnalyticsDto> getCountOfUserLikes(
            @PathVariable int userId,
            @PathVariable int daysAmount
    ) {
        Map<LocalDateTime, Long> analyticResult = historyAudit.getCountOfUserLikesPerLastDays(userId, daysAmount);
        return convertMapInDtoList(analyticResult);
    }


    @GetMapping("comments/{daysAmount}")
    public List<HistoryAnalyticsDto> getCountOfAllComments(
            @PathVariable int daysAmount
    ) {
        Map<LocalDateTime, Long> analyticResult = historyAudit.getCountOfCommentsPerLastDays(daysAmount);
        return convertMapInDtoList(analyticResult);
    }

    @GetMapping("users/{userId}/comments/{daysAmount}")
    public List<HistoryAnalyticsDto> getCountOfUserComments(
            @PathVariable int userId,
            @PathVariable int daysAmount
    ) {
        Map<LocalDateTime, Long> analyticResult = historyAudit.getCountOfUserCommentsPerLastDays(userId, daysAmount);
        return convertMapInDtoList(analyticResult);
    }
}
