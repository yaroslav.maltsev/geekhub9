package org.geekhub.socialnetwork.likes.dto;

import java.time.LocalDateTime;

public class LikeInfoDto {
    private LocalDateTime date;
    private int authorId;
    private String authorUsername;

    public LikeInfoDto(LocalDateTime date, int authorId, String authorUsername) {
        this.date = date;
        this.authorId = authorId;
        this.authorUsername = authorUsername;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getAuthorUsername() {
        return authorUsername;
    }

    public void setAuthorUsername(String authorUsername) {
        this.authorUsername = authorUsername;
    }
}
