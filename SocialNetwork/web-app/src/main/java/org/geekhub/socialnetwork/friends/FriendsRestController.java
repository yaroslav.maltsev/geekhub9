package org.geekhub.socialnetwork.friends;

import io.swagger.annotations.ApiParam;
import org.geekhub.socialnetwork.CustomUserDetails;
import org.geekhub.socialnetwork.manager.friends.FriendsManager;
import org.geekhub.socialnetwork.persistance.user_friend.UserFriendRecord;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/users/me/friends/")
@RestController
public class FriendsRestController {
    private final FriendsManager friendsManager;

    public FriendsRestController(
            FriendsManager friendsManager
    ) {
        this.friendsManager = friendsManager;
    }

    @PostMapping("{friendId}/add")
    public void addFriend(
            @PathVariable int friendId,
            @ApiParam(hidden = true) @AuthenticationPrincipal CustomUserDetails user
    ) {
        int userId = user.getUserRecord().getId();
        friendsManager.addUserFriendRequest(userId, friendId);
    }

    @PostMapping("{friendId}/confirm")
    public void confirmFriendship(
            @PathVariable int friendId,
            @ApiParam(hidden = true) @AuthenticationPrincipal CustomUserDetails user
    ) {
        int userId = user.getUserRecord().getId();

        UserFriendRecord recordByIds = friendsManager.getRecordByIds(userId, friendId);

        if (recordByIds.getFriendId() == userId) {
            friendsManager.confirmFriendship(userId, friendId);
        } else {
            throw new IllegalArgumentException("friendship must be confirmed by the friend, not the initiator!");
        }
    }

    @DeleteMapping("{friendId}/delete")
    public void deleteFriend (
            @PathVariable int friendId,
            @ApiParam(hidden = true) @AuthenticationPrincipal CustomUserDetails user
    ) {
        int userId = user.getUserRecord().getId();

        friendsManager.deleteRecord(userId, friendId);

    }
}
