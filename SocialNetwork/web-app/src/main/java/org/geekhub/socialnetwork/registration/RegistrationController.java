package org.geekhub.socialnetwork.registration;

import io.swagger.annotations.ApiParam;
import org.geekhub.socialnetwork.PasswordNotMatchException;
import org.geekhub.socialnetwork.mail.MailSender;
import org.geekhub.socialnetwork.manager.registration.RegistrationManager;
import org.geekhub.socialnetwork.persistance.user.Gender;
import org.geekhub.socialnetwork.persistance.user.UserRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;

@RestController
public class RegistrationController {
    private final RegistrationManager registrationManager;
    private final PasswordEncoder passwordEncoder;
    private final MailSender mailSender;

    @Value("${spring.root.path}")
    private String rootPath;

    public RegistrationController(
            RegistrationManager registrationManager,
            PasswordEncoder passwordEncoder,
            MailSender mailSender
    ) {
        this.registrationManager = registrationManager;
        this.passwordEncoder = passwordEncoder;
        this.mailSender = mailSender;
    }

    @PostMapping(value = "/register")
    @ResponseBody
    public void registerUser(
            @ModelAttribute UserRegistrationDto registrationDto,
            @ApiParam(hidden = true) HttpServletResponse response
    ) throws IOException {
        String username = registrationDto.getUsername();
        String email = registrationDto.getEmail();
        String password = registrationDto.getPassword();
        String confirmPassword = registrationDto.getConfirmPassword();
        Gender gender = registrationDto.getGender();

        checkIfParamsAreBlank(username, email, password, confirmPassword);
        checkIfPasswordsAreEquals(password, confirmPassword);

        final String defaultRole = "ROLE_USER";
        final String activationCode = UUID.randomUUID().toString();

        addUserInDao(LocalDateTime.now(), email, password, defaultRole, username, gender, activationCode);

        confirmRegistration(email, username, activationCode);

        response.sendRedirect("/register/confirm");
    }

    @GetMapping("/register/confirm")
    public String showPageAfterSuccessRegistration() {
        return "Registration was success. You need to confirm account activation in your email.";
    }

    @GetMapping("/activate/{activationCode}")
    public void activateAccount(
            @PathVariable String activationCode,
            @ApiParam(hidden = true) HttpServletResponse response
    ) throws IOException {
        registrationManager.activateUser(activationCode);
        response.sendRedirect("/users/me");
    }


    private void addUserInDao(
            LocalDateTime registrationDate,
            String email,
            String password,
            String role,
            String name,
            Gender gender,
            String activationCode
    ) {
        UserRecord userRecord = new UserRecord(
                registrationDate,
                email,
                passwordEncoder.encode(password),
                role,
                name,
                gender
        );

        registrationManager.registerUser(userRecord, activationCode);
    }

    private void confirmRegistration(
            String email,
            String name,
            String activationCode

    ) {
        mailSender.send(
                email,
                "Fakebook",
                String.format(
                        "Hello, %s. Welcome to fakebook. You need to confirm account activation. Click the link down below \n" +
                                "%s/activate/%s",
                        name,
                        rootPath,
                        activationCode
                )
        );
    }

    private void checkIfPasswordsAreEquals(String password, String confirmPassword) {
        if (!password.equals(confirmPassword)) {
            throw new PasswordNotMatchException("Passwords are not equals!");
        }
    }

    private void checkIfParamsAreBlank(
            String username,
            String email,
            String passwordParam,
            String confirmPassword
    ) {
        if (username.isBlank() || email.isBlank() || passwordParam.isBlank() || confirmPassword.isBlank()) {
            throw new IllegalArgumentException("Parameters shouldn't be blank");
        }
    }
}
