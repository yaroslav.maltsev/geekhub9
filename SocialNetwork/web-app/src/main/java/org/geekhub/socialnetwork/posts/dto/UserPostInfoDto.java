package org.geekhub.socialnetwork.posts.dto;

import java.time.LocalDateTime;

public class UserPostInfoDto {
    private int postId;
    private LocalDateTime date;
    private String description;
    private int authorId;
    private String authorUsername;

    public UserPostInfoDto(int postId, LocalDateTime date, String description, int authorId, String authorUsername) {
        this.postId = postId;
        this.date = date;
        this.description = description;
        this.authorId = authorId;
        this.authorUsername = authorUsername;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getAuthorUsername() {
        return authorUsername;
    }

    public void setAuthorUsername(String authorUsername) {
        this.authorUsername = authorUsername;
    }
}
