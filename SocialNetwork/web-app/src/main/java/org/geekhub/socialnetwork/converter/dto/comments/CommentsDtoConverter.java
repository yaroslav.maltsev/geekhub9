package org.geekhub.socialnetwork.converter.dto.comments;

import org.geekhub.socialnetwork.comments.dto.CommentRecordDto;
import org.geekhub.socialnetwork.persistance.comment.CommentRecord;

import java.util.ArrayList;
import java.util.List;

public class CommentsDtoConverter {
    public static List<CommentRecordDto> convertCommentRecordListInDtoList(List<CommentRecord> allPostComments) {
        List<CommentRecordDto> dtoList = new ArrayList<>();
        for (CommentRecord record : allPostComments) {
            CommentRecordDto dto = new CommentRecordDto(
                    record.getAuthorId(),
                    record.getId(),
                    record.getDate(),
                    record.getText(),
                    record.getAuthorUsername()
            );
            dtoList.add(dto);
        }
        return dtoList;
    }
}
