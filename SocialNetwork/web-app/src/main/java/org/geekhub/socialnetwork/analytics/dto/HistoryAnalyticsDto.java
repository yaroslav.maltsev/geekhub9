package org.geekhub.socialnetwork.analytics.dto;

import java.time.LocalDateTime;

public class HistoryAnalyticsDto {
    private LocalDateTime date;
    private Long amount;

    public HistoryAnalyticsDto(LocalDateTime date, Long amount) {
        this.date = date;
        this.amount = amount;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }
}
