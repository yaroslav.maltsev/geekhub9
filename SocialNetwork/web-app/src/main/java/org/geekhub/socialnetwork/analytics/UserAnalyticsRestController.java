package org.geekhub.socialnetwork.analytics;

import org.geekhub.socialnetwork.UserAnalytics;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/analytics/users/{userId}")
@RestController
public class UserAnalyticsRestController {
    private final UserAnalytics userAnalytics;

    public UserAnalyticsRestController(UserAnalytics userAnalytics) {
        this.userAnalytics = userAnalytics;
    }

    @GetMapping("/likes")
    public int getAmountOfLikesOnAllPosts(
            @PathVariable int userId
    ) {
        return userAnalytics.getAmountOfAllLikesOnUserPosts(userId);
    }

    @GetMapping("/comments")
    public int getAmountOfCommentsOnAllPosts(
            @PathVariable int userId
    ) {
        return userAnalytics.getAmountOfAllCommentsOnUserPosts(userId);
    }

    @GetMapping("/posts")
    public int getAmountOfAllUserPosts(
            @PathVariable int userId
    ) {
        return userAnalytics.getAmountOfAllUserPosts(userId);
    }
}
