package org.geekhub.socialnetwork.users.dto;

import org.geekhub.socialnetwork.persistance.user.UserRecord;
import org.geekhub.socialnetwork.posts.dto.UserPostInfoDto;

import java.util.List;

public class UsersInfoDto {
    private String username;
    private int userId;
    private List<UserPostInfoDto> posts;

    public UsersInfoDto(UserRecord record, List<UserPostInfoDto> posts) {
        this.userId = record.getId();
        this.username = record.getUsername();
        this.posts = posts;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<UserPostInfoDto> getPosts() {
        return posts;
    }

    public void setPosts(List<UserPostInfoDto> posts) {
        this.posts = posts;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
