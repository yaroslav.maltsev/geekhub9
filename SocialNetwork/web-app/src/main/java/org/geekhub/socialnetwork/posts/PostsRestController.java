package org.geekhub.socialnetwork.posts;

import io.swagger.annotations.ApiParam;
import org.geekhub.socialnetwork.CustomUserDetails;
import org.geekhub.socialnetwork.manager.post.PostManager;
import org.geekhub.socialnetwork.persistance.post.PostRecord;
import org.geekhub.socialnetwork.persistance.user.UserRecord;
import org.geekhub.socialnetwork.posts.dto.UserPostInfoDto;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

import static org.geekhub.socialnetwork.converter.dto.posts.PostDtoConverter.convertRecordsInDtos;

@RequestMapping("/users")
@RestController
public class PostsRestController {

    private final PostManager postManager;

    public PostsRestController(
            PostManager postManager
    ) {
        this.postManager = postManager;
    }

    @GetMapping("/{userId}/posts/")
    public List<UserPostInfoDto> getUserPosts(@PathVariable int userId) {
        List<PostRecord> userPosts = postManager.getAllUserPosts(userId);
        return convertRecordsInDtos(userPosts);
    }

    @GetMapping("/{userId}/posts/{postId}")
    public UserPostInfoDto getUserPost(@PathVariable int userId, @PathVariable int postId) {
        checkIfAuthorAndUserInPathAreIdentical(userId, postId);
        PostRecord post = postManager.getPostById(postId);
        return new UserPostInfoDto(
                post.getId(),
                post.getDate(),
                post.getDescription(),
                post.getAuthorId(),
                post.getAuthorUsername()
        );
    }

    @GetMapping("/me/posts/")
    public List<UserPostInfoDto> getMyPosts( @ApiParam(hidden = true) @AuthenticationPrincipal CustomUserDetails user) {
        int userId = user.getUserRecord().getId();
        List<PostRecord> userPosts = postManager.getAllUserPosts(userId);
        return convertRecordsInDtos(userPosts);
    }

    @PostMapping("/me/posts/add")
    public void createPost(
            @RequestParam String text,
            @ApiParam(hidden = true) @AuthenticationPrincipal CustomUserDetails user
    ) {
        UserRecord userRecord = user.getUserRecord();
        PostRecord postRecord = new PostRecord(
                LocalDateTime.now(),
                text,
                userRecord.getId(),
                userRecord.getUsername()
        );
        postManager.addPost(postRecord);
    }

    @PostMapping("/me/posts/{postId}/edit")
    public void editPost(
            @PathVariable int postId,
            @RequestParam String text,
            @ApiParam(hidden = true) @AuthenticationPrincipal CustomUserDetails user
    ) {
        int userId = user.getUserRecord().getId();

        checkIfAuthorAndUserInPathAreIdentical(userId, postId);

        PostRecord postRecord = postManager.getPostById(postId);
        postRecord.setAuthorId(userId);
        postRecord.setDescription(text);
        postManager.editPost(postRecord);
    }

    @DeleteMapping("/me/posts/{postId}/delete")
    public void deletePost(
            @PathVariable int postId,
            @ApiParam(hidden = true) @AuthenticationPrincipal CustomUserDetails user
    ) {
        int userId = user.getUserRecord().getId();

        checkIfAuthorAndUserInPathAreIdentical(userId, postId);

        postManager.deletePost(postId);
    }


    private void checkIfAuthorAndUserInPathAreIdentical(int userId, int postId) {
        PostRecord post = postManager.getPostById(postId);
        if (post == null) {
            throw new IllegalArgumentException(String.format("Post with id %s not exist!", postId));
        } else if (post.getAuthorId() != userId) {
            throw new IllegalArgumentException("Author of post and user in path must be equal!");
        }
    }


}
