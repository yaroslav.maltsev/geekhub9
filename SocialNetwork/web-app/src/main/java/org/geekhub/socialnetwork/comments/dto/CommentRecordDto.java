package org.geekhub.socialnetwork.comments.dto;

import java.time.LocalDateTime;

public class CommentRecordDto {
    private int authorId;
    private int id;
    private LocalDateTime date;
    private String text;
    private String authorUsername;

    public CommentRecordDto(int authorId, int id, LocalDateTime date, String text, String authorUsername) {
        this.authorId = authorId;
        this.id = id;
        this.date = date;
        this.text = text;
        this.authorUsername = authorUsername;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAuthorUsername() {
        return authorUsername;
    }

    public void setAuthorUsername(String authorUsername) {
        this.authorUsername = authorUsername;
    }
}
