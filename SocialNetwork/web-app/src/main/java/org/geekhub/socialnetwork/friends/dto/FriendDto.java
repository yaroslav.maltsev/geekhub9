package org.geekhub.socialnetwork.friends.dto;

public class FriendDto {
    private int friendId;
    private String friendUsername;

    public FriendDto(int friendId, String friendUsername) {
        this.friendId = friendId;
        this.friendUsername = friendUsername;
    }

    public int getFriendId() {
        return friendId;
    }

    public void setFriendId(int friendId) {
        this.friendId = friendId;
    }

    public String getFriendUsername() {
        return friendUsername;
    }

    public void setFriendUsername(String friendUsername) {
        this.friendUsername = friendUsername;
    }
}
