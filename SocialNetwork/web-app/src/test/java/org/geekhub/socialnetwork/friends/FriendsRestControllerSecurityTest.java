package org.geekhub.socialnetwork.friends;

import org.geekhub.socialnetwork.manager.friends.FriendsManager;
import org.geekhub.socialnetwork.manager.user.UsersManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = FriendsRestController.class)
public class FriendsRestControllerSecurityTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UsersManager usersManager;
    @MockBean
    private FriendsManager friendsManager;
    @MockBean
    private UserDetailsService userDetailsService;
    @MockBean
    private PasswordEncoder passwordEncoder;

    @Test
    public void test_add_security_is_success() throws Exception {
        mockMvc.perform(
                get("/users/me/friends/1/add")
                        .with(user("user").authorities(
                                Collections.singletonList(new SimpleGrantedAuthority("ROLE_GUEST"))
                        ))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    public void test_confirm_security_is_success() throws Exception {
        mockMvc.perform(
                get("/users/me/friends/1/confirm")
                        .with(user("user").authorities(
                                Collections.singletonList(new SimpleGrantedAuthority("ROLE_GUEST"))
                        ))
                        .contentType(MediaType.APPLICATION_JSON)

        )
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    public void test_delete_security_is_success() throws Exception {
        mockMvc.perform(
                get("/users/me/friends/1/delete")
                        .with(user("user").authorities(
                                Collections.singletonList(new SimpleGrantedAuthority("ROLE_GUEST"))
                        ))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isForbidden());
    }
}
