package org.geekhub.socialnetwork.users;

import org.geekhub.socialnetwork.CustomUserDetails;
import org.geekhub.socialnetwork.manager.post.PostManager;
import org.geekhub.socialnetwork.manager.user.UsersManager;
import org.geekhub.socialnetwork.persistance.user.Gender;
import org.geekhub.socialnetwork.persistance.user.UserRecord;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.security.web.method.annotation.AuthenticationPrincipalArgumentResolver;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = UserRestController.class)
public class UserRestControllerTest extends AbstractTestNGSpringContextTests {
    private MockMvc mockMvc;

    @MockBean
    private UsersManager usersManager;
    @MockBean
    private PostManager postManager;
    @MockBean
    private UserDetailsService userDetailsService;
    @MockBean
    private PasswordEncoder passwordEncoder;

    private Authentication authentication;
    private UserRecord userRecord;

    @BeforeMethod
    public void setUp() {
        usersManager = mock(UsersManager.class);
        postManager = mock(PostManager.class);
        passwordEncoder = mock(PasswordEncoder.class);

        UserRestController userRestController = new UserRestController(usersManager, postManager, passwordEncoder);
        mockMvc = MockMvcBuilders.standaloneSetup(userRestController)
                .setCustomArgumentResolvers(new AuthenticationPrincipalArgumentResolver())
                .build();

        userRecord = new UserRecord();
        userRecord.setUsername("name");
        userRecord.setEmail("email");
        userRecord.setEncodedPassword("password");
        userRecord.setRole("ROLE_SUPER_ADMIN");
        userRecord.setEmail("email");
        CustomUserDetails customUserDetails = new CustomUserDetails(
                userRecord.getEmail(), userRecord.getEncodedPassword(), Collections.singletonList(new SimpleGrantedAuthority(userRecord.getRole()))
        );
        customUserDetails.setUserRecord(userRecord);

        authentication = new UsernamePasswordAuthenticationToken(
                customUserDetails,
                null,
                Collections.singletonList(new SimpleGrantedAuthority(userRecord.getRole()))
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_in_getUserInfo_when_user_not_exist() throws Exception {
        mockMvc.perform(
                get("users/1/info")
                        .with(user("admin"))
                        .contentType(MediaType.APPLICATION_JSON)
        );
    }

    @Test
    public void test_getUserInfo_without_friends_is_success() throws Exception {
        int id = 1;
        LocalDateTime date = LocalDateTime.now();
        String email = "email";
        String password = "password";
        String role = "ROLE_USER";
        String name = "name";
        String surname = "surname";
        Gender gender = Gender.MALE;

        UserRecord userRecord = new UserRecord();
        userRecord.setId(id);
        userRecord.setRegistrationDate(date);
        userRecord.setEmail(email);
        userRecord.setEncodedPassword(password);
        userRecord.setRole(role);
        userRecord.setUsername(name);
        userRecord.setGender(gender);

        when(usersManager.getUserById(anyInt())).thenReturn(userRecord);
        when(postManager.getAllUserPosts(userRecord.getId())).thenReturn(new ArrayList<>());

        mockMvc.perform(
                get(String.format("/users/%s/info/", id))
                        .with(user("admin"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$['username']").value(name))
                .andExpect(jsonPath("$['userId']").value(userRecord.getId()))
                .andExpect(jsonPath("$['posts']").value(new ArrayList<>()));
    }

    @Test
    public void test_getInfoAboutMe_without_friend_posts_is_success() throws Exception {

        when(postManager.getAllUserPosts(anyInt())).thenReturn(new ArrayList<>());

        mockMvc.perform(
                get("/users/me")
                        .with(user("user"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$['username']").value(userRecord.getUsername()))
                .andExpect(jsonPath("$['userId']").value(userRecord.getId()))
                .andExpect(jsonPath("$['posts']").value(new ArrayList<>()));
    }


    @Test
    public void test_getConfirmedUserFriends_is_success() throws Exception {
        int id = 1;

        UserRecord userRecord = new UserRecord(
                LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                "email",
                "pass",
                "ROLE_USER",
                "username",
                Gender.MALE
        );
        userRecord.setId(id);
        List<UserRecord> list = List.of(
                userRecord, userRecord, userRecord
        );
        when(usersManager.getConfirmedUserFriends(id)).thenReturn(list);

        mockMvc.perform(
                get(String.format("/users/%s/friends/confirmed", id))
                        .with(user("admin"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(list.size())))
                .andExpect(jsonPath("[0].friendId").value(userRecord.getId()))
                .andExpect(jsonPath("[0].friendUsername").value(userRecord.getUsername()))
                .andExpect(jsonPath("[1].friendId").value(userRecord.getId()))
                .andExpect(jsonPath("[1].friendUsername").value(userRecord.getUsername()))
                .andExpect(jsonPath("[2].friendId").value(userRecord.getId()))
                .andExpect(jsonPath("[2].friendUsername").value(userRecord.getUsername()));
    }


    @Test
    public void test_getNotConfirmedUserFriends_is_success() throws Exception {
        int id = 1;

        UserRecord userRecord = new UserRecord(
                LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                "email",
                "pass",
                "ROLE_USER",
                "username",
                Gender.MALE
        );
        userRecord.setId(id);
        List<UserRecord> list = List.of(
                userRecord, userRecord, userRecord
        );
        when(usersManager.getNotConfirmedUserFriends(id)).thenReturn(list);

        mockMvc.perform(
                get(String.format("/users/%s/friends/notConfirmed", id))
                        .with(user("admin"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(list.size())))
                .andExpect(jsonPath("[0].friendId").value(userRecord.getId()))
                .andExpect(jsonPath("[0].friendUsername").value(userRecord.getUsername()))
                .andExpect(jsonPath("[1].friendId").value(userRecord.getId()))
                .andExpect(jsonPath("[1].friendUsername").value(userRecord.getUsername()))
                .andExpect(jsonPath("[2].friendId").value(userRecord.getId()))
                .andExpect(jsonPath("[2].friendUsername").value(userRecord.getUsername()));
    }


    @Test
    public void test_getUserRequestsToFriends_is_success() throws Exception {
        int id = 1;

        UserRecord userRecord = new UserRecord(
                LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                "email",
                "pass",
                "ROLE_USER",
                "username",
                Gender.MALE
        );
        userRecord.setId(id);
        List<UserRecord> list = List.of(
                userRecord, userRecord, userRecord
        );
        when(usersManager.getFriendshipRequestForUser(id)).thenReturn(list);

        mockMvc.perform(
                get(String.format("/users/%s/friends/requests", id))
                        .with(user("admin"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(list.size())))
                .andExpect(jsonPath("[0].friendId").value(userRecord.getId()))
                .andExpect(jsonPath("[0].friendUsername").value(userRecord.getUsername()))
                .andExpect(jsonPath("[1].friendId").value(userRecord.getId()))
                .andExpect(jsonPath("[1].friendUsername").value(userRecord.getUsername()))
                .andExpect(jsonPath("[2].friendId").value(userRecord.getId()))
                .andExpect(jsonPath("[2].friendUsername").value(userRecord.getUsername()));
    }


    @Test(expectedExceptions = NestedServletException.class)
    public void should_throwException_in_updateUserRole_when_super_admin_try_to_change_his_role() throws Exception {
        String adminRole = "ROLE_ADMIN";
        String superAdminRole = "ROLE_SUPER_ADMIN";

        int superAdminId = 1;
        int userId = 2;
        String superAdminEmail = "super";

        UserRecord superAdmin = new UserRecord();
        superAdmin.setEmail(superAdminEmail);
        superAdmin.setId(superAdminId);
        superAdmin.setRole(superAdminRole);

        when(usersManager.getUserById(userId)).thenReturn(superAdmin);
        when(usersManager.getUserByEmail(any())).thenReturn(superAdmin);

        mockMvc.perform(
                post(String.format("/users/%s/update/role", superAdminId))
                        .param("role", adminRole)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print());
    }

    @Test(expectedExceptions = NestedServletException.class)
    public void should_throwException_in_updateUserRole_when_super_admin_try_to_add_new_super_admin() throws Exception {
        String userRole = "ROLE_USER";
        String superAdminRole = "ROLE_SUPER_ADMIN";

        int superAdminId = 1;
        int userId = 2;
        String superAdminEmail = "super";

        UserRecord superAdmin = new UserRecord();
        superAdmin.setEmail(superAdminEmail);
        superAdmin.setId(superAdminId);
        superAdmin.setRole(superAdminRole);

        UserRecord userForUpdate = new UserRecord();
        userForUpdate.setId(userId);
        userForUpdate.setRole(userRole);

        when(usersManager.getUserById(userId)).thenReturn(userForUpdate);
        when(usersManager.getUserByEmail(any())).thenReturn(superAdmin);

        mockMvc.perform(
                post(String.format("/users/%s/update/role", userId))
                        .param("role", superAdminRole)
                        .with(user("user"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print());
    }

    @Test(expectedExceptions = NestedServletException.class)
    public void should_throwException_in_updateUserRole_when_role_is_invalid() throws Exception {
        String userRole = "ROLE_USER";
        String superAdminRole = "ROLE_SUPER_ADMIN";

        int superAdminId = 1;
        int userId = 2;
        String superAdminEmail = "super";

        UserRecord superAdmin = new UserRecord();
        superAdmin.setEmail(superAdminEmail);
        superAdmin.setId(superAdminId);
        superAdmin.setRole(superAdminRole);

        UserRecord userForUpdate = new UserRecord();
        userForUpdate.setId(userId);
        userForUpdate.setRole(userRole);

        when(usersManager.getUserById(userId)).thenReturn(userForUpdate);
        when(usersManager.getUserByEmail(any())).thenReturn(superAdmin);

        mockMvc.perform(
                post(String.format("/users/%s/update/role", userId))
                        .param("role", "invalid-role")
                        .with(user("user"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print());
    }

    @Test
    public void test_updateUserRole_is_success_when_user_already_has_setted_role_is_success() throws Exception {
        String userRole = "ROLE_USER";
        String superAdminRole = "ROLE_SUPER_ADMIN";

        int superAdminId = 1;
        int userId = 2;
        String superAdminEmail = "super";

        UserRecord superAdmin = new UserRecord();
        superAdmin.setEmail(superAdminEmail);
        superAdmin.setId(superAdminId);
        superAdmin.setRole(superAdminRole);

        UserRecord userForUpdate = new UserRecord();
        userForUpdate.setId(userId);
        userForUpdate.setRole(userRole);

        when(usersManager.getUserById(userId)).thenReturn(userForUpdate);
        when(usersManager.getUserByEmail(any())).thenReturn(superAdmin);

        mockMvc.perform(
                post(String.format("/users/%s/update/role", userId))
                        .param("role", userRole)
                        .with(user("user"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void test_updateUserRole_is_success() throws Exception {
        String userRole = "ROLE_USER";
        String adminRole = "ROLE_ADMIN";
        String superAdminRole = "ROLE_SUPER_ADMIN";

        int superAdminId = 1;
        int userId = 2;
        String superAdminEmail = "super";

        UserRecord superAdmin = new UserRecord();
        superAdmin.setEmail(superAdminEmail);
        superAdmin.setId(superAdminId);
        superAdmin.setRole(superAdminRole);

        UserRecord userForUpdate = new UserRecord();
        userForUpdate.setId(userId);
        userForUpdate.setRole(userRole);

        when(usersManager.getUserById(userId)).thenReturn(userForUpdate);
        when(usersManager.getUserByEmail(any())).thenReturn(superAdmin);

        mockMvc.perform(
                post(String.format("/users/%s/update/role", userId))
                        .param("role", adminRole)
                        .with(user("user"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());

        userForUpdate.setRole(adminRole);
        verify(usersManager, times(1)).updateUser(userForUpdate);
    }

    @Test
    public void test_updateUser_is_success() throws Exception {
        String username = "new-username";
        String password = "new-password";
        Gender gender = Gender.FEMALE;

        userRecord.setUsername(username);
        userRecord.setEncodedPassword(passwordEncoder.encode(password));
        userRecord.setGender(gender);

        mockMvc.perform(
                post("/users/me/update/")
                        .param("username", username)
                        .param("password", password)
                        .param("gender", gender.name())
                        .with(user("user"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(usersManager, times(1)).updateUser(userRecord);
    }


    @WithUserDetails("email")
    @Test
    public void test_deleteMyAccount_is_success() throws Exception {

        mockMvc.perform(
                delete("/users/me/delete/")
                        .with(authentication(authentication))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(usersManager, times(1)).deleteUser(anyInt());
    }


    @Test(expectedExceptions = NestedServletException.class)
    public void should_throwException_in_deleteUserAccount_when_user_has_not_enough_rights() throws Exception {
        int userId = 1;
        String email = "email";

        UserRecord userRecord = new UserRecord();
        userRecord.setId(userId);
        userRecord.setEmail(email);

        UserRecord admin = new UserRecord();
        admin.setRole("ROLE_USER");

        when(usersManager.getUserByEmail(any())).thenReturn(admin);

        mockMvc.perform(
                delete(String.format("/users/%s/delete", userId))
                        .with(user("user"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(usersManager, times(1)).deleteUser(userId);
    }

    @Test
    public void test_deleteUserAccount_is_success() throws Exception {
        int userId = 1;
        String email = "email";

        UserRecord userRecord = new UserRecord();
        userRecord.setId(userId);
        userRecord.setEmail(email);

        UserRecord admin = new UserRecord();
        admin.setRole("ROLE_ADMIN");

        when(usersManager.getUserByEmail(any())).thenReturn(admin);

        mockMvc.perform(
                delete(String.format("/users/%s/delete", userId))
                        .with(user("user"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(usersManager, times(1)).deleteUser(userId);
    }
}
