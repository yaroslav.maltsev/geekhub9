package org.geekhub.socialnetwork.analytics;

import org.geekhub.socialnetwork.UserAnalytics;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class UserAnalyticsRestControllerTest {

    private MockMvc mockMvc;

    @MockBean
    private UserAnalytics userAnalytics;
    @MockBean
    private UserDetailsService userDetailsService;
    @MockBean
    private PasswordEncoder passwordEncoder;

    private int userId;

    @BeforeMethod
    public void setUp() {
        userId = 1;
        userAnalytics = mock(UserAnalytics.class);

        UserAnalyticsRestController userAnalyticsRestController = new UserAnalyticsRestController(userAnalytics);
        mockMvc = MockMvcBuilders.standaloneSetup(userAnalyticsRestController).build();
    }

    @Test
    public void test_getAmountOfLikesOnAllPosts_is_success() throws Exception{
        int amount = 1;

        when(userAnalytics.getAmountOfAllLikesOnUserPosts(userId)).thenReturn(amount);

        mockMvc.perform(
                get(String.format("/analytics/users/%s/likes", userId))
                        .with(user("admin").roles("SUPER_ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(amount));

        verify(userAnalytics, times(1)).getAmountOfAllLikesOnUserPosts(userId);
    }

    @Test
    public void test_getAmountOfCommentsOnAllPosts_is_success() throws Exception{
        int amount = 1;

        when(userAnalytics.getAmountOfAllCommentsOnUserPosts(userId)).thenReturn(amount);

        mockMvc.perform(
                get(String.format("/analytics/users/%s/comments", userId))
                        .with(user("admin").roles("SUPER_ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(amount));

        verify(userAnalytics, times(1)).getAmountOfAllCommentsOnUserPosts(userId);
    }

    @Test
    public void test_getAmountOfAllUserPosts_is_success() throws Exception {
        int amount = 1;

        when(userAnalytics.getAmountOfAllUserPosts(userId)).thenReturn(amount);

        mockMvc.perform(
                get(String.format("/analytics/users/%s/posts", userId))
                        .with(user("admin").roles("SUPER_ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(amount));

        verify(userAnalytics, times(1)).getAmountOfAllUserPosts(userId);
    }
}