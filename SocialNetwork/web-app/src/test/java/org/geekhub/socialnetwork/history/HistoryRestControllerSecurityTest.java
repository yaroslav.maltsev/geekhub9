package org.geekhub.socialnetwork.history;

import org.geekhub.socialnetwork.HistoryReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = HistoryRestController.class)
public class HistoryRestControllerSecurityTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HistoryReader historyReader;
    @MockBean
    private UserDetailsService userDetailsService;
    @MockBean
    private PasswordEncoder passwordEncoder;

    @Test
    public void test_getGlobalLikesHistory_security_for_user_is_success() throws Exception {
        mockMvc.perform(
                get("/history/global/likes")
                        .with(user("user").authorities(
                                Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER"))
                        ))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    public void test_getGlobalLikesHistory_security_for_admin_is_success() throws Exception {
        mockMvc.perform(
                get("/history/global/likes")
                        .with(user("admin").authorities(
                                Collections.singletonList(new SimpleGrantedAuthority("ROLE_ADMIN"))
                        ))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isForbidden());
    }


    @Test
    public void test_getGlobalCommentsHistory_security_for_user_is_success() throws Exception {
        mockMvc.perform(
                get("/history/global/comments")
                        .with(user("user").authorities(
                                Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER"))
                        ))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    public void test_getGlobalCommentsHistory_security_for_admin_is_success() throws Exception {
        mockMvc.perform(
                get("/history/global/comments")
                        .with(user("admin").authorities(
                                Collections.singletonList(new SimpleGrantedAuthority("ROLE_ADMIN"))
                        ))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isForbidden());
    }


    @Test
    public void test_getGlobalPostsHistory_security_for_user_is_success() throws Exception {
        mockMvc.perform(
                get("/history/global/posts")
                        .with(user("user").authorities(
                                Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER"))
                        ))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    public void test_getGlobalPostsHistory_security_for_admin_is_success() throws Exception {
        mockMvc.perform(
                get("/history/global/posts")
                        .with(user("admin").authorities(
                                Collections.singletonList(new SimpleGrantedAuthority("ROLE_ADMIN"))
                        ))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isForbidden());
    }


    @Test
    public void test_getUserLikesHistory_security_for_user_is_success() throws Exception {
        mockMvc.perform(
                get("/history/likes/1")
                        .with(user("user").authorities(
                                Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER"))
                        ))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    public void test_getUserCommentsHistory_security_for_user_is_success() throws Exception {
        mockMvc.perform(
                get("/history/comments/1")
                        .with(user("user").authorities(
                                Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER"))
                        ))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    public void test_getUserPostsHistory_security_for_user_is_success() throws Exception {
        mockMvc.perform(
                get("/history/posts/1")
                        .with(user("user").authorities(
                                Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER"))
                        ))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isForbidden());
    }
}