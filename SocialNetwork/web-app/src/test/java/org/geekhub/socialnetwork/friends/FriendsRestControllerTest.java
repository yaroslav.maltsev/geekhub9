package org.geekhub.socialnetwork.friends;

import org.geekhub.socialnetwork.CustomUserDetails;
import org.geekhub.socialnetwork.RecordNotExistException;
import org.geekhub.socialnetwork.manager.friends.FriendsManager;
import org.geekhub.socialnetwork.persistance.user.UserRecord;
import org.geekhub.socialnetwork.persistance.user_friend.UserFriendRecord;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.method.annotation.AuthenticationPrincipalArgumentResolver;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = FriendsRestController.class)
public class FriendsRestControllerTest extends AbstractTestNGSpringContextTests {
    private MockMvc mockMvc;

    @MockBean
    private FriendsManager friendsManager;
    @MockBean
    private UserDetailsService userDetailsService;
    @MockBean
    private PasswordEncoder passwordEncoder;

    private Authentication authentication;
    private UserRecord userRecord;

    @BeforeMethod
    public void setUp() {
        friendsManager = mock(FriendsManager.class);

        FriendsRestController userRestController = new FriendsRestController(friendsManager);
        mockMvc = MockMvcBuilders
                .standaloneSetup(userRestController)
                .setCustomArgumentResolvers(new AuthenticationPrincipalArgumentResolver())
                .build();

        userRecord = new UserRecord();
        userRecord.setUsername("name");
        userRecord.setEmail("email");
        userRecord.setEncodedPassword("password");
        userRecord.setRole("ROLE_SUPER_ADMIN");
        userRecord.setEmail("email");
        CustomUserDetails customUserDetails = new CustomUserDetails(
                userRecord.getEmail(), userRecord.getEncodedPassword(), Collections.singletonList(new SimpleGrantedAuthority(userRecord.getRole()))
        );
        customUserDetails.setUserRecord(userRecord);

        authentication = new UsernamePasswordAuthenticationToken(
                customUserDetails,
                null,
                Collections.singletonList(new SimpleGrantedAuthority(userRecord.getRole()))
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
    }


    @Test
    public void test_addFriend_is_success() throws Exception {
        int userId = userRecord.getId();
        int friendId = userId + 1;
        String email = "email";

        when(friendsManager.getRecordByIds(userId, friendId)).thenThrow(new RecordNotExistException("", new Exception()));

        mockMvc.perform(
                post(String.format("/users/me/friends/%s/add", friendId))
                        .with(user("user"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(friendsManager, times(1)).addUserFriendRequest(userId, friendId);
    }

    @Test
    public void test_confirmFriendship_is_success() throws Exception {
        int userId = userRecord.getId();
        int friendId = userId + 1;

        UserFriendRecord userFriendRecord = new UserFriendRecord();
        userFriendRecord.setInitiatorId(friendId);
        userFriendRecord.setFriendId(userId);

        when(friendsManager.getRecordByIds(userId, friendId)).thenReturn(userFriendRecord);

        mockMvc.perform(
                post(String.format("/users/me/friends/%s/confirm", friendId))
                        .with(user("user"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(friendsManager, times(1)).confirmFriendship(userId, friendId);
    }

    @Test
    public void test_deleteFriend_is_success() throws Exception {
        int userId = userRecord.getId();
        int friendId = userId + 1;

        UserFriendRecord userFriendRecord = new UserFriendRecord();
        userFriendRecord.setInitiatorId(userId);
        userFriendRecord.setFriendId(friendId);

        when(friendsManager.getRecordByIds(userId, friendId)).thenReturn(userFriendRecord);

        mockMvc.perform(
                delete(String.format("/users/me/friends/%s/delete", friendId))
                        .with(user("user"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(friendsManager, times(1)).deleteRecord(userId, friendId);
    }
}