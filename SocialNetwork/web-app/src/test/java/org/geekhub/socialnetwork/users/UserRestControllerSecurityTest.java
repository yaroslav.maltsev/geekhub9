package org.geekhub.socialnetwork.users;

import org.geekhub.socialnetwork.manager.post.PostManager;
import org.geekhub.socialnetwork.manager.user.UsersManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;

import java.util.Collections;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserRestController.class)
public class UserRestControllerSecurityTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UsersManager usersManager;
    @MockBean
    private PostManager postManager;
    @MockBean
    private UserDetailsService userDetailsService;
    @MockBean
    private PasswordEncoder passwordEncoder;

    @Test
    public void test_users_path_security_is_success() throws Exception {
        mockMvc.perform(
                get("/users/")
                        .with(user("user").authorities(
                                Collections.singletonList(new SimpleGrantedAuthority("ROLE_GUEST"))
                        ))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    public void test_users_me_path_security_is_success() throws Exception {
        mockMvc.perform(
                get("/users/me")
                        .with(user("user").authorities(
                                Collections.singletonList(new SimpleGrantedAuthority("ROLE_GUEST"))
                        ))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    public void test_users_posts_path_security_is_success() throws Exception {
        mockMvc.perform(
                get("/users/1/posts")
                        .with(user("user").authorities(
                                Collections.singletonList(new SimpleGrantedAuthority("ROLE_GUEST"))
                        ))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isForbidden());
    }
}
