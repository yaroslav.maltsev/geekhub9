package org.geekhub.socialnetwork.likes;

import org.geekhub.socialnetwork.CustomUserDetails;
import org.geekhub.socialnetwork.manager.like.LikeManager;
import org.geekhub.socialnetwork.manager.post.PostManager;
import org.geekhub.socialnetwork.persistance.like.LikeRecord;
import org.geekhub.socialnetwork.persistance.post.PostRecord;
import org.geekhub.socialnetwork.persistance.user.UserRecord;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.method.annotation.AuthenticationPrincipalArgumentResolver;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = LikesRestController.class)
public class LikesRestControllerTest extends AbstractTestNGSpringContextTests {
    private MockMvc mockMvc;

    @MockBean
    private PostManager postManager;
    @MockBean
    private LikeManager likeManager;
    @MockBean
    private UserDetailsService userDetailsService;
    @MockBean
    private PasswordEncoder passwordEncoder;

    private Authentication authentication;
    private UserRecord userRecord;

    @BeforeMethod
    public void setUp() {
        postManager = mock(PostManager.class);
        likeManager = mock(LikeManager.class);

        LikesRestController likesRestController = new LikesRestController(
                postManager,
                likeManager
        );
        mockMvc = MockMvcBuilders
                .standaloneSetup(likesRestController)
                .setCustomArgumentResolvers(new AuthenticationPrincipalArgumentResolver())
                .build();

        userRecord = new UserRecord();
        userRecord.setUsername("name");
        userRecord.setEmail("email");
        userRecord.setEncodedPassword("password");
        userRecord.setRole("ROLE_SUPER_ADMIN");
        userRecord.setEmail("email");
        CustomUserDetails customUserDetails = new CustomUserDetails(
                userRecord.getEmail(), userRecord.getEncodedPassword(), Collections.singletonList(new SimpleGrantedAuthority(userRecord.getRole()))
        );
        customUserDetails.setUserRecord(userRecord);

        authentication = new UsernamePasswordAuthenticationToken(
                customUserDetails,
                null,
                Collections.singletonList(new SimpleGrantedAuthority(userRecord.getRole()))
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test
    public void test_getAllPostLikes_is_success() throws Exception {
        int userId = 1;
        int postId = 2;
        int likeAuthor = 3;
        LocalDateTime date = LocalDateTime.now();

        LikeRecord likeRecord = new LikeRecord();
        likeRecord.setAuthorId(likeAuthor);
        likeRecord.setPostId(postId);
        likeRecord.setDate(date);

        List<LikeRecord> likesList = List.of(
                likeRecord,
                likeRecord,
                likeRecord,
                likeRecord
        );
        PostRecord postRecord = new PostRecord();
        postRecord.setId(postId);
        postRecord.setAuthorId(userId);

        when(postManager.getPostById(anyInt())).thenReturn(postRecord);
        when(likeManager.getAllPostLikes(postId)).thenReturn(likesList);

        mockMvc.perform(
                get(String.format("/users/%s/posts/%s/likes", userId, postId))
                        .with(authentication(authentication))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(likesList.size())))
                .andExpect(jsonPath("$[0].authorId").value(likesList.get(0).getAuthorId()))
                .andExpect(jsonPath("$[1].authorId").value(likesList.get(1).getAuthorId()))
                .andExpect(jsonPath("$[2].authorId").value(likesList.get(2).getAuthorId()));
    }

    @Test
    public void test_addLikeToPost_is_success() throws Exception {
        int userId = userRecord.getId();
        int postId = 2;
        String email = "email";

        UserRecord userRecord = new UserRecord();
        userRecord.setId(userId);
        userRecord.setEmail(email);

        PostRecord postRecord = new PostRecord();
        postRecord.setId(postId);
        postRecord.setAuthorId(userId);

        when(postManager.getPostById(postId)).thenReturn(postRecord);

        mockMvc.perform(
                post(String.format("/users/%s/posts/%s/likes/add", userId, postId))
                        .with(authentication(authentication))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(likeManager, times(1)).addLike(any());
    }


    @Test(expectedExceptions = NestedServletException.class)
    public void should_throwException_in_deleteLike_when_user_is_not_author() throws Exception {
        int postId = 2;
        int userId = userRecord.getId();
        int notAuthorUserId = 2;
        int likeId = 1;

        UserRecord userRecord = new UserRecord();
        userRecord.setId(userId);

        PostRecord postRecord = new PostRecord();
        postRecord.setId(postId);
        postRecord.setAuthorId(userId);

        LikeRecord likeRecord = new LikeRecord();
        likeRecord.setId(likeId);
        likeRecord.setAuthorId(userId);
        likeRecord.setPostId(postId);

        when(postManager.getPostById(postId)).thenReturn(postRecord);
        when(likeManager.getLikeById(likeId)).thenReturn(likeRecord);

        mockMvc.perform(
                delete(String.format("/users/%s/posts/%s/likes/%s/delete", notAuthorUserId, postId, likeId))
                        .with(authentication(authentication))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test(expectedExceptions = NestedServletException.class)
    public void should_throwException_in_deleteLike_when_user_in_path_and_author_of_post_are_different() throws Exception {
        int postId = 2;
        int postAuthorId = 2;
        int userId = userRecord.getId();
        int likeId = 1;

        UserRecord userRecord = new UserRecord();
        userRecord.setId(userId);

        PostRecord postRecord = new PostRecord();
        postRecord.setId(postId);
        postRecord.setAuthorId(postAuthorId);

        when(postManager.getPostById(postId)).thenReturn(postRecord);

        mockMvc.perform(
                delete(String.format("/users/%s/posts/%s/likes/%s/delete", userId, postId, likeId))
                        .with(authentication(authentication))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print());
    }

    @Test(expectedExceptions = NestedServletException.class)
    public void should_throwException_in_deleteLike_when_postId_in_comment_and_postId_in_path_are_different() throws Exception {
        int postIdInComment = 2;
        int postAuthorIdInPath = 3;
        int userId = userRecord.getId();
        int likeId = 1;

        UserRecord userRecord = new UserRecord();
        userRecord.setId(userId);

        PostRecord postRecord = new PostRecord();
        postRecord.setId(postAuthorIdInPath);
        postRecord.setAuthorId(userId);

        LikeRecord likeRecord = new LikeRecord();
        likeRecord.setId(likeId);
        likeRecord.setAuthorId(userId);
        likeRecord.setPostId(postIdInComment);

        when(postManager.getPostById(postAuthorIdInPath)).thenReturn(postRecord);
        when(likeManager.getLikeById(likeId)).thenReturn(likeRecord);

        mockMvc.perform(
                delete(String.format("/users/%s/posts/%s/likes/%s/delete", userId, postAuthorIdInPath, likeId))
                        .with(authentication(authentication))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print());
    }

    @Test
    public void test_deleteLike_is_success() throws Exception {
        int postId = 2;
        int userId = userRecord.getId();
        int likeId = 1;

        UserRecord userRecord = new UserRecord();
        userRecord.setId(userId);

        PostRecord postRecord = new PostRecord();
        postRecord.setId(postId);
        postRecord.setAuthorId(userId);

        LikeRecord likeRecord = new LikeRecord();
        likeRecord.setId(likeId);
        likeRecord.setAuthorId(userId);
        likeRecord.setPostId(postId);

        when(postManager.getPostById(postId)).thenReturn(postRecord);
        when(likeManager.getLikeById(likeId)).thenReturn(likeRecord);

        mockMvc.perform(
                delete(String.format("/users/%s/posts/%s/likes/%s/delete", userId, postId, likeId))
                        .with(authentication(authentication))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(likeManager, times(1)).deleteLike(likeId);
    }
}