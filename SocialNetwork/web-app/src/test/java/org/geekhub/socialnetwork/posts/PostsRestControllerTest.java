package org.geekhub.socialnetwork.posts;

import org.geekhub.socialnetwork.CustomUserDetails;
import org.geekhub.socialnetwork.manager.post.PostManager;
import org.geekhub.socialnetwork.persistance.post.PostRecord;
import org.geekhub.socialnetwork.persistance.user.UserRecord;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.method.annotation.AuthenticationPrincipalArgumentResolver;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = PostsRestController.class)
public class PostsRestControllerTest {
    private MockMvc mockMvc;

    @MockBean
    private PostManager postManager;
    @MockBean
    private UserDetailsService userDetailsService;
    @MockBean
    private PasswordEncoder passwordEncoder;

    private UserRecord userRecord;

    @BeforeMethod
    public void setUp() {
        postManager = mock(PostManager.class);

        PostsRestController postsRestController = new PostsRestController(postManager);
        mockMvc = MockMvcBuilders
                .standaloneSetup(postsRestController)
                .setCustomArgumentResolvers(new AuthenticationPrincipalArgumentResolver())
                .build();

        userRecord = new UserRecord();
        userRecord.setUsername("name");
        userRecord.setEmail("email");
        userRecord.setEncodedPassword("password");
        userRecord.setRole("ROLE_SUPER_ADMIN");
        userRecord.setEmail("email");
        CustomUserDetails customUserDetails = new CustomUserDetails(
                userRecord.getEmail(), userRecord.getEncodedPassword(), Collections.singletonList(new SimpleGrantedAuthority(userRecord.getRole()))
        );
        customUserDetails.setUserRecord(userRecord);

        Authentication authentication = new UsernamePasswordAuthenticationToken(
                customUserDetails,
                null,
                Collections.singletonList(new SimpleGrantedAuthority(userRecord.getRole()))
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_in_getUserPost_when_user_not_exist() throws Exception {
        when(postManager.getPostById(anyInt())).thenReturn(null);
        mockMvc.perform(
                get("users/1/posts/2")
                        .contentType(MediaType.APPLICATION_JSON)
        );
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_in_getUserPost_when_user_and_author_are_different() throws Exception {
        int postId = 2;
        PostRecord postRecord = new PostRecord();
        postRecord.setAuthorId(2);
        postRecord.setDescription("text");
        postRecord.setDate(LocalDateTime.now());
        postRecord.setId(postId);

        when(postManager.getPostById(anyInt())).thenReturn(postRecord);
        mockMvc.perform(
                get("users/1/posts/" + postId)
                        .contentType(MediaType.APPLICATION_JSON)
        );
    }

    @Test
    public void test_getUserPost_is_success() throws Exception {
        int userId = 1;
        int postId = 2;
        PostRecord postRecord = new PostRecord();
        postRecord.setAuthorId(userId);
        String description = "text";
        postRecord.setDescription(description);
        LocalDateTime date = LocalDateTime.now();
        postRecord.setDate(date);
        postRecord.setId(postId);

        when(postManager.getPostById(anyInt())).thenReturn(postRecord);

        mockMvc.perform(
                get(String.format("/users/%s/posts/%s", userId, postId))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$['authorId']").value(userId))
                .andExpect(jsonPath("$['description']").value(description));
    }


    @Test
    public void test_getUserPosts_is_success() throws Exception {
        int userId = 1;
        int postId = 2;
        PostRecord postRecord = new PostRecord();
        postRecord.setAuthorId(userId);
        String description = "text";
        postRecord.setDescription(description);
        LocalDateTime date = LocalDateTime.now();
        postRecord.setDate(date);
        postRecord.setId(postId);

        List<PostRecord> postList = List.of(
                postRecord,
                postRecord,
                postRecord,
                postRecord
        );
        when(postManager.getAllUserPosts(anyInt())).thenReturn(postList);

        mockMvc.perform(
                get(String.format("/users/%s/posts/", userId))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(postList.size())))
                .andExpect(jsonPath("$[0].authorId").value(postList.get(0).getAuthorId()))
                .andExpect(jsonPath("$[1].authorId").value(postList.get(1).getAuthorId()))
                .andExpect(jsonPath("$[2].authorId").value(postList.get(2).getAuthorId()))
                .andExpect(jsonPath("$[3].authorId").value(postList.get(3).getAuthorId()));
    }

    @Test
    public void test_getMyPosts_is_success() throws Exception {
        int userId = userRecord.getId();
        int postId = 2;
        PostRecord postRecord = new PostRecord();
        postRecord.setAuthorId(userId);
        String description = "text";
        postRecord.setDescription(description);
        LocalDateTime date = LocalDateTime.now();
        postRecord.setDate(date);
        postRecord.setId(postId);

        List<PostRecord> postList = List.of(
                postRecord,
                postRecord,
                postRecord,
                postRecord
        );
        UserRecord userRecord = new UserRecord();
        userRecord.setId(userId);

        when(postManager.getAllUserPosts(anyInt())).thenReturn(postList);

        mockMvc.perform(
                get(("/users/me/posts/"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(postList.size())))
                .andExpect(jsonPath("$[0].authorId").value(postList.get(0).getAuthorId()))
                .andExpect(jsonPath("$[1].authorId").value(postList.get(1).getAuthorId()))
                .andExpect(jsonPath("$[2].authorId").value(postList.get(2).getAuthorId()))
                .andExpect(jsonPath("$[3].authorId").value(postList.get(3).getAuthorId()));
    }


    @Test
    public void test_createPost_is_success() throws Exception {
        String text = "text";

        mockMvc.perform(
                post("/users/me/posts/add")
                        .param("text", text)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(postManager, times(1)).addPost(any());
    }

    @Test
    public void test_editPost_is_success() throws Exception {
        int postId = 2;
        int userId = userRecord.getId();
        String text = "text";

        PostRecord postRecord = new PostRecord();
        postRecord.setId(postId);
        postRecord.setAuthorId(userId);
        postRecord.setDescription("text");

        when(postManager.getPostById(postId)).thenReturn(postRecord);

        mockMvc.perform(
                post(String.format("/users/me/posts/%s/edit", postId))
                        .param("text", text)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(postManager, times(1)).editPost(any());
    }

    @Test
    public void test_deletePost_is_success() throws Exception {
        int postId = 2;
        int userId = userRecord.getId();

        PostRecord postRecord = new PostRecord();
        postRecord.setId(postId);
        postRecord.setAuthorId(userId);
        postRecord.setDescription("text");

        when(postManager.getPostById(postId)).thenReturn(postRecord);

        mockMvc.perform(
                delete(String.format("/users/me/posts/%s/delete", postId))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(postManager, times(1)).deletePost(postId);
    }
}