package org.geekhub.socialnetwork.analytics;

import org.geekhub.socialnetwork.HistoryAudit;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Map;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class HistoryAnalyticsRestControllerTest {
    private MockMvc mockMvc;

    @MockBean
    private HistoryAudit historyAudit;
    @MockBean
    private UserDetailsService userDetailsService;
    @MockBean
    private PasswordEncoder passwordEncoder;

    @BeforeMethod
    public void setUp() {
        historyAudit = mock(HistoryAudit.class);

        HistoryAnalyticsRestController historyAnalyticsRestController = new HistoryAnalyticsRestController(historyAudit);
        mockMvc = MockMvcBuilders.standaloneSetup(historyAnalyticsRestController).build();
    }

    @Test
    public void test_getCountOfAllPosts_is_success() throws Exception {
        int daysAmount = 1;
        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);

        Map<LocalDateTime, Long> map = Map.ofEntries(
                Map.entry(now, 1L),
                Map.entry(now.plusDays(1), 2L),
                Map.entry(now.plusDays(2), 3L)
        );

        when(historyAudit.getCountOfPostsPerLastDays(daysAmount)).thenReturn(map);
        mockMvc.perform(
                get("/analytics/history/posts/" + daysAmount)
                        .with(user("admin").roles("SUPER_ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(map.size())));

        verify(historyAudit, times(1)).getCountOfPostsPerLastDays(daysAmount);
    }

    @Test
    public void test_getCountOfUserPosts_is_success() throws Exception {
        int daysAmount = 1;
        int userId = 1;
        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);

        Map<LocalDateTime, Long> map = Map.ofEntries(
                Map.entry(now, 1L),
                Map.entry(now.plusDays(1), 2L),
                Map.entry(now.plusDays(2), 3L)
        );

        when(historyAudit.getCountOfUserPostsPerLastDays(userId, daysAmount)).thenReturn(map);
        mockMvc.perform(
                get(String.format("/analytics/history/users/%s/posts/%s", userId, daysAmount))
                        .with(user("admin").roles("ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(map.size())));

        verify(historyAudit, times(1)).getCountOfUserPostsPerLastDays(userId, daysAmount);
    }


    @Test
    public void test_getCountOfAllLikes_is_success() throws Exception {
        int daysAmount = 1;
        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);

        Map<LocalDateTime, Long> map = Map.ofEntries(
                Map.entry(now, 1L),
                Map.entry(now.plusDays(1), 2L),
                Map.entry(now.plusDays(2), 3L)
        );

        when(historyAudit.getCountOfLikesPerLastDays(daysAmount)).thenReturn(map);
        mockMvc.perform(
                get("/analytics/history/likes/" + daysAmount)
                        .with(user("admin").roles("SUPER_ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(map.size())));

        verify(historyAudit, times(1)).getCountOfLikesPerLastDays(daysAmount);
    }

    @Test
    public void test_getCountOfUserLikes_is_success() throws Exception {
        int daysAmount = 1;
        int userId = 1;
        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);

        Map<LocalDateTime, Long> map = Map.ofEntries(
                Map.entry(now, 1L),
                Map.entry(now.plusDays(1), 2L),
                Map.entry(now.plusDays(2), 3L)
        );

        when(historyAudit.getCountOfUserLikesPerLastDays(userId, daysAmount)).thenReturn(map);
        mockMvc.perform(
                get(String.format("/analytics/history/users/%s/likes/%s", userId, daysAmount))
                        .with(user("admin").roles("ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(map.size())));

        verify(historyAudit, times(1)).getCountOfUserLikesPerLastDays(userId, daysAmount);
    }


    @Test
    public void test_getCountOfAllComments_is_success() throws Exception {
        int daysAmount = 1;
        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);

        Map<LocalDateTime, Long> map = Map.ofEntries(
                Map.entry(now, 1L),
                Map.entry(now.plusDays(1), 2L),
                Map.entry(now.plusDays(2), 3L)
        );

        when(historyAudit.getCountOfCommentsPerLastDays(daysAmount)).thenReturn(map);
        mockMvc.perform(
                get("/analytics/history/comments/" + daysAmount)
                        .with(user("admin").roles("SUPER_ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(map.size())));

        verify(historyAudit, times(1)).getCountOfCommentsPerLastDays(daysAmount);
    }

    @Test
    public void test_getCountOfUserComments_is_success() throws Exception {
        int daysAmount = 1;
        int userId = 1;
        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);

        Map<LocalDateTime, Long> map = Map.ofEntries(
                Map.entry(now, 1L),
                Map.entry(now.plusDays(1), 2L),
                Map.entry(now.plusDays(2), 3L)
        );

        when(historyAudit.getCountOfUserCommentsPerLastDays(userId, daysAmount)).thenReturn(map);
        mockMvc.perform(
                get(String.format("/analytics/history/users/%s/comments/%s", userId, daysAmount))
                        .with(user("admin").roles("ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(map.size())));

        verify(historyAudit, times(1)).getCountOfUserCommentsPerLastDays(userId, daysAmount);
    }
}