package org.geekhub.socialnetwork.comments;

import org.geekhub.socialnetwork.CustomUserDetails;
import org.geekhub.socialnetwork.manager.comment.CommentManager;
import org.geekhub.socialnetwork.manager.post.PostManager;
import org.geekhub.socialnetwork.persistance.comment.CommentRecord;
import org.geekhub.socialnetwork.persistance.post.PostRecord;
import org.geekhub.socialnetwork.persistance.user.UserRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.method.annotation.AuthenticationPrincipalArgumentResolver;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = CommentsRestController.class)
public class CommentsRestControllerTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private PostManager postManager;
    @MockBean
    private CommentManager commentsManager;
    @MockBean
    private UserDetailsService userDetailsService;
    @MockBean
    private PasswordEncoder passwordEncoder;

    private UserRecord userRecord;


    @BeforeMethod
    public void setUp() {
        postManager = mock(PostManager.class);
        commentsManager = mock(CommentManager.class);

        CommentsRestController userRestController = new CommentsRestController(
                postManager,
                commentsManager
        );
        mockMvc = MockMvcBuilders
                .standaloneSetup(userRestController)
                .setCustomArgumentResolvers(new AuthenticationPrincipalArgumentResolver())
                .build();

        userRecord = new UserRecord();
        userRecord.setUsername("name");
        userRecord.setEmail("email");
        userRecord.setEncodedPassword("password");
        userRecord.setRole("ROLE_SUPER_ADMIN");
        userRecord.setEmail("email");

        CustomUserDetails customUserDetails = new CustomUserDetails(
                userRecord.getEmail(), userRecord.getEncodedPassword(), Collections.singletonList(new SimpleGrantedAuthority(userRecord.getRole()))
        );
        customUserDetails.setUserRecord(userRecord);

        Authentication authentication = new UsernamePasswordAuthenticationToken(
                customUserDetails,
                null,
                Collections.singletonList(new SimpleGrantedAuthority(userRecord.getRole()))
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
    }


    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_in_getPostComments_when_user_not_exist() throws Exception {
        when(postManager.getPostById(anyInt())).thenReturn(null);
        mockMvc.perform(
                get("users/1/posts/2/comments")
                        .contentType(MediaType.APPLICATION_JSON)
        );
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_in_getPostComments_when_user_and_author_are_different() throws Exception {
        int postId = 2;
        PostRecord postRecord = new PostRecord();
        postRecord.setAuthorId(2);
        postRecord.setDescription("text");
        postRecord.setDate(LocalDateTime.now());
        postRecord.setId(postId);

        when(postManager.getPostById(anyInt())).thenReturn(postRecord);
        mockMvc.perform(
                get("users/1/posts/1/comments")
                        .contentType(MediaType.APPLICATION_JSON)
        );
    }

    @Test
    public void test_getPostComments_is_success() throws Exception {
        int userId = 1;
        int postId = 2;
        int commentAuthor = 3;
        LocalDateTime date = LocalDateTime.now();
        String text = "text";

        CommentRecord commentRecord = new CommentRecord();
        commentRecord.setAuthorId(commentAuthor);
        commentRecord.setPostId(postId);
        commentRecord.setDate(date);
        commentRecord.setText(text);

        List<CommentRecord> commentList = List.of(
                commentRecord,
                commentRecord,
                commentRecord,
                commentRecord
        );
        PostRecord postRecord = new PostRecord();
        postRecord.setId(postId);
        postRecord.setAuthorId(userId);

        when(postManager.getPostById(anyInt())).thenReturn(postRecord);
        when(commentsManager.getAllPostComments(postId)).thenReturn(commentList);

        mockMvc.perform(
                get(String.format("/users/%s/posts/%s/comments", userId, postId))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(commentList.size())))
                .andExpect(jsonPath("$[0].authorId").value(commentList.get(0).getAuthorId()))
                .andExpect(jsonPath("$[1].authorId").value(commentList.get(1).getAuthorId()))
                .andExpect(jsonPath("$[2].authorId").value(commentList.get(2).getAuthorId()))
                .andExpect(jsonPath("$[3].authorId").value(commentList.get(3).getAuthorId()));
    }

    @Test
    public void test_addCommentToPost_is_success() throws Exception {
        int userId = 1;
        int postId = 2;
        String text = "text";

        PostRecord postRecord = new PostRecord();
        postRecord.setId(postId);
        postRecord.setAuthorId(userId);

        when(postManager.getPostById(postId)).thenReturn(postRecord);

        mockMvc.perform(
                post(String.format("/users/%s/posts/%s/comments/add", userId, postId))
                        .param("text", text)
                        .with(user("user"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(commentsManager, times(1)).addComment(any());
    }


    @Test(expectedExceptions = NestedServletException.class)
    public void should_throwException_in_deleteComment_when_user_is_not_author() throws Exception {
        int postId = 2;
        int userId = 1;
        int notAuthorUserId = 2;
        int commentId = 1;

        PostRecord postRecord = new PostRecord();
        postRecord.setId(postId);
        postRecord.setAuthorId(userId);

        CommentRecord commentRecord = new CommentRecord();
        commentRecord.setId(commentId);
        commentRecord.setAuthorId(userId);
        commentRecord.setPostId(postId);

        when(postManager.getPostById(postId)).thenReturn(postRecord);
        when(commentsManager.getCommentById(commentId)).thenReturn(commentRecord);

        mockMvc.perform(
                delete(String.format("/users/%s/posts/%s/comments/%s/delete", notAuthorUserId, postId, commentId))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test(expectedExceptions = NestedServletException.class)
    public void should_throwException_in_deleteComment_when_user_in_path_and_author_of_post_are_different() throws Exception {
        int postId = 2;
        int postAuthorId = 2;
        int userId = 1;
        int commentId = 1;

        PostRecord postRecord = new PostRecord();
        postRecord.setId(postId);
        postRecord.setAuthorId(postAuthorId);

        when(postManager.getPostById(postId)).thenReturn(postRecord);

        mockMvc.perform(
                delete(String.format("/users/%s/posts/%s/comments/%s/delete", userId, postId, commentId))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print());
    }

    @Test(expectedExceptions = NestedServletException.class)
    public void should_throwException_in_deleteComment_when_postId_in_comment_and_postId_in_path_are_different() throws Exception {
        int postIdInComment = 2;
        int postAuthorIdInPath = 3;
        int userId = 1;
        int commentId = 1;

        PostRecord postRecord = new PostRecord();
        postRecord.setId(postAuthorIdInPath);
        postRecord.setAuthorId(userId);

        CommentRecord commentRecord = new CommentRecord();
        commentRecord.setId(commentId);
        commentRecord.setAuthorId(userId);
        commentRecord.setPostId(postIdInComment);

        when(postManager.getPostById(postAuthorIdInPath)).thenReturn(postRecord);
        when(commentsManager.getCommentById(commentId)).thenReturn(commentRecord);

        mockMvc.perform(
                delete(String.format("/users/%s/posts/%s/comments/%s/delete", userId, postAuthorIdInPath, commentId))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print());
    }

    @Test
    public void test_deleteComment_is_success() throws Exception {
        int postId = 2;
        int userId = 1;
        int commentId = 1;

        PostRecord postRecord = new PostRecord();
        postRecord.setId(postId);
        postRecord.setAuthorId(userId);

        CommentRecord commentRecord = new CommentRecord();
        commentRecord.setId(commentId);
        commentRecord.setAuthorId(userRecord.getId());
        commentRecord.setPostId(postId);

        when(postManager.getPostById(postId)).thenReturn(postRecord);
        when(commentsManager.getCommentById(commentId)).thenReturn(commentRecord);

        mockMvc.perform(
                delete(String.format("/users/%s/posts/%s/comments/%s/delete", userId, postId, commentId))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(commentsManager, times(1)).deleteComment(commentId);
    }


    @Test(expectedExceptions = NestedServletException.class)
    public void should_throwException_in_editComment_when_user_is_not_author() throws Exception {
        int postId = 2;
        int userId = 1;
        int notAuthorUserId = 2;
        int commentId = 1;
        String editedText = "new text";

        PostRecord postRecord = new PostRecord();
        postRecord.setId(postId);
        postRecord.setAuthorId(userId);

        CommentRecord commentRecord = new CommentRecord();
        commentRecord.setId(commentId);
        commentRecord.setAuthorId(userId);
        commentRecord.setPostId(postId);

        when(postManager.getPostById(postId)).thenReturn(postRecord);
        when(commentsManager.getCommentById(commentId)).thenReturn(commentRecord);

        mockMvc.perform(
                post(String.format("/users/%s/posts/%s/comments/%s/edit", notAuthorUserId, postId, commentId))
                        .param("editedText", editedText)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test(expectedExceptions = NestedServletException.class)
    public void should_throwException_in_editComment_when_user_in_path_and_author_of_post_are_different() throws Exception {
        int postId = 2;
        int postAuthorId = 2;
        int userId = 1;
        int commentId = 1;
        String editedText = "new text";

        UserRecord userRecord = new UserRecord();
        userRecord.setId(userId);

        PostRecord postRecord = new PostRecord();
        postRecord.setId(postId);
        postRecord.setAuthorId(postAuthorId);

        when(postManager.getPostById(postId)).thenReturn(postRecord);

        mockMvc.perform(
                post(String.format("/users/%s/posts/%s/comments/%s/edit", userId, postId, commentId))
                        .param("editedText", editedText)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print());
    }

    @Test(expectedExceptions = NestedServletException.class)
    public void should_throwException_in_editComment_when_postId_in_comment_and_postId_in_path_are_different() throws Exception {
        int postIdInComment = 2;
        int postAuthorIdInPath = 3;
        int userId = 1;
        int commentId = 1;
        String editedText = "new text";

        UserRecord userRecord = new UserRecord();
        userRecord.setId(userId);

        PostRecord postRecord = new PostRecord();
        postRecord.setId(postAuthorIdInPath);
        postRecord.setAuthorId(userId);

        CommentRecord commentRecord = new CommentRecord();
        commentRecord.setId(commentId);
        commentRecord.setAuthorId(userId);
        commentRecord.setPostId(postIdInComment);

        when(postManager.getPostById(postAuthorIdInPath)).thenReturn(postRecord);
        when(commentsManager.getCommentById(commentId)).thenReturn(commentRecord);

        mockMvc.perform(
                post(String.format("/users/%s/posts/%s/comments/%s/edit", userId, postAuthorIdInPath, commentId))
                        .param("editedText", editedText)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print());
    }

    @Test
    public void test_editComment_is_success() throws Exception {
        int postId = 2;
        int userId = userRecord.getId();
        int commentId = 1;
        String editedText = "new text";

        UserRecord userRecord = new UserRecord();
        userRecord.setId(userId);

        PostRecord postRecord = new PostRecord();
        postRecord.setId(postId);
        postRecord.setAuthorId(userId);

        CommentRecord commentRecord = new CommentRecord();
        commentRecord.setId(commentId);
        commentRecord.setAuthorId(userId);
        commentRecord.setPostId(postId);

        when(postManager.getPostById(postId)).thenReturn(postRecord);
        when(commentsManager.getCommentById(commentId)).thenReturn(commentRecord);

        mockMvc.perform(
                post(String.format("/users/%s/posts/%s/comments/%s/edit", userId, postId, commentId))
                        .param("editedText", editedText)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(commentsManager, times(1)).editComment(any());
    }
}