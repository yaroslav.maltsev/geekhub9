package org.geekhub.socialnetwork.history;

import org.geekhub.socialnetwork.HistoryReader;
import org.geekhub.socialnetwork.persistance.comment.CommentRecord;
import org.geekhub.socialnetwork.persistance.like.LikeRecord;
import org.geekhub.socialnetwork.persistance.post.PostRecord;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.Map;

import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = HistoryRestController.class)
public class HistoryRestControllerTest {
    private MockMvc mockMvc;

    @MockBean
    private HistoryReader historyReader;
    @MockBean
    private UserDetailsService userDetailsService;
    @MockBean
    private PasswordEncoder passwordEncoder;


    @BeforeMethod
    public void setUp() {
        historyReader = mock(HistoryReader.class);

        HistoryRestController historyRestController = new HistoryRestController(historyReader);
        mockMvc = MockMvcBuilders.standaloneSetup(historyRestController).build();
    }

    @Test
    public void test_getGlobalLikesHistory_is_success() throws Exception {
        LikeRecord record = new LikeRecord();
        record.setAuthorId(2);
        record.setPostId(1);
        record.setId(1);

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime secondDate = now.plusDays(1);
        LocalDateTime thirdDate = now.minusDays(1);

        Map<LocalDateTime, LikeRecord> history = Map.ofEntries(
                Map.entry(now, record),
                Map.entry(secondDate, record),
                Map.entry(thirdDate, record)
        );
        when(historyReader.getGlobalLikeHistory()).thenReturn(history);
        mockMvc.perform(
                get("/history/global/likes/")
                        .with(user("admin").roles("SUPER_ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(historyReader, times(1)).getGlobalLikeHistory();
    }

    @Test
    public void test_getUserLikesHistory_is_success() throws Exception {
        int userId = 1;
        LikeRecord record = new LikeRecord();
        record.setAuthorId(userId);
        record.setPostId(1);
        record.setId(1);

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime secondDate = now.plusDays(1);
        LocalDateTime thirdDate = now.minusDays(1);

        Map<LocalDateTime, LikeRecord> history = Map.ofEntries(
                Map.entry(now, record),
                Map.entry(secondDate, record),
                Map.entry(thirdDate, record)
        );
        when(historyReader.getLikeHistoryForUser(anyInt())).thenReturn(history);
        mockMvc.perform(
                get("/history/likes/" + userId)
                        .with(user("admin").roles("SUPER_ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(historyReader, times(1)).getLikeHistoryForUser(userId);
    }


    @Test
    public void test_getGlobalCommentsHistory_is_success() throws Exception {
        int userId = 1;
        CommentRecord record = new CommentRecord();
        record.setAuthorId(userId);
        record.setPostId(1);
        record.setId(1);

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime secondDate = now.plusDays(1);
        LocalDateTime thirdDate = now.minusDays(1);

        Map<LocalDateTime, CommentRecord> history = Map.ofEntries(
                Map.entry(now, record),
                Map.entry(secondDate, record),
                Map.entry(thirdDate, record)
        );
        when(historyReader.getGlobalCommentRecordHistory()).thenReturn(history);
        mockMvc.perform(
                get("/history/global/comments/")
                        .with(user("admin").roles("SUPER_ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(historyReader, times(1)).getGlobalCommentRecordHistory();
    }

    @Test
    public void test_getCommentHistoryForUser_is_success() throws Exception {
        int userId = 1;
        CommentRecord record = new CommentRecord();
        record.setAuthorId(userId);
        record.setPostId(1);
        record.setId(1);

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime secondDate = now.plusDays(1);
        LocalDateTime thirdDate = now.minusDays(1);

        Map<LocalDateTime, CommentRecord> history = Map.ofEntries(
                Map.entry(now, record),
                Map.entry(secondDate, record),
                Map.entry(thirdDate, record)
        );
        when(historyReader.getCommentHistoryForUser(userId)).thenReturn(history);
        mockMvc.perform(
                get("/history/comments/" + userId)
                        .with(user("admin").roles("SUPER_ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(historyReader, times(1)).getCommentHistoryForUser(userId);
    }


    @Test
    public void test_getGlobalPostHistory_is_success() throws Exception {
        int userId = 1;
        PostRecord record = new PostRecord();
        record.setAuthorId(userId);
        record.setId(1);

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime secondDate = now.plusDays(1);
        LocalDateTime thirdDate = now.minusDays(1);

        Map<LocalDateTime, PostRecord> history = Map.ofEntries(
                Map.entry(now, record),
                Map.entry(secondDate, record),
                Map.entry(thirdDate, record)
        );
        when(historyReader.getGlobalPostHistory()).thenReturn(history);
        mockMvc.perform(
                get("/history/global/posts/")
                        .with(user("admin").roles("SUPER_ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(historyReader, times(1)).getGlobalPostHistory();
    }

    @Test
    public void test_getPostHistoryForUser_is_success() throws Exception {
        int userId = 1;
        PostRecord record = new PostRecord();
        record.setAuthorId(userId);
        record.setId(1);

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime secondDate = now.plusDays(1);
        LocalDateTime thirdDate = now.minusDays(1);

        Map<LocalDateTime, PostRecord> history = Map.ofEntries(
                Map.entry(now, record),
                Map.entry(secondDate, record),
                Map.entry(thirdDate, record)
        );
        when(historyReader.getPostHistoryForUser(userId)).thenReturn(history);
        mockMvc.perform(
                get("/history/posts/" + userId)
                        .with(user("admin").roles("SUPER_ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());

        verify(historyReader, times(1)).getPostHistoryForUser(userId);
    }
}