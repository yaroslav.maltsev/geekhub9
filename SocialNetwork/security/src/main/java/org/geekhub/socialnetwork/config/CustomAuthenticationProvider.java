package org.geekhub.socialnetwork.config;

import org.geekhub.socialnetwork.AccountNotActivatedException;
import org.geekhub.socialnetwork.PasswordNotMatchException;
import org.geekhub.socialnetwork.manager.user.UsersManager;
import org.geekhub.socialnetwork.persistance.user.UserRecord;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
    private final UsersManager usersManager;
    private final PasswordEncoder passwordEncoder;

    public CustomAuthenticationProvider(UsersManager usersManager, PasswordEncoder passwordEncoder) {
        this.usersManager = usersManager;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication) {
        final String email = authentication.getName();
        final String password = authentication.getCredentials().toString();
        final UserRecord user = usersManager.getUserByEmail(email);

        if (!user.isActivated()) {
            throw new AccountNotActivatedException("User account is not activated!", user.getId());
        } else if (!passwordEncoder.matches(password, user.getEncodedPassword())){
            throw new PasswordNotMatchException("Passwords with authenticated user must be identical!");
        }

        final List<SimpleGrantedAuthority> authorities = Collections.singletonList(new SimpleGrantedAuthority(user.getRole()));
        return new UsernamePasswordAuthenticationToken(user, null, authorities);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
