package org.geekhub.socialnetwork;

import org.geekhub.socialnetwork.manager.user.UsersManager;
import org.geekhub.socialnetwork.persistance.user.Gender;
import org.geekhub.socialnetwork.persistance.user.UserRecord;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Profile("dev")
@Service
public class UsersDevInitializer implements ApplicationRunner {
    private final UsersManager usersManager;
    private final PasswordEncoder passwordEncoder;

    public UsersDevInitializer(UsersManager usersManager, PasswordEncoder passwordEncoder) {
        this.usersManager = usersManager;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(ApplicationArguments args) {

        UserRecord user = new UserRecord();
        user.setRegistrationDate(LocalDateTime.now());
        user.setEmail("user@gmail.com");
        user.setEncodedPassword(passwordEncoder.encode("user"));
        user.setUsername("User");
        user.setGender(Gender.FEMALE);
        user.setRole("ROLE_USER");
        user.setActivated(true);

        UserRecord admin = new UserRecord();
        admin.setRegistrationDate(LocalDateTime.now());
        admin.setEmail("admin@gmail.com");
        admin.setEncodedPassword(passwordEncoder.encode("admin"));
        admin.setUsername("Admin");
        admin.setGender(Gender.MALE);
        admin.setRole("ROLE_ADMIN");
        admin.setActivated(true);

        usersManager.addUser(user);
        usersManager.addUser(admin);
    }
}
