package org.geekhub.socialnetwork.service;

import org.geekhub.socialnetwork.AccountNotActivatedException;
import org.geekhub.socialnetwork.CustomUserDetails;
import org.geekhub.socialnetwork.manager.user.UsersManager;
import org.geekhub.socialnetwork.persistance.user.UserRecord;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UsersManager usersManager;

    public UserDetailsServiceImpl(UsersManager usersManager) {
        this.usersManager = usersManager;
    }

    @Override
    public UserDetails loadUserByUsername(String email) {
        final UserRecord userRecord = usersManager.getUserByEmail(email);
        if (!userRecord.isActivated()) {
            throw new AccountNotActivatedException("User account is not activated!", userRecord.getId());
        }
        CustomUserDetails user = new CustomUserDetails(
                userRecord.getEmail(), userRecord.getEncodedPassword(), Collections.singletonList(new SimpleGrantedAuthority(userRecord.getRole()))
        );
        user.setUserRecord(userRecord);
        return user;
    }
}
