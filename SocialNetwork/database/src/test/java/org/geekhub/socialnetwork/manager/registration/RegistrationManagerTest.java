package org.geekhub.socialnetwork.manager.registration;

import org.geekhub.socialnetwork.dao.model.UsersRepository;
import org.geekhub.socialnetwork.dao.relational.UnactivatedUsersDao;
import org.geekhub.socialnetwork.persistance.user.UnactivatedUserRecord;
import org.geekhub.socialnetwork.persistance.user.UserRecord;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

import static org.mockito.Mockito.*;

public class RegistrationManagerTest {
    private UsersRepository usersRepository;
    private UnactivatedUsersDao unactivatedUsersDao;
    private RegistrationManager manager;

    @BeforeMethod
    public void setUp() {
        usersRepository = mock(UsersRepository.class);
        unactivatedUsersDao = mock(UnactivatedUsersDao.class);

        manager = new RegistrationManager(usersRepository, unactivatedUsersDao);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_in_activateUser_when_code_is_blank() {
        manager.activateUser("  ");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_in_activateUser_when_code_is_null() {
        manager.activateUser(null);
    }

    @Test
    public void test_activateUser_is_success() {
        String activationCode = "code";

        UnactivatedUserRecord record = new UnactivatedUserRecord(2, activationCode, LocalDateTime.now());

        when(unactivatedUsersDao.getByActivationCode(activationCode)).thenReturn(record);

        manager.activateUser(activationCode);

        verify(usersRepository, times(1)).activateUser(record.getUserId());
        verify(unactivatedUsersDao, times(1)).deleteById(record.getId());
    }


    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_in_registerUser_when_params_are_null() {
        manager.registerUser(null, null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_in_registerUser_when_code_is_blank() {
        manager.registerUser(new UserRecord(), "  ");
    }

    @Test
    public void test_registerUser_is_success() {
        int id = 1;
        String code = "code";

        UserRecord userRecord = new UserRecord();
        userRecord.setId(id);

        when(usersRepository.save(userRecord)).thenReturn(userRecord);

        manager.registerUser(userRecord, code);

        verify(usersRepository, times(1)).save(userRecord);
        verify(unactivatedUsersDao, times(1)).save(any());
    }
}