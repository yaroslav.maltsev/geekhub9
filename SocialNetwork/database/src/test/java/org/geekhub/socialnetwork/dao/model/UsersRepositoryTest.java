package org.geekhub.socialnetwork.dao.model;

import org.geekhub.socialnetwork.config.DatabaseConfig;
import org.geekhub.socialnetwork.persistance.user.Gender;
import org.geekhub.socialnetwork.persistance.user.UserRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.testng.Assert.*;

@ContextConfiguration(classes = DatabaseConfig.class)
@JdbcTest
public class UsersRepositoryTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private UsersRepository usersRepository;
    private UserRecord userRecord;
    private final String testEmailValue = "test@email.com";

    @BeforeMethod
    public void setUp() {
        userRecord = new UserRecord();
        userRecord.setRegistrationDate(LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS));
        userRecord.setEmail(testEmailValue);
        userRecord.setEncodedPassword("password");
        userRecord.setRole("ROLE_USER");
        userRecord.setUsername("name");
        userRecord.setGender(Gender.MALE);

        usersRepository.deleteAll();
    }

    @Test
    public void test_addToHistory_when_table_is_empty_is_success() {
        UserRecord savedRecord = usersRepository.save(userRecord);

        assertEquals(usersRepository.count(), 1);
        assertEquals(savedRecord.getEmail(), userRecord.getEmail());
    }


    @Test
    public void test_getUserById_is_success() {
        int userRecordId = usersRepository.save(userRecord).getId();

        UserRecord userById = usersRepository.findById(userRecordId).get();

        assertNotNull(userById);
        assertTrue(areRecordsEquals(userById, userRecord));
    }

    @Test
    public void test_getUserByEmail_is_success() {
        usersRepository.save(userRecord);

        UserRecord userById = usersRepository.getUserByEmail(testEmailValue).get();

        assertNotNull(userById);
        assertEquals(userById.getEncodedPassword(), userRecord.getEncodedPassword());
        assertEquals(userById.getGender(), userRecord.getGender());
        assertEquals(userById.getUsername(), userRecord.getUsername());
        assertEquals(userById.getRole(), userRecord.getRole());
    }

    @Test
    public void test_getAllUserRecords_when_table_is_empty_is_success() {
        List<UserRecord> allUserRecords = (List<UserRecord>) usersRepository.findAll();

        assertNotNull(allUserRecords);
        assertEquals(allUserRecords.size(), 0);
    }

    @Test
    public void test_getAllUserRecords_when_table_is_not_empty_is_success() {
        LocalDateTime now = LocalDateTime.now();
        Gender gender = Gender.MALE;

        UserRecord userRecord1 = new UserRecord(now, "e1", "p", "r", "n", gender);
        UserRecord userRecord2 = new UserRecord(now, "e1", "p", "r", "n", gender);
        UserRecord userRecord3 = new UserRecord(now, "e1", "p", "r", "n", gender);
        UserRecord userRecord4 = new UserRecord(now, "e1", "p", "r", "n", gender);
        UserRecord userRecord5 = new UserRecord(now, "e1", "p", "r", "n", gender);

        usersRepository.save(userRecord1);
        usersRepository.save(userRecord2);
        usersRepository.save(userRecord3);
        usersRepository.save(userRecord4);
        usersRepository.save(userRecord5);


        List<UserRecord> allUserRecords = (List<UserRecord>) usersRepository.findAll();

        assertEquals(allUserRecords.size(), 5);
        assertTrue(areRecordsEquals(allUserRecords.get(0), userRecord1));
        assertTrue(areRecordsEquals(allUserRecords.get(1), userRecord2));
        assertTrue(areRecordsEquals(allUserRecords.get(2), userRecord3));
        assertTrue(areRecordsEquals(allUserRecords.get(3), userRecord4));
        assertTrue(areRecordsEquals(allUserRecords.get(4), userRecord5));
    }

    @Test
    public void test_deleteUserById_when_table_is_empty() {
        usersRepository.deleteById(1);
        assertEquals(usersRepository.count(), 0);
    }

    @Test
    public void test_deleteUserById_when_record_is_last() {
        int userId = usersRepository.save(userRecord).getId();
        usersRepository.deleteById(userId);
        assertEquals(usersRepository.count(), 0);
    }

    @Test
    public void test_deleteUserById_when_record_is_not_last() {
        int firstUserId = usersRepository.save(userRecord).getId();

        LocalDateTime now = LocalDateTime.now();
        Gender gender = Gender.MALE;

        usersRepository.save(new UserRecord(now, "e1", "p", "r", "n", gender));
        usersRepository.save(new UserRecord(now, "e2", "p", "r", "n", gender));
        usersRepository.save(new UserRecord(now, "e3", "p", "r", "n", gender));

        usersRepository.deleteById(firstUserId);
        assertEquals(usersRepository.count(), 3);
    }


    @Test
    public void test_deleteAllUsers_when_table_is_empty() {
        usersRepository.deleteAll();
        assertEquals(usersRepository.count(), 0);
    }

    @Test
    public void test_deleteAllUsers_when_record_is_last() {
        usersRepository.save(userRecord);
        usersRepository.deleteAll();
        assertEquals(usersRepository.count(), 0);
    }

    @Test
    public void test_deleteAllUsers_when_record_is_not_last() {
        LocalDateTime now = LocalDateTime.now();
        Gender gender = Gender.MALE;
        usersRepository.save(new UserRecord(now, "e1", "p", "r", "n", gender));
        usersRepository.save(new UserRecord(now, "e2", "p", "r", "n", gender));
        usersRepository.save(new UserRecord(now, "e3", "p", "r", "n", gender));
        usersRepository.save(new UserRecord(now, "e4", "p", "r", "n", gender));
        usersRepository.save(new UserRecord(now, "e5", "p", "r", "n", gender));

        usersRepository.deleteAll();

        assertEquals(usersRepository.count(), 0);
    }

    @Test
    public void test_updateUser_when_user_exist_is_success() {
        
        UserRecord savedRecord = usersRepository.save(userRecord);
        UserRecord oldRecord = new UserRecord(
                savedRecord.getRegistrationDate(),
                savedRecord.getEmail(),
                savedRecord.getEncodedPassword(),
                savedRecord.getRole(),
                savedRecord.getUsername(),
                savedRecord.getGender()
        );
        oldRecord.setId(savedRecord.getId());

        userRecord.setUsername("New surname");
        usersRepository.save(userRecord);

        assertNotEquals(oldRecord.getUsername(), userRecord.getUsername());
        assertEquals(oldRecord.getEmail(), userRecord.getEmail());
        assertEquals(oldRecord.getEncodedPassword(), userRecord.getEncodedPassword());
        assertEquals(oldRecord.getRole(), userRecord.getRole());
        assertEquals(oldRecord.getGender(), userRecord.getGender());
    }

    @Test
    public void test_updateUser_when_changed_parameters_are_equals_is_success() {
        UserRecord savedRecord = usersRepository.save(userRecord);
        UserRecord oldRecord = new UserRecord(
                savedRecord.getRegistrationDate(),
                savedRecord.getEmail(),
                savedRecord.getEncodedPassword(),
                savedRecord.getRole(),
                savedRecord.getUsername(),
                savedRecord.getGender()
        );
        oldRecord.setId(savedRecord.getId());;

        userRecord.setUsername("name");
        usersRepository.save(userRecord);

        assertEquals(oldRecord.getEmail(), userRecord.getEmail());
        assertEquals(oldRecord.getEncodedPassword(), userRecord.getEncodedPassword());
        assertEquals(oldRecord.getRole(), userRecord.getRole());
        assertEquals(oldRecord.getUsername(), userRecord.getUsername());
        assertEquals(oldRecord.getGender(), userRecord.getGender());
    }

    @Test
    public void test_updateUser_when_changed_all_parameters_is_success() {
        UserRecord savedRecord = usersRepository.save(userRecord);
        UserRecord oldRecord = new UserRecord(
                savedRecord.getRegistrationDate(),
                savedRecord.getEmail(),
                savedRecord.getEncodedPassword(),
                savedRecord.getRole(),
                savedRecord.getUsername(),
                savedRecord.getGender()
        );
        oldRecord.setId(savedRecord.getId());

        userRecord.setEmail("new");
        userRecord.setEncodedPassword("new");
        userRecord.setRole("new");
        userRecord.setGender(Gender.FEMALE);
        usersRepository.save(userRecord);

        assertEquals(userRecord.getId(), oldRecord.getId());
        assertEquals(userRecord.getRegistrationDate(), oldRecord.getRegistrationDate());
        assertNotEquals(userRecord.getEmail(), oldRecord.getEmail());
        assertNotEquals(userRecord.getEncodedPassword(), oldRecord.getEncodedPassword());
        assertNotEquals(userRecord.getRole(), oldRecord.getRole());
        assertNotEquals(userRecord.getGender(), oldRecord.getGender());
    }


    @Test
    public void test_activateUser_when_user_not_exist_is_success() {
        usersRepository.activateUser(1);
    }

    @Test
    public void test_activateUser_when_user_activated_is_success() {
        UserRecord savedRecord = usersRepository.save(userRecord);
        usersRepository.activateUser(savedRecord.getId());
        usersRepository.activateUser(savedRecord.getId());

        UserRecord activatedRecord = usersRepository.findById(savedRecord.getId()).get();
        assertTrue(activatedRecord.isActivated());
    }

    @Test
    public void test_activateUser_is_success() {
        assertFalse(userRecord.isActivated());

        UserRecord savedRecord = usersRepository.save(userRecord);

        usersRepository.activateUser(savedRecord.getId());

        UserRecord activatedRecord = usersRepository.findById(savedRecord.getId()).get();
        assertTrue(activatedRecord.isActivated());
    }


    private boolean areRecordsEquals(UserRecord first, UserRecord second) {
        return  first.getEncodedPassword().equals(second.getEncodedPassword()) &&
                first.getGender().equals(second.getGender()) &&
                first.getUsername().equals(second.getUsername()) &&
                first.getRole().equals(second.getRole());
    }
}