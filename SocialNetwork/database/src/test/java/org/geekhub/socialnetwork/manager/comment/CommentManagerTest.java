package org.geekhub.socialnetwork.manager.comment;

import org.geekhub.socialnetwork.dao.model.CommentsRepository;
import org.geekhub.socialnetwork.manager.history.HistoryManager;
import org.geekhub.socialnetwork.persistance.comment.CommentRecord;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.testng.Assert.assertEquals;

public class CommentManagerTest {
    private CommentManager commentManager;
    private CommentsRepository commentsRepository;
    private HistoryManager historyManager;

    @BeforeMethod
    public void setUp() {
        commentsRepository = mock(CommentsRepository.class);
        historyManager = mock(HistoryManager.class);
        commentManager = new CommentManager(commentsRepository, historyManager);
    }

    @Test
    public void test_getAllPostComments_is_success() {
        int postId = 1;

        ArrayList<CommentRecord> records = new ArrayList<>();
        when(commentsRepository.getAllPostComments(postId)).thenReturn(records);

        List<CommentRecord> postComments = commentManager.getAllPostComments(postId);

        assertEquals(postComments, records);
        verify(commentsRepository, times(1)).getAllPostComments(postId);
    }

    @Test
    public void test_getCommentById_is_success() {
        int postId = 1;

        CommentRecord record = new CommentRecord();
        when(commentsRepository.findById(postId)).thenReturn(Optional.of(record));

        CommentRecord comment = commentManager.getCommentById(postId);

        assertEquals(comment, record);
        verify(commentsRepository, times(1)).findById(postId);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_in_getCommentById_when_record_not_exist() {
        int id = 2;
        when(commentsRepository.findById(id)).thenReturn(Optional.empty());
        commentManager.getCommentById(id);
    }

    @Test
    public void test_deleteComment_is_success() {
        int postId = 1;
        commentManager.deleteComment(postId);
        verify(commentsRepository, times(1)).deleteById(postId);
    }


    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_in_addComment_when_argument_is_null() {
        commentManager.addComment(null);
    }

    @Test
    public void test_addComment_is_success() {
        CommentRecord record = new CommentRecord();
        commentManager.addComment(record);

        verify(commentsRepository, times(1)).save(record);
        verify(historyManager, times(1)).addToHistory(any());
    }


    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_in_editComment_when_argument_is_null() {
        commentManager.editComment(null);
    }

    @Test
    public void test_editComment_is_success() {
        CommentRecord record = new CommentRecord();
        commentManager.editComment(record);

        verify(commentsRepository, times(1)).save(record);
    }
}
