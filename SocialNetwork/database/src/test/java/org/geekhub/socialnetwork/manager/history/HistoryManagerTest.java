package org.geekhub.socialnetwork.manager.history;

import org.geekhub.socialnetwork.dao.model.history.HistoryDao;
import org.geekhub.socialnetwork.persistance.comment.CommentRecord;
import org.geekhub.socialnetwork.persistance.history.HistoryElement;
import org.geekhub.socialnetwork.persistance.history.HistoryRecord;
import org.geekhub.socialnetwork.persistance.history.RecordType;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.testng.Assert.assertEquals;

public class HistoryManagerTest {
    private HistoryDao historyDao;
    private HistoryManager historyManager;

    @BeforeMethod
    public void setUp() {
        historyDao = mock(HistoryDao.class);
        historyManager = new HistoryManager(historyDao);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_in_addToHistory_when_argument_is_null() {
        historyManager.addToHistory(null);
    }

    @Test
    public void test_addToHistory_is_success() {
        HistoryElement historyElement = new CommentRecord();
        historyManager.addToHistory(historyElement);

        verify(historyDao, times(1)).save(any());
    }

    @Test
    public void test_getAllRecordsOfType_is_success() {
        List<HistoryRecord> list = List.of(new HistoryRecord(), new HistoryRecord());
        RecordType type = RecordType.POST;

        when(historyDao.getAllRecordsOfType(type.name())).thenReturn(list);

        List<HistoryRecord> result = historyManager.getAllRecordsOfType(type);

        assertEquals(result, list);

        verify(historyDao, times(1)).getAllRecordsOfType(type.name());
    }

    @Test
    public void test_getAllUserRecordsOfType_is_success() {
        List<HistoryRecord> list = List.of(new HistoryRecord(), new HistoryRecord());
        RecordType type = RecordType.POST;
        int userId = 1;

        when(historyDao.getAllUserRecordsOfType(userId, type.name())).thenReturn(list);

        List<HistoryRecord> result = historyManager.getAllUserRecordsOfType(userId, type);

        assertEquals(result, list);

        verify(historyDao, times(1)).getAllUserRecordsOfType(userId, type.name());
    }

    @Test
    public void test_getAllHistoryRecordsOfUser_is_success() {
        List<HistoryRecord> list = List.of(new HistoryRecord(), new HistoryRecord());
        int userId = 1;

        when(historyDao.getAllHistoryRecordsOfUser(userId)).thenReturn(list);

        List<HistoryRecord> result = historyManager.getAllHistoryRecordsOfUser(userId);

        assertEquals(result, list);

        verify(historyDao, times(1)).getAllHistoryRecordsOfUser(userId);
    }

    @Test
    public void test_clearUserHistory_is_success() {
        int userId = 1;
        historyManager.clearUserHistory(userId);

        verify(historyDao, times(1)).clearUserHistory(userId);
    }
}