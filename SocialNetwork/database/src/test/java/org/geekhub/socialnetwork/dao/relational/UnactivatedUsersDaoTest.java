package org.geekhub.socialnetwork.dao.relational;

import org.geekhub.socialnetwork.config.DatabaseConfig;
import org.geekhub.socialnetwork.persistance.user.UnactivatedUserRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.testng.Assert.assertEquals;

@ContextConfiguration(classes = DatabaseConfig.class)
@JdbcTest
public class UnactivatedUsersDaoTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private UnactivatedUsersDao dao;

    @Test
    public void test_getUserIdByActivationCode_when_record_not_exist_is_success() {
        dao.getByActivationCode("456789");
    }

    @Test
    public void test_getUserIdByActivationCode_is_success() {
        int userId = 1;
        String activationCode = "1234567890";

        dao.save(new UnactivatedUserRecord(userId, activationCode, LocalDateTime.now()));

        UnactivatedUserRecord record = dao.getByActivationCode(activationCode);
        int idByActivationCode = record.getUserId();

        assertEquals(userId, idByActivationCode);
    }


    @Test
    public void test_deleteRecordsWithExpiredTime_is_success() {
        int userId = 1;
        String code = "235";
        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES);

        dao.save(new UnactivatedUserRecord(userId, code, now));
        dao.save(new UnactivatedUserRecord(userId, code, now.plusDays(1)));
        dao.save(new UnactivatedUserRecord(userId, code, now.plusDays(2)));

        dao.deleteRecordsWithExpiredTime(now, 60*24);

        assertEquals(dao.count(), 1);
    }


    @Test
    public void test_getIdsOfUsersInactiveAccounts_is_success() {
        int userId = 1;
        String code = "235";
        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES);

        dao.save(new UnactivatedUserRecord(userId, code, now));
        dao.save(new UnactivatedUserRecord(userId + 1, code, now.plusDays(1)));
        dao.save(new UnactivatedUserRecord(userId + 2, code, now.plusDays(2)));

        List<Integer> ids = dao.getIdsOfUsersInactiveAccounts(now, 60 * 24);

        assertEquals(ids, List.of(userId + 1, userId + 2));
    }
}