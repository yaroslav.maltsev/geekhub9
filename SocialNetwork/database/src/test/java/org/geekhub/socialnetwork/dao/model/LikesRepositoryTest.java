package org.geekhub.socialnetwork.dao.model;

import org.geekhub.socialnetwork.config.DatabaseConfig;
import org.geekhub.socialnetwork.persistance.like.LikeRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.testng.Assert.*;

@ContextConfiguration(classes = DatabaseConfig.class)
@JdbcTest
public class LikesRepositoryTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private LikesRepository likesRepository;
    private LikeRecord likeRecord;
    private String authorUsername = "username";

    @BeforeMethod
    public void setUp() {
        likesRepository.deleteAll();

        int postId = 1;
        int authorId = 1;
        likeRecord = new LikeRecord(
                postId,
                authorId,
                "username",
                LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS)
        );
    }

    @Test
    public void test_addLike_is_success() {
        int likeId = likesRepository.save(likeRecord).getId();
        LikeRecord likeRecordById = likesRepository.findById(likeId).get();

        assertEquals(likeRecordById.getAuthorId(), likeRecord.getAuthorId());
        assertEquals(likeRecordById.getPostId(), likeRecord.getPostId());
    }


    @Test
    public void test_getAllPostLikes_when_table_is_empty_is_success() {
        List<LikeRecord> allPostLikeRecords = likesRepository.getAllPostLikes(1);

        assertNotNull(likesRepository);
        assertEquals(allPostLikeRecords.size(), 0);
    }

    @Test
    public void test_getAllPostLikes_when_table_has_one_record_is_success() {
        likesRepository.save(likeRecord);

        List<LikeRecord> allPostLikeRecords = likesRepository.getAllPostLikes(1);

        assertNotNull(likesRepository);
        assertEquals(allPostLikeRecords.size(), 1);
        assertEquals(allPostLikeRecords.get(0).getAuthorId(), likeRecord.getAuthorId());
    }

    @Test
    public void test_getAllPostLikes_when_table_has_more_than_one_record_is_success() {
        int authorId = likeRecord.getAuthorId();
        int postId = likeRecord.getPostId();
        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);

        likesRepository.save(likeRecord);
        likesRepository.save(new LikeRecord(postId, authorId, authorUsername, now));
        likesRepository.save(new LikeRecord(postId, authorId, authorUsername, now));
        likesRepository.save(new LikeRecord(postId, authorId, authorUsername, now));

        List<LikeRecord> allPostLikeRecords = likesRepository.getAllPostLikes(postId);

        assertNotNull(allPostLikeRecords);
        assertEquals(allPostLikeRecords.size(), 4);
        assertEquals(allPostLikeRecords.get(0).getAuthorId(), authorId);
        assertEquals(allPostLikeRecords.get(1).getAuthorId(), authorId);
        assertEquals(allPostLikeRecords.get(2).getAuthorId(), authorId);
        assertEquals(allPostLikeRecords.get(3).getAuthorId(), authorId);
    }


    @Test
    public void test_getLikeById_when_table_has_only_one_record_is_success() {
        int id = likesRepository.save(likeRecord).getId();

        LikeRecord record = likesRepository.findById(id).get();

        assertNotNull(record);
        assertEquals(record.getAuthorId(), likeRecord.getAuthorId());
        assertEquals(record.getPostId(), likeRecord.getPostId());
    }

    @Test
    public void test_getLikeById_when_table_has_more_than_one_record_is_success() {
        int authorId = likeRecord.getAuthorId();
        int postId = likeRecord.getPostId();
        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);

        int firstId = likesRepository.save(likeRecord).getId();
        likesRepository.save(new LikeRecord(postId, authorId, authorUsername, now));
        likesRepository.save(new LikeRecord(postId, authorId, authorUsername, now));
        likesRepository.save(new LikeRecord(postId, authorId, authorUsername, now));
        likesRepository.save(new LikeRecord(postId, authorId, authorUsername, now));

        LikeRecord record = likesRepository.findById(firstId).get();

        assertNotNull(record);
        assertEquals(record.getAuthorId(), likeRecord.getAuthorId());
        assertEquals(record.getPostId(), likeRecord.getPostId());
    }
    
    
    @Test
    public void test_clearTable_is_success() {
        int authorId = likeRecord.getAuthorId();
        int postId = likeRecord.getPostId();
        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);

        likesRepository.save(likeRecord);
        likesRepository.save(new LikeRecord(postId, authorId, authorUsername, now));
        likesRepository.save(new LikeRecord(postId, authorId, authorUsername, now));
        likesRepository.save(new LikeRecord(postId, authorId, authorUsername, now));
        likesRepository.save(new LikeRecord(postId, authorId, authorUsername, now));
        likesRepository.save(new LikeRecord(postId, authorId, authorUsername, now));

        likesRepository.deleteAll();

        assertEquals(likesRepository.count(), 0);
    }


    @Test
    public void test_deleteLike_when_post_not_exist_is_success() {
        likesRepository.deleteById(1);
    }

    @Test
    public void test_deleteLike_is_success() {
        int recordId =likesRepository.save(likeRecord).getId();

        likesRepository.deleteById(recordId);

        assertEquals(likesRepository.getAllPostLikes(1).size(), 0);
    }

    @Test
    public void test_deleteLike_when_table_is_not_empty_is_success() {
        int authorId = likeRecord.getAuthorId();
        int postId = likeRecord.getPostId();
        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);

        int firstLikeId = likesRepository.save(likeRecord).getId();
        likesRepository.save(new LikeRecord(postId, authorId, authorUsername, now));
        likesRepository.save(new LikeRecord(postId, authorId, authorUsername, now));
        likesRepository.save(new LikeRecord(postId, authorId, authorUsername, now));

        List<LikeRecord> allLikesBeforeDeleting = (List<LikeRecord>) likesRepository.findAll();

        likesRepository.deleteById(firstLikeId);

        assertNotNull(likesRepository.findById(allLikesBeforeDeleting.get(1).getId()));
        assertNotNull(likesRepository.findById(allLikesBeforeDeleting.get(2).getId()));
        assertNotNull(likesRepository.findById(allLikesBeforeDeleting.get(3).getId()));

        assertEquals(likesRepository.count(), 3);
    }


    @Test
    public void test_deleteAllUserLikes_when_user_not_exist_is_success() {
        likesRepository.deleteByAuthorId(1);
    }

    @Test
    public void test_deleteAllUserLikes_is_success() {
        likesRepository.save(likeRecord);

        likesRepository.deleteByAuthorId(likeRecord.getAuthorId());

        assertEquals(likesRepository.count(), 0);
    }

    @Test
    public void test_deleteAllUserLikes_when_table_is_not_empty_is_success() {
        int authorId = likeRecord.getAuthorId();
        int postId = likeRecord.getPostId();
        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);

        likesRepository.save(likeRecord);
        likesRepository.save(new LikeRecord(postId, authorId, authorUsername, now));
        likesRepository.save(new LikeRecord(postId, authorId, authorUsername, now));
        likesRepository.save(new LikeRecord(postId, authorId, authorUsername, now));

        likesRepository.deleteByAuthorId(likeRecord.getAuthorId());

        assertEquals(likesRepository.count(), 0);
    }


    @Test
    public void test_deleteAllPostLikes_when_user_not_exist_is_success() {
        likesRepository.deleteAllPostLikes(1);
    }

    @Test
    public void test_deleteAllPostLikes_is_success() {
        likesRepository.save(likeRecord);

        likesRepository.deleteByAuthorId(likeRecord.getPostId());

        assertEquals(likesRepository.count(), 0);
    }

    @Test
    public void test_deleteAllPostLikes_when_table_is_not_empty_is_success() {
        int authorId = likeRecord.getAuthorId();
        int postId = likeRecord.getPostId();
        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);

        likesRepository.save(likeRecord);
        likesRepository.save(new LikeRecord(postId, authorId, authorUsername, now));
        likesRepository.save(new LikeRecord(postId, authorId, authorUsername, now));
        likesRepository.save(new LikeRecord(postId, authorId, authorUsername, now));

        likesRepository.deleteAllPostLikes(likeRecord.getPostId());

        assertEquals(likesRepository.count(), 0);
    }
}
