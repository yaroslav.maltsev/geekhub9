package org.geekhub.socialnetwork.manager.post;

import org.geekhub.socialnetwork.dao.model.CommentsRepository;
import org.geekhub.socialnetwork.dao.model.LikesRepository;
import org.geekhub.socialnetwork.dao.model.PostsRepository;
import org.geekhub.socialnetwork.manager.history.HistoryManager;
import org.geekhub.socialnetwork.persistance.post.PostRecord;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class PostManagerTest {
    private PostsRepository postsRepository;
    private CommentsRepository commentsRepository;
    private LikesRepository likesRepository;
    private HistoryManager historyManager;
    private PostManager postManager;

    @BeforeMethod
    public void setUp() {
        postsRepository = mock(PostsRepository.class);
        commentsRepository = mock(CommentsRepository.class);
        likesRepository = mock(LikesRepository.class);
        historyManager = mock(HistoryManager.class);
        postManager = new PostManager(postsRepository, commentsRepository, likesRepository, historyManager);
    }

    @Test
    public void test_getPostById_is_success() {
        int id = 1;
        PostRecord postRecord = new PostRecord();

        when(postsRepository.findById(id)).thenReturn(Optional.of(postRecord));

        PostRecord postById = postManager.getPostById(id);

        verify(postsRepository, times(1)).findById(id);

        assertEquals(postById, postRecord);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_in_getPostById_when_record_not_exist() {
        int id = 2;
        when(postsRepository.findById(id)).thenReturn(Optional.empty());
        postManager.getPostById(id);
    }

    @Test
    public void test_getAllUserPosts_is_success() {
        int id = 1;
        postManager.getAllUserPosts(id);
        verify(postsRepository, times(1)).getAllUserPosts(id);
    }

    @Test
    public void test_getFriendsPostRecordByLastThreeDays_is_success() {
        List<Integer> friendsIds = List.of(1, 2, 5, 7, 9);

        postManager.getFriendsPostRecordByLastThreeDays(friendsIds);

        verify(postsRepository, times(1)).getPostsOfUserFriendsByDate(
                friendsIds,
                LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES),
                3);
    }

    @Test
    public void test_getFriendsPostRecordByLastThreeDays_when_friendsIdsList_if_empty_is_success() {
        List<Integer> friendsIds = new ArrayList<>();

        List<PostRecord> list = postManager.getFriendsPostRecordByLastThreeDays(friendsIds);
        assertTrue(list.isEmpty());
    }


    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_in_addPost_when_argument_is_null() {
        postManager.addPost(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_in_addPost_when_post_parameters_are_null() {
        postManager.addPost(new PostRecord());
    }

    @Test
    public void test_addPost_with_changing_date_is_success() {
        int postId = 1;
        PostRecord postRecord = new PostRecord();
        postRecord.setId(postId);
        LocalDateTime date = LocalDateTime.now().minusDays(2);
        postRecord.setDate(date);
        postRecord.setDescription("desc");
        postRecord.setAuthorId(2);

        when(postsRepository.findById(postId)).thenReturn(Optional.of(postRecord));

        postManager.addPost(postRecord);

        verify(postsRepository, times(1)).save(postRecord);
        verify(historyManager, times(1)).addToHistory(any());
    }


    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_in_editPost_when_argument_is_null() {
        postManager.editPost(null);
    }

    @Test
    public void test_editPost_is_success() {
        PostRecord postRecord = new PostRecord();
        LocalDateTime date = LocalDateTime.now().minusDays(2);
        postRecord.setDate(date);
        postRecord.setDescription("desc");
        postRecord.setAuthorId(2);

        postManager.editPost(postRecord);

        verify(postsRepository, times(1)).save(postRecord);
    }

    @Test
    public void test_deletePost_is_success() {
        int postId = 1;

        postManager.deletePost(postId);

        verify(commentsRepository, times(1)).deleteAllPostComments(postId);
        verify(likesRepository, times(1)).deleteAllPostLikes(postId);
        verify(postsRepository, times(1)).deleteById(postId);
    }

    @Test
    public void test_delete_all_user_posts_is_success() {
        int userId = 1;

        postManager.deleteAllUserPosts(userId);

        verify(postsRepository, times(1)).deleteAllUserPosts(userId);
        verify(commentsRepository, times(1)).deleteAllUserComments(userId);
        verify(likesRepository, times(1)).deleteByAuthorId(userId);
    }
}
