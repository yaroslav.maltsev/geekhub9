package org.geekhub.socialnetwork.dao.relational;

import org.geekhub.socialnetwork.config.DatabaseConfig;
import org.geekhub.socialnetwork.persistance.user_friend.UserFriendRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.List;

import static org.testng.Assert.*;

@ContextConfiguration(classes = {DatabaseConfig.class})
@JdbcTest
public class FriendsRepositoryTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private FriendsRepository friendsRepository;
    private UserFriendRecord userFriendRecord;
    private LocalDateTime now;

    @BeforeMethod
    public void setUp() {
        friendsRepository.deleteAll();

        now = LocalDateTime.now();
        userFriendRecord = new UserFriendRecord(
                now,
                now,
                1,
                2,
                false
        );
    }

    @Test
    public void test_addUserFriendRequest_when_table_is_empty_is_success() {
        int initiatorId = 1;
        int friendId = 2;
        friendsRepository.save(userFriendRecord);

        List<UserFriendRecord> allRecords = (List<UserFriendRecord>) friendsRepository.findAll();

        assertEquals(allRecords.size(), 1);
        assertEquals(allRecords.get(0).getInitiatorId(), initiatorId);
        assertEquals(allRecords.get(0).getFriendId(), friendId);
    }

    @Test
    public void test_addUserFriendRequest_with_default_isConfirmed_false_value_is_success() {
        int initiatorId = 1;
        int friendId = 2;
        friendsRepository.save(userFriendRecord);

        UserFriendRecord recordByIds = friendsRepository.getRecordByIds(initiatorId, friendId);
        assertFalse(recordByIds.isConfirmed());
    }

    @Test
    public void test_multiply_addUserFriendRequest_with_different_ids_is_success() {
        LocalDateTime now = LocalDateTime.now();
        friendsRepository.save(userFriendRecord);
        friendsRepository.save(new UserFriendRecord(now, now, 2, 3, false));
        friendsRepository.save(new UserFriendRecord(now, now, 3, 4, false));
        friendsRepository.save(new UserFriendRecord(now, now, 4, 5, false));

        List<UserFriendRecord> allRecords = (List<UserFriendRecord>) friendsRepository.findAll();

        assertEquals(allRecords.size(), 4);
    }

    @Test
    public void test_confirmFriendship_when_friendship_is_not_confirmed_is_success() {
        int initiatorId = 1;
        int friendId = 2;
        friendsRepository.save(userFriendRecord);
        UserFriendRecord recordByIdsBeforeConfirming = friendsRepository.getRecordByIds(initiatorId, friendId);
        assertFalse(recordByIdsBeforeConfirming.isConfirmed());

        friendsRepository.confirmFriendship(initiatorId, friendId, now);
        UserFriendRecord recordByIdsAfterConfirming = friendsRepository.getRecordByIds(initiatorId, friendId);

        assertTrue(recordByIdsAfterConfirming.isConfirmed());
    }

    @Test
    public void test_confirmFriendship_when_friendship_already_confirmed_is_success() {
        int initiatorId = 1;
        int friendId = 2;
        UserFriendRecord recordByIdsBeforeConfirming = friendsRepository.save(userFriendRecord);
        assertFalse(recordByIdsBeforeConfirming.isConfirmed());

        friendsRepository.confirmFriendship(initiatorId, friendId, now);
        friendsRepository.confirmFriendship(initiatorId, friendId, now);
        UserFriendRecord recordByIdsAfterConfirming = friendsRepository.getRecordByIds(initiatorId, friendId);

        assertTrue(recordByIdsAfterConfirming.isConfirmed());
    }

    @Test
    public void test_confirmFriendship_when_record_not_exist_is_success() {
        friendsRepository.confirmFriendship(2, 15, now);
    }


    @Test
    public void test_refuseFriendshipRequest_when_friendship_is_not_confirmed_is_success() {
        int initiatorId = 1;
        int friendId = 2;
        friendsRepository.save(userFriendRecord);
        UserFriendRecord recordByIdsBeforeRefusing = friendsRepository.getRecordByIds(initiatorId, friendId);
        assertFalse(recordByIdsBeforeRefusing.isConfirmed());

        friendsRepository.refuseFriendshipRequest(initiatorId, friendId);
        UserFriendRecord recordByIdsAfterRefusing = friendsRepository.getRecordByIds(initiatorId, friendId);

        assertFalse(recordByIdsAfterRefusing.isConfirmed());
    }

    @Test
    public void test_refuseFriendshipRequest_when_friendship_already_confirmed_is_success() {
        int initiatorId = 1;
        int friendId = 2;
        friendsRepository.save(userFriendRecord);
        UserFriendRecord recordByIdsBeforeConfirming = friendsRepository.getRecordByIds(initiatorId, friendId);
        assertFalse(recordByIdsBeforeConfirming.isConfirmed());

        friendsRepository.refuseFriendshipRequest(initiatorId, friendId);
        friendsRepository.refuseFriendshipRequest(initiatorId, friendId);
        UserFriendRecord recordByIdsAfterConfirming = friendsRepository.getRecordByIds(initiatorId, friendId);

        assertFalse(recordByIdsAfterConfirming.isConfirmed());
    }

    @Test
    public void test_refuseFriendshipRequest_when_record_not_exist_is_success() {
        friendsRepository.refuseFriendshipRequest(2, 15);
    }


    @Test
    public void test_deleteAllUserFriends_when_user_has_no_one_friend_is_success() {
        friendsRepository.deleteAllUserFriends(1);

        List<UserFriendRecord> records = friendsRepository.getAllUserFriendshipRecords(1);

        assertEquals(records.size(), 0);
    }

    @Test
    public void test_deleteAllUserFriends_when_user_has_one_friend_is_success() {
        friendsRepository.save(userFriendRecord);
        friendsRepository.deleteAllUserFriends(1);

        List<UserFriendRecord> records = friendsRepository.getAllUserFriendshipRecords(1);

        assertEquals(records.size(), 0);
    }

    @Test
    public void test_deleteAllUserFriends_when_table_has_another_records_is_success() {
        friendsRepository.save(userFriendRecord);
        friendsRepository.save(new UserFriendRecord(now, now, 2, 3, false));
        friendsRepository.save(new UserFriendRecord(now, now, 3, 4, false));
        friendsRepository.deleteAllUserFriends(1);

        List<UserFriendRecord> records = friendsRepository.getAllUserFriendshipRecords(1);

        assertEquals(records.size(), 0);
    }

    @Test
    public void test_deleteAllUserFriends_when_user_has_more_than_one_friend_is_success() {
        friendsRepository.save(new UserFriendRecord(now, now, 1, 2, false));
        friendsRepository.save(new UserFriendRecord(now, now, 1, 3, false));
        friendsRepository.save(new UserFriendRecord(now, now, 3, 4, false));
        friendsRepository.save(new UserFriendRecord(now, now, 4, 1, false));
        friendsRepository.deleteAllUserFriends(1);

        List<UserFriendRecord> records = friendsRepository.getAllUserFriendshipRecords(1);

        assertEquals(records.size(), 0);
    }

    @Test
    public void test_deleteAllUserFriends_when_user_is_not_initiator_is_success() {
        friendsRepository.save(new UserFriendRecord(now, now, 1, 2, false));
        friendsRepository.save(new UserFriendRecord(now, now, 3, 1, false));
        friendsRepository.save(new UserFriendRecord(now, now, 4, 1, false));
        friendsRepository.save(new UserFriendRecord(now, now, 5, 1, false));
        friendsRepository.deleteAllUserFriends(1);

        List<UserFriendRecord> records = friendsRepository.getAllUserFriendshipRecords(1);

        assertEquals(records.size(), 0);
    }


    @Test
    public void test_deleteRecord_when_record_is_not_exist_is_success() {
        friendsRepository.deleteRecord(1, 2);

        assertEquals(friendsRepository.count(), 0);
    }

    @Test
    public void test_deleteRecord_when_user_has_one_friend_is_success() {
        friendsRepository.save(userFriendRecord);
        friendsRepository.deleteRecord(1, 2);

        assertEquals(friendsRepository.count(), 0);
    }

    @Test
    public void test_deleteRecord_when_table_has_another_records_is_success() {
        friendsRepository.save(new UserFriendRecord(now, now, 1, 2, false));
        friendsRepository.save(new UserFriendRecord(now, now, 2, 3, false));
        friendsRepository.save(new UserFriendRecord(now, now, 3, 4, false));
        friendsRepository.deleteRecord(1, 2);

        assertNotNull(friendsRepository.getRecordByIds(2, 3));
        assertNotNull(friendsRepository.getRecordByIds(3, 4));
        assertEquals(friendsRepository.count(), 2);
    }

    @Test
    public void test_deleteRecord_when_user_has_more_than_one_friend_is_success() {
        friendsRepository.save(new UserFriendRecord(now, now, 1, 2, false));
        friendsRepository.save(new UserFriendRecord(now, now, 1, 3, false));
        friendsRepository.save(new UserFriendRecord(now, now, 3, 4, false));
        friendsRepository.save(new UserFriendRecord(now, now, 4, 1, false));
        friendsRepository.deleteRecord(1, 3);

        assertNotNull(friendsRepository.getRecordByIds(1, 2));
        assertNotNull(friendsRepository.getRecordByIds(3, 4));
        assertNotNull(friendsRepository.getRecordByIds(4, 1));
        assertEquals(friendsRepository.count(), 3);
    }

    @Test
    public void test_deleteRecord_when_user_is_not_initiator_is_success() {
        friendsRepository.save(new UserFriendRecord(now, now, 1, 2, false));
        friendsRepository.save(new UserFriendRecord(now, now, 3, 1, false));
        friendsRepository.save(new UserFriendRecord(now, now, 4, 1, false));
        friendsRepository.save(new UserFriendRecord(now, now, 5, 1, false));
        friendsRepository.deleteRecord(1, 3);

        assertNotNull(friendsRepository.getRecordByIds(1, 2));
        assertNotNull(friendsRepository.getRecordByIds(4, 1));
        assertNotNull(friendsRepository.getRecordByIds(5, 1));
        assertEquals(friendsRepository.count(), 3);
    }


    @Test
    public void test_getAllRecords_when_table_is_empty_is_success() {
        List<UserFriendRecord> allRecords = (List<UserFriendRecord>) friendsRepository.findAll();

        assertNotNull(allRecords);
        assertEquals(allRecords.size(), 0);
    }

    @Test
    public void test_getAllRecords_when_exist_one_record_is_success() {
        friendsRepository.save(userFriendRecord);

        List<UserFriendRecord> allRecords = (List<UserFriendRecord>) friendsRepository.findAll();

        assertNotNull(allRecords);
        assertEquals(allRecords.size(), 1);
    }

    @Test
    public void test_getAllRecords_when_exist_more_than_one_record_is_success() {
        friendsRepository.save(new UserFriendRecord(now, now, 1, 4, false));
        friendsRepository.save(new UserFriendRecord(now, now, 2, 1, false));
        friendsRepository.save(new UserFriendRecord(now, now, 1, 3, false));

        assertEquals(friendsRepository.count(), 3);
    }

    @Test
    public void test_getAllRecords_to_ensure_that_integrity_is_maintained_is_success() {
        friendsRepository.save(new UserFriendRecord(now, now, 1, 4, false));
        friendsRepository.save(new UserFriendRecord(now, now, 2, 1, false));
        friendsRepository.save(new UserFriendRecord(now, now, 1, 3, false));


        List<UserFriendRecord> allRecords = (List<UserFriendRecord>) friendsRepository.findAll();

        assertNotNull(allRecords);
        assertEquals(allRecords.size(), 3);


        assertEquals(allRecords.get(0).getInitiatorId(), 1);
        assertEquals(allRecords.get(0).getFriendId(), 4);

        assertEquals(allRecords.get(1).getInitiatorId(), 2);
        assertEquals(allRecords.get(1).getFriendId(), 1);

        assertEquals(allRecords.get(2).getInitiatorId(), 1);
        assertEquals(allRecords.get(2).getFriendId(), 3);
    }


    @Test
    public void test_clearTable_when_table_is_empty_is_success() {
        friendsRepository.deleteAll();

        assertEquals(friendsRepository.count(), 0);
    }

    @Test
    public void test_clearTable_when_table_has_one_record_is_success() {
        friendsRepository.save(userFriendRecord);
        friendsRepository.deleteAll();

        assertEquals(friendsRepository.count(), 0);
    }

    @Test
    public void test_clearTable_when_table_has_more_than_one_record_is_success() {
        friendsRepository.save(new UserFriendRecord(now, now, 1, 2, false));
        friendsRepository.save(new UserFriendRecord(now, now, 2, 3, false));
        friendsRepository.save(new UserFriendRecord(now, now, 3, 4, false));
        friendsRepository.deleteAll();

        assertEquals(friendsRepository.count(), 0);
    }


    @Test
    public void test_getUserConfirmedFriendship_when_table_is_empty_is_success() {
        friendsRepository.getUserFriendshipByConfirming(1, true);
    }

    @Test
    public void test_getUserConfirmedFriendship_when_user_has_one_not_confirmed_friend_is_success() {
        friendsRepository.save(userFriendRecord);

        List<UserFriendRecord> confirmedList = friendsRepository.getUserFriendshipByConfirming(1, true);

        assertNotNull(confirmedList);
        assertEquals(confirmedList.size(), 0);
    }

    @Test
    public void test_getUserConfirmedFriendship_when_user_has_one_confirmed_friend_is_success() {
        friendsRepository.save(userFriendRecord);
        friendsRepository.confirmFriendship(1, 2, now);

        List<UserFriendRecord> confirmedList = friendsRepository.getUserFriendshipByConfirming(1, true);

        assertNotNull(confirmedList);
        assertEquals(confirmedList.size(), 1);
        assertEquals(confirmedList.get(0).getInitiatorId(), 1);
        assertEquals(confirmedList.get(0).getFriendId(), 2);
    }

    @Test
    public void test_getUserConfirmedFriendship_when_user_has_more_than_one_confirmed_friend_is_success() {
        friendsRepository.save(new UserFriendRecord(now, now, 1, 2, true));
        friendsRepository.save(new UserFriendRecord(now, now, 1, 3, true));
        friendsRepository.save(new UserFriendRecord(now, now, 1, 4, true));

        List<UserFriendRecord> confirmedList = friendsRepository.getUserFriendshipByConfirming(1, true);

        assertNotNull(confirmedList);
        assertEquals(confirmedList.size(), 3);

        assertEquals(confirmedList.get(0).getInitiatorId(), 1);
        assertEquals(confirmedList.get(0).getFriendId(), 2);

        assertEquals(confirmedList.get(1).getInitiatorId(), 1);
        assertEquals(confirmedList.get(1).getFriendId(), 3);

        assertEquals(confirmedList.get(2).getInitiatorId(), 1);
        assertEquals(confirmedList.get(2).getFriendId(), 4);
    }


    @Test
    public void test_getUserNotConfirmedFriendship_when_table_is_empty_is_success() {
        friendsRepository.getUserFriendshipByConfirming(1, false);
    }

    @Test
    public void test_getUserNotConfirmedFriendship_when_user_has_one_confirmed_friend_is_success() {
        friendsRepository.save(userFriendRecord);
        friendsRepository.confirmFriendship(1, 2, now);

        List<UserFriendRecord> listOfNotConfirmed = friendsRepository.getUserFriendshipByConfirming(1, false);

        assertNotNull(listOfNotConfirmed);
        assertEquals(listOfNotConfirmed.size(), 0);
    }

    @Test
    public void test_getUserNotConfirmedFriendship_when_user_has_one_not_confirmed_friend_is_success() {
        userFriendRecord.setIsConfirmed(false);
        friendsRepository.save(userFriendRecord);

        List<UserFriendRecord> listOfNotConfirmed = friendsRepository.getUserFriendshipByConfirming(userFriendRecord.getInitiatorId(), false);

        assertNotNull(listOfNotConfirmed);
        assertEquals(listOfNotConfirmed.size(), 1);
        assertEquals(listOfNotConfirmed.get(0).getInitiatorId(), userFriendRecord.getInitiatorId());
        assertEquals(listOfNotConfirmed.get(0).getFriendId(), userFriendRecord.getFriendId());
    }

    @Test
    public void test_getUserNotConfirmedFriendship_when_user_has_more_than_one_not_confirmed_friend_is_success() {
        friendsRepository.save(new UserFriendRecord(now, now, 2, 1, false));
        friendsRepository.save(new UserFriendRecord(now, now, 3, 1, false));
        friendsRepository.save(new UserFriendRecord(now, now, 4, 1, false));

        List<UserFriendRecord> listOfNotConfirmed = friendsRepository.getUserFriendshipByConfirming(1, false);

        assertNotNull(listOfNotConfirmed);
        assertEquals(listOfNotConfirmed.size(), 3);

        assertEquals(listOfNotConfirmed.get(0).getInitiatorId(), 2);
        assertEquals(listOfNotConfirmed.get(0).getFriendId(), 1);

        assertEquals(listOfNotConfirmed.get(1).getInitiatorId(), 3);
        assertEquals(listOfNotConfirmed.get(1).getFriendId(), 1);

        assertEquals(listOfNotConfirmed.get(2).getInitiatorId(), 4);
        assertEquals(listOfNotConfirmed.get(2).getFriendId(), 1);
    }


    @Test
    public void test_getUsersToWhomTheFriendshipRequestWasSent_when_table_is_empty_is_success() {
        friendsRepository.getUsersToWhomTheFriendshipRequestWasSent(1);
    }

    @Test
    public void test_getUsersToWhomTheFriendshipRequestWasSent_when_user_has_one_confirmed_friend_is_success() {
        friendsRepository.save(userFriendRecord);
        friendsRepository.confirmFriendship(1, 2, now);

        List<UserFriendRecord> listOfNotConfirmed = friendsRepository.getUsersToWhomTheFriendshipRequestWasSent(1);

        assertNotNull(listOfNotConfirmed);
        assertEquals(listOfNotConfirmed.size(), 0);
    }

    @Test
    public void test_getUsersToWhomTheFriendshipRequestWasSent_when_user_has_one_not_confirmed_friend_is_success() {
        friendsRepository.save(userFriendRecord);

        List<UserFriendRecord> listOfNotConfirmed = friendsRepository.getUsersToWhomTheFriendshipRequestWasSent(1);

        assertNotNull(listOfNotConfirmed);
        assertEquals(listOfNotConfirmed.size(), 1);
        assertEquals(listOfNotConfirmed.get(0).getInitiatorId(), 1);
        assertEquals(listOfNotConfirmed.get(0).getFriendId(), 2);
    }

    @Test
    public void test_getUsersToWhomTheFriendshipRequestWasSent_when_user_has_more_than_one_not_confirmed_friend_is_success() {
        friendsRepository.save(new UserFriendRecord(now, now, 1, 2, false));
        friendsRepository.save(new UserFriendRecord(now, now, 1, 3, false));
        friendsRepository.save(new UserFriendRecord(now, now, 1, 4, false));

        List<UserFriendRecord> listOfNotConfirmed = friendsRepository.getUsersToWhomTheFriendshipRequestWasSent(1);

        assertNotNull(listOfNotConfirmed);
        assertEquals(listOfNotConfirmed.size(), 3);

        assertEquals(listOfNotConfirmed.get(0).getInitiatorId(), 1);
        assertEquals(listOfNotConfirmed.get(0).getFriendId(), 2);

        assertEquals(listOfNotConfirmed.get(1).getInitiatorId(), 1);
        assertEquals(listOfNotConfirmed.get(1).getFriendId(), 3);

        assertEquals(listOfNotConfirmed.get(2).getInitiatorId(), 1);
        assertEquals(listOfNotConfirmed.get(2).getFriendId(), 4);
    }


    @Test
    public void test_getAllUserFriendshipRecords_when_table_is_empty_is_success() {
        friendsRepository.getAllUserFriendshipRecords(1);
    }

    @Test
    public void test_getAllUserFriendshipRecords_when_user_has_one_not_confirmed_friend_is_success() {
        friendsRepository.save(userFriendRecord);

        List<UserFriendRecord> recordList = friendsRepository.getAllUserFriendshipRecords(1);

        assertNotNull(recordList);
        assertEquals(recordList.size(), 1);
    }

    @Test
    public void test_getAllUserFriendshipRecords_when_user_has_one_confirmed_friend_is_success() {
        friendsRepository.save(userFriendRecord);
        friendsRepository.confirmFriendship(1, 2, now);

        List<UserFriendRecord> recordList = friendsRepository.getAllUserFriendshipRecords(1);

        assertNotNull(recordList);
        assertEquals(recordList.size(), 1);
        assertEquals(recordList.get(0).getInitiatorId(), 1);
        assertEquals(recordList.get(0).getFriendId(), 2);
    }

    @Test
    public void test_getAllUserFriendshipRecords_when_user_has_more_than_one_confirmed_friend_is_success() {
        friendsRepository.save(new UserFriendRecord(now, now, 1, 2, true));
        friendsRepository.save(new UserFriendRecord(now, now, 1, 3, true));
        friendsRepository.save(new UserFriendRecord(now, now, 1, 4, true));

        List<UserFriendRecord> confirmedList = friendsRepository.getAllUserFriendshipRecords(1);

        assertNotNull(confirmedList);
        assertEquals(confirmedList.size(), 3);

        assertEquals(confirmedList.get(0).getInitiatorId(), 1);
        assertEquals(confirmedList.get(0).getFriendId(), 2);

        assertEquals(confirmedList.get(1).getInitiatorId(), 1);
        assertEquals(confirmedList.get(1).getFriendId(), 3);

        assertEquals(confirmedList.get(2).getInitiatorId(), 1);
        assertEquals(confirmedList.get(2).getFriendId(), 4);
    }

    @Test
    public void test_getAllUserFriendshipRecords_when_user_has_more_than_one_not_confirmed_friend_is_success() {
        friendsRepository.save(new UserFriendRecord(now, now, 1, 2, false));
        friendsRepository.save(new UserFriendRecord(now, now, 1, 3, false));
        friendsRepository.save(new UserFriendRecord(now, now, 1, 4, false));

        List<UserFriendRecord> listOfNotConfirmedRecords = friendsRepository.getAllUserFriendshipRecords(1);

        assertNotNull(listOfNotConfirmedRecords);
        assertEquals(listOfNotConfirmedRecords.size(), 3);

        assertEquals(listOfNotConfirmedRecords.get(0).getInitiatorId(), 1);
        assertEquals(listOfNotConfirmedRecords.get(0).getFriendId(), 2);

        assertEquals(listOfNotConfirmedRecords.get(1).getInitiatorId(), 1);
        assertEquals(listOfNotConfirmedRecords.get(1).getFriendId(), 3);

        assertEquals(listOfNotConfirmedRecords.get(2).getInitiatorId(), 1);
        assertEquals(listOfNotConfirmedRecords.get(2).getFriendId(), 4);
    }
}