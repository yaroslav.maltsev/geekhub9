package org.geekhub.socialnetwork.dao.model;

import org.geekhub.socialnetwork.config.DatabaseConfig;
import org.geekhub.socialnetwork.dao.model.history.HistoryDao;
import org.geekhub.socialnetwork.persistance.comment.CommentRecord;
import org.geekhub.socialnetwork.persistance.history.HistoryRecord;
import org.geekhub.socialnetwork.persistance.history.RecordType;
import org.geekhub.socialnetwork.persistance.like.LikeRecord;
import org.geekhub.socialnetwork.persistance.post.PostRecord;
import org.geekhub.socialnetwork.persistance.user_friend.UserFriendRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;


@ContextConfiguration(classes = {DatabaseConfig.class})
@JdbcTest
public class HistoryDaoTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private HistoryDao historyDao;
    private PostRecord postRecord;
    private LikeRecord likeRecord;
    private CommentRecord commentRecord;
    private UserFriendRecord friendRecord;

    @BeforeMethod
    public void setUp() {
        historyDao.deleteAll();
        LocalDateTime now = LocalDateTime.now();
        int id = 1;
        int postId = 1;
        int authorId = 1;

        commentRecord = new CommentRecord();
        commentRecord.setId(id);
        commentRecord.setPostId(postId);
        commentRecord.setAuthorId(authorId);
        commentRecord.setDate(now);
        commentRecord.setText("comment");

        likeRecord = new LikeRecord();
        likeRecord.setId(id);
        likeRecord.setPostId(postId);
        likeRecord.setDate(now);
        likeRecord.setAuthorId(authorId);

        postRecord = new PostRecord();
        postRecord.setId(postId);
        postRecord.setDate(now);
        postRecord.setDescription("description");
        postRecord.setAuthorId(authorId);

        friendRecord = new UserFriendRecord();
        friendRecord.setId(id);
        friendRecord.setDate(now);
        friendRecord.setAuthorId(authorId);
    }

    @Test
    public void test_addRecordToHistory_when_history_is_empty_is_success() {
        HistoryRecord entity = new HistoryRecord(commentRecord);
        HistoryRecord savedRecord = historyDao.save(entity);

        assertEquals(historyDao.count(), 1);
        assertEquals(savedRecord.getUserId(), commentRecord.getAuthorId());
    }

    @Test
    public void test_multiply_addRecordToHistory_is_success() {
        HistoryRecord record0 = historyDao.save(new HistoryRecord(commentRecord));
        HistoryRecord record1 = historyDao.save(new HistoryRecord(likeRecord));
        HistoryRecord record2 = historyDao.save(new HistoryRecord(postRecord));
        HistoryRecord record3 = historyDao.save(new HistoryRecord(friendRecord));

        assertEquals(historyDao.count(), 4);

        assertEquals(record0.getRecordId(), commentRecord.getId());
        assertEquals(record1.getRecordId(), likeRecord.getId());
        assertEquals(record2.getRecordId(), postRecord.getId());
        assertEquals(record3.getRecordId(), friendRecord.getId());
    }


    @Test
    public void test_getAllHistoryRecordsOfType_when_history_is_empty_is_success() {
        List<HistoryRecord> allRecords = historyDao.getAllRecordsOfType(RecordType.COMMENT.name());

        assertNotNull(allRecords);
        assertEquals(allRecords.size(), 0);
    }

    @Test
    public void test_getAllHistoryRecordsOfType_when_history_has_one_record_is_success() {
        historyDao.save(new HistoryRecord(commentRecord));

        List<HistoryRecord> allComments = historyDao.getAllRecordsOfType(RecordType.COMMENT.name());

        assertNotNull(allComments);
        assertEquals(allComments.size(), 1);
        assertEquals(commentRecord.getId(), allComments.get(0).getRecordId());
    }

    @Test
    public void test_getAllHistoryRecordsOfType_when_history_has_another_records_is_success() {
        historyDao.save(new HistoryRecord(commentRecord));
        historyDao.save(new HistoryRecord(commentRecord));
        historyDao.save(new HistoryRecord(postRecord));
        historyDao.save(new HistoryRecord(likeRecord));
        historyDao.save(new HistoryRecord(likeRecord));
        historyDao.save(new HistoryRecord(friendRecord));
        historyDao.save(new HistoryRecord(friendRecord));

        assertEquals(historyDao.count(), 7);


        List<HistoryRecord> allComments = historyDao.getAllRecordsOfType(RecordType.COMMENT.name());
        assertNotNull(allComments);
        assertEquals(allComments.size(), 2);

        List<HistoryRecord> allLikes = historyDao.getAllRecordsOfType(RecordType.LIKE.name());
        assertNotNull(allLikes);
        assertEquals(allLikes.size(), 2);

        List<HistoryRecord> allPosts = historyDao.getAllRecordsOfType(RecordType.POST.name());
        assertNotNull(allPosts);
        assertEquals(allPosts.size(), 1);

        List<HistoryRecord> allFriends = historyDao.getAllRecordsOfType(RecordType.FRIEND.name());
        assertNotNull(allFriends);
        assertEquals(allFriends.size(), 2);


        assertEquals(commentRecord.getId(), allComments.get(0).getRecordId());
        assertEquals(commentRecord.getId(), allComments.get(1).getRecordId());

        assertEquals(likeRecord.getId(), allLikes.get(0).getRecordId());
        assertEquals(likeRecord.getId(), allLikes.get(1).getRecordId());

        assertEquals(postRecord.getId(), allPosts.get(0).getRecordId());

        assertEquals(friendRecord.getId(), allFriends.get(0).getRecordId());
        assertEquals(friendRecord.getId(), allFriends.get(1).getRecordId());
    }


    @Test
    public void test_getAllUserHistoryRecordsOfType_when_history_is_empty_is_success() {
        List<HistoryRecord> allRecords = historyDao.getAllUserRecordsOfType(1, RecordType.COMMENT.name());

        assertNotNull(allRecords);
        assertEquals(allRecords.size(), 0);
    }

    @Test
    public void test_getAllUserHistoryRecordsOfType_when_history_has_one_record_is_success() {
        historyDao.save(new HistoryRecord(commentRecord));

        List<HistoryRecord> allRecords = historyDao.getAllUserRecordsOfType(1, RecordType.COMMENT.name());

        assertNotNull(allRecords);
        assertEquals(allRecords.size(), 1);
        assertEquals(commentRecord.getId(), allRecords.get(0).getRecordId());
    }

    @Test
    public void test_getAllUserHistoryRecordsOfType_when_history_has_different_records_is_success() {
        historyDao.save(new HistoryRecord(commentRecord));
        historyDao.save(new HistoryRecord(commentRecord));
        historyDao.save(new HistoryRecord(postRecord));
        historyDao.save(new HistoryRecord(likeRecord));
        historyDao.save(new HistoryRecord(likeRecord));
        historyDao.save(new HistoryRecord(friendRecord));
        historyDao.save(new HistoryRecord(friendRecord));

        assertEquals(historyDao.count(), 7);

        List<HistoryRecord> allComments = historyDao.getAllUserRecordsOfType(1, RecordType.COMMENT.name());
        assertNotNull(allComments);
        assertEquals(allComments.size(), 2);

        List<HistoryRecord> allLikes = historyDao.getAllUserRecordsOfType(1, RecordType.LIKE.name());
        assertNotNull(allLikes);
        assertEquals(allLikes.size(), 2);

        List<HistoryRecord> allPosts = historyDao.getAllUserRecordsOfType(1, RecordType.POST.name());
        assertNotNull(allPosts);
        assertEquals(allPosts.size(), 1);

        List<HistoryRecord> allFriends = historyDao.getAllUserRecordsOfType(1, RecordType.FRIEND.name());
        assertNotNull(allFriends);
        assertEquals(allFriends.size(), 2);


        assertEquals(commentRecord.getId(), allComments.get(0).getRecordId());
        assertEquals(commentRecord.getId(), allComments.get(1).getRecordId());

        assertEquals(likeRecord.getId(), allLikes.get(0).getRecordId());
        assertEquals(likeRecord.getId(), allLikes.get(1).getRecordId());

        assertEquals(postRecord.getId(), allPosts.get(0).getRecordId());

        assertEquals(friendRecord.getId(), allFriends.get(0).getRecordId());
        assertEquals(friendRecord.getId(), allFriends.get(1).getRecordId());
    }

    @Test
    public void test_getAllUserHistoryRecordsOfType_when_history_has_another_users_is_success() {

        historyDao.save(new HistoryRecord(commentRecord));

        CommentRecord anotherCommentRecord = new CommentRecord();
        anotherCommentRecord.setId(2);
        anotherCommentRecord.setText("text2");
        anotherCommentRecord.setPostId(11);
        anotherCommentRecord.setDate(LocalDateTime.now());

        anotherCommentRecord.setAuthorId(2);
        historyDao.save(new HistoryRecord(anotherCommentRecord));

        anotherCommentRecord.setAuthorId(3);
        historyDao.save(new HistoryRecord(anotherCommentRecord));

        anotherCommentRecord.setAuthorId(4);
        historyDao.save(new HistoryRecord(anotherCommentRecord));

        List<HistoryRecord> allComments = historyDao.getAllUserRecordsOfType(1, RecordType.COMMENT.name());

        assertNotNull(allComments);
        assertEquals(allComments.size(), 1);

        assertEquals(commentRecord.getId(), allComments.get(0).getRecordId());
        assertEquals(commentRecord.getAuthorId(), allComments.get(0).getUserId());
    }


    @Test
    public void test_clearTable_when_table_is_empty_is_success() {
        historyDao.deleteAll();
    }

    @Test
    public void test_clearTable_when_exist_only_one_record_is_success() {
        historyDao.save(new HistoryRecord(commentRecord));

        historyDao.deleteAll();

        assertEquals(historyDao.count(), 0);
    }

    @Test
    public void test_clearTable_when_exist_more_than_one_record_is_success() {
        historyDao.save(new HistoryRecord(commentRecord));
        historyDao.save(new HistoryRecord(commentRecord));
        historyDao.save(new HistoryRecord(likeRecord));
        historyDao.save(new HistoryRecord(likeRecord));
        historyDao.save(new HistoryRecord(postRecord));
        historyDao.save(new HistoryRecord(postRecord));

        historyDao.deleteAll();

        assertEquals(historyDao.count(), 0);
    }


    @Test
    public void clearTable_table_is_empty_is_success() {
        historyDao.clearUserHistory(1);
    }

    @Test
    public void test_clearUserHistory_when_exist_only_one_record_is_success() {
        historyDao.save(new HistoryRecord(commentRecord));

        historyDao.clearUserHistory(1);

        assertEquals(historyDao.count(), 0);
    }

    @Test
    public void test_clearUserHistory_when_exist_more_than_one_record_is_success() {
        historyDao.save(new HistoryRecord(commentRecord));
        historyDao.save(new HistoryRecord(commentRecord));
        historyDao.save(new HistoryRecord(likeRecord));
        historyDao.save(new HistoryRecord(likeRecord));
        historyDao.save(new HistoryRecord(postRecord));
        historyDao.save(new HistoryRecord(postRecord));

        historyDao.clearUserHistory(1);

        assertEquals(historyDao.count(), 0);
    }


    @Test
    public void test_clearUserHistory_when_exist_more_than_one_user_record_is_success() {
        historyDao.save(new HistoryRecord(commentRecord));
        commentRecord.setAuthorId(2);
        historyDao.save(new HistoryRecord(commentRecord));

        historyDao.save(new HistoryRecord(likeRecord));
        likeRecord.setAuthorId(3);
        historyDao.save(new HistoryRecord(likeRecord));

        historyDao.save(new HistoryRecord(postRecord));
        postRecord.setAuthorId(4);
        historyDao.save(new HistoryRecord(postRecord));

        historyDao.clearUserHistory(1);

        assertEquals(historyDao.count(), 3);
    }


    @Test
    public void test_getAllHistoryRecords_when_history_is_empty_is_success() {
        List<HistoryRecord> allComments = (List<HistoryRecord>) historyDao.findAll();

        assertNotNull(allComments);
        assertEquals(allComments.size(), 0);
    }

    @Test
    public void test_getAllHistoryRecords_when_history_has_one_record_is_success() {
        historyDao.save(new HistoryRecord(commentRecord));

        List<HistoryRecord> allComments = (List<HistoryRecord>) historyDao.findAll();

        assertNotNull(allComments);
        assertEquals(allComments.size(), 1);
        assertEquals(commentRecord.getId(), allComments.get(0).getRecordId());
    }

    @Test
    public void test_getAllHistoryRecords_when_history_has_another_records_is_success() {
        historyDao.save(new HistoryRecord(commentRecord));
        historyDao.save(new HistoryRecord(commentRecord));
        historyDao.save(new HistoryRecord(postRecord));
        historyDao.save(new HistoryRecord(postRecord));
        historyDao.save(new HistoryRecord(likeRecord));
        historyDao.save(new HistoryRecord(likeRecord));

        List<HistoryRecord> allComments = (List<HistoryRecord>) historyDao.findAll();

        assertNotNull(allComments);
        assertEquals(allComments.size(), 6);

        assertEquals(commentRecord.getId(), allComments.get(0).getRecordId());
        assertEquals(commentRecord.getId(), allComments.get(1).getRecordId());

        assertEquals(postRecord.getId(), allComments.get(2).getRecordId());
        assertEquals(postRecord.getId(), allComments.get(3).getRecordId());

        assertEquals(likeRecord.getId(), allComments.get(4).getRecordId());
        assertEquals(likeRecord.getId(), allComments.get(5).getRecordId());
    }

    @Test
    public void test_getAllHistoryRecords_when_history_has_different_users_is_success() {

        historyDao.save(new HistoryRecord(commentRecord));

        CommentRecord anotherCommentRecord = new CommentRecord();
        anotherCommentRecord.setId(2);
        anotherCommentRecord.setText("text2");
        anotherCommentRecord.setPostId(11);
        anotherCommentRecord.setDate(LocalDateTime.now());

        anotherCommentRecord.setAuthorId(2);
        historyDao.save(new HistoryRecord(anotherCommentRecord));

        anotherCommentRecord.setAuthorId(3);
        historyDao.save(new HistoryRecord(anotherCommentRecord));

        anotherCommentRecord.setAuthorId(4);
        historyDao.save(new HistoryRecord(anotherCommentRecord));

        List<HistoryRecord> allComments = (List<HistoryRecord>) historyDao.findAll();

        assertNotNull(allComments);
        assertEquals(allComments.size(), 4);

        assertEquals(commentRecord.getId(), allComments.get(0).getRecordId());
        assertEquals(commentRecord.getAuthorId(), allComments.get(0).getUserId());
    }


    @Test
    public void test_getAllHistoryRecordsOfUser_when_history_is_empty_is_success() {
        List<HistoryRecord> allRecords = historyDao.getAllHistoryRecordsOfUser(1);

        assertNotNull(allRecords);
        assertEquals(allRecords.size(), 0);
    }

    @Test
    public void test_getAllHistoryRecordsOfUser_when_history_has_one_record_is_success() {
        historyDao.save(new HistoryRecord(commentRecord));

        List<HistoryRecord> allRecords = historyDao.getAllHistoryRecordsOfUser(commentRecord.getAuthorId());

        assertNotNull(allRecords);
        assertEquals(allRecords.size(), 1);
        assertEquals(commentRecord.getId(), allRecords.get(0).getRecordId());
    }

    @Test
    public void test_getAllHistoryRecordsOfUser_when_history_has_another_records_is_success() {
        historyDao.save(new HistoryRecord(commentRecord));
        historyDao.save(new HistoryRecord(commentRecord));
        historyDao.save(new HistoryRecord(postRecord));
        historyDao.save(new HistoryRecord(postRecord));
        historyDao.save(new HistoryRecord(likeRecord));
        historyDao.save(new HistoryRecord(likeRecord));

        List<HistoryRecord> allRecords = historyDao.getAllHistoryRecordsOfUser(commentRecord.getAuthorId());

        assertNotNull(allRecords);
        assertEquals(allRecords.size(), 6);

        assertEquals(commentRecord.getId(), allRecords.get(0).getRecordId());
        assertEquals(commentRecord.getId(), allRecords.get(1).getRecordId());

        assertEquals(postRecord.getId(), allRecords.get(2).getRecordId());
        assertEquals(postRecord.getId(), allRecords.get(3).getRecordId());

        assertEquals(likeRecord.getId(), allRecords.get(4).getRecordId());
        assertEquals(likeRecord.getId(), allRecords.get(5).getRecordId());
    }

    @Test
    public void test_getAllHistoryRecordsOfUser_when_history_has_different_users_is_success() {

        historyDao.save(new HistoryRecord(commentRecord));

        CommentRecord anotherCommentRecord = new CommentRecord();
        anotherCommentRecord.setId(2);
        anotherCommentRecord.setText("text2");
        anotherCommentRecord.setPostId(11);
        anotherCommentRecord.setDate(LocalDateTime.now());

        anotherCommentRecord.setAuthorId(2);
        historyDao.save(new HistoryRecord(anotherCommentRecord));

        anotherCommentRecord.setAuthorId(3);
        historyDao.save(new HistoryRecord(anotherCommentRecord));

        anotherCommentRecord.setAuthorId(4);
        historyDao.save(new HistoryRecord(anotherCommentRecord));

        List<HistoryRecord> allRecords = historyDao.getAllHistoryRecordsOfUser(commentRecord.getAuthorId());

        assertNotNull(allRecords);
        assertEquals(allRecords.size(), 1);

        assertEquals(commentRecord.getId(), allRecords.get(0).getRecordId());
        assertEquals(commentRecord.getAuthorId(), allRecords.get(0).getUserId());
    }


    @Test
    public void test_getHistoryRecordsOfTypeByTime_when_history_is_empty_is_success() {
        LocalDateTime now = LocalDateTime.now();
        List<HistoryRecord> allRecords = historyDao.getHistoryRecordsOfTypeByTime(
                RecordType.LIKE.name(),
                now.minusDays(2),
                now
        );

        assertNotNull(allRecords);
        assertEquals(allRecords.size(), 0);
    }

    @Test
    public void test_getHistoryRecordsOfTypeByTime_when_history_has_one_record_is_success() {
        historyDao.save(new HistoryRecord(commentRecord));

        List<HistoryRecord> allRecords = historyDao.getHistoryRecordsOfTypeByTime(
                RecordType.COMMENT.name(),
                commentRecord.getDate(),
                commentRecord.getDate().plusDays(3)
        );

        assertNotNull(allRecords);
        assertEquals(allRecords.size(), 1);
        assertEquals(commentRecord.getId(), allRecords.get(0).getRecordId());
    }

    @Test
    public void test_getHistoryRecordsOfTypeByTime_when_history_has_another_records_is_success() {
        historyDao.save(new HistoryRecord(commentRecord));
        historyDao.save(new HistoryRecord(commentRecord));
        historyDao.save(new HistoryRecord(postRecord));
        historyDao.save(new HistoryRecord(postRecord));
        historyDao.save(new HistoryRecord(likeRecord));
        historyDao.save(new HistoryRecord(likeRecord));

        List<HistoryRecord> comments = historyDao.getHistoryRecordsOfTypeByTime(
                RecordType.COMMENT.name(),
                commentRecord.getDate(),
                commentRecord.getDate().plusDays(3)
        );

        List<HistoryRecord> likes = historyDao.getHistoryRecordsOfTypeByTime(
                RecordType.LIKE.name(),
                likeRecord.getDate(),
                likeRecord.getDate().plusDays(3)
        );

        List<HistoryRecord> posts = historyDao.getHistoryRecordsOfTypeByTime(
                RecordType.POST.name(),
                postRecord.getDate(),
                postRecord.getDate().plusDays(3)
        );

        assertNotNull(comments);
        assertEquals(comments.size(), 2);
        assertNotNull(likes);
        assertEquals(likes.size(), 2);
        assertNotNull(comments);
        assertEquals(posts.size(), 2);
    }


    @Test
    public void test_getUserHistoryRecordsOfTypeByTime_when_history_is_empty_is_success() {
        LocalDateTime now = LocalDateTime.now();
        List<HistoryRecord> allRecords = historyDao.getUserHistoryRecordsOfTypeByTime(
                1,
                RecordType.LIKE.name(),
                now.minusDays(2),
                now
        );

        assertNotNull(allRecords);
        assertEquals(allRecords.size(), 0);
    }

    @Test
    public void test_getUserHistoryRecordsOfTypeByTime_when_history_has_one_record_is_success() {
        historyDao.save(new HistoryRecord(commentRecord));

        List<HistoryRecord> allRecords = historyDao.getUserHistoryRecordsOfTypeByTime(
                commentRecord.getAuthorId(),
                RecordType.COMMENT.name(),
                commentRecord.getDate(),
                commentRecord.getDate().plusDays(3)
        );

        assertNotNull(allRecords);
        assertEquals(allRecords.size(), 1);
        assertEquals(commentRecord.getId(), allRecords.get(0).getRecordId());
    }

    @Test
    public void test_getUserHistoryRecordsOfTypeByTime_when_history_has_another_records_is_success() {
        historyDao.save(new HistoryRecord(commentRecord));
        historyDao.save(new HistoryRecord(commentRecord));
        historyDao.save(new HistoryRecord(postRecord));
        historyDao.save(new HistoryRecord(postRecord));
        historyDao.save(new HistoryRecord(likeRecord));
        historyDao.save(new HistoryRecord(likeRecord));

        List<HistoryRecord> comments = historyDao.getUserHistoryRecordsOfTypeByTime(
                commentRecord.getAuthorId(),
                RecordType.COMMENT.name(),
                commentRecord.getDate(),
                commentRecord.getDate().plusDays(3)
        );

        List<HistoryRecord> likes = historyDao.getUserHistoryRecordsOfTypeByTime(
                likeRecord.getAuthorId(),
                RecordType.LIKE.name(),
                likeRecord.getDate(),
                likeRecord.getDate().plusDays(3)
        );

        List<HistoryRecord> posts = historyDao.getUserHistoryRecordsOfTypeByTime(
                postRecord.getAuthorId(),
                RecordType.POST.name(),
                postRecord.getDate(),
                postRecord.getDate().plusDays(3)
        );

        assertNotNull(comments);
        assertEquals(comments.size(), 2);
        assertNotNull(likes);
        assertEquals(likes.size(), 2);
        assertNotNull(comments);
        assertEquals(posts.size(), 2);
    }
}
