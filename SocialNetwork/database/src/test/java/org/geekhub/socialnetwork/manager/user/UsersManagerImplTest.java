package org.geekhub.socialnetwork.manager.user;

import org.geekhub.socialnetwork.RecordNotExistException;
import org.geekhub.socialnetwork.dao.model.CommentsRepository;
import org.geekhub.socialnetwork.dao.model.LikesRepository;
import org.geekhub.socialnetwork.dao.model.UsersRepository;
import org.geekhub.socialnetwork.manager.friends.FriendsManager;
import org.geekhub.socialnetwork.manager.post.PostManager;
import org.geekhub.socialnetwork.persistance.user.Gender;
import org.geekhub.socialnetwork.persistance.user.UserRecord;
import org.geekhub.socialnetwork.persistance.user_friend.UserFriendRecord;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

public class UsersManagerImplTest {
    private UsersRepository usersRepository;
    private FriendsManager friendsManager;
    private PostManager postManager;
    private CommentsRepository commentsRepository;
    private LikesRepository likesRepository;
    private UsersManagerImpl usersManager;

    @BeforeMethod
    public void setUp() {
        usersRepository = mock(UsersRepository.class);
        friendsManager = mock(FriendsManager.class);
        postManager = mock(PostManager.class);
        commentsRepository = mock(CommentsRepository.class);
        likesRepository = mock(LikesRepository.class);
        usersManager = new UsersManagerImpl(
                usersRepository,
                friendsManager,
                commentsRepository,
                likesRepository,
                postManager
        );
    }


    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_in_updateUser_argument_is_null() {
        usersManager.updateUser(null);
    }

    @Test
    public void test_updateUser_is_success() {
        UserRecord userRecord = new UserRecord();

        usersManager.updateUser(userRecord);

        verify(usersRepository, times(1)).save(userRecord);
    }


    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_in_registerUser_argument_is_null() {
        usersManager.addUser(null);
    }

    @Test
    public void test_registerUser_is_success() {
        int id = 2;
        UserRecord userRecord = new UserRecord();
        userRecord.setId(id);

        when(usersRepository.save(userRecord)).thenReturn(userRecord);

        usersManager.addUser(userRecord);

        verify(usersRepository, times(1)).save(userRecord);
    }


    @Test
    public void test_getAllUserRecords_is_success() {
        List<UserRecord> allUsers = usersManager.getAllUsers();
        assertNotNull(allUsers);
        verify(usersRepository, times(1)).findAll();
    }


    @Test
    public void test_deleteUser_is_success() {
        int id = 1;
        when(usersRepository.findById(id)).thenReturn(Optional.of(new UserRecord(
                LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                "e",
                "p",
                "ROLE_USER",
                "username",
                Gender.MALE
        )));
        usersManager.deleteUser(id);
        verify(usersRepository, times(1)).deleteById(id);
        verify(postManager, times(1)).deleteAllUserPosts(id);
        verify(commentsRepository, times(1)).deleteAllUserComments(id);
        verify(likesRepository, times(1)).deleteByAuthorId(id);
        verify(friendsManager, times(1)).deleteAllUserFriends(id);
    }

    @Test
    public void test_getUserById_is_success() {
        int id = 1;
        when(usersRepository.findById(id)).thenReturn(Optional.of(new UserRecord()));
        usersManager.getUserById(id);
        verify(usersRepository, times(1)).findById(id);
    }

    @Test(expectedExceptions = RecordNotExistException.class)
    public void should_throwException_in_getUserById_when_record_not_exist() {
        int id = 2;
        when(usersRepository.findById(id)).thenReturn(Optional.empty());
        usersManager.getUserById(id);
    }


    @Test(expectedExceptions = RecordNotExistException.class)
    public void should_throwException_in_getUserByEmail_when_record_not_exist() {
        String email = "email";
        when(usersRepository.getUserByEmail(email)).thenReturn(Optional.empty());
        usersManager.getUserByEmail(email);
    }

    @Test
    public void test_getUserByEmail_is_success() {
        String email = "email";
        when(usersRepository.getUserByEmail(email)).thenReturn(Optional.of(new UserRecord()));
        usersManager.getUserByEmail(email);
        verify(usersRepository, times(1)).getUserByEmail(email);
    }


    @Test
    public void test_getConfirmedUserFriends_is_success() {
        int userId = 1;
        usersManager.getConfirmedUserFriends(userId);
        verify(friendsManager, times(1)).getUserConfirmedFriendship(userId);
    }

    @Test
    public void test_getConfirmedUserFriends_when_list_is_not_empty_is_success() {
        int userId = 1;
        List<UserFriendRecord> list = new ArrayList<>();

        UserFriendRecord firstRecord = new UserFriendRecord();
        firstRecord.setIsConfirmed(true);
        firstRecord.setInitiatorId(userId);
        firstRecord.setFriendId(2);
        list.add(firstRecord);

        UserFriendRecord secondRecord = new UserFriendRecord();
        secondRecord.setIsConfirmed(true);
        secondRecord.setInitiatorId(userId);
        secondRecord.setFriendId(3);
        list.add(secondRecord);

        UserFriendRecord thirdRecord = new UserFriendRecord();
        thirdRecord.setIsConfirmed(true);
        thirdRecord.setInitiatorId(userId);
        thirdRecord.setFriendId(4);
        list.add(thirdRecord);

        UserFriendRecord fourthRecord = new UserFriendRecord();
        fourthRecord.setIsConfirmed(true);
        fourthRecord.setInitiatorId(5);
        fourthRecord.setFriendId(userId);
        list.add(fourthRecord);

        when(friendsManager.getUserConfirmedFriendship(1)).thenReturn(list);

        UserRecord firstUser = new UserRecord();
        firstUser.setId(2);
        when(usersRepository.findById(2)).thenReturn(Optional.of(firstUser));

        UserRecord secondUser = new UserRecord();
        secondUser.setId(3);
        when(usersRepository.findById(3)).thenReturn(Optional.of(secondUser));

        UserRecord thirdUser = new UserRecord();
        thirdUser.setId(4);
        when(usersRepository.findById(4)).thenReturn(Optional.of(thirdUser));

        UserRecord fourthUser = new UserRecord();
        fourthUser.setId(5);
        when(usersRepository.findById(5)).thenReturn(Optional.of(fourthUser));

        List<UserRecord> confirmedUserFriends = usersManager.getConfirmedUserFriends(userId);

        verify(friendsManager, times(1)).getUserConfirmedFriendship(userId);

        assertEquals(confirmedUserFriends.size(), 4);
        assertTrue(areRecordsEquals(userId, confirmedUserFriends.get(0).getId(), list.get(0)));
        assertTrue(areRecordsEquals(userId, confirmedUserFriends.get(1).getId(), list.get(1)));
        assertTrue(areRecordsEquals(userId, confirmedUserFriends.get(2).getId(), list.get(2)));
        assertTrue(areRecordsEquals(userId, confirmedUserFriends.get(3).getId(), list.get(3)));
    }


    @Test
    public void test_getNotConfirmedUserFriends_is_success() {
        int userId = 1;
        usersManager.getNotConfirmedUserFriends(userId);
        verify(friendsManager, times(1)).getUserNotConfirmedFriendship(userId);
    }

    @Test
    public void test_getNotConfirmedUserFriends_when_list_is_not_empty_is_success() {
        int userId = 1;
        List<UserFriendRecord> list = new ArrayList<>();

        UserFriendRecord firstRecord = new UserFriendRecord();
        firstRecord.setIsConfirmed(false);
        firstRecord.setInitiatorId(userId);
        firstRecord.setFriendId(2);
        list.add(firstRecord);

        UserFriendRecord secondRecord = new UserFriendRecord();
        secondRecord.setIsConfirmed(false);
        secondRecord.setInitiatorId(userId);
        secondRecord.setFriendId(3);
        list.add(secondRecord);

        UserFriendRecord thirdRecord = new UserFriendRecord();
        thirdRecord.setIsConfirmed(false);
        thirdRecord.setInitiatorId(userId);
        thirdRecord.setFriendId(4);
        list.add(thirdRecord);

        UserFriendRecord fourthRecord = new UserFriendRecord();
        fourthRecord.setIsConfirmed(false);
        fourthRecord.setInitiatorId(5);
        fourthRecord.setFriendId(userId);
        list.add(fourthRecord);

        when(friendsManager.getUserNotConfirmedFriendship(1)).thenReturn(list);

        UserRecord firstUser = new UserRecord();
        firstUser.setId(2);
        when(usersRepository.findById(2)).thenReturn(Optional.of(firstUser));

        UserRecord secondUser = new UserRecord();
        secondUser.setId(3);
        when(usersRepository.findById(3)).thenReturn(Optional.of(secondUser));

        UserRecord thirdUser = new UserRecord();
        thirdUser.setId(4);
        when(usersRepository.findById(4)).thenReturn(Optional.of(thirdUser));

        UserRecord fourthUser = new UserRecord();
        fourthUser.setId(5);
        when(usersRepository.findById(5)).thenReturn(Optional.of(fourthUser));

        List<UserRecord> confirmedUserFriends = usersManager.getNotConfirmedUserFriends(userId);

        verify(friendsManager, times(1)).getUserNotConfirmedFriendship(userId);

        assertEquals(confirmedUserFriends.size(), 4);
        assertTrue(areRecordsEquals(userId, confirmedUserFriends.get(0).getId(), list.get(0)));
        assertTrue(areRecordsEquals(userId, confirmedUserFriends.get(1).getId(), list.get(1)));
        assertTrue(areRecordsEquals(userId, confirmedUserFriends.get(2).getId(), list.get(2)));
        assertTrue(areRecordsEquals(userId, confirmedUserFriends.get(3).getId(), list.get(3)));
    }


    @Test
    public void test_getFriendshipRequestForUser_is_success() {
        int userId = 1;
        usersManager.getFriendshipRequestForUser(userId);
        verify(friendsManager, times(1)).getUsersToWhomTheFriendshipRequestWasSent(userId);
    }

    @Test
    public void test_getFriendshipRequestForUser_when_list_is_not_empty_is_success() {
        int userId = 1;
        List<UserFriendRecord> list = new ArrayList<>();

        UserFriendRecord firstRecord = new UserFriendRecord();
        firstRecord.setIsConfirmed(false);
        firstRecord.setInitiatorId(userId);
        firstRecord.setFriendId(2);
        list.add(firstRecord);

        UserFriendRecord secondRecord = new UserFriendRecord();
        secondRecord.setIsConfirmed(false);
        secondRecord.setInitiatorId(userId);
        secondRecord.setFriendId(3);
        list.add(secondRecord);

        UserFriendRecord thirdRecord = new UserFriendRecord();
        thirdRecord.setIsConfirmed(false);
        thirdRecord.setInitiatorId(userId);
        thirdRecord.setFriendId(4);
        list.add(thirdRecord);

        UserFriendRecord fourthRecord = new UserFriendRecord();
        fourthRecord.setIsConfirmed(false);
        fourthRecord.setInitiatorId(5);
        fourthRecord.setFriendId(userId);
        list.add(fourthRecord);

        when(friendsManager.getUsersToWhomTheFriendshipRequestWasSent(1)).thenReturn(list);

        UserRecord firstUser = new UserRecord();
        firstUser.setId(2);
        when(usersRepository.findById(2)).thenReturn(Optional.of(firstUser));

        UserRecord secondUser = new UserRecord();
        secondUser.setId(3);
        when(usersRepository.findById(3)).thenReturn(Optional.of(secondUser));

        UserRecord thirdUser = new UserRecord();
        thirdUser.setId(4);
        when(usersRepository.findById(4)).thenReturn(Optional.of(thirdUser));

        UserRecord fourthUser = new UserRecord();
        fourthUser.setId(5);
        when(usersRepository.findById(5)).thenReturn(Optional.of(fourthUser));

        List<UserRecord> confirmedUserFriends = usersManager.getFriendshipRequestForUser(userId);

        verify(friendsManager, times(1)).getUsersToWhomTheFriendshipRequestWasSent(userId);

        assertEquals(confirmedUserFriends.size(), 4);
        assertTrue(areRecordsEquals(userId, confirmedUserFriends.get(0).getId(), list.get(0)));
        assertTrue(areRecordsEquals(userId, confirmedUserFriends.get(1).getId(), list.get(1)));
        assertTrue(areRecordsEquals(userId, confirmedUserFriends.get(2).getId(), list.get(2)));
        assertTrue(areRecordsEquals(userId, confirmedUserFriends.get(3).getId(), list.get(3)));
    }


    private boolean areRecordsEquals(int userId, int friendId, UserFriendRecord userFriendRecord) {
        int recordFriendId = userFriendRecord.getFriendId();
        int initiatorId = userFriendRecord.getInitiatorId();

        return recordFriendId == friendId && initiatorId == userId ||
                recordFriendId == userId && initiatorId == friendId;
    }
}
