package org.geekhub.socialnetwork.dao.model;

import org.geekhub.socialnetwork.config.DatabaseConfig;
import org.geekhub.socialnetwork.persistance.post.PostRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.testng.Assert.*;

@ContextConfiguration(classes = {DatabaseConfig.class})
@JdbcTest
public class PostsRepositoryTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private PostsRepository postsRepository;
    private PostRecord postRecord;

    @BeforeMethod
    public void setUp() {
        postRecord = new PostRecord(
                LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                "description",
                1,
                "username"
        );
        postsRepository.deleteAll();
    }

    @Test
    public void test_addPost_when_table_is_empty_is_success() {
        postsRepository.save(postRecord);

        assertEquals(postsRepository.count(), 1);
    }

    @Test
    public void test_multiply_addPost_is_success() {
        postsRepository.save(postRecord);
        postsRepository.save(new PostRecord(postRecord));
        postsRepository.save(new PostRecord(postRecord));
        postsRepository.save(new PostRecord(postRecord));

        List<PostRecord> allPostRecords = (List<PostRecord>) postsRepository.findAll();
        assertEquals(allPostRecords.size(), 4);
        assertTrue(areRecordsEquals(allPostRecords.get(0), postRecord));
        assertTrue(areRecordsEquals(allPostRecords.get(1), postRecord));
        assertTrue(areRecordsEquals(allPostRecords.get(2), postRecord));
        assertTrue(areRecordsEquals(allPostRecords.get(3), postRecord));
    }

    
    @Test
    public void test_getPostById_when_exist_only_one_record_is_success() {
        int postId = postsRepository.save(postRecord).getId();

        PostRecord postById = postsRepository.findById(postId).get();

        assertNotNull(postById);
        assertTrue(areRecordsEquals(postById, postRecord));
    }

    @Test
    public void test_getPostById_table_is_not_empty_is_success() {
        int postId = postsRepository.save(postRecord).getId();
        postsRepository.save(new PostRecord(postRecord));
        postsRepository.save(new PostRecord(postRecord));
        postsRepository.save(new PostRecord(postRecord));

        PostRecord postById = postsRepository.findById(postId).get();

        assertNotNull(postById);
        assertTrue(areRecordsEquals(postById, postRecord));
    }
    

    @Test
    public void test_getAllPostRecords_when_exist_only_one_record_is_success() {
        postsRepository.save(postRecord);

        List<PostRecord> allPostRecords = (List<PostRecord>) postsRepository.findAll();

        assertNotNull(allPostRecords);
        assertEquals(allPostRecords.size(), 1);
        assertTrue(areRecordsEquals(allPostRecords.get(0), postRecord));
    }

    @Test
    public void test_getAllPostRecords_table_is_not_empty_is_success() {
        postsRepository.save(postRecord);
        postsRepository.save(new PostRecord(postRecord));
        postsRepository.save(new PostRecord(postRecord));
        postsRepository.save(new PostRecord(postRecord));
        postsRepository.save(new PostRecord(postRecord));

        List<PostRecord> allPostRecords = (List<PostRecord>) postsRepository.findAll();

        assertNotNull(allPostRecords);
        assertEquals(allPostRecords.size(), 5);
        assertTrue(areRecordsEquals(allPostRecords.get(0), postRecord));
        assertTrue(areRecordsEquals(allPostRecords.get(1), postRecord));
        assertTrue(areRecordsEquals(allPostRecords.get(2), postRecord));
        assertTrue(areRecordsEquals(allPostRecords.get(3), postRecord));
        assertTrue(areRecordsEquals(allPostRecords.get(4), postRecord));
    }


    @Test
    public void test_getPostsOfUserFriendsByDate_is_success() {
        List<Integer> friendsIds = List.of(1, 2, 3, 4, 5);
        String desc = "desc";
        String authorUsername = postRecord.getAuthorUsername();
        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES);
        int termInDays = 3;

        postsRepository.save(new PostRecord(now, desc, friendsIds.get(0), authorUsername));
        postsRepository.save(new PostRecord(now, desc, friendsIds.get(1), authorUsername));
        postsRepository.save(new PostRecord(now, desc, friendsIds.get(2), authorUsername));
        postsRepository.save(new PostRecord(now, desc, friendsIds.get(3), authorUsername));
        postsRepository.save(new PostRecord(now, desc, friendsIds.get(4), authorUsername));
        postsRepository.save(new PostRecord(now, desc, 6, authorUsername));
        postsRepository.save(new PostRecord(now, desc, 7, authorUsername));
        postsRepository.save(new PostRecord(now, desc, 8, authorUsername));

        postsRepository.save(new PostRecord(now.minusDays(1), desc, friendsIds.get(0), authorUsername));
        postsRepository.save(new PostRecord(now.minusDays(2), desc, friendsIds.get(1), authorUsername));
        postsRepository.save(new PostRecord(now.minusDays(3), desc, friendsIds.get(2), authorUsername));
        postsRepository.save(new PostRecord(now.minusDays(4), desc, friendsIds.get(3), authorUsername));
        postsRepository.save(new PostRecord(now.minusDays(5), desc, friendsIds.get(4), authorUsername));
        postsRepository.save(new PostRecord(now.minusDays(5), desc, 6, authorUsername));
        postsRepository.save(new PostRecord(now.minusDays(5), desc, 9, authorUsername));
        postsRepository.save(new PostRecord(now.minusDays(5), desc, 10, authorUsername));

        List<PostRecord> posts = postsRepository.getPostsOfUserFriendsByDate(friendsIds, now, termInDays);

        assertEquals(posts.size(), 7);


    }


    @Test
    public void test_clearTable_when_exist_only_one_record_is_success() {
        postsRepository.save(postRecord);

        assertEquals(postsRepository.count(), 1);

        postsRepository.deleteAll();

        assertEquals(postsRepository.count(), 0);
    }

    @Test
    public void clearTable_table_is_not_empty_is_success() {
        postsRepository.save(postRecord);
        postsRepository.save(new PostRecord(postRecord));
        postsRepository.save(new PostRecord(postRecord));
        postsRepository.save(new PostRecord(postRecord));
        postsRepository.save(new PostRecord(postRecord));

        assertEquals(postsRepository.count(), 5);

        postsRepository.deleteAll();

        assertEquals(postsRepository.count(), 0);
    }
    

    @Test
    public void test_editDescription_when_editedText_and_old_are_equals_is_success() {
        int id = postsRepository.save(postRecord).getId();

        String editedText = postRecord.getDescription();
        PostRecord editedRecord = new PostRecord(postRecord);
        editedRecord.setId(id);
        editedRecord.setDescription(editedText);
        
        editedRecord = postsRepository.save(editedRecord);

        assertEquals(postRecord.getDescription(), editedText);
        assertEquals(postRecord.getAuthorId(), editedRecord.getAuthorId());
        assertEquals(postRecord.getDate().toLocalDate(), editedRecord.getDate().toLocalDate());
        assertEquals(postRecord.getId(), editedRecord.getId());
    }

    @Test
    public void test_editDescription_when_post_exist_is_success() {
        int id = postsRepository.save(postRecord).getId();

        String editedText = "new desc";
        PostRecord editedRecord = new PostRecord(postRecord);
        editedRecord.setId(id);
        postRecord.setDescription(editedText);
        editedRecord = postsRepository.save(editedRecord);

        assertEquals(postRecord.getDescription(), editedText);
        assertEquals(postRecord.getAuthorId(), editedRecord.getAuthorId());
        assertEquals(postRecord.getDate().toLocalDate(), editedRecord.getDate().toLocalDate());
        assertEquals(postRecord.getId(), editedRecord.getId());
    }


    @Test
    public void test_deletePost_when_post_not_exist_is_success() {
        postsRepository.deleteById(1);
    }

    @Test
    public void test_deletePost_is_success() {
        int id = postsRepository.save(postRecord).getId();

        postsRepository.deleteById(id);

        assertEquals(postsRepository.count(), 0);
    }

    @Test
    public void test_deletePost_when_table_is_not_empty_is_success() {
        int firstId = postsRepository.save(postRecord).getId();
        postsRepository.save(new PostRecord(postRecord));
        postsRepository.save(new PostRecord(postRecord));
        postsRepository.save(new PostRecord(postRecord));

        postsRepository.deleteById(firstId);

        assertEquals(postsRepository.count(), 3);
    }


    @Test
    public void test_deleteAllUserPosts_when_user_not_exist_is_success() {
        postsRepository.deleteAllUserPosts(1);
    }

    @Test
    public void test_deleteAllUserPosts_is_success() {
        postsRepository.save(postRecord);

        postsRepository.deleteAllUserPosts(postRecord.getAuthorId());

        assertEquals(postsRepository.count(), 0);
    }

    @Test
    public void test_deleteAllUserPosts_when_table_is_not_empty_is_success() {
        postsRepository.save(postRecord);
        postsRepository.save(new PostRecord(postRecord));
        postsRepository.save(new PostRecord(postRecord));
        postsRepository.save(new PostRecord(postRecord));

        postsRepository.deleteAllUserPosts(postRecord.getAuthorId());

        assertEquals(postsRepository.count(), 0);
    }


    @Test
    public void test_getAllUserPosts_when_table_is_empty_is_success() {
        List<PostRecord> allUserPosts = postsRepository.getAllUserPosts(postRecord.getAuthorId());

        assertNotNull(allUserPosts);
        assertEquals(allUserPosts.size(), 0);
    }

    @Test
    public void test_getAllUserPosts_when_table_has_records_but_another_user_is_success() {
        postsRepository.save(postRecord);
        postsRepository.save(postRecord);
        postsRepository.save(postRecord);

        List<PostRecord> allUserPosts = postsRepository.getAllUserPosts(postRecord.getAuthorId() + 2);

        assertNotNull(allUserPosts);
        assertEquals(allUserPosts.size(), 0);
    }

    @Test
    public void test_getAllUserPosts_when_exist_only_one_record_is_success() {
        postsRepository.save(postRecord);

        List<PostRecord> allUserPosts = postsRepository.getAllUserPosts(postRecord.getAuthorId());

        assertNotNull(allUserPosts);
        assertEquals(allUserPosts.size(), 1);
        assertTrue(areRecordsEquals(allUserPosts.get(0), postRecord));
    }

    @Test
    public void test_getAllUserPosts_when_exist_more_than_one_record_is_success() {
        postsRepository.save(postRecord);
        postsRepository.save(new PostRecord(postRecord));
        postsRepository.save(new PostRecord(postRecord));
        postsRepository.save(new PostRecord(postRecord));

        List<PostRecord> allUserPosts = postsRepository.getAllUserPosts(postRecord.getAuthorId());

        assertNotNull(allUserPosts);
        assertEquals(allUserPosts.size(), 4);
        assertTrue(areRecordsEquals(allUserPosts.get(0), postRecord));
        assertTrue(areRecordsEquals(allUserPosts.get(1), postRecord));
        assertTrue(areRecordsEquals(allUserPosts.get(2), postRecord));
        assertTrue(areRecordsEquals(allUserPosts.get(3), postRecord));
    }

    private boolean areRecordsEquals(PostRecord first, PostRecord second) {
        return first.getAuthorId() == second.getAuthorId() &&  first.getDescription().equals(second.getDescription());
    }
}
