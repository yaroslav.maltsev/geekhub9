package org.geekhub.socialnetwork.manager.friends;

import org.geekhub.socialnetwork.RecordNotExistException;
import org.geekhub.socialnetwork.dao.relational.FriendsRepository;
import org.geekhub.socialnetwork.persistance.user_friend.UserFriendRecord;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.testng.Assert.assertEquals;

public class FriendsManagerTest {
    private FriendsRepository friendsRepository;
    private FriendsManager friendsManager;

    @BeforeMethod
    public void setUp() {
        friendsRepository = mock(FriendsRepository.class);
        friendsManager = new FriendsManager(friendsRepository);
    }

    @Test
    public void test_getRecordByIds_is_success() {
        int userId = 1;
        int friendId = 2;

        friendsManager.getRecordByIds(userId, friendId);

        verify(friendsRepository, times(1)).getRecordByIds(userId, friendId);
    }

    @Test
    public void test_addUserFriendRequest_when_record_already_exist_is_success() {
        int userId = 1;
        int friendId = 2;

        friendsManager.addUserFriendRequest(userId, friendId);
    }

    @Test
    public void test_addUserFriendRequest_is_success() {
        int userId = 1;
        int friendId = 2;

        when(friendsRepository.getRecordByIds(userId, friendId)).thenThrow(new RecordNotExistException("", new Exception()));

        friendsManager.addUserFriendRequest(userId, friendId);

        verify(friendsRepository, times(1)).save(any());
    }

    @Test
    public void test_confirmFriendship_is_success() {
        int userId = 1;
        int friendId = 2;

        friendsManager.confirmFriendship(userId, friendId);

        verify(friendsRepository, times(1)).confirmFriendship(anyInt(), anyInt(), any());
    }

    @Test
    public void test_deleteRecord_is_success() {
        int userId = 1;
        int friendId = 2;

        friendsManager.deleteRecord(userId, friendId);

        verify(friendsRepository, times(1)).deleteRecord(userId, friendId);
    }


    @Test
    public void test_deleteAllUserFriends_is_success() {
        int userId = 1;

        friendsManager.deleteAllUserFriends(userId);

        verify(friendsRepository, times(1)).deleteAllUserFriends(userId);
    }

    @Test
    public void test_getUserConfirmedFriendship_is_success() {
        int userId = 1;

        ArrayList<UserFriendRecord> list = new ArrayList<>();
        when(friendsRepository.getUserFriendshipByConfirming(userId, false)).thenReturn(list);

        List<UserFriendRecord> value = friendsManager.getUserConfirmedFriendship(userId);

        verify(friendsRepository, times(1)).getUserFriendshipByConfirming(userId, true);

        assertEquals(value, list);
    }

    @Test
    public void test_getUserNotConfirmedFriendship_is_success() {
        int userId = 1;

        ArrayList<UserFriendRecord> list = new ArrayList<>();
        when(friendsRepository.getUserFriendshipByConfirming(userId, true)).thenReturn(list);

        List<UserFriendRecord> value = friendsManager.getUserNotConfirmedFriendship(userId);

        verify(friendsRepository, times(1)).getUserFriendshipByConfirming(userId, false);

        assertEquals(value, list);
    }

    @Test
    public void test_getUsersToWhomTheFriendshipRequestWasSent_is_success() {
        int userId = 1;

        ArrayList<UserFriendRecord> list = new ArrayList<>();
        when(friendsRepository.getUsersToWhomTheFriendshipRequestWasSent(userId)).thenReturn(list);

        List<UserFriendRecord> value = friendsManager.getUsersToWhomTheFriendshipRequestWasSent(userId);

        verify(friendsRepository, times(1)).getUsersToWhomTheFriendshipRequestWasSent(userId);

        assertEquals(value, list);
    }
}