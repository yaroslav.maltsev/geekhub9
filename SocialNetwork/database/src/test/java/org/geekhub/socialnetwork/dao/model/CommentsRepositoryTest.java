package org.geekhub.socialnetwork.dao.model;

import org.geekhub.socialnetwork.config.DatabaseConfig;
import org.geekhub.socialnetwork.persistance.comment.CommentRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.testng.Assert.*;

@ContextConfiguration(classes = {DatabaseConfig.class})
@JdbcTest
public class CommentsRepositoryTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private CommentsRepository commentsRepository;
    private CommentRecord commentRecord;

    @BeforeMethod
    public void setUp() {
        commentRecord = new CommentRecord(
                1,
                1,
                LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                "text",
                "username"
        );
        commentsRepository.deleteAll();
    }


    @Test
    public void test_multiply_addComment_is_success() {
        commentsRepository.save(commentRecord);
        commentsRepository.save(new CommentRecord(commentRecord));
        commentsRepository.save(new CommentRecord(commentRecord));
        commentsRepository.save(new CommentRecord(commentRecord));

        List<CommentRecord> allRecords = (List<CommentRecord>) commentsRepository.findAll();

        assertEquals(allRecords.size(), 4);
        assertTrue(areCommentsEquals(allRecords.get(0), commentRecord));
        assertTrue(areCommentsEquals(allRecords.get(1), commentRecord));
        assertTrue(areCommentsEquals(allRecords.get(2), commentRecord));
        assertTrue(areCommentsEquals(allRecords.get(3), commentRecord));
    }

    @Test
    public void test_addComment_when_table_is_empty_is_success() {
        commentsRepository.save(commentRecord);

        assertEquals(commentsRepository.count(), 1);
    }


    @Test
    public void test_editComment_is_success() {
        int firstRecordId = commentsRepository.save(commentRecord).getId();
        commentsRepository.save(new CommentRecord(commentRecord));
        commentsRepository.save(new CommentRecord(commentRecord));
        commentsRepository.save(new CommentRecord(commentRecord));


        String editedText = "new text";
        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);

        CommentRecord newRecord = new CommentRecord(
                commentRecord.getPostId(),
                commentRecord.getAuthorId(),
                now,
                editedText,
                commentRecord.getAuthorUsername()
        );
        newRecord.setId(firstRecordId);

        CommentRecord editedRecord = commentsRepository.save(newRecord);

        assertEquals(commentsRepository.count(), 4);

        assertTrue(areCommentsEquals(editedRecord, newRecord));
        assertFalse(areCommentsEquals(editedRecord, commentRecord));
        assertEquals(editedRecord.getDate(), now.truncatedTo(ChronoUnit.SECONDS));
        assertEquals(editedRecord.getText(), editedText);
    }


    @Test
    public void test_getAllPostComments_when_table_is_empty_is_success() {
        commentsRepository.getAllPostComments(1);
    }

    @Test
    public void test_getAllPostComments_when_table_has_one_record_is_success() {
        int postId = commentsRepository.save(commentRecord).getPostId();
        List<CommentRecord> allPostCommentRecords = commentsRepository.getAllPostComments(postId);

        assertNotNull(allPostCommentRecords);
        assertEquals(allPostCommentRecords.size(), 1);
        assertTrue(areCommentsEquals(allPostCommentRecords.get(0), commentRecord));
    }

    @Test
    public void test_getAllPostComments_when_table_has_more_than_one_record_is_success() {
        int postId = commentsRepository.save(commentRecord).getPostId();
        commentsRepository.save(new CommentRecord(commentRecord));
        commentsRepository.save(new CommentRecord(commentRecord));
        commentsRepository.save(new CommentRecord(commentRecord));

        List<CommentRecord> allPostCommentRecords = commentsRepository.getAllPostComments(postId);

        assertNotNull(allPostCommentRecords);
        assertEquals(allPostCommentRecords.size(), 4);

        assertTrue(areCommentsEquals(allPostCommentRecords.get(0), commentRecord));
        assertTrue(areCommentsEquals(allPostCommentRecords.get(1), new CommentRecord(commentRecord)));
        assertTrue(areCommentsEquals(allPostCommentRecords.get(2), new CommentRecord(commentRecord)));
        assertTrue(areCommentsEquals(allPostCommentRecords.get(3), new CommentRecord(commentRecord)));
    }


    @Test
    public void test_clearTable_when_table_is_empty_is_success() {
        commentsRepository.deleteAll();
    }

    @Test
    public void test_clearTable_when_table_has_only_one_record_is_success() {
        commentsRepository.save(commentRecord);

        assertEquals(commentsRepository.count(), 1);

        commentsRepository.deleteAll();

        assertEquals(commentsRepository.count(), 0);
    }

    @Test
    public void test_clearTable_when_table_has_more_than_one_record_is_success() {
        commentsRepository.save(commentRecord);
        commentsRepository.save(new CommentRecord(commentRecord));
        commentsRepository.save(new CommentRecord(commentRecord));
        commentsRepository.save(new CommentRecord(commentRecord));
        commentsRepository.save(new CommentRecord(commentRecord));

        assertEquals(commentsRepository.count(), 5);

        commentsRepository.deleteAll();

        assertEquals(commentsRepository.count(), 0);
    }


    @Test
    public void test_getAllRecords_when_table_is_empty_is_success() {
        List<CommentRecord> allRecords = (List<CommentRecord>) commentsRepository.findAll();

        assertNotNull(allRecords);
        assertEquals(allRecords.size(), 0);
    }

    @Test
    public void test_getAllRecords_when_table_has_only_one_record_is_success() {
        commentsRepository.save(commentRecord);

        List<CommentRecord> allRecords = (List<CommentRecord>) commentsRepository.findAll();

        assertNotNull(allRecords);
        assertEquals(allRecords.size(), 1);
        assertTrue(areCommentsEquals(allRecords.get(0), commentRecord));
    }

    @Test
    public void test_getAllRecords_when_table_has_more_than_one_record_is_success() {
        commentsRepository.save(commentRecord);
        commentsRepository.save(new CommentRecord(commentRecord));
        commentsRepository.save(new CommentRecord(commentRecord));
        commentsRepository.save(new CommentRecord(commentRecord));
        commentsRepository.save(new CommentRecord(commentRecord));

        List<CommentRecord> allRecords = (List<CommentRecord>) commentsRepository.findAll();

        assertNotNull(allRecords);
        assertEquals(allRecords.size(), 5);
        assertTrue(areCommentsEquals(allRecords.get(0), commentRecord));
        assertTrue(areCommentsEquals(allRecords.get(1), new CommentRecord(commentRecord)));
        assertTrue(areCommentsEquals(allRecords.get(2), new CommentRecord(commentRecord)));
        assertTrue(areCommentsEquals(allRecords.get(3), new CommentRecord(commentRecord)));
        assertTrue(areCommentsEquals(allRecords.get(4), new CommentRecord(commentRecord)));
    }

    @Test
    public void test_getCommentById_when_table_has_only_one_record_is_success() {
        int id = commentsRepository.save(commentRecord).getId();

        CommentRecord comment = commentsRepository.findById(id).get();

        assertNotNull(comment);
        assertTrue(areCommentsEquals(comment, commentRecord));
    }

    @Test
    public void test_getCommentById_when_table_has_more_than_one_record_is_success() {
        int id = commentsRepository.save(commentRecord).getId();
        commentsRepository.save(new CommentRecord(commentRecord));
        commentsRepository.save(new CommentRecord(commentRecord));
        commentsRepository.save(new CommentRecord(commentRecord));
        commentsRepository.save(new CommentRecord(commentRecord));

        CommentRecord comment = commentsRepository.findById(id).get();

        assertNotNull(comment);
        assertTrue(areCommentsEquals(comment, commentRecord));
    }


    @Test
    public void test_deleteComment_when_post_not_exist_is_success() {
        commentsRepository.deleteById(1);
    }

    @Test
    public void test_deleteComment_is_success() {
        commentsRepository.save(commentRecord);
        int recordId = commentsRepository.getAllPostComments(1).get(0).getId();

        commentsRepository.deleteById(recordId);

        assertEquals(commentsRepository.getAllPostComments(1).size(), 0);
    }

    @Test
    public void test_deleteComment_when_table_is_not_empty_is_success() {
        commentsRepository.save(commentRecord);
        commentsRepository.save(new CommentRecord(commentRecord));
        commentsRepository.save(new CommentRecord(commentRecord));
        commentsRepository.save(new CommentRecord(commentRecord));

        List<CommentRecord> allCommentsBeforeDeleting = (List<CommentRecord>) commentsRepository.findAll();

        commentsRepository.deleteById(allCommentsBeforeDeleting.get(0).getId());

        commentsRepository.findById(allCommentsBeforeDeleting.get(1).getId());
        commentsRepository.findById(allCommentsBeforeDeleting.get(2).getId());
        commentsRepository.findById(allCommentsBeforeDeleting.get(3).getId());

        assertEquals(commentsRepository.getAllPostComments(1).size(), 3);
    }


    @Test
    public void test_deleteAllUserComments_when_user_not_exist_is_success() {
        commentsRepository.deleteAllUserComments(1);
    }

    @Test
    public void test_deleteAllUserComments_is_success() {
        commentsRepository.save(commentRecord);

        commentsRepository.deleteAllUserComments(commentRecord.getAuthorId());

        assertEquals(commentsRepository.getAllPostComments(commentRecord.getPostId()).size(), 0);
    }

    @Test
    public void test_deleteAllUserComments_when_table_is_not_empty_is_success() {
        commentsRepository.save(commentRecord);
        commentsRepository.save(new CommentRecord(commentRecord));
        commentsRepository.save(new CommentRecord(commentRecord));
        commentsRepository.save(new CommentRecord(commentRecord));

        commentsRepository.deleteAllUserComments(commentRecord.getAuthorId());

        assertEquals(commentsRepository.getAllPostComments(1).size(), 0);
    }


    @Test
    public void test_deleteAllPostComments_when_user_not_exist_is_success() {
        commentsRepository.deleteAllPostComments(1);
    }

    @Test
    public void test_deleteAllPostComments_is_success() {
        commentsRepository.save(commentRecord);

        commentsRepository.deleteAllPostComments(commentRecord.getPostId());

        assertEquals(commentsRepository.getAllPostComments(commentRecord.getPostId()).size(), 0);
    }

    @Test
    public void test_deleteAllPostComments_when_table_is_not_empty_is_success() {
        commentsRepository.save(commentRecord);
        commentsRepository.save(new CommentRecord(commentRecord));
        commentsRepository.save(new CommentRecord(commentRecord));
        commentsRepository.save(new CommentRecord(commentRecord));

        int postId = commentRecord.getPostId();
        commentsRepository.deleteAllPostComments(postId);

        assertEquals(commentsRepository.getAllPostComments(postId).size(), 0);
    }


    private boolean areCommentsEquals(CommentRecord first, CommentRecord second) {
        return  first.getAuthorId() == second.getAuthorId() &&
                first.getPostId() == second.getPostId() &&
                first.getText().equals(second.getText());
    }
}
