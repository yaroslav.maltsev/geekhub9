package org.geekhub.socialnetwork.manager.like;

import org.geekhub.socialnetwork.dao.model.LikesRepository;
import org.geekhub.socialnetwork.manager.history.HistoryManager;
import org.geekhub.socialnetwork.persistance.like.LikeRecord;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.testng.Assert.assertEquals;

public class LikeManagerTest {
    private LikesRepository likesRepository;
    private HistoryManager historyManager;
    private LikeManager likeManager;
    
    @BeforeMethod
    public void setUp() {
        likesRepository = mock(LikesRepository.class);
        historyManager = mock(HistoryManager.class);
        likeManager = new LikeManager(likesRepository, historyManager);
    }

    @Test
    public void test_getAllPostLikes_is_success() {
        int postId = 1;
        ArrayList<LikeRecord> likeRecords = new ArrayList<>();
        when(likesRepository.getAllPostLikes(postId)).thenReturn(likeRecords);

        List<LikeRecord> allPostLikes = likeManager.getAllPostLikes(postId);

        assertEquals(allPostLikes, likeRecords);
        verify(likesRepository, times(1)).getAllPostLikes(postId);
    }

    @Test
    public void test_getLikeById_is_success() {
        int postId = 1;
        LikeRecord likeRecord = new LikeRecord();
        when(likesRepository.findById(postId)).thenReturn(Optional.of(likeRecord));

        LikeRecord likeById = likeManager.getLikeById(postId);

        assertEquals(likeById, likeRecord);
        verify(likesRepository, times(1)).findById(postId);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_in_getLikeById_when_record_not_exist() {
        int id = 2;
        when(likesRepository.findById(id)).thenReturn(Optional.empty());
        likeManager.getLikeById(id);
    }


    @Test(expectedExceptions = IllegalArgumentException.class)
    public void _should_throwException_in_addLike_when_argument_is_null() {
        likeManager.addLike(null);
    }

    @Test
    public void test_addLike_is_success() {
        LikeRecord likeRecord = new LikeRecord();

        likeManager.addLike(likeRecord);

        verify(likesRepository, times(1)).save(likeRecord);
        verify(historyManager, times(1)).addToHistory(any());
    }


    @Test
    public void test_deleteLike_is_success() {
        int likeId = 2;

        likeManager.deleteLike(likeId);

        verify(likesRepository, times(1)).deleteById(likeId);
    }
}
