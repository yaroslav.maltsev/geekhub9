package org.geekhub.socialnetwork.manager.activation;

import org.geekhub.socialnetwork.dao.model.UsersRepository;
import org.geekhub.socialnetwork.dao.relational.UnactivatedUsersDao;
import org.geekhub.socialnetwork.persistance.user.UnactivatedUserRecord;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

public class SchedulerTest {
    private Scheduler scheduler;
    private UnactivatedUsersDao unactivatedUsersDao;
    private UsersRepository usersRepository;

    @BeforeMethod
    public void setUp() {
        unactivatedUsersDao = mock(UnactivatedUsersDao.class);
        usersRepository = mock(UsersRepository.class);

        scheduler = new Scheduler(unactivatedUsersDao, usersRepository);
    }

    @Test
    public void test_checkRecordsOnRelevanceByDate_is_success() {
        List<Integer> idList = List.of(1, 2, 3, 4, 5);
        when(unactivatedUsersDao.getIdsOfUsersInactiveAccounts(any(), anyInt())).thenReturn(idList);

        scheduler.checkRecordsOnRelevanceByDate();

        verify(unactivatedUsersDao, times(1)).deleteRecordsWithExpiredTime(any(), anyInt());
        for (int id : idList) {
            verify(usersRepository, times(1)).deleteById(id);
        }
    }
}