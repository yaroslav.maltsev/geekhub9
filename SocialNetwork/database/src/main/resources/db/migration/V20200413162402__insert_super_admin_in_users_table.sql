INSERT into users(registrationDate, email, encodedPassword, role, username, gender, isActivated)
values(
      '2020-03-29',
      'super@gmail.com',
      '$2a$10$U01UMfHK35j13wGascPAn.tI6Vs2wfWTwfRVWVKI7NGjOt.oLkUhG',
      'ROLE_SUPER_ADMIN',
      'admin',
      'MALE',
      'true'
)
