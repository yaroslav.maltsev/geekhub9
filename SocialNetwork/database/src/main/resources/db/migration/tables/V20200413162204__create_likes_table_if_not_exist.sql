create table if not exists public.likes(
   id                serial          primary key,
   postId            int             not null,
   date              timestamp       not null,
   authorId          int             not null,
   authorUsername    varchar(100)    not null
);
