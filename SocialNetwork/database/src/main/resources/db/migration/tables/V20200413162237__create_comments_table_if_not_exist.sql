create table if not exists public.comments (
   id                serial           primary key,
   postId            int              not null,
   authorId          int              not null,
   date              timestamp        not null,
   text              varchar(1000)    not null,
   authorUsername    varchar(100)     not null
);
