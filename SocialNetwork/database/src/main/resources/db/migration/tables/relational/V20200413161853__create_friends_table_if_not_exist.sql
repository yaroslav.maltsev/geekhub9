create table if not exists public.user_friend (
                                                  id                  serial     not null,
                                                  dateOfConfirming    timestamp       not null,
                                                  dateOfRequest       timestamp       not null,
                                                  initiatorId         int        not null,
                                                  friendId            int        not null,
                                                  isConfirmed         boolean    not null
);
