create table if not exists public.users (
    id                  serial           primary key,
    registrationDate    timestamp        not null,
    email               varchar(100)     not null,
    encodedPassword     varchar(1000)    not null,
    role                varchar(100)     not null,
    username                varchar(100)     not null,
    gender              varchar(10)      not null,
    isActivated         boolean          not null
);
