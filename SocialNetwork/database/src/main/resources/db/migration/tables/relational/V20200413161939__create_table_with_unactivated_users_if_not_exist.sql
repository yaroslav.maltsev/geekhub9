create table if not exists public.unactivatedUsers (
   id                serial          not null,
   userId            int             not null,
   activationCode    varchar(100)    not null,
   date              timestamp       not null
);
