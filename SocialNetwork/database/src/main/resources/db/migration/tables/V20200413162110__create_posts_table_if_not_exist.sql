create table if not exists public.posts (
    id             serial           primary key,
    authorId       int              not null,
    authorUsername varchar(100)     not null,
    description    varchar(1000)    not null,
    date           timestamp        not null
);
