create table if not exists public.history (
  id              serial         primary key,
  userId          int            not null,
  recordId        int            not null,
  dateOfAction    timestamp      not null,
  recordType      varchar(15)    not null
);
