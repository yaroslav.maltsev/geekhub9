package org.geekhub.socialnetwork.persistance.post;

import org.geekhub.socialnetwork.persistance.history.HistoryElement;
import org.geekhub.socialnetwork.persistance.history.RecordType;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@Table("posts")
public class PostRecord implements HistoryElement {
    @Id
    private int id;
    private LocalDateTime date;
    private String description;
    @Column("authorId")
    private int authorId;
    @Column("authorUsername")
    private String authorUsername;

    public PostRecord() {
    }

    public PostRecord(LocalDateTime date, String description, int authorId, String authorUsername) {
        this.date = date;
        this.description = description;
        this.authorId = authorId;
        this.authorUsername = authorUsername;
    }

    public PostRecord(PostRecord postRecord) {
        this.date = postRecord.getDate();
        this.description = postRecord.getDescription();
        this.authorId = postRecord.getAuthorId();
        this.authorUsername = postRecord.getAuthorUsername();
    }

    @Override
    public int getId() {
        return id;
    }
    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public LocalDateTime getDate() {
        return date;
    }
    @Override
    public void setDate(LocalDateTime date) {
        this.date = date;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int getAuthorId() {
        return authorId;
    }
    @Override
    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }


    @Override
    public RecordType getRecordType() {
        return RecordType.POST;
    }

    public String getAuthorUsername() {
        return authorUsername;
    }

    public void setAuthorUsername(String authorUsername) {
        this.authorUsername = authorUsername;
    }
}
