package org.geekhub.socialnetwork.manager.activation;

import org.geekhub.socialnetwork.dao.model.UsersRepository;
import org.geekhub.socialnetwork.dao.relational.UnactivatedUsersDao;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class Scheduler {
    private static final int FREQUENCY_IN_MS = 1000*60*60*2;

    @Value("${activation.code.validity.time}")
    private int maxTimeForActivationCodeStoringInMin;
    private final UnactivatedUsersDao unactivatedUsersDao;
    private final UsersRepository usersRepository;

    public Scheduler(
            UnactivatedUsersDao unactivatedUsersDao,
            UsersRepository usersRepository
    ) {
        this.unactivatedUsersDao = unactivatedUsersDao;
        this.usersRepository = usersRepository;
    }

    @Scheduled(fixedRate = FREQUENCY_IN_MS)
    @Transactional
    public void checkRecordsOnRelevanceByDate() {
        deleteInactiveAccounts();

        unactivatedUsersDao.deleteRecordsWithExpiredTime(
                LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES),
                maxTimeForActivationCodeStoringInMin
        );

    }

    private void deleteInactiveAccounts() {
        List<Integer> ids = unactivatedUsersDao.getIdsOfUsersInactiveAccounts(
                LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                maxTimeForActivationCodeStoringInMin
        );
        deleteAccountsByIdList(ids);
    }

    private void deleteAccountsByIdList(List<Integer> idList) {
        for (int id : idList) {
            usersRepository.deleteById(id);
        }
    }
}
