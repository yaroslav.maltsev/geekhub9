package org.geekhub.socialnetwork.dao.model;

import org.geekhub.socialnetwork.persistance.post.PostRecord;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface PostsRepository extends CrudRepository<PostRecord, Integer> {

    @Query("SELECT id, authorId, authorUsername, description, date FROM posts WHERE authorId = :authorId")
    List<PostRecord> getAllUserPosts(@Param("authorId") int authorId);

    @Query("SELECT id, authorId, authorUsername, description, date FROM posts WHERE authorId IN (:friendsIdList) AND DATEDIFF(dd, :now, date) > :term * -1")
    List<PostRecord> getPostsOfUserFriendsByDate(
            @Param("friendsIdList") List<Integer> friendsIdList,
            @Param("now") LocalDateTime now,
            @Param("term") int termInDays
    );

    @Modifying
    @Query("DELETE FROM posts WHERE authorId = :authorId")
    void deleteAllUserPosts(@Param("authorId") int userId);
}
