package org.geekhub.socialnetwork.persistance.history;

import java.time.LocalDateTime;

public interface HistoryElement {
    int getId();

    LocalDateTime getDate();

    RecordType getRecordType();

    int getAuthorId();


    void setId(int id);

    void setDate(LocalDateTime date);

    void setAuthorId(int authorId);
}
