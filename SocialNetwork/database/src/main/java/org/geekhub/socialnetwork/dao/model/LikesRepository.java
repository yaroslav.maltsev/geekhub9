package org.geekhub.socialnetwork.dao.model;

import org.geekhub.socialnetwork.persistance.like.LikeRecord;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface LikesRepository extends CrudRepository<LikeRecord, Integer> {
    @Query("SELECT id, postId, date, authorId, authorUsername FROM likes WHERE postId = :postId")
    List<LikeRecord> getAllPostLikes(@Param("postId") int postId);

    @Modifying
    @Query("DELETE FROM likes WHERE authorId = :authorId")
    void deleteByAuthorId(@Param("authorId") int authorId);

    @Modifying
    @Query("DELETE FROM likes WHERE postId = :postId")
    void deleteAllPostLikes(@Param("postId") int postId);
}
