package org.geekhub.socialnetwork.dao.model;

import org.geekhub.socialnetwork.persistance.user.UserRecord;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UsersRepository extends CrudRepository<UserRecord, Integer> {
    @Query("SELECT id, registrationDate, email, encodedPassword, role, username, gender, isActivated FROM users WHERE email = :email")
    Optional<UserRecord> getUserByEmail(@Param("email") String email);

    @Modifying
    @Query("UPDATE users SET isActivated = true WHERE id = :userId")
    void activateUser(@Param("userId") int userId);
}