package org.geekhub.socialnetwork.manager.like;

import org.geekhub.socialnetwork.dao.model.LikesRepository;
import org.geekhub.socialnetwork.manager.history.HistoryManager;
import org.geekhub.socialnetwork.persistance.like.LikeRecord;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class LikeManager {
    private final LikesRepository likesRepository;
    private final HistoryManager historyManager;

    public LikeManager(LikesRepository likesRepository, HistoryManager historyManager) {
        this.likesRepository = likesRepository;
        this.historyManager = historyManager;
    }

    public List<LikeRecord> getAllPostLikes(int postId) {
        return likesRepository.getAllPostLikes(postId);
    }

    public LikeRecord getLikeById(int likeId) {
        Optional<LikeRecord> likeRecord = likesRepository.findById(likeId);
        if (likeRecord.isEmpty()) {
            throw new IllegalArgumentException("Like with id " + likeId + " not exist!");
        }
        return likeRecord.get();
    }

    @Transactional
    public void addLike(LikeRecord likeRecord) {
        if (Objects.isNull(likeRecord)) {
            throw new IllegalArgumentException("LikeRecord must be not null!");
        }
        LikeRecord savedRecord = likesRepository.save(likeRecord);
        historyManager.addToHistory(savedRecord);
    }

    public void deleteLike(int likeId) {
        likesRepository.deleteById(likeId);
    }

}
