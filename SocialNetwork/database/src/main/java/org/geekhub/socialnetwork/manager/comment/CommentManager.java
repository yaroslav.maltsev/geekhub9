package org.geekhub.socialnetwork.manager.comment;

import org.geekhub.socialnetwork.dao.model.CommentsRepository;
import org.geekhub.socialnetwork.manager.history.HistoryManager;
import org.geekhub.socialnetwork.persistance.comment.CommentRecord;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CommentManager {
    private final CommentsRepository commentsRepository;
    private final HistoryManager historyManager;

    public CommentManager(CommentsRepository commentsRepository, HistoryManager historyManager) {
        this.commentsRepository = commentsRepository;
        this.historyManager = historyManager;
    }

    public List<CommentRecord> getAllPostComments(int postId) {
        return commentsRepository.getAllPostComments(postId);
    }

    public CommentRecord getCommentById(int id) {
        Optional<CommentRecord> optional = commentsRepository.findById(id);
        if (optional.isEmpty()) {
            throw new IllegalArgumentException("Comment with id: " + id + " not exist!");
        }
        return optional.get();
    }

    @Transactional
    public void addComment(CommentRecord commentRecord) {
        if (Objects.isNull(commentRecord)) {
            throw new IllegalArgumentException("CommentRecord must be not null!");
        }
        CommentRecord savedRecord = commentsRepository.save(commentRecord);
        historyManager.addToHistory(savedRecord);
    }

    @Transactional
    public void editComment(CommentRecord editedComment) {
        if (Objects.isNull(editedComment)) {
            throw new IllegalArgumentException("CommentRecord must be not null!");
        }
        editedComment.setDate(LocalDateTime.now());
        commentsRepository.save(editedComment);
    }

    public void deleteComment(int commentId) {
        commentsRepository.deleteById(commentId);
    }
}
