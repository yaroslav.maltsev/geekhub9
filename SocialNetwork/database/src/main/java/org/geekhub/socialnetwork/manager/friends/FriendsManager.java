package org.geekhub.socialnetwork.manager.friends;

import org.geekhub.socialnetwork.dao.relational.FriendsRepository;
import org.geekhub.socialnetwork.persistance.user_friend.UserFriendRecord;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class FriendsManager {
    private final FriendsRepository friendsRepository;

    public FriendsManager(FriendsRepository friendsRepository) {
        this.friendsRepository = friendsRepository;
    }

    public UserFriendRecord getRecordByIds(int userId, int friendId) {
        return friendsRepository.getRecordByIds(userId, friendId);
    }

    public void addUserFriendRequest(int userId, int friendId) {
        friendsRepository.save(
                new UserFriendRecord(
                        LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                        LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                        userId,
                        friendId,
                        false
                )
        );
    }

    public void confirmFriendship(int userId, int friendId) {
        friendsRepository.confirmFriendship(userId, friendId, LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS));
    }

    public void deleteRecord(int userId, int friendId) {
        friendsRepository.deleteRecord(userId, friendId);
    }


    public void deleteAllUserFriends(int userId) {
        friendsRepository.deleteAllUserFriends(userId);
    }

    public List<UserFriendRecord> getUserConfirmedFriendship(int userId) {
        return friendsRepository.getUserFriendshipByConfirming(userId, true);
    }

    public List<UserFriendRecord> getUserNotConfirmedFriendship(int userId) {
        return friendsRepository.getUserFriendshipByConfirming(userId, false);
    }

    public List<UserFriendRecord> getUsersToWhomTheFriendshipRequestWasSent(int userId) {
        return friendsRepository.getUsersToWhomTheFriendshipRequestWasSent(userId);
    }
}
