package org.geekhub.socialnetwork.dao.model.history;

import org.geekhub.socialnetwork.persistance.history.HistoryRecord;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface HistoryDao extends CrudRepository<HistoryRecord, Integer> {

    @Query("SELECT id, dateOfAction, userId, recordId, recordType FROM history WHERE recordType = :recordType")
    List<HistoryRecord> getAllRecordsOfType(@Param("recordType") String type);

    @Query("SELECT id, dateOfAction, userId, recordId, recordType FROM history WHERE recordType = :recordType AND userId = :userId")
    List<HistoryRecord> getAllUserRecordsOfType(
            @Param("userId") int userId,
            @Param("recordType") String type
    );


    @Query("SELECT id, dateOfAction, userId, recordId, recordType FROM history WHERE userId = :userId")
    List<HistoryRecord> getAllHistoryRecordsOfUser (@Param("userId") int userId);

    @Modifying
    @Query("DELETE FROM history WHERE userId = :userId")
    void clearUserHistory(@Param("userId") int userId);

    @Query("SELECT id, dateOfAction, userId, recordId, recordType FROM history " +
            "WHERE recordType = :recordType AND (DATEDIFF(minute, dateOfAction, :start) <= 0 AND DATEDIFF(minute, dateOfAction, :finish) >= 0)")
    List<HistoryRecord> getHistoryRecordsOfTypeByTime(
            @Param("recordType") String type,
            @Param("start") LocalDateTime start,
            @Param("finish") LocalDateTime finish
    );

    @Query("SELECT id, dateOfAction, userId, recordId, recordType FROM history " +
            "WHERE userId = :userId AND " +
            "recordType = :recordType " +
            "AND (DATEDIFF(minute, dateOfAction, :start) <= 0 AND DATEDIFF(minute, dateOfAction, :finish) >= 0)")
    List<HistoryRecord> getUserHistoryRecordsOfTypeByTime(
            @Param("userId") int userId,
            @Param("recordType") String type,
            @Param("start") LocalDateTime start,
            @Param("finish") LocalDateTime finish
    );
}
