package org.geekhub.socialnetwork.persistance.comment;

import org.geekhub.socialnetwork.persistance.history.HistoryElement;
import org.geekhub.socialnetwork.persistance.history.RecordType;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@Table("comments")
public class CommentRecord implements HistoryElement {
    @Id
    private int id;
    @Column("postId")
    private int postId;
    @Column("authorId")
    private int authorId;
    private LocalDateTime date;
    private String text;
    @Column("authorUsername")
    private String authorUsername;

    public CommentRecord() {
    }

    public CommentRecord(int postId, int authorId, LocalDateTime date, String text, String authorUsername) {
        this.postId = postId;
        this.authorId = authorId;
        this.date = date;
        this.text = text;
        this.authorUsername = authorUsername;
    }

    public CommentRecord(CommentRecord commentRecord) {
        this.postId = commentRecord.getPostId();
        this.authorId = commentRecord.getAuthorId();
        this.date = commentRecord.getDate();
        this.text = commentRecord.getText();
        this.authorUsername = commentRecord.getAuthorUsername();
    }

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    @Override
    public int getAuthorId() {
        return authorId;
    }

    @Override
    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }


    @Override
    public LocalDateTime getDate() {
        return date;
    }

    @Override
    public void setDate(LocalDateTime date) {
        this.date = date;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public RecordType getRecordType() {
        return RecordType.COMMENT;
    }

    public String getAuthorUsername() {
        return authorUsername;
    }

    public void setAuthorUsername(String authorUsername) {
        this.authorUsername = authorUsername;
    }
}
