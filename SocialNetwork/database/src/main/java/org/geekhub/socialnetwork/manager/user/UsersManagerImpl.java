package org.geekhub.socialnetwork.manager.user;

import org.geekhub.socialnetwork.RecordNotExistException;
import org.geekhub.socialnetwork.dao.model.CommentsRepository;
import org.geekhub.socialnetwork.dao.model.LikesRepository;
import org.geekhub.socialnetwork.dao.model.UsersRepository;
import org.geekhub.socialnetwork.manager.friends.FriendsManager;
import org.geekhub.socialnetwork.manager.post.PostManager;
import org.geekhub.socialnetwork.persistance.user.UserRecord;
import org.geekhub.socialnetwork.persistance.user_friend.UserFriendRecord;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class UsersManagerImpl implements UsersManager{
    private final UsersRepository usersRepository;
    private final FriendsManager friendsManager;
    private final CommentsRepository commentsRepository;
    private final LikesRepository likesRepository;
    private final PostManager postManager;

    public UsersManagerImpl(
            UsersRepository usersRepository,
            FriendsManager friendsManager,
            CommentsRepository commentsRepository,
            LikesRepository likesRepository,
            PostManager postManager
    ) {
        this.usersRepository = usersRepository;
        this.friendsManager = friendsManager;
        this.commentsRepository = commentsRepository;
        this.likesRepository = likesRepository;
        this.postManager = postManager;
    }

    @Override
    public void updateUser(UserRecord userRecord) {
        if (Objects.isNull(userRecord)) {
            throw new IllegalArgumentException("UserRecord must be not null!");
        }
        usersRepository.save(userRecord);
    }

    @Override
    public void addUser(UserRecord userRecord) {
        if (Objects.isNull(userRecord)) {
            throw new IllegalArgumentException("Registration parameters must be not empty");
        }
        usersRepository.save(userRecord);
    }

    @Transactional
    @Override
    public void deleteUser(int id) {
        if (getUserById(id).getRole().equals("ROLE_SUPER_ADMIN")) {
            throw new IllegalArgumentException("It's impossible to delete super admin");
        }
        usersRepository.deleteById(id);
        postManager.deleteAllUserPosts(id);
        commentsRepository.deleteAllUserComments(id);
        likesRepository.deleteByAuthorId(id);
        friendsManager.deleteAllUserFriends(id);
    }

    @Override
    public List<UserRecord> getAllUsers() {
        return (List<UserRecord>) usersRepository.findAll();
    }

    @Override
    public UserRecord getUserById(int id) {
        Optional<UserRecord> userRecord = usersRepository.findById(id);
        if (userRecord.isEmpty()) {
            throw new RecordNotExistException("User with id '" + id + "' not exist!" );
        }
        return userRecord.get();
    }

    @Override
    public UserRecord getUserByEmail(String email) {
        Optional<UserRecord> userRecord = usersRepository.getUserByEmail(email);
        if (userRecord.isEmpty()) {
            throw new RecordNotExistException("User with email '" + email + "' not exist!" );
        }
        return userRecord.get();
    }

    @Override
    public List<UserRecord> getFriendshipRequestForUser(int userId) {
        List<UserFriendRecord> usersRequests = friendsManager.getUsersToWhomTheFriendshipRequestWasSent(userId);
        return getUserFriendsFromUserFriendRecordList(usersRequests, userId);
    }

    @Override
    public List<UserRecord> getNotConfirmedUserFriends(int userId) {
        List<UserFriendRecord> userNotConfirmedFriendship = friendsManager.getUserNotConfirmedFriendship(userId);
        return getUserFriendsFromUserFriendRecordList(userNotConfirmedFriendship, userId);
    }

    @Override
    public List<UserRecord> getConfirmedUserFriends(int userId) {
        List<UserFriendRecord> userConfirmedFriendship = friendsManager.getUserConfirmedFriendship(userId);
        return getUserFriendsFromUserFriendRecordList(userConfirmedFriendship, userId);
    }

    @Override
    public List<Integer> getIdsOfUserFriends(int userId) {
        List<UserRecord> userFriends = getConfirmedUserFriends(userId);
        List<Integer> ids = new ArrayList<>();
        for (UserRecord record : userFriends) {
            ids.add(record.getId());
        }
        return ids;
    }

    private List<UserRecord> getUserFriendsFromUserFriendRecordList(List<UserFriendRecord> userFriendRecordList, int userId) {
        List<UserRecord> userFriends = new ArrayList<>();
        for (UserFriendRecord userFriendRecord : userFriendRecordList) {
            userFriends.add(getFriendInUserFriendRecord(userFriendRecord, userId));
        }
        return userFriends;
    }

    private UserRecord getFriendInUserFriendRecord(UserFriendRecord userFriendRecord, int userId) {
        int friendId;
        int recordFriendIdValue = userFriendRecord.getFriendId();
        if (recordFriendIdValue == userId) {
            friendId = userFriendRecord.getInitiatorId();
        } else {
            friendId = recordFriendIdValue;
        }

        return getUserById(friendId);
    }
}
