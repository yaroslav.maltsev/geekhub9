package org.geekhub.socialnetwork.manager.post;

import org.geekhub.socialnetwork.dao.model.CommentsRepository;
import org.geekhub.socialnetwork.dao.model.LikesRepository;
import org.geekhub.socialnetwork.dao.model.PostsRepository;
import org.geekhub.socialnetwork.manager.history.HistoryManager;
import org.geekhub.socialnetwork.persistance.post.PostRecord;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class PostManager {
    private final PostsRepository postsRepository;
    private final CommentsRepository commentsRepository;
    private final LikesRepository likesRepository;
    private final HistoryManager historyManager;

    public PostManager(PostsRepository postsRepository, CommentsRepository commentsRepository, LikesRepository likesRepository, HistoryManager historyManager) {
        this.postsRepository = postsRepository;
        this.commentsRepository = commentsRepository;
        this.likesRepository = likesRepository;
        this.historyManager = historyManager;
    }

    public PostRecord getPostById(int postId) {
        Optional<PostRecord> record = postsRepository.findById(postId);
        if (record.isEmpty()) {
            throw new IllegalArgumentException("Post with id: " + postId + " not exist!");
        }
        return record.get();
    }

    public List<PostRecord> getAllUserPosts(int userId) {
        return postsRepository.getAllUserPosts(userId);
    }

    public List<PostRecord> getFriendsPostRecordByLastThreeDays(List<Integer> friendsIds) {
        if (friendsIds.isEmpty()) {
            return new ArrayList<>();
        } else {
            return postsRepository.getPostsOfUserFriendsByDate(
                    friendsIds,
                    LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES),
                    3
            );
        }
    }

    @Transactional
    public void addPost(PostRecord postRecord) {
        if (Objects.isNull(postRecord)) {
            throw new IllegalArgumentException("PostRecord must be not null!");
        }
        if (postRecord.getDescription() == null || postRecord.getAuthorId() == 0) {
            throw new IllegalArgumentException("PostRecord parameters must be not null!");
        }

        postRecord.setDate(LocalDateTime.now().truncatedTo(ChronoUnit.MILLIS));
        PostRecord savedRecord = postsRepository.save(postRecord);
        historyManager.addToHistory(savedRecord);
    }

    public void editPost(PostRecord postRecord) {
        if (Objects.isNull(postRecord)) {
            throw new IllegalArgumentException("PostRecord must be not null!");
        }

        postsRepository.save(postRecord);
    }

    @Transactional
    public void deletePost(int postId) {
        postsRepository.deleteById(postId);
        commentsRepository.deleteAllPostComments(postId);
        likesRepository.deleteAllPostLikes(postId);
    }

    @Transactional
    public void deleteAllUserPosts(int userId) {
        postsRepository.deleteAllUserPosts(userId);
        commentsRepository.deleteAllUserComments(userId);
        likesRepository.deleteByAuthorId(userId);
    }
}
