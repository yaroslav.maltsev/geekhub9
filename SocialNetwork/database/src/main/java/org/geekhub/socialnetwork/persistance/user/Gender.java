package org.geekhub.socialnetwork.persistance.user;

public enum Gender {
    MALE, FEMALE
}
