package org.geekhub.socialnetwork.manager.user;

import org.geekhub.socialnetwork.persistance.user.UserRecord;

import java.util.List;

public interface UsersManager {
    void updateUser(UserRecord userRecord);

    void addUser(UserRecord userRecord);

    void deleteUser(int id);

    List<UserRecord> getAllUsers();

    UserRecord getUserById(int id);

    UserRecord getUserByEmail(String email);

    List<UserRecord> getFriendshipRequestForUser(int userId);

    List<UserRecord> getNotConfirmedUserFriends(int userId);

    List<UserRecord> getConfirmedUserFriends(int userId);

    List<Integer> getIdsOfUserFriends(int userId);
}
