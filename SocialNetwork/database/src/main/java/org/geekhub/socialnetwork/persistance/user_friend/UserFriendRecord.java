package org.geekhub.socialnetwork.persistance.user_friend;

import org.geekhub.socialnetwork.persistance.history.HistoryElement;
import org.geekhub.socialnetwork.persistance.history.RecordType;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@Table("user_friend")
public class UserFriendRecord implements HistoryElement {
    @Id
    private int id;
    @Column("dateOfConfirming")
    private LocalDateTime dateOfConfirming;
    @Column("dateOfRequest")
    private LocalDateTime dateOfRequest;
    @Column("initiatorId")
    private int initiatorId;
    @Column("friendId")
    private int friendId;
    @Column("isConfirmed")
    private boolean isConfirmed;

    public UserFriendRecord() {
    }

    public UserFriendRecord(
            LocalDateTime dateOfConfirming,
            LocalDateTime dateOfRequest,
            int initiatorId,
            int friendId,
            boolean isConfirmed
    ) {
        this.dateOfConfirming = dateOfConfirming;
        this.dateOfRequest = dateOfRequest;
        this.initiatorId = initiatorId;
        this.friendId = friendId;
        this.isConfirmed = isConfirmed;
    }

    @Override
    public int getId() {
        return id;
    }
    @Override
    public void setId(int id) {
        this.id = id;
    }


    public LocalDateTime getDateOfConfirming() {
        return dateOfConfirming;
    }

    public void setDateOfConfirming(LocalDateTime dateOfConfirming) {
        this.dateOfConfirming = dateOfConfirming;
    }


    public LocalDateTime getDateOfRequest() {
        return dateOfRequest;
    }

    public void setDateOfRequest(LocalDateTime dateOfRequest) {
        this.dateOfRequest = dateOfRequest;
    }


    public int getInitiatorId() {
        return initiatorId;
    }

    public void setInitiatorId(int initiatorId) {
        this.initiatorId = initiatorId;
    }


    public int getFriendId() {
        return friendId;
    }

    public void setFriendId(int friendId) {
        this.friendId = friendId;
    }


    public boolean isConfirmed() {
        return isConfirmed;
    }

    public void setIsConfirmed(boolean confirmed) {
        isConfirmed = confirmed;
    }

    @Override
    public LocalDateTime getDate() {
        if (isConfirmed) {
            return dateOfConfirming;
        } else {
            return dateOfRequest;
        }
    }

    @Override
    public void setDate(LocalDateTime date) {
        if (isConfirmed) {
            this.dateOfConfirming = date;
        } else {
            this.dateOfRequest = date;
        }
    }

    @Override
    public RecordType getRecordType() {
        return RecordType.FRIEND;
    }

    @Override
    public int getAuthorId() {
        if (isConfirmed) {
            return friendId;
        } else {
            return initiatorId;
        }
    }

    @Override
    public void setAuthorId(int authorId) {
        if (isConfirmed) {
            this.friendId = authorId;
        } else {
            this.initiatorId = authorId;
        }
    }
}
