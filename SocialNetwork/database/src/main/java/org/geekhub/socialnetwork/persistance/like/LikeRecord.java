package org.geekhub.socialnetwork.persistance.like;

import org.geekhub.socialnetwork.persistance.history.HistoryElement;
import org.geekhub.socialnetwork.persistance.history.RecordType;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@Table("likes")
public class LikeRecord implements HistoryElement {
    @Id
    private int id;

    @Column("postId")
    private int postId;

    @Column("authorId")
    private int authorId;
    @Column("authorUsername")
    private String authorUsername;
    private LocalDateTime date;

    public LikeRecord() {
    }

    public LikeRecord(int postId, int authorId, String authorUsername, LocalDateTime date) {
        this.postId = postId;
        this.authorId = authorId;
        this.authorUsername = authorUsername;
        this.date = date;
    }

    @Override
    public int getId() {
        return id;
    }
    @Override
    public void setId(int id) {
        this.id = id;
    }


    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    @Override
    public LocalDateTime getDate() {
        return date;
    }
    @Override
    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public int getAuthorId() {
        return authorId;
    }
    @Override
    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    @Override
    public RecordType getRecordType() {
        return RecordType.LIKE;
    }

    public String getAuthorUsername() {
        return authorUsername;
    }

    public void setAuthorUsername(String authorUsername) {
        this.authorUsername = authorUsername;
    }
}
