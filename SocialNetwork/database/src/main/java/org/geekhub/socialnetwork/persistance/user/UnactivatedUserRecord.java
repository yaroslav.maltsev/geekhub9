package org.geekhub.socialnetwork.persistance.user;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@Table("unactivatedUsers")
public class UnactivatedUserRecord {
    @Id
    private int id;
    @Column("userId")
    private int userId;
    @Column("activationCode")
    private String activationCode;
    @Column("date")
    private LocalDateTime date;

    public UnactivatedUserRecord() {}

    public UnactivatedUserRecord(int userId, String activationCode, LocalDateTime date) {
        this.userId = userId;
        this.activationCode = activationCode;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }

    public LocalDateTime getDate() {
        return date;
    }
}
