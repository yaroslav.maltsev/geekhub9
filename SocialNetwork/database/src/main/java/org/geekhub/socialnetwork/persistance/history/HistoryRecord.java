package org.geekhub.socialnetwork.persistance.history;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@Table("history")
public class HistoryRecord {
    @Id
    private int id;
    @Column("recordId")
    private int recordId;
    @Column("userId")
    private int userId;
    @Column("dateOfAction")
    private LocalDateTime dateOfAction;
    @Column("recordType")
    private RecordType recordType;

    public HistoryRecord() {
    }

    public HistoryRecord(HistoryElement historyElement) {
        this.recordId = historyElement.getId();
        this.userId = historyElement.getAuthorId();
        this.dateOfAction = historyElement.getDate();
        this.recordType = historyElement.getRecordType();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public LocalDateTime getDateOfAction() {
        return dateOfAction;
    }

    public void setDateOfAction(LocalDateTime dateOfAction) {
        this.dateOfAction = dateOfAction;
    }

    public RecordType getRecordType() {
        return recordType;
    }

    public void setRecordType(RecordType recordType) {
        this.recordType = recordType;
    }
}
