package org.geekhub.socialnetwork.manager.history;

import org.geekhub.socialnetwork.dao.model.history.HistoryDao;
import org.geekhub.socialnetwork.persistance.history.HistoryElement;
import org.geekhub.socialnetwork.persistance.history.HistoryRecord;
import org.geekhub.socialnetwork.persistance.history.RecordType;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
public class HistoryManager {
    private final HistoryDao historyDao;

    public HistoryManager(HistoryDao historyDao) {
        this.historyDao = historyDao;
    }

    public void addToHistory(HistoryElement historyElement) {
        if (Objects.isNull(historyElement)) {
            throw new IllegalArgumentException("HistoryElement must be not null!");
        }
        HistoryRecord entity = new HistoryRecord(historyElement);
        historyDao.save(entity);
    }

    public List<HistoryRecord> getAllRecordsOfType(RecordType type) {
        return historyDao.getAllRecordsOfType(type.name());
    }

    public List<HistoryRecord> getAllUserRecordsOfType(int userId, RecordType type) {
        return historyDao.getAllUserRecordsOfType(userId, type.name());
    }

    public List<HistoryRecord> getAllHistoryRecordsOfUser (int userId) {
        return historyDao.getAllHistoryRecordsOfUser(userId);
    }

    public List<HistoryRecord> getRecordsOfTypeByTime(RecordType type, LocalDateTime start, LocalDateTime finish) {
        return historyDao.getHistoryRecordsOfTypeByTime(
                type.name(),
                start,
                finish
        );
    }

    public List<HistoryRecord> getUserRecordsOfTypeByTime(int userId, RecordType type, LocalDateTime start, LocalDateTime finish) {
        return historyDao.getUserHistoryRecordsOfTypeByTime(
                userId,
                type.name(),
                start,
                finish
        );
    }

    public void clearUserHistory(int userId) {
        historyDao.clearUserHistory(userId);
    }
}
