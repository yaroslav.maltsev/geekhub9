package org.geekhub.socialnetwork.manager.registration;

import org.geekhub.socialnetwork.dao.model.UsersRepository;
import org.geekhub.socialnetwork.dao.relational.UnactivatedUsersDao;
import org.geekhub.socialnetwork.persistance.user.UnactivatedUserRecord;
import org.geekhub.socialnetwork.persistance.user.UserRecord;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

@Service
public class RegistrationManager {
    private final UsersRepository usersRepository;
    private final UnactivatedUsersDao unactivatedUsersDao;

    public RegistrationManager(UsersRepository usersRepository, UnactivatedUsersDao unactivatedUsersDao) {
        this.usersRepository = usersRepository;
        this.unactivatedUsersDao = unactivatedUsersDao;
    }


    @Transactional
    public void activateUser(String activationCode) {
        if (Objects.isNull(activationCode) || activationCode.isBlank()) {
            throw new IllegalArgumentException("Activation code must be not blank!");
        }
        UnactivatedUserRecord record = unactivatedUsersDao.getByActivationCode(activationCode);

        usersRepository.activateUser(record.getUserId());

        unactivatedUsersDao.deleteById(record.getId());
    }

    @Transactional
    public void registerUser(UserRecord userRecord, String activationCode) {
        if (Objects.isNull(userRecord) || Objects.isNull(activationCode) || activationCode.isBlank()) {
            throw new IllegalArgumentException("Registration parameters must be not empty");
        }
        UserRecord savedRecord = usersRepository.save(userRecord);
        addRecord(savedRecord.getId(), activationCode);
    }

    private void addRecord(int userId, String activationCode) {
        unactivatedUsersDao.save(
                new UnactivatedUserRecord(
                        userId,
                        activationCode,
                        LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS)
                )
        );
    }
}
