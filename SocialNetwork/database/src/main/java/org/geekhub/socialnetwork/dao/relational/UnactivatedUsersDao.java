package org.geekhub.socialnetwork.dao.relational;

import org.geekhub.socialnetwork.persistance.user.UnactivatedUserRecord;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface UnactivatedUsersDao extends CrudRepository<UnactivatedUserRecord, Integer> {
    @Query("SELECT id, userId, activationCode, date FROM unactivatedUsers WHERE activationCode = :activationCode")
    UnactivatedUserRecord getByActivationCode(@Param("activationCode") String activationCode);

    @Modifying
    @Query("DELETE FROM unactivatedUsers WHERE DATEDIFF(minute, :now, date) >= :term")
    void deleteRecordsWithExpiredTime(
            @Param("now") LocalDateTime now,
            @Param("term") int term
    );

    @Query("SELECT userId FROM unactivatedUsers WHERE (DATEDIFF(minute, :now, date) >= :term)")
    List<Integer> getIdsOfUsersInactiveAccounts(
            @Param("now") LocalDateTime now,
            @Param("term") int term
    );
}
