package org.geekhub.socialnetwork.persistance.user;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@Table("users")
public class UserRecord {
    @Id
    private int id;
    @Column("registrationDate")
    private LocalDateTime registrationDate;
    private String email;
    @Column("encodedPassword")
    private String encodedPassword;
    private String role;
    private String username;
    private Gender gender;
    @Column("isActivated")
    private boolean isActivated;

    public UserRecord() {
    }

    public UserRecord(LocalDateTime registrationDate, String email, String encodedPassword, String role, String username, Gender gender) {
        this.registrationDate = registrationDate;
        this.email = email;
        this.encodedPassword = encodedPassword;
        this.role = role;
        this.username = username;
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDateTime registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEncodedPassword() {
        return encodedPassword;
    }

    public void setEncodedPassword(String encodedPassword) {
        this.encodedPassword = encodedPassword;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public boolean isActivated() {
        return isActivated;
    }

    public void setActivated(boolean activated) {
        isActivated = activated;
    }
}
