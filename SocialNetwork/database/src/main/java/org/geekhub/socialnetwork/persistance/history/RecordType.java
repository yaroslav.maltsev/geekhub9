package org.geekhub.socialnetwork.persistance.history;

public enum RecordType {
    LIKE, COMMENT, POST, FRIEND
}
