package org.geekhub.socialnetwork.dao.model;

import org.geekhub.socialnetwork.persistance.comment.CommentRecord;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CommentsRepository extends CrudRepository<CommentRecord, Integer> {

    @Query("SELECT id, postId, authorId, date, text, authorUsername FROM comments WHERE postId = :postId")
    List<CommentRecord> getAllPostComments(@Param("postId") int postId);

    @Modifying
    @Query("DELETE FROM comments WHERE authorId = :authorId")
    void deleteAllUserComments(@Param("authorId") int authorId);

    @Modifying
    @Query("DELETE FROM comments WHERE postId = :postId")
    void deleteAllPostComments(@Param("postId") int postId);
}
