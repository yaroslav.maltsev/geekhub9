package org.geekhub.socialnetwork.dao.relational;

import org.geekhub.socialnetwork.persistance.user_friend.UserFriendRecord;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface FriendsRepository extends CrudRepository<UserFriendRecord, Integer> {

    @Modifying
    @Query("UPDATE user_friend SET isConfirmed = true, dateOfConfirming = :dateOfConfirming WHERE (initiatorId = :initiatorId AND friendId = :friendId) OR (initiatorId = :friendId AND friendId = :initiatorId)")
    void confirmFriendship(
            @Param("initiatorId") int initiatorId,
            @Param("friendId") int friendId,
            @Param("dateOfConfirming") LocalDateTime dateOfConfirming
    );

    @Modifying
    @Query("UPDATE user_friend SET isConfirmed = false WHERE (initiatorId = :initiatorId AND friendId = :friendId) OR (initiatorId = :friendId AND friendId = :initiatorId)")
    void refuseFriendshipRequest(
            @Param("initiatorId") int initiatorId,
            @Param("friendId") int friendId
    );

    @Modifying
    @Query("DELETE FROM user_friend WHERE initiatorId = :userId OR friendId = :userId")
    void deleteAllUserFriends( @Param("userId") int userId);

    @Modifying
    @Query("DELETE from user_friend WHERE (initiatorId = :initiatorId AND friendId = :friendId) OR (initiatorId = :friendId AND friendId = :initiatorId)")
    void deleteRecord(
            @Param("initiatorId") int initiatorId,
            @Param("friendId") int friendId
    );

    @Query(
            "SELECT id, dateOfConfirming, dateOfRequest, initiatorId, friendId, isConfirmed FROM user_friend " +
            "WHERE (initiatorId = :initiatorId AND friendId = :friendId) " +
            "OR (initiatorId = :friendId AND friendId = :initiatorId)"
    )
    UserFriendRecord getRecordByIds(
            @Param("initiatorId") int initiatorId,
            @Param("friendId") int friendId
    );

    @Query(
            "SELECT id, dateOfConfirming, dateOfRequest, initiatorId, friendId, isConfirmed FROM user_friend " +
                    "WHERE (initiatorId = :userId OR friendId = :userId) "
    )
    List<UserFriendRecord> getAllUserFriendshipRecords(@Param("userId") int userId);

    @Query(
            "SELECT id, dateOfConfirming, dateOfRequest, initiatorId, friendId, isConfirmed FROM user_friend " +
                    "WHERE (initiatorId = :userId OR friendId = :userId) " +
                    "AND isConfirmed = :isConfirmed"
    )
    List<UserFriendRecord> getUserFriendshipByConfirming(
            @Param("userId") int userId,
            @Param("isConfirmed") boolean isConfirmed
    );


    @Query(
            "SELECT id, dateOfConfirming, dateOfRequest, initiatorId, friendId, isConfirmed FROM user_friend " +
                    "WHERE initiatorId = :userId " +
                    "AND isConfirmed = false"
    )
    List<UserFriendRecord> getUsersToWhomTheFriendshipRequestWasSent(@Param("userId") int userId);
}
