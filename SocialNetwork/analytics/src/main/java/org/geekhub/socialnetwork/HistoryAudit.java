package org.geekhub.socialnetwork;

import org.geekhub.socialnetwork.manager.history.HistoryManager;
import org.geekhub.socialnetwork.persistance.history.HistoryRecord;
import org.geekhub.socialnetwork.persistance.history.RecordType;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class HistoryAudit {
    private final HistoryManager historyManager;

    public HistoryAudit(HistoryManager historyManager) {
        this.historyManager = historyManager;
    }

    public Map<LocalDateTime, Long> getCountOfPostsPerLastDays(int amountOfDays) {
        List<HistoryRecord> recordsPerTimeInterval = historyManager.getRecordsOfTypeByTime(
                RecordType.POST,
                LocalDateTime.now().minusDays(amountOfDays),
                LocalDateTime.now()
        );
        return getCountOfRecords(recordsPerTimeInterval);
    }

    public Map<LocalDateTime, Long> getCountOfUserPostsPerLastDays(int userId, int amountOfDays) {
        List<HistoryRecord> recordsPerTimeInterval = historyManager.getUserRecordsOfTypeByTime(
                userId,
                RecordType.POST,
                LocalDateTime.now().minusDays(amountOfDays),
                LocalDateTime.now()
        );
        return getCountOfRecords(recordsPerTimeInterval);
    }


    public Map<LocalDateTime, Long> getCountOfLikesPerLastDays(int amountOfDays) {
        List<HistoryRecord> recordsPerTimeInterval = historyManager.getRecordsOfTypeByTime(
                RecordType.LIKE,
                LocalDateTime.now().minusDays(amountOfDays),
                LocalDateTime.now()
        );
        return getCountOfRecords(recordsPerTimeInterval);
    }

    public Map<LocalDateTime, Long> getCountOfUserLikesPerLastDays(int userId, int amountOfDays) {
        List<HistoryRecord> recordsPerTimeInterval = historyManager.getUserRecordsOfTypeByTime(
                userId,
                RecordType.LIKE,
                LocalDateTime.now().minusDays(amountOfDays),
                LocalDateTime.now()
        );
        return getCountOfRecords(recordsPerTimeInterval);
    }


    public Map<LocalDateTime, Long> getCountOfCommentsPerLastDays(int amountOfDays) {
        List<HistoryRecord> recordsPerTimeInterval = historyManager.getRecordsOfTypeByTime(
                RecordType.COMMENT,
                LocalDateTime.now().minusDays(amountOfDays),
                LocalDateTime.now()
        );
        return getCountOfRecords(recordsPerTimeInterval);
    }

    public Map<LocalDateTime, Long> getCountOfUserCommentsPerLastDays(int userId, int amountOfDays) {
        List<HistoryRecord> recordsPerTimeInterval = historyManager.getUserRecordsOfTypeByTime(
                userId,
                RecordType.COMMENT,
                LocalDateTime.now().minusDays(amountOfDays),
                LocalDateTime.now()
        );
        return getCountOfRecords(recordsPerTimeInterval);
    }

    private Map<LocalDateTime, Long> getCountOfRecords(List<HistoryRecord> list) {
        return list.stream()
                .collect(
                        Collectors.groupingBy(
                                HistoryRecord::getDateOfAction,
                                HashMap::new,
                                Collectors.counting()
                        )
                );
    }
}
