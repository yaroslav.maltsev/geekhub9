package org.geekhub.socialnetwork;

import org.geekhub.socialnetwork.manager.comment.CommentManager;
import org.geekhub.socialnetwork.manager.like.LikeManager;
import org.geekhub.socialnetwork.manager.post.PostManager;
import org.geekhub.socialnetwork.persistance.post.PostRecord;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.IntFunction;

@Service
public class UserAnalytics {
    private final PostManager postManager;
    private final LikeManager likeManager;
    private final CommentManager commentManager;

    public UserAnalytics(PostManager postManager, LikeManager likeManager, CommentManager commentManager) {
        this.postManager = postManager;
        this.likeManager = likeManager;
        this.commentManager = commentManager;
    }

    public int getAmountOfAllLikesOnUserPosts(int userId) {
        return getAmountOfRecordsOnPost(userId, likeManager::getAllPostLikes);
    }

    public int getAmountOfAllCommentsOnUserPosts(int userId) {
        return getAmountOfRecordsOnPost(userId, commentManager::getAllPostComments);
    }

    public int getAmountOfAllUserPosts(int userId) {
        return postManager.getAllUserPosts(userId).size();
    }

    private <T> int getAmountOfRecordsOnPost(int userId, IntFunction<List<T>> func) {
        List<PostRecord> allUserPosts = postManager.getAllUserPosts(userId);
        int summ = 0;
        for (PostRecord record : allUserPosts) {
            int amountOfCommentsOnPost = func.apply(record.getId()).size();
            summ += amountOfCommentsOnPost;
        }
        return summ;
    }
}
