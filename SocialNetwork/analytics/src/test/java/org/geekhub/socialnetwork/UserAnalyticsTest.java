package org.geekhub.socialnetwork;

import org.geekhub.socialnetwork.manager.comment.CommentManager;
import org.geekhub.socialnetwork.manager.like.LikeManager;
import org.geekhub.socialnetwork.manager.post.PostManager;
import org.geekhub.socialnetwork.persistance.comment.CommentRecord;
import org.geekhub.socialnetwork.persistance.like.LikeRecord;
import org.geekhub.socialnetwork.persistance.post.PostRecord;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class UserAnalyticsTest {
    private PostManager postManager;
    private LikeManager likeManager;
    private CommentManager commentManager;
    private UserAnalytics userAnalytics;

    @BeforeMethod
    public void setUp() {
        postManager = mock(PostManager.class);
        likeManager = mock(LikeManager.class);
        commentManager = mock(CommentManager.class);

        userAnalytics = new UserAnalytics(postManager, likeManager, commentManager);
    }

    @Test
    public void test_getAmountOfAllLikesOnUserPosts_when_history_is_empty_is_success() {
        when(postManager.getAllUserPosts(anyInt())).thenReturn(new ArrayList<>());
        when(likeManager.getAllPostLikes(anyInt())).thenReturn(new ArrayList<>());
        when(commentManager.getAllPostComments(anyInt())).thenReturn(new ArrayList<>());

        userAnalytics.getAmountOfAllLikesOnUserPosts(1);
    }

    @Test
    public void test_getAmountOfAllLikesOnUserPosts_when_history_is_not_empty_is_success() {
        when(postManager.getAllUserPosts(anyInt())).thenReturn(List.of(new PostRecord()));
        when(likeManager.getAllPostLikes(anyInt())).thenReturn(List.of(new LikeRecord()));
        when(commentManager.getAllPostComments(anyInt())).thenReturn(List.of(new CommentRecord()));

        int amountOfAllLikesOnUserPosts = userAnalytics.getAmountOfAllLikesOnUserPosts(1);
        assertEquals(amountOfAllLikesOnUserPosts, 1);
    }

    @Test
    public void test_getAmountOfAllLikesOnUserPosts_when_history_has_more_than_one_like_is_success() {
        when(postManager.getAllUserPosts(anyInt())).thenReturn(List.of(new PostRecord()));
        when(likeManager.getAllPostLikes(anyInt())).thenReturn(List.of(new LikeRecord(), new LikeRecord(), new LikeRecord()));

        int amountOfAllLikesOnUserPosts = userAnalytics.getAmountOfAllLikesOnUserPosts(1);
        assertEquals(amountOfAllLikesOnUserPosts, 3);
    }


    @Test
    public void test_getAmountOfAllCommentsOnUserPosts_when_history_is_empty_is_success() {
        when(postManager.getAllUserPosts(anyInt())).thenReturn(new ArrayList<>());
        when(likeManager.getAllPostLikes(anyInt())).thenReturn(new ArrayList<>());
        when(commentManager.getAllPostComments(anyInt())).thenReturn(new ArrayList<>());

        userAnalytics.getAmountOfAllCommentsOnUserPosts(1);
    }

    @Test
    public void test_getAmountOfAllCommentsOnUserPosts_when_history_is_not_empty_is_success() {
        when(postManager.getAllUserPosts(anyInt())).thenReturn(List.of(new PostRecord()));
        when(commentManager.getAllPostComments(anyInt())).thenReturn(List.of(new CommentRecord()));

        int amountOfAllCommentsOnUserPosts = userAnalytics.getAmountOfAllCommentsOnUserPosts(1);
        assertEquals(amountOfAllCommentsOnUserPosts, 1);
    }

    @Test
    public void test_getAmountOfAllCommentsOnUserPosts_when_history_has_more_than_one_like_is_success() {
        when(postManager.getAllUserPosts(anyInt())).thenReturn(List.of(new PostRecord()));
        when(commentManager.getAllPostComments(anyInt())).thenReturn(List.of(new CommentRecord(), new CommentRecord(), new CommentRecord()));


        int amountOfAllCommentsOnUserPosts = userAnalytics.getAmountOfAllCommentsOnUserPosts(1);
        assertEquals(amountOfAllCommentsOnUserPosts, 3);
    }


    @Test
    public void test_getAmountOfAllUserPosts_when_history_is_empty_is_success() {
        when(postManager.getAllUserPosts(anyInt())).thenReturn(new ArrayList<>());

        userAnalytics.getAmountOfAllUserPosts(1);
    }

    @Test
    public void test_getAmountOfAllUserPosts_when_history_is_not_empty_is_success() {
        when(postManager.getAllUserPosts(anyInt())).thenReturn(List.of(new PostRecord()));

        int amountOfAllUserPosts = userAnalytics.getAmountOfAllUserPosts(1);
        assertEquals(amountOfAllUserPosts, 1);
    }

    @Test
    public void test_getAmountOfAllUserPosts_when_history_has_more_than_one_like_is_success() {
        when(postManager.getAllUserPosts(anyInt())).thenReturn(List.of(new PostRecord(), new PostRecord(), new PostRecord()));


        int amountOfAllUserPosts = userAnalytics.getAmountOfAllUserPosts(1);
        assertEquals(amountOfAllUserPosts, 3);
    }
}