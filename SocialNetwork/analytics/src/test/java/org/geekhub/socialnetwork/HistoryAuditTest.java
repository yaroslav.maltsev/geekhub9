package org.geekhub.socialnetwork;

import org.geekhub.socialnetwork.manager.history.HistoryManager;
import org.geekhub.socialnetwork.persistance.comment.CommentRecord;
import org.geekhub.socialnetwork.persistance.history.HistoryRecord;
import org.geekhub.socialnetwork.persistance.like.LikeRecord;
import org.geekhub.socialnetwork.persistance.post.PostRecord;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class HistoryAuditTest {
    private HistoryManager historyManager;
    private HistoryAudit historyAudit;

    @BeforeMethod
    public void setUp() {
        historyManager = mock(HistoryManager.class);
        historyAudit = new HistoryAudit(historyManager);
    }

    @Test
    public void test_getCountOfPostsPerLastDays_when_history_is_empty_is_success() {
        when(historyManager.getRecordsOfTypeByTime(any(), any(), any())).thenReturn(new ArrayList<>());
        Map<LocalDateTime, Long> countOfPostsPerLastDays = historyAudit.getCountOfPostsPerLastDays(1);

        assertNotNull(countOfPostsPerLastDays);
        assertEquals(countOfPostsPerLastDays.size(), 0);
    }

    @Test
    public void test_getCountOfPostsPerLastDays_when_history_has_one_record_is_success() {
        LocalDateTime date = LocalDateTime.now().minusDays(1);
        HistoryRecord record = new HistoryRecord();
        record.setDateOfAction(date);
        when(historyManager.getRecordsOfTypeByTime(any(), any(), any())).thenReturn(List.of(
                record
        ));
        Map<LocalDateTime, Long> countOfPostsPerLastDays = historyAudit.getCountOfPostsPerLastDays(2);

        assertNotNull(countOfPostsPerLastDays);
        assertEquals(countOfPostsPerLastDays.size(), 1);
        assertEquals(countOfPostsPerLastDays.get(date).longValue(), 1L);
    }

    @Test
    public void test_getCountOfPostsPerLastDays_when_history_has_more_than_one_record_is_success() {
        LocalDateTime earlier = LocalDateTime.now().minusDays(2);
        LocalDateTime yesterday = LocalDateTime.now().minusDays(1);
        LocalDateTime threeDaysEarlier = LocalDateTime.now().minusDays(3);

        HistoryRecord record1 = new HistoryRecord();
        record1.setDateOfAction(earlier);
        HistoryRecord record2 = new HistoryRecord();
        record2.setDateOfAction(yesterday);
        HistoryRecord record3 = new HistoryRecord();
        record3.setDateOfAction(threeDaysEarlier);

        when(historyManager.getRecordsOfTypeByTime(any(), any(), any())).thenReturn(List.of(
                record1,
                record2,
                record3
        ));
        Map<LocalDateTime, Long> countOfPostsPerLastDays = historyAudit.getCountOfPostsPerLastDays(4);

        assertNotNull(countOfPostsPerLastDays);
        assertEquals(countOfPostsPerLastDays.size(), 3);
        assertEquals(countOfPostsPerLastDays.get(earlier).longValue(), 1L);
        assertEquals(countOfPostsPerLastDays.get(threeDaysEarlier).longValue(), 1L);
        assertEquals(countOfPostsPerLastDays.get(yesterday).longValue(), 1L);
    }


    @Test
    public void test_getCountOfUserPostsPerLastDays_when_history_is_empty_is_success() {
        int userId = 1;
        when(historyManager.getUserRecordsOfTypeByTime(anyInt(), any(), any(), any())).thenReturn(new ArrayList<>());
        Map<LocalDateTime, Long> countOfPostsPerLastDays = historyAudit.getCountOfUserPostsPerLastDays(userId, 7);

        assertNotNull(countOfPostsPerLastDays);
        assertEquals(countOfPostsPerLastDays.size(), 0);
    }

    @Test
    public void test_getCountOfUserPostsPerLastDays_when_history_has_one_record_is_success() {
        int userId = 1;
        LocalDateTime date = LocalDateTime.now().minusDays(1);
        HistoryRecord record = new HistoryRecord();
        record.setDateOfAction(date);
        when(historyManager.getUserRecordsOfTypeByTime(anyInt(), any(), any(), any())).thenReturn(List.of(
                record
        ));
        Map<LocalDateTime, Long> countOfPostsPerLastDays = historyAudit.getCountOfUserPostsPerLastDays(userId, 2);

        assertNotNull(countOfPostsPerLastDays);
        assertEquals(countOfPostsPerLastDays.size(), 1);
        assertEquals(countOfPostsPerLastDays.get(date).longValue(), 1L);
    }

    @Test
    public void test_getCountOfUserPostsPerLastDays_when_history_has_more_than_one_record_is_success() {
        LocalDateTime earlier = LocalDateTime.now().minusDays(2);
        LocalDateTime yesterday = LocalDateTime.now().minusDays(1);
        LocalDateTime threeDaysEarlier = LocalDateTime.now().minusDays(3);

        HistoryRecord record1 = new HistoryRecord();
        record1.setDateOfAction(earlier);
        HistoryRecord record2 = new HistoryRecord();
        record2.setDateOfAction(yesterday);
        HistoryRecord record3 = new HistoryRecord();
        record3.setDateOfAction(threeDaysEarlier);

        when(historyManager.getRecordsOfTypeByTime(any(), any(), any())).thenReturn(List.of(
                record1,
                record2,
                record3
        ));
        Map<LocalDateTime, Long> countOfPostsPerLastDays = historyAudit.getCountOfPostsPerLastDays(4);

        assertNotNull(countOfPostsPerLastDays);
        assertEquals(countOfPostsPerLastDays.size(), 3);
        assertEquals(countOfPostsPerLastDays.get(earlier).longValue(), 1L);
        assertEquals(countOfPostsPerLastDays.get(threeDaysEarlier).longValue(), 1L);
        assertEquals(countOfPostsPerLastDays.get(yesterday).longValue(), 1L);
    }


    @Test
    public void test_getCountOfLikesPerLastDays_when_history_is_empty_is_success() {
        when(historyManager.getRecordsOfTypeByTime(any(), any(), any())).thenReturn(new ArrayList<>());
        Map<LocalDateTime, Long> countOfPostsPerLastDays = historyAudit.getCountOfPostsPerLastDays(1);

        assertNotNull(countOfPostsPerLastDays);
        assertEquals(countOfPostsPerLastDays.size(), 0);
    }

    @Test
    public void test_getCountOfLikesPerLastDays_when_history_has_one_record_is_success() {
        LocalDateTime date = LocalDateTime.now().minusDays(1);
        HistoryRecord record = new HistoryRecord();
        record.setDateOfAction(date);
        when(historyManager.getRecordsOfTypeByTime(any(), any(), any())).thenReturn(List.of(
                record
        ));
        Map<LocalDateTime, Long> countOfPostsPerLastDays = historyAudit.getCountOfPostsPerLastDays(2);

        assertNotNull(countOfPostsPerLastDays);
        assertEquals(countOfPostsPerLastDays.size(), 1);
        assertEquals(countOfPostsPerLastDays.get(date).longValue(), 1L);
    }

    @Test
    public void test_getCountOfLikesPerLastDays_when_history_has_more_than_one_record_is_success() {
        LocalDateTime earlier = LocalDateTime.now().minusDays(2);
        LocalDateTime yesterday = LocalDateTime.now().minusDays(1);
        LocalDateTime threeDaysEarlier = LocalDateTime.now().minusDays(3);

        HistoryRecord record1 = new HistoryRecord();
        record1.setDateOfAction(earlier);
        HistoryRecord record2 = new HistoryRecord();
        record2.setDateOfAction(yesterday);
        HistoryRecord record3 = new HistoryRecord();
        record3.setDateOfAction(threeDaysEarlier);

        when(historyManager.getRecordsOfTypeByTime(any(), any(), any())).thenReturn(List.of(
                record1,
                record2,
                record3
        ));
        Map<LocalDateTime, Long> countOfPostsPerLastDays = historyAudit.getCountOfPostsPerLastDays(4);

        assertNotNull(countOfPostsPerLastDays);
        assertEquals(countOfPostsPerLastDays.size(), 3);
        assertEquals(countOfPostsPerLastDays.get(earlier).longValue(), 1L);
        assertEquals(countOfPostsPerLastDays.get(threeDaysEarlier).longValue(), 1L);
        assertEquals(countOfPostsPerLastDays.get(yesterday).longValue(), 1L);
    }


    @Test
    public void test_getCountOfUserLikesPerLastDays_when_history_is_empty_is_success() {
        int userId = 1;
        when(historyManager.getUserRecordsOfTypeByTime(anyInt(), any(), any(), any())).thenReturn(new ArrayList<>());
        Map<LocalDateTime, Long> countOfPostsPerLastDays = historyAudit.getCountOfUserPostsPerLastDays(userId, 7);

        assertNotNull(countOfPostsPerLastDays);
        assertEquals(countOfPostsPerLastDays.size(), 0);
    }

    @Test
    public void test_getCountOfUserLikesPerLastDays_when_history_has_one_record_is_success() {
        int userId = 1;
        LocalDateTime date = LocalDateTime.now().minusDays(1);
        HistoryRecord record = new HistoryRecord();
        record.setDateOfAction(date);
        when(historyManager.getUserRecordsOfTypeByTime(anyInt(), any(), any(), any())).thenReturn(List.of(
                record
        ));
        Map<LocalDateTime, Long> countOfPostsPerLastDays = historyAudit.getCountOfUserPostsPerLastDays(userId, 2);

        assertNotNull(countOfPostsPerLastDays);
        assertEquals(countOfPostsPerLastDays.size(), 1);
        assertEquals(countOfPostsPerLastDays.get(date).longValue(), 1L);
    }

    @Test
    public void test_getCountOfUserLikesPerLastDays_when_history_has_more_than_one_record_is_success() {
        LocalDateTime earlier = LocalDateTime.now().minusDays(2);
        LocalDateTime yesterday = LocalDateTime.now().minusDays(1);
        LocalDateTime threeDaysEarlier = LocalDateTime.now().minusDays(3);

        HistoryRecord record1 = new HistoryRecord();
        record1.setDateOfAction(earlier);
        HistoryRecord record2 = new HistoryRecord();
        record2.setDateOfAction(yesterday);
        HistoryRecord record3 = new HistoryRecord();
        record3.setDateOfAction(threeDaysEarlier);

        when(historyManager.getRecordsOfTypeByTime(any(), any(), any())).thenReturn(List.of(
                record1,
                record2,
                record3
        ));
        Map<LocalDateTime, Long> countOfPostsPerLastDays = historyAudit.getCountOfPostsPerLastDays(4);

        assertNotNull(countOfPostsPerLastDays);
        assertEquals(countOfPostsPerLastDays.size(), 3);
        assertEquals(countOfPostsPerLastDays.get(earlier).longValue(), 1L);
        assertEquals(countOfPostsPerLastDays.get(threeDaysEarlier).longValue(), 1L);
        assertEquals(countOfPostsPerLastDays.get(yesterday).longValue(), 1L);
    }


    @Test
    public void test_getCountOfCommentsPerLastDays_when_history_is_empty_is_success() {
        when(historyManager.getRecordsOfTypeByTime(any(), any(), any())).thenReturn(new ArrayList<>());
        Map<LocalDateTime, Long> countOfPostsPerLastDays = historyAudit.getCountOfPostsPerLastDays(1);

        assertNotNull(countOfPostsPerLastDays);
        assertEquals(countOfPostsPerLastDays.size(), 0);
    }

    @Test
    public void test_getCountOfCommentsPerLastDays_when_history_has_one_record_is_success() {
        LocalDateTime date = LocalDateTime.now().minusDays(1);
        HistoryRecord record = new HistoryRecord();
        record.setDateOfAction(date);
        when(historyManager.getRecordsOfTypeByTime(any(), any(), any())).thenReturn(List.of(
                record
        ));
        Map<LocalDateTime, Long> countOfPostsPerLastDays = historyAudit.getCountOfPostsPerLastDays(2);

        assertNotNull(countOfPostsPerLastDays);
        assertEquals(countOfPostsPerLastDays.size(), 1);
        assertEquals(countOfPostsPerLastDays.get(date).longValue(), 1L);
    }

    @Test
    public void test_getCountOfCommentsPerLastDays_when_history_has_more_than_one_record_is_success() {
        LocalDateTime earlier = LocalDateTime.now().minusDays(2);
        LocalDateTime yesterday = LocalDateTime.now().minusDays(1);
        LocalDateTime threeDaysEarlier = LocalDateTime.now().minusDays(3);

        HistoryRecord record1 = new HistoryRecord();
        record1.setDateOfAction(earlier);
        HistoryRecord record2 = new HistoryRecord();
        record2.setDateOfAction(yesterday);
        HistoryRecord record3 = new HistoryRecord();
        record3.setDateOfAction(threeDaysEarlier);

        when(historyManager.getRecordsOfTypeByTime(any(), any(), any())).thenReturn(List.of(
                record1,
                record2,
                record3
        ));
        Map<LocalDateTime, Long> countOfPostsPerLastDays = historyAudit.getCountOfPostsPerLastDays(4);

        assertNotNull(countOfPostsPerLastDays);
        assertEquals(countOfPostsPerLastDays.size(), 3);
        assertEquals(countOfPostsPerLastDays.get(earlier).longValue(), 1L);
        assertEquals(countOfPostsPerLastDays.get(threeDaysEarlier).longValue(), 1L);
        assertEquals(countOfPostsPerLastDays.get(yesterday).longValue(), 1L);
    }


    @Test
    public void test_getCountOfUserCommentsPerLastDays_when_history_is_empty_is_success() {
        int userId = 1;
        when(historyManager.getUserRecordsOfTypeByTime(anyInt(), any(), any(), any())).thenReturn(new ArrayList<>());
        Map<LocalDateTime, Long> countOfPostsPerLastDays = historyAudit.getCountOfUserPostsPerLastDays(userId, 7);

        assertNotNull(countOfPostsPerLastDays);
        assertEquals(countOfPostsPerLastDays.size(), 0);
    }

    @Test
    public void test_getCountOfUserCommentsPerLastDays_when_history_has_one_record_is_success() {
        int userId = 1;
        LocalDateTime date = LocalDateTime.now().minusDays(1);
        HistoryRecord record = new HistoryRecord();
        record.setDateOfAction(date);
        when(historyManager.getUserRecordsOfTypeByTime(anyInt(), any(), any(), any())).thenReturn(List.of(
                record
        ));
        Map<LocalDateTime, Long> countOfPostsPerLastDays = historyAudit.getCountOfUserPostsPerLastDays(userId, 2);

        assertNotNull(countOfPostsPerLastDays);
        assertEquals(countOfPostsPerLastDays.size(), 1);
        assertEquals(countOfPostsPerLastDays.get(date).longValue(), 1L);
    }

    @Test
    public void test_getCountOfUserCommentsPerLastDays_when_history_has_more_than_one_record_is_success() {
        LocalDateTime earlier = LocalDateTime.now().minusDays(2);
        LocalDateTime yesterday = LocalDateTime.now().minusDays(1);
        LocalDateTime threeDaysEarlier = LocalDateTime.now().minusDays(3);

        HistoryRecord record1 = new HistoryRecord();
        record1.setDateOfAction(earlier);
        HistoryRecord record2 = new HistoryRecord();
        record2.setDateOfAction(yesterday);
        HistoryRecord record3 = new HistoryRecord();
        record3.setDateOfAction(threeDaysEarlier);

        when(historyManager.getRecordsOfTypeByTime(any(), any(), any())).thenReturn(List.of(
                record1,
                record2,
                record3
        ));
        Map<LocalDateTime, Long> countOfPostsPerLastDays = historyAudit.getCountOfPostsPerLastDays(4);

        assertNotNull(countOfPostsPerLastDays);
        assertEquals(countOfPostsPerLastDays.size(), 3);
        assertEquals(countOfPostsPerLastDays.get(earlier).longValue(), 1L);
        assertEquals(countOfPostsPerLastDays.get(threeDaysEarlier).longValue(), 1L);
        assertEquals(countOfPostsPerLastDays.get(yesterday).longValue(), 1L);
    }
}
