package org.geekhub.socialnetwork;

public class FailedAutoLoginException extends RuntimeException{
    private final String email;

    public FailedAutoLoginException(String email, Throwable cause) {
        super("Auto login was failed for user with email " + email, cause);
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}
