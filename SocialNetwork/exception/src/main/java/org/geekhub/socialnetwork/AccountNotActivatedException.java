package org.geekhub.socialnetwork;

public class AccountNotActivatedException extends RuntimeException {
    private final int accountId;

    public AccountNotActivatedException(String message, int accountId) {
        super(message);
        this.accountId = accountId;
    }

    public int getAccountId() {
        return accountId;
    }
}
