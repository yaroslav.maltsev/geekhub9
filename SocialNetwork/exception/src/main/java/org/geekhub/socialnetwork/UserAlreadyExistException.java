package org.geekhub.socialnetwork;

public class UserAlreadyExistException extends RuntimeException {
    private final String email;
    public UserAlreadyExistException(String email) {
        super("User with email " + email + " already exist!");
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}
