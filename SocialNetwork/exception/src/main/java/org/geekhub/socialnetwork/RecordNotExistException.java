package org.geekhub.socialnetwork;

public class RecordNotExistException extends RuntimeException {
    public RecordNotExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public RecordNotExistException(String message) {
        super(message);
    }
}
