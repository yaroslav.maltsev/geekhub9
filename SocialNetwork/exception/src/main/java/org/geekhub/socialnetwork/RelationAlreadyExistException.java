package org.geekhub.socialnetwork;

public class RelationAlreadyExistException extends RuntimeException {
    private final int userId;
    private final int friendId;

    public RelationAlreadyExistException(int userId, int friendId) {
        super(String.format("Friendship with users with ids %s and %s already exist!", userId, friendId));

        this.userId = userId;
        this.friendId = friendId;
    }

    public int getUserId() {
        return userId;
    }

    public int getFriendId() {
        return friendId;
    }
}
