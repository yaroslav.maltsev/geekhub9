package org.geekhub.crypto.codecs.translator;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.testng.Assert.assertEquals;

public class SchedulerTest {
    private TranslateCacheStorage cacheStorage;
    private Scheduler scheduler;

    @BeforeMethod
    public void setUp() {
        cacheStorage = new TranslateCacheStorage();
        scheduler = new Scheduler(cacheStorage);
    }

    @Test
    public void test_addTranslatingInCache_when_maps_are_empty_is_success() {
        scheduler.addTranslatingInCache();
    }

    @Test
    public void test_addTranslatingInCache_when_cache_has_many_elems_and_scheduler_is_empty() {
        Map<String, String> map = new ConcurrentHashMap<>();

        for (int i = 0; i < 100; i++) {
            map.put("" + i, "" + (i + 1));
        }

        cacheStorage.setEngToUkrCache(map);
        cacheStorage.setUkrToEngCache(map);

        scheduler.addTranslatingInCache();

        Map<String, String> engToUkrCache = cacheStorage.getEngToUkrCache();
        Map<String, String> ukrToEngCache = cacheStorage.getUkrToEngCache();

        assertEquals(engToUkrCache.size(), map.size());
        assertEquals(ukrToEngCache.size(), map.size());
    }

    @Test
    public void test_run_when_cache_has_is_empty_and_scheduler_has_many_elems() {
        int amountOfElems = 100;
        for (int i = 0; i < amountOfElems; i++) {
            scheduler.setTranslateFromEngInCache("" + i, "" + (i + 1));
            scheduler.setTranslateFromUkrInCache("" + i, "" + (i + 1));
        }

        scheduler.run();

        Map<String, String> engToUkrCache = cacheStorage.getEngToUkrCache();
        Map<String, String> ukrToEngCache = cacheStorage.getUkrToEngCache();

        assertEquals(engToUkrCache.size(), amountOfElems);
        assertEquals(ukrToEngCache.size(), amountOfElems);
    }

    @Test
    public void test_run_when_cache_has_and_scheduler_have_many_elems() {
        Map<String, String> map = new ConcurrentHashMap<>();
        int amountOfElems = 100;
        for (int i = 0; i < amountOfElems; i++) {
            map.put("" + i, "" + (i + 1));
        }

        for (int i = amountOfElems; i < amountOfElems * 2; i++) {
            scheduler.setTranslateFromEngInCache("" + i, "" + (i + 1));
            scheduler.setTranslateFromUkrInCache("" + i, "" + (i + 1));
        }

        cacheStorage.setEngToUkrCache(map);
        cacheStorage.setUkrToEngCache(map);

        scheduler.run();

        Map<String, String> engToUkrCache = cacheStorage.getEngToUkrCache();
        Map<String, String> ukrToEngCache = cacheStorage.getUkrToEngCache();

        assertEquals(engToUkrCache.size(), amountOfElems * 2);
        assertEquals(ukrToEngCache.size(), amountOfElems * 2);
    }
}