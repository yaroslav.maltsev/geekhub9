package org.geekhub.crypto.codecs.translator;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.testng.Assert.assertEquals;

public class OnlineTranslatorTest {
    private OnlineTranslator onlineTranslator;
    @BeforeMethod
    public void setUp() {
        onlineTranslator = new OnlineTranslator(mock(TranslateCacheManager.class));
    }

    @Test
    public void test_when_input_in_encode_is_blank_is_success() {
        String encodedBlankInput = onlineTranslator.encode("    ");
        assertEquals(encodedBlankInput, "    ");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_in_encode_is_null() {
        onlineTranslator.encode(null);
    }

    @Test
    public void test_sentence_encoding_is_success() {
        String text = "я тебе люблю";
        String expectedResult = "i love you";
        String answer = onlineTranslator.encode(text);
        assertEquals(answer, expectedResult);
    }

    @Test
    public void test_when_input_in_decode_is_blank_is_success() {
        String decodedBlankInput = onlineTranslator.decode("    ");
        assertEquals(decodedBlankInput, "    ");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_in_decode_is_null() {
        onlineTranslator.decode(null);
    }

    @Test
    public void test_sentence_decoding_is_success() {
        String text = "i love you";
        String expectedResult = "я тебе люблю";
        String answer = onlineTranslator.decode(text);
        assertEquals(answer, expectedResult);
    }
}