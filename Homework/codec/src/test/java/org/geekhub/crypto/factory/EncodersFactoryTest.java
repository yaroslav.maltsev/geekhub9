package org.geekhub.crypto.factory;

import org.geekhub.crypto.Encoder;
import org.geekhub.crypto.codecs.CaesarCodec;
import org.geekhub.crypto.codecs.MorseCodec;
import org.geekhub.crypto.codecs.VigenereCodec;
import org.geekhub.crypto.codecs.VigenereOverCaesar;
import org.geekhub.crypto.codecs.translator.TranslatorCodec;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;

import static org.testng.Assert.assertEquals;

public class EncodersFactoryTest {

    private EncodersFactory encodersFactory;

    @BeforeMethod
    public void setUp() {
        Encoder[] allEncoders = {
                new CaesarCodec(0),
                new MorseCodec(new HashMap<>()),
                new VigenereCodec(null),
                new VigenereOverCaesar(null, null),
                new TranslatorCodec(null, null)
        };
        encodersFactory = new EncodersFactory(allEncoders);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_algorithm_is_null() {
        encodersFactory.getEncoder(null);
    }

    @Test
    public void getting_CaesarCodec_is_success() {
        Encoder encoder = encodersFactory.getEncoder(Algorithms.CAESAR);

        assertEquals(encoder.getClass(), CaesarCodec.class);
    }

    @Test
    public void getting_MorseCodec_is_success() {
        Encoder encoder = encodersFactory.getEncoder(Algorithms.MORSE);

        assertEquals(encoder.getClass(), MorseCodec.class);
    }

    @Test
    public void getting_VigenereCodec_is_success() {
        Encoder encoder = encodersFactory.getEncoder(Algorithms.VIGENERE);

        assertEquals(encoder.getClass(), VigenereCodec.class);
    }

    @Test
    public void getting_VigenereOverCaesarCodec_is_success() {
        Encoder encoder = encodersFactory.getEncoder(Algorithms.VIGENERE_OVER_CAESAR);

        assertEquals(encoder.getClass(), VigenereOverCaesar.class);
    }

    @Test
    public void test_to_check_whether_all_codecs_are_included() {
        Algorithms[] arrOfAlgorithmEnums = Algorithms.values();
        for (Algorithms elem : arrOfAlgorithmEnums) {
            encodersFactory.getEncoder(elem);
        }
    }
}