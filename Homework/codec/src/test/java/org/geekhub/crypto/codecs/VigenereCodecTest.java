package org.geekhub.crypto.codecs;

import org.geekhub.crypto.exceptions.IllegalInputException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class VigenereCodecTest {
    private VigenereCodec vigenereCodec;

    @BeforeMethod
    public void setUp() {
        vigenereCodec = new VigenereCodec("geekhub");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void encode_when_input_is_empty_is_success() {
        String input = "";
        vigenereCodec.encode(input);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_for_encoding_is_null() {
        vigenereCodec.encode(null);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_encoding_is_not_in_english() {
        String input = "не англійський текст";
        vigenereCodec.encode(input);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_encoding_is_sign() {
        String input = "- , .";
        vigenereCodec.encode(input);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_encoding_is_number() {
        String input = "1245";
        vigenereCodec.encode(input);
    }

    @Test
    public void encode_when_input_is_sentence_is_success() {
        String input = "test sentence with spaces";
        String expectedOutput = "ziwd zyozirml qjzl wzhwfy";

        String output = vigenereCodec.encode(input);

        assertEquals(output, expectedOutput);
    }

    @Test
    public void encode_when_input_is_letter_is_success() {
        String input = "l";
        String expectedOutput = "r";

        String output = vigenereCodec.encode(input);

        assertEquals(output, expectedOutput);
    }

    @Test
    public void encode_when_letters_are_in_different_registers_is_success() {
        String input = "TeStWoRd";
        String expectedOutput = "ziwddisj";

        String output = vigenereCodec.encode(input);

        assertEquals(output, expectedOutput);
    }


    @Test(expectedExceptions = IllegalArgumentException.class)
    public void decode_when_input_is_empty_is_success(){
        String input = "";

        vigenereCodec.decode(input);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_for_decoding_is_null(){
        vigenereCodec.decode(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_for_decoding_is_empty(){
        vigenereCodec.decode("");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_for_decoding_is_blank(){
        vigenereCodec.decode("      ");
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_decoding_is_not_in_english(){
        String input = "не англійський текст";
        vigenereCodec.decode(input);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_decoding_is_sign(){
        String input = "- , .";

        vigenereCodec.decode(input);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_decoding_is_number() {
        String input = "1245";

        vigenereCodec.decode(input);
    }

    @Test
    public void decode_when_input_is_sentence_is_success() {
        String input = "ziwd zyozirml qjzl wzhwfy";
        String expectedOutput = "test sentence with spaces";

        String output = vigenereCodec.decode(input);

        assertEquals(output, expectedOutput);
    }

    @Test
    public void decode_when_input_is_letter_is_success() {
        String input = "r";
        String expectedOutput = "l";

        String output = vigenereCodec.decode(input);

        assertEquals(output, expectedOutput);
    }

    @Test
    public void decode_when_letters_are_in_different_registers_is_success() {
        String input = "ZiWdDiSj";
        String expectedOutput = "testword";

        String output = vigenereCodec.decode(input);

        assertEquals(output, expectedOutput);
    }
}