package org.geekhub.crypto.codecs.translator;

import org.geekhub.crypto.exceptions.IllegalInputException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static org.testng.Assert.assertEquals;

public class OfflineTranslatorTest {

    private OfflineTranslator offlineTranslator;

    @BeforeMethod
    public void setUp() {
        offlineTranslator = new OfflineTranslator(Map.ofEntries(
                Map.entry("gun", "пістолет"),
                Map.entry("some", "дещо"),
                Map.entry("air", "повітря"),
                Map.entry("idea", "ідея")
        ));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_is_null_in_encode() {
        offlineTranslator.encode(null);
    }

    @Test
    public void test_when_input_in_encode_is_blank_is_success() {
        String encodedBlankInput = offlineTranslator.decode("    ");
        assertEquals(encodedBlankInput, "    ");
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_word_in_encode_not_exist_in_vocabulary() {
        offlineTranslator.encode("це_слово_відсутнє");
    }

    @Test
    public void test_word_encoding_is_success() {
        String answer = offlineTranslator.encode("пістолет");
            assertEquals(answer, "gun");
    }

    @Test
    public void test_sentence_encoding_is_success() {
        String text = "дещо повітря ідея";
        String expectedResult = "some air idea";
        String answer = offlineTranslator.encode(text);
        assertEquals(answer, expectedResult);
    }


    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_is_null_in_decode() {
        offlineTranslator.decode(null);
    }

    @Test
    public void test_when_input_in_decode_is_blank_is_success() {
        String decodedBlankInput = offlineTranslator.decode("    ");
        assertEquals(decodedBlankInput, "    ");
    }
    @Test (expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_word_in_decode_not_exist_in_vocabulary() {
        offlineTranslator.decode("це_слово_відсутнє");
    }

    @Test
    public void test_word_decoding_is_success() {
        String answer = offlineTranslator.decode("gun");
        assertEquals(answer, "пістолет");
    }

    @Test
    public void test_sentence_decoding_is_success() {
        String text = "some air idea";
        String expectedResult = "дещо повітря ідея";
        String answer = offlineTranslator.decode(text);
        assertEquals(answer, expectedResult);
    }
}