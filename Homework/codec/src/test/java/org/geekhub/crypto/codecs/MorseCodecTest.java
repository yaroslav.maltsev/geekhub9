package org.geekhub.crypto.codecs;

import org.geekhub.crypto.exceptions.IllegalInputException;
import org.testng.annotations.Test;

import java.util.Map;

import static org.testng.Assert.assertEquals;

public class MorseCodecTest {
    private MorseCodec morseCodec = new MorseCodec(Map.ofEntries(
            Map.entry("a", ".-"),
            Map.entry("b", "-..."),
            Map.entry("c", "-.-."),
            Map.entry("d", "-.."),
            Map.entry("e", "."),
            Map.entry("f", "..-."),
            Map.entry("g", "--."),
            Map.entry("h", "...."),
            Map.entry("i", ".."),
            Map.entry("j", ".---"),
            Map.entry("k", "-.-"),
            Map.entry("l", ".-.."),
            Map.entry("m", "--"),
            Map.entry("n", "-."),
            Map.entry("o", "---"),
            Map.entry("p", ".--."),
            Map.entry("q", "--.-"),
            Map.entry("r", ".-."),
            Map.entry("s", "..."),
            Map.entry("t", "-"),
            Map.entry("u", "..-"),
            Map.entry("v", "...-"),
            Map.entry("w", ".--"),
            Map.entry("x", "-..-"),
            Map.entry("y", "-.--"),
            Map.entry("z", "--.."),
            Map.entry("\t", ".......")
    ));

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_for_encoding_is_empty() {
        morseCodec.encode("");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_for_encoding_is_null() {
        morseCodec.encode(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_for_encoding_is_blank() {
        morseCodec.encode("     ");
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_encoding_is_not_in_english() {
        String input = "не англійський текст";

        morseCodec.encode(input);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_encoding_is_sign() {
        String input = "- , .";

        morseCodec.encode(input);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_encoding_is_number() {
        String input = "1245";

        morseCodec.encode(input);
    }

    @Test
    public void encode_when_input_is_sentence_is_success() {
        String input = "test sentence with spaces";
        String expectedOutput = "-/./.../-/......./..././-./-/./-./-.-././......./.--/../-/..../......./.../.--./.-/-.-././.../";

        String output = morseCodec.encode(input);

        assertEquals(output, expectedOutput);
    }

    @Test
    public void encode_when_input_is_letter_is_success() {
        String input = "l";
        String expectedOutput = ".-../";

        String output = morseCodec.encode(input);

        assertEquals(output, expectedOutput);
    }

    @Test
    public void encode_when_letters_are_in_different_registers_is_success() {
        String input = "tImE FoR TeSt";
        String expectedOutput = "-/../--/./......./..-./---/.-./......./-/./.../-/";

        String output = morseCodec.encode(input);

        assertEquals(output, expectedOutput);
    }


    @Test(expectedExceptions = IllegalArgumentException.class)
    public void decode_when_input_is_empty_is_success() {
        String input = "";
        morseCodec.decode(input);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_for_decoding_is_null() {
        morseCodec.decode(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_for_decoding_is_empty() {
        morseCodec.decode("");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_for_decoding_is_blank() {
        morseCodec.decode("    ");
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_decoding_is_not_in_english() {
        String input = "не англійський текст";
        morseCodec.decode(input);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_decoding_is_sign() {
        String input = "- , .";

        morseCodec.decode(input);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_decoding_is_number() {
        String input = "1245";

        morseCodec.decode(input);
    }

    @Test
    public void decode_when_input_is_sentence_is_success() {
        String input = "-/./.../-/......./..././-./-/./-./-.-././......./.--/../-/..../......./.../.--./.-/-.-././.../";
        String expectedOutput = "test sentence with spaces";

        String output = morseCodec.decode(input);

        assertEquals(output, expectedOutput);
    }

    @Test
    public void decode_when_input_is_letter_is_success() {
        String input = ".-../";
        String expectedOutput = "l";

        String output = morseCodec.decode(input);

        assertEquals(output, expectedOutput);
    }

    @Test
    public void decode_when_input_is_spaces_is_success() {
        String input = "......./......./......./......./......./......./";
        String expectedOutput = "      ";

        String output = morseCodec.decode(input);

        assertEquals(output, expectedOutput);
    }
}