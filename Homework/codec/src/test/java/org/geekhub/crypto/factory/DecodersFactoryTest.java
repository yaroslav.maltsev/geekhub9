package org.geekhub.crypto.factory;

import org.geekhub.crypto.Decoder;
import org.geekhub.crypto.codecs.CaesarCodec;
import org.geekhub.crypto.codecs.MorseCodec;
import org.geekhub.crypto.codecs.VigenereCodec;
import org.geekhub.crypto.codecs.VigenereOverCaesar;
import org.geekhub.crypto.codecs.translator.TranslatorCodec;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;

import static org.testng.Assert.assertEquals;

public class DecodersFactoryTest {

    private DecodersFactory decodersFactory;

    @BeforeMethod
    public void setUp() {
        Decoder[] allEncoders = {
                new CaesarCodec(0),
                new MorseCodec(new HashMap<>()),
                new VigenereCodec(null),
                new VigenereOverCaesar(null, null),
                new TranslatorCodec(null, null)
        };
        decodersFactory = new DecodersFactory(allEncoders);
    }

    @Test
    public void getting_CaesarCodec_is_success() {
        Decoder decoder = decodersFactory.getDecoder(Algorithms.CAESAR);

        assertEquals(decoder.getClass(), CaesarCodec.class);
    }

    @Test
    public void getting_MorseCodec_is_success() {
        Decoder decoder = decodersFactory.getDecoder(Algorithms.MORSE);

        assertEquals(decoder.getClass(), MorseCodec.class);
    }

    @Test
    public void getting_VigenereCodec_is_success() {
        Decoder decoder = decodersFactory.getDecoder(Algorithms.VIGENERE);

        assertEquals(decoder.getClass(), VigenereCodec.class);
    }

    @Test
    public void getting_VigenereOverCaesarCodec_is_success() {
        Decoder decoder = decodersFactory.getDecoder(Algorithms.VIGENERE_OVER_CAESAR);

        assertEquals(decoder.getClass(), VigenereOverCaesar.class);
    }

    @Test
    public void test_to_check_whether_all_codecs_are_included() {
        Algorithms[] arrOfAlgorithmEnums = Algorithms.values();
        for (Algorithms elem : arrOfAlgorithmEnums) {
            decodersFactory.getDecoder(elem);
        }
    }
}