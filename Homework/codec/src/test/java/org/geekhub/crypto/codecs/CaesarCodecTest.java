package org.geekhub.crypto.codecs;

import org.geekhub.crypto.exceptions.IllegalInputException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CaesarCodecTest {

    private CaesarCodec caesarCodec;
    @BeforeMethod
    public void setUp() {
        caesarCodec = new CaesarCodec(7);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void encode_when_input_is_empty_is_success() {
        String input = "";

        caesarCodec.encode(input);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_for_encoding_is_null() {
        caesarCodec.encode(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_for_encoding_is_blank() {
        caesarCodec.encode("     ");
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_encoding_is_not_in_english() {
        String input = "не англійський текст";
        caesarCodec.encode(input);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_encoding_is_sign() {
        String input = "- , .";

        caesarCodec.encode(input);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_encoding_is_number() {
        String input = "1245";

        caesarCodec.encode(input);
    }

    @Test
    public void encode_when_input_is_sentence_is_success() {
        String input = "test sentence with spaces";
        String expectedOutput = "alza zlualujl dpao zwhjlz";

        String output = caesarCodec.encode(input);

        assertEquals(output, expectedOutput);
    }

    @Test
    public void encode_when_input_is_letter_is_success() {
        String input = "l";
        String expectedOutput = "s";

        String output = caesarCodec.encode(input);

        assertEquals(output, expectedOutput);
    }

    @Test
    public void encode_when_letters_are_in_different_registers_is_success() {
        String input = "TeStWoRd";
        String expectedOutput = "alzadvyk";

        String output = caesarCodec.encode(input);

        assertEquals(output, expectedOutput);
    }


    @Test(expectedExceptions = IllegalArgumentException.class)
    public void decode_when_input_is_empty_is_success() {
        String input = "";

        caesarCodec.decode(input);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_for_decoding_is_null() {
        caesarCodec.decode(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_for_decoding_is_blank() {
        caesarCodec.decode("    ");
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_decoding_is_not_in_english() {
        String input = "не англійський текст";
        caesarCodec.decode(input);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_decoding_is_sign() {
        String input = "- , .";

        caesarCodec.decode(input);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_decoding_is_number() {
        String input = "1245";

        caesarCodec.decode(input);
    }

    @Test
    public void decode_when_input_is_sentence_is_success() {
        String input = "alza zlualujl dpao zwhjlz";
        String expectedOutput = "test sentence with spaces";

        String output = caesarCodec.decode(input);

        assertEquals(output, expectedOutput);
    }

    @Test
    public void decode_when_input_is_letter_is_success() {
        String input = "s";
        String expectedOutput = "l";

        String output = caesarCodec.decode(input);

        assertEquals(output, expectedOutput);
    }

    @Test
    public void decode_when_letters_are_in_different_registers_is_success() {
        String input = "AlZaDvYk";
        String expectedOutput = "testword";

        String output = caesarCodec.decode(input);

        assertEquals(output, expectedOutput);
    }
}