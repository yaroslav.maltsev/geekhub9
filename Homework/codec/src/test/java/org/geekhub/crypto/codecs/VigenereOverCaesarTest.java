
package org.geekhub.crypto.codecs;

import org.geekhub.crypto.exceptions.IllegalInputException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class VigenereOverCaesarTest {
    private CaesarCodec caesarCodec = new CaesarCodec(7);
    private VigenereCodec vigenereCodec = new VigenereCodec("geekhub");

    private VigenereOverCaesar vigenereOverCaesarCodec;

    @BeforeMethod
    public void setUp() {
        vigenereOverCaesarCodec = new VigenereOverCaesar(caesarCodec, vigenereCodec);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void encode_when_input_is_empty_is_success() {
        String input = "";
        vigenereOverCaesarCodec.encode(input);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_for_encoding_is_null() {
        vigenereOverCaesarCodec.encode(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_for_encoding_is_empty() {
        vigenereOverCaesarCodec.encode("");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_for_encoding_is_blank() {
        vigenereOverCaesarCodec.encode("       ");
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_encoding_is_not_in_english() {
        String input = "не англійський текст";
        vigenereOverCaesarCodec.encode(input);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_encoding_is_sign() {
        String input = "- , .";
        vigenereOverCaesarCodec.encode(input);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_encoding_is_number() {
        String input = "1245";
        vigenereOverCaesarCodec.encode(input);
    }

    @Test
    public void encode_when_input_is_sentence_is_success() {
        String input = "test sentence with spaces";
        String expectedOutput = "gpdk gfvgpyts xqgs dgodmf";

        String output = vigenereOverCaesarCodec.encode(input);

        assertEquals(output, expectedOutput);
    }

    @Test
    public void encode_when_input_is_letter_is_success() {
        String input = "l";
        String expectedOutput = "y";

        String output = vigenereOverCaesarCodec.encode(input);

        assertEquals(output, expectedOutput);
    }

    @Test
    public void encode_when_letters_are_in_different_registers_is_success() {
        String input = "TeStWoRd";
        String expectedOutput = "gpdkkpzq";

        String output = vigenereOverCaesarCodec.encode(input);

        assertEquals(output, expectedOutput);
    }


    @Test(expectedExceptions = IllegalArgumentException.class)
    public void decode_when_input_is_empty_is_success() {
        String input = "";

        vigenereOverCaesarCodec.decode(input);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_for_decoding_is_null() {
        vigenereOverCaesarCodec.decode(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_for_decoding_is_empty() {
        vigenereOverCaesarCodec.decode("");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_for_decoding_is_blank() {
        vigenereOverCaesarCodec.decode("     ");
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_decoding_is_not_in_english() {
        String input = "не англійський текст";
        vigenereOverCaesarCodec.decode(input);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_decoding_is_sign() {
        String input = "- , .";

        vigenereOverCaesarCodec.decode(input);
    }

    @Test(expectedExceptions = IllegalInputException.class)
    public void should_throwException_when_input_for_decoding_is_number() {
        String input = "1245";

        vigenereOverCaesarCodec.decode(input);
    }

    @Test
    public void decode_when_input_is_sentence_is_success() {
        String input = "gpdk gfvgpyts xqgs dgodmf";
        String expectedOutput = "test sentence with spaces";

        String output = vigenereOverCaesarCodec.decode(input);

        assertEquals(output, expectedOutput);
    }

    @Test
    public void decode_when_input_is_letter_is_success() {
        String input = "y";
        String expectedOutput = "l";

        String output = vigenereOverCaesarCodec.decode(input);

        assertEquals(output, expectedOutput);
    }

    @Test
    public void decode_when_letters_are_in_different_registers_is_success() {
        String input = "GpDkKpZq";
        String expectedOutput = "testword";

        String output = vigenereOverCaesarCodec.decode(input);

        assertEquals(output, expectedOutput);
    }
}