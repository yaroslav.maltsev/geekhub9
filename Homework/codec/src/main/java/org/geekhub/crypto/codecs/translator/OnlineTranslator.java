package org.geekhub.crypto.codecs.translator;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.geekhub.crypto.exceptions.NoInternetException;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Objects;

@Service
class OnlineTranslator {
    private static final String ENGLISH_LANG = "en";
    private static final String UKRAINIAN_LANG = "uk";
    private TranslateCacheManager translateCacheManager;

    private static final String KEY = "key=AIzaSyB2HijQLlsmI1udH9ARl45oC5eAj4XfjTw";

    public OnlineTranslator(TranslateCacheManager translateCacheManager) {
        this.translateCacheManager = translateCacheManager;
    }

    String decode(String input) {

        if (input == null) {
            throw new IllegalArgumentException("Input must be not null or blank");
        } else if (input.isBlank()) {
            return input;
        }

        String translateFromEng = translateCacheManager.getTranslateFromEng(input);
        if (Objects.nonNull(translateFromEng)) {
            return translateFromEng;
        } else {
            String translateSentence = translateSentence(input, ENGLISH_LANG, UKRAINIAN_LANG);
            translateCacheManager.setTranslateFromEngInCache(input, translateSentence);
            return translateSentence;
        }
    }

    String encode(String input) {
        if (input == null) {
            throw new IllegalArgumentException("Input must be not null or blank");
        } else if (input.isBlank()) {
            return input;
        }

        String translateFromEng = translateCacheManager.getTranslateFromUkr(input);
        if (Objects.nonNull(translateFromEng)) {
            return translateFromEng;
        } else {
            String translateSentence = translateSentence(input, UKRAINIAN_LANG, ENGLISH_LANG);
            translateCacheManager.setTranslateFromUkrInCache(input, translateSentence);
            return translateSentence;
        }
    }

    private String translateSentence(String text, String sourceLang, String targetLang) {
        String adaptedText = text.replace(" ", "_");
        String responseJson = getJsonWithTranslating(adaptedText, sourceLang, targetLang);
        return getTranslatedTextFromJson(responseJson);
    }

    private String getTranslatedTextFromJson(String jsonText) {

        Gson gson = new Gson();
        JsonObject jsonObject = gson.fromJson(jsonText, JsonObject.class);
        JsonObject data = jsonObject.getAsJsonObject("data");
        JsonArray translations = data.getAsJsonArray("translations");
        JsonElement firstMember = translations.get(0);

        return firstMember.getAsJsonObject().get("translatedText").getAsString();
    }

    private String getJsonWithTranslating(String word, String sourceLang, String targetLang) {
        String query = KEY
                + "&source=" + sourceLang
                + "&target=" + targetLang
                + "&q=" + word;
        HttpRequest request = getRequestText(query);
        return getResponseJson(request);

    }

    private String getResponseJson(HttpRequest request) {
        String responseJson;
        try {
            responseJson = HttpClient.newHttpClient()
                    .send(request, HttpResponse.BodyHandlers.ofString())
                    .body();
        } catch (IOException | InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new NoInternetException("No internet connection! Try again.", e);
        }

        return responseJson;
    }

    private HttpRequest getRequestText(String query) {
        return HttpRequest.newBuilder()
                .uri(URI.create("https://www.googleapis.com/language/translate/v2?" + query))
                .header("Referer", "https://www.daytranslations.com/free-translation-online/")
                .GET()
                .build();
    }
}
