package org.geekhub.crypto.exceptions;

public class NoInternetException extends RuntimeException{
    public NoInternetException(String message, Throwable cause) {
        super(message, cause);
    }
}
