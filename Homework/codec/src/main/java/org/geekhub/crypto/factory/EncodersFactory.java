package org.geekhub.crypto.factory;

import org.geekhub.crypto.Encoder;
import org.geekhub.crypto.exceptions.CodecUnsupportedException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class EncodersFactory {
    private Map<Algorithms, Encoder> enumEncoderMap;
    public EncodersFactory(Encoder[] allEncoders) {
        this.enumEncoderMap = getEnumEncoderMap(allEncoders);
    }

    private Map<Algorithms, Encoder> getEnumEncoderMap(Encoder[] allEncoders) {
        Map<Algorithms, Encoder> map = new HashMap<>();
        for (Encoder encoder : allEncoders) {
            map.put(encoder.getSupportedAlgorithm(), encoder);
        }
        return map;
    }

    public Encoder getEncoder(Algorithms algorithms) {
        if (algorithms == null) {
            throw new IllegalArgumentException("Algorithm must be not null!");
        }
        Encoder encoder = enumEncoderMap.get(algorithms);
        if (encoder == null) {
            throw new CodecUnsupportedException("It's impossible to found codec", algorithms);
        }
        return encoder;
    }
}