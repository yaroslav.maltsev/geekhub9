package org.geekhub.crypto.codecs.translator;

import org.springframework.stereotype.Service;

@Service
public class TranslateCacheManager {
    private TranslateCacheStorage translateCacheStorage;
    private Scheduler scheduler;

    public TranslateCacheManager(TranslateCacheStorage translateCacheStorage, Scheduler scheduler) {
        this.translateCacheStorage = translateCacheStorage;
        this.scheduler = scheduler;
    }

    public String getTranslateFromUkr(String ukr) {
        return translateCacheStorage.getTranslateFromUkr(ukr);
    }

    public String getTranslateFromEng(String eng) {
        return translateCacheStorage.getTranslateFromEng(eng);
    }

    public void setTranslateFromUkrInCache(String ukr, String eng) {
        scheduler.setTranslateFromUkrInCache(ukr, eng);
    }

    public void setTranslateFromEngInCache(String eng, String ukr) {
        scheduler.setTranslateFromEngInCache(eng, ukr);
    }
}
