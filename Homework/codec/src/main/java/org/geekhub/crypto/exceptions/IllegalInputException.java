package org.geekhub.crypto.exceptions;

public class IllegalInputException extends RuntimeException {
    private final String illegalText;

    public IllegalInputException(String message, String illegalText) {
        super(message);
        this.illegalText = illegalText;
    }

    public String getIllegalText() {
        return illegalText;
    }
}
