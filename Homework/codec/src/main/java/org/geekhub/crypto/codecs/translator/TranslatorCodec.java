package org.geekhub.crypto.codecs.translator;

import org.geekhub.crypto.Decoder;
import org.geekhub.crypto.Encoder;
import org.geekhub.crypto.factory.Algorithms;
import org.geekhub.crypto.exceptions.NoInternetException;
import org.springframework.stereotype.Component;

@Component(value = "TRANSLATOR")
public class TranslatorCodec implements Encoder, Decoder {
    private OnlineTranslator onlineTranslator;
    private OfflineTranslator offlineTranslator;

    public TranslatorCodec(OnlineTranslator onlineTranslator, OfflineTranslator offlineTranslator) {
        this.onlineTranslator = onlineTranslator;
        this.offlineTranslator = offlineTranslator;
    }

    @Override
    public String decode(String input) {
        validateInput(input);

        try {
            return onlineTranslator.decode(input);
        } catch (NoInternetException e) {
            return offlineTranslator.decode(input);
        }
    }

    @Override
    public String encode(String input) {
        validateInput(input);
        try {
            return onlineTranslator.encode(input);
        } catch (NoInternetException e) {
            return offlineTranslator.encode(input);
        }
    }

    @Override
    public Algorithms getSupportedAlgorithm() {
        return Algorithms.TRANSLATOR;
    }


    private void validateInput(String input) {
        if (input == null || input.isBlank()) {
            throw new IllegalArgumentException("Input must be not blank or null");
        }
    }
}
