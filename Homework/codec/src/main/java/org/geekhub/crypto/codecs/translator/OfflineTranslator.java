package org.geekhub.crypto.codecs.translator;

import org.geekhub.crypto.exceptions.IllegalInputException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.stream.Collectors;

@Service
class OfflineTranslator {
    private final Map<String, String> engToUkrVocabulary;
    private final Map<String, String> ukrToEngVocabulary;

    public OfflineTranslator(@Qualifier("vocabularyMap") Map<String, String> vocabularyMap) {
        engToUkrVocabulary = vocabularyMap;
        ukrToEngVocabulary = engToUkrVocabulary.entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
    }

    String decode(String input) {
        if (input == null) {
            throw new IllegalArgumentException("Input must be not null or blank");
        } else if (input.isBlank()) {
            return input;
        }
        return codeInput(input, engToUkrVocabulary);
    }

    String encode(String input) {
        if (input == null) {
            throw new IllegalArgumentException("Input must be not null or blank");
        } else if (input.isBlank()) {
            return input;
        }
        return codeInput(input, ukrToEngVocabulary);
    }

    private String codeInput(String input, Map<String, String> choosedMap) {
        String spaceString = " ";
        String[] arrOfWords = input.split(spaceString);
        StringBuilder result = new StringBuilder();

        int arrOfWordsLength = arrOfWords.length;
        for (int i = 0; i < arrOfWordsLength; i++) {
            String elem = arrOfWords[i];
            String codedWord = codeWord(elem, choosedMap);

            result.append(codedWord);
            if (arrOfWordsLength - 1 != i) {
                result.append(spaceString);
            }
        }

        return result.toString();
    }

    private String codeWord(String word, Map<String, String> choosedMap) {
        String codedWord = choosedMap.get(word);

        if (codedWord == null) {
            throw new IllegalInputException("No such word in vocabulary", word);
        }

        return codedWord;
    }
}
