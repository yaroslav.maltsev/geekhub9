package org.geekhub.crypto.codecs;

import org.geekhub.crypto.Decoder;
import org.geekhub.crypto.Encoder;
import org.geekhub.crypto.factory.Algorithms;
import org.springframework.stereotype.Component;

@Component(value = "VIGENERE_OVER_CAESAR")
public class VigenereOverCaesar implements Decoder, Encoder {
    private final CaesarCodec caesarCodec;
    private final VigenereCodec vigenereCodec;

    public VigenereOverCaesar(CaesarCodec caesarCodec, VigenereCodec vigenereCodec) {
        this.caesarCodec = caesarCodec;
        this.vigenereCodec = vigenereCodec;
    }

    @Override
    public String decode(String input) {
        String decodedWithVigenere = vigenereCodec.decode(input);
        return caesarCodec.decode(decodedWithVigenere);
    }

    @Override
    public String encode(String input) {
        String encodedWithCaesar = caesarCodec.encode(input);
        return vigenereCodec.encode(encodedWithCaesar);
    }

    @Override
    public Algorithms getSupportedAlgorithm() {
        return Algorithms.VIGENERE_OVER_CAESAR;
    }
}
