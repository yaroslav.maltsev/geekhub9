package org.geekhub.crypto.factory;

import org.geekhub.crypto.Decoder;
import org.geekhub.crypto.exceptions.CodecUnsupportedException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class DecodersFactory {
    private Map<Algorithms, Decoder> enumDecoderMap;
    
    public DecodersFactory(Decoder[] allDecoders) {
        this.enumDecoderMap = getEnumdecoderMap(allDecoders);
    }

    private Map<Algorithms, Decoder> getEnumdecoderMap(Decoder[] alldecoders) {
        Map<Algorithms, Decoder> map = new HashMap<>();
        for (Decoder decoder : alldecoders) {
            map.put(decoder.getSupportedAlgorithm(), decoder);
        }
        return map;
    }

    public Decoder getDecoder(Algorithms algorithms) {
        if (algorithms == null) {
            throw new IllegalArgumentException("Algorithm must be not null!");
        }

        Decoder decoder = enumDecoderMap.get(algorithms);
        if (decoder == null) {
            throw new CodecUnsupportedException("It's impossible to found codec", algorithms);
        }
        return decoder;
    }
}