package org.geekhub.crypto.codecs;

import org.geekhub.crypto.Decoder;
import org.geekhub.crypto.Encoder;
import org.geekhub.crypto.factory.Algorithms;
import org.geekhub.crypto.exceptions.IllegalInputException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.function.Function;
import java.util.function.IntUnaryOperator;

@Component(value = "CAESAR")
public class CaesarCodec implements Encoder, Decoder {
    private final int caesarShift;
    private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz";
    private static final String SPACE_STRING = " ";

    public CaesarCodec(@Value("${caesar.shift}") int caesarShift) {
        this.caesarShift = caesarShift;
    }

    @Override
    public Algorithms getSupportedAlgorithm() {
        return Algorithms.CAESAR;
    }

    @Override
    public String encode(String input) {
        validateInput(input);

        return executeInputCoding(input, this::encodeLetter);
    }


    @Override
    public String decode(String input) {
        validateInput(input);

        return executeInputCoding(input, this::decodeLetter);
    }

    private String executeInputCoding(String input, Function<String, Character> func) {
        StringBuilder result = new StringBuilder();
        for (String letter : input.split("")) {
            if (SPACE_STRING.equals(letter)) {
                result.append(SPACE_STRING);
            } else {
                result.append(func.apply(letter));
            }
        }
        return result.toString();
    }

    private char encodeLetter(String letter) {
        return codeLetter(letter, index -> index + caesarShift);
    }

    private char decodeLetter(String letter) {
        return codeLetter(letter, index -> index - caesarShift);
    }

    private char codeLetter(String letter, IntUnaryOperator func) {
        letter = letter.toLowerCase();
        int index = getIndexOfLetterFromAlphabet(letter);
        int indexOfLetterAfterShift = calculateIndexOfLetterAfterShift(func.applyAsInt(index));

        return ALPHABET.charAt(indexOfLetterAfterShift);
    }

    private static int calculateIndexOfLetterAfterShift(int indexOfLetterAfterShift) {
        while (indexOfLetterAfterShift >= ALPHABET.length()) {
            indexOfLetterAfterShift -= ALPHABET.length();
        }
        while (indexOfLetterAfterShift < 0) {
            indexOfLetterAfterShift += ALPHABET.length();
        }
        return indexOfLetterAfterShift;
    }

    private void validateInput(String input) {
        if (input == null || input.isBlank()) {
            throw new IllegalArgumentException("Input can't be empty, or null");
        }
    }

    private static int getIndexOfLetterFromAlphabet(String letter) {
        int index = ALPHABET.indexOf(letter);
        if (index == -1) {
            throw new IllegalInputException("Wrong character", letter);
        }
        return index;
    }
}
