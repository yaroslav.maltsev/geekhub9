package org.geekhub.crypto.factory;

public enum Algorithms {
    CAESAR,
    MORSE,
    VIGENERE,
    VIGENERE_OVER_CAESAR,
    TRANSLATOR
}