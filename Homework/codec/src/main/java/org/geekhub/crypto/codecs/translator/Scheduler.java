package org.geekhub.crypto.codecs.translator;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class Scheduler implements Runnable {
    private Map<String, String> ukrEngMap = new ConcurrentHashMap<>();
    private Map<String, String> engUkrMap = new ConcurrentHashMap<>();
    private TranslateCacheStorage translateCacheStorage;

    public Scheduler(TranslateCacheStorage translateCacheStorage) {
        this.translateCacheStorage = translateCacheStorage;
    }

    @Scheduled(fixedRate = 3600000)
    public void addTranslatingInCache() {
        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(10);
            service.execute(this);
        } finally {
            if (service != null) {
                service.shutdown();
            }
        }
    }

    public void setTranslateFromUkrInCache(String ukr, String eng) {
        ukrEngMap.put(ukr, eng);
    }

    public void setTranslateFromEngInCache(String eng, String ukr) {
        engUkrMap.put(eng, ukr);
    }

    @Override
    public void run() {
        Map<String, String> engToUkrCache = translateCacheStorage.getEngToUkrCache();
        Map<String, String> ukrToEngCache = translateCacheStorage.getUkrToEngCache();

        ukrEngMap.forEach(ukrToEngCache::put);
        engUkrMap.forEach(engToUkrCache::put);

        translateCacheStorage.setUkrToEngCache(ukrToEngCache);
        translateCacheStorage.setEngToUkrCache(engToUkrCache);
    }
}
