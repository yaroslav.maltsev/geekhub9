package org.geekhub.crypto;

import org.geekhub.crypto.factory.Algorithms;

public interface Decoder {

    String decode(String input);

    Algorithms getSupportedAlgorithm();
}
