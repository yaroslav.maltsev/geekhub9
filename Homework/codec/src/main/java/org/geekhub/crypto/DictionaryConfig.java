package org.geekhub.crypto;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
@ConfigurationProperties("")
public class DictionaryConfig {
    private final Map<String, String> morse = new HashMap<>();
    private final Map<String, String> vocabulary = new HashMap<>();

    @Bean("morseMap")
    public Map<String, String> getMorse() {
        return morse;
    }

    @Bean("vocabularyMap")
    public Map<String, String> getVocabulary() {
        return vocabulary;
    }
}