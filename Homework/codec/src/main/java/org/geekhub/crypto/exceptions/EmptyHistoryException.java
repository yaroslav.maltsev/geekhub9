package org.geekhub.crypto.exceptions;

public class EmptyHistoryException extends RuntimeException{
    public EmptyHistoryException(String message) {
        super(message);
    }
}
