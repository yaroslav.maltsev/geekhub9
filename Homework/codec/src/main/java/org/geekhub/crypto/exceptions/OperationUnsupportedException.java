package org.geekhub.crypto.exceptions;

public class OperationUnsupportedException extends RuntimeException {
    public OperationUnsupportedException(String message) {
        super(message);
    }

    public OperationUnsupportedException(String message, Throwable cause) {
        super(message, cause);
    }
}
