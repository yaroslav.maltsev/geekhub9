package org.geekhub.crypto;

import org.geekhub.crypto.factory.Algorithms;

public interface Encoder {

    String encode(String input);

    Algorithms getSupportedAlgorithm();
}
