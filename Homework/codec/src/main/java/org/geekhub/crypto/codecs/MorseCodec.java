package org.geekhub.crypto.codecs;

import org.geekhub.crypto.Decoder;
import org.geekhub.crypto.Encoder;
import org.geekhub.crypto.factory.Algorithms;
import org.geekhub.crypto.exceptions.IllegalInputException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

@Component(value = "MORSE")
public class MorseCodec implements Encoder, Decoder {
    private static final String ENCODED_SPACE = ".......";
    private static final String SPACE_STRING = " ";
    private static final String WORD_SEPARATOR = "/";
    private final Map<String, String> mapForEncoding;
    private final Map<String, String> mapForDecoding;

    public MorseCodec(@Qualifier("morseMap") Map<String, String> morseMap) {
        mapForEncoding = morseMap;
        mapForDecoding = mapForEncoding.entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
    }

    @Override
    public String encode(String input) {
        validateInput(input);
        String[] splitInput = input.split("");

        return executeInputCoding(splitInput, this::encodeLetter);
    }

    @Override
    public String decode(String input) {
        validateInput(input);
        String[] splitInput = input.split(WORD_SEPARATOR);

        return executeInputCoding(splitInput, this::decodeLetter);
    }

    @Override
    public Algorithms getSupportedAlgorithm() {
        return Algorithms.MORSE;
    }

    private String executeInputCoding(String[] splitInput, UnaryOperator<String> func) {
        StringBuilder result = new StringBuilder();
        for (String elem : splitInput) {
            String codedElem = func.apply(elem);
            result.append(codedElem);
        }

        return result.toString();
    }

    private String encodeLetter(String letter) {
        if (letter.equals(SPACE_STRING)) {
            return ENCODED_SPACE + WORD_SEPARATOR;
        } else {
            letter = letter.toLowerCase();
            String encodedLetter = getLetter(mapForEncoding, letter);

            return encodedLetter + WORD_SEPARATOR;
        }
    }

    private String decodeLetter(String letter) {
        if (letter.equals(ENCODED_SPACE)) {
            return SPACE_STRING;
        } else {
            return getLetter(mapForDecoding, letter);
        }
    }

    private void validateInput(String input) {
        if (input == null || input.isBlank()) {
            throw new IllegalArgumentException("Input can't be empty, or null");
        }
    }

    private String getLetter(Map<String, String> map, String letter) {
        String receivedLetter = map.get(letter);
        if (receivedLetter == null) {
            throw new IllegalInputException("\nLETTER IS WRONG", letter);
        }
        return receivedLetter;
    }
}