package org.geekhub.crypto.codecs;

import org.geekhub.crypto.Decoder;
import org.geekhub.crypto.Encoder;
import org.geekhub.crypto.factory.Algorithms;
import org.geekhub.crypto.exceptions.IllegalInputException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.function.BiFunction;
import java.util.function.IntBinaryOperator;

@Component(value = "VIGENERE")
public class VigenereCodec implements Encoder, Decoder {
    private String key;
    private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz";

    public VigenereCodec(@Value("${vigenere.key}") String key) {
        this.key = key;
    }

    @Override
    public String encode(String input) {
        validateInput(input);

        return executeInputCoding(input, VigenereCodec::encodeLetter);
    }

    @Override
    public String decode(String input) {
        validateInput(input);

        return executeInputCoding(input, VigenereCodec::decodeLetter);
    }

    @Override
    public Algorithms getSupportedAlgorithm() {
        return Algorithms.VIGENERE;
    }

    private String executeInputCoding(String input, BiFunction<String, String, Character> function) {
        StringBuilder result = new StringBuilder();
        String[] keyLettersLocal = key.split("");
        String[] inputLetters = input.split("");

        if (inputLetters.length > keyLettersLocal.length) {
            keyLettersLocal = increaseKey(keyLettersLocal, inputLetters.length);
        }
        int indexOfKeyLetter = 0;
        for (int indexOfInputLetter = 0; indexOfInputLetter < inputLetters.length; indexOfInputLetter++, indexOfKeyLetter++) {
            final String SPACE_STRING = " ";

            if (SPACE_STRING.equals(inputLetters[indexOfInputLetter])) {
                result.append(SPACE_STRING);
                indexOfKeyLetter--;
            } else {
                result.append(function.apply(inputLetters[indexOfInputLetter], keyLettersLocal[indexOfKeyLetter]));
            }
        }

        return result.toString();
    }

    private static char encodeLetter(String inputLetter, String keyLetter) {
        return letterCoding(inputLetter, keyLetter, Integer::sum);
    }

    private static char decodeLetter(String inputLetter, String keyLetter) {
        return letterCoding(inputLetter, keyLetter, (inputIndex, keyIndex) -> inputIndex - keyIndex);
    }

    private static char letterCoding(String inputLetter, String keyLetter, IntBinaryOperator function) {
        inputLetter = inputLetter.toLowerCase();
        keyLetter = keyLetter.toLowerCase();

        int inputLetterIndex = getIndexOfLetterFromAlphabet(inputLetter);
        int keyLetterIndex = getIndexOfLetterFromAlphabet(keyLetter);

        int resultIndex = getIndex(
                function.applyAsInt(inputLetterIndex, keyLetterIndex)
        );

        return ALPHABET.charAt(resultIndex);
    }

    private static int getIndex(int index) {
        while (index < 0) {
            index += ALPHABET.length();
        }
        while (index >= ALPHABET.length()) {
            index -= ALPHABET.length();
        }

        return index;
    }

    private String[] increaseKey(String[] keyLetters, int inputLettersLength) {
        int numberOfKeyRepetitions = Math.round(inputLettersLength / keyLetters.length) + 1;
        String keyLocal = key.repeat(numberOfKeyRepetitions);
        return keyLocal.split("");
    }

    private void validateInput(String input) {
        if (input == null || input.isBlank()) {
            throw new IllegalArgumentException("Input can't be blank, or null");
        }
    }

    private static int getIndexOfLetterFromAlphabet(String letter) {
        int index = ALPHABET.indexOf(letter);
        if (index == -1) {
            throw new IllegalInputException("Wrong character", letter);
        }
        return index;
    }
}
