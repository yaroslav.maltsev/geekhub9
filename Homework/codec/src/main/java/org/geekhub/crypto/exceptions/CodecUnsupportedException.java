package org.geekhub.crypto.exceptions;

import org.geekhub.crypto.factory.Algorithms;

public class CodecUnsupportedException extends OperationUnsupportedException {
    private final Algorithms unsupportedCodec;

    public CodecUnsupportedException(String message, Algorithms unsupportedCodec) {
        super(message);
        this.unsupportedCodec = unsupportedCodec;
    }

    public Algorithms getCodec() {
        return unsupportedCodec;
    }
}
