package org.geekhub.crypto.codecs.translator;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class TranslateCacheStorage {
    private Map<String, String> ukrToEngCache = new ConcurrentHashMap<>();
    private Map<String, String> engToUkrCache = new ConcurrentHashMap<>();

    @Cacheable(
            value = "engTranslating",
            unless="#result == null"
    )
    public String getTranslateFromUkr(String ukr) {
        return ukrToEngCache.get(ukr);
    }

    @Cacheable(
            value = "ukrTranslating",
            unless="#result == null"
    )
    public String
    getTranslateFromEng(String eng) {
        return engToUkrCache.get(eng);
    }

    public Map<String, String> getUkrToEngCache() {
        return ukrToEngCache;
    }

    public Map<String, String> getEngToUkrCache() {
        return engToUkrCache;
    }

    public void setUkrToEngCache(Map<String, String> ukrToEngCache) {
        this.ukrToEngCache = ukrToEngCache;
    }

    public void setEngToUkrCache(Map<String, String> engToUkrCache) {
        this.engToUkrCache = engToUkrCache;
    }
}
