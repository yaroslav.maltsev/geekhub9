package org.geekhub.crypto;

import org.geekhub.crypto.exceptions.EmptyHistoryException;
import org.geekhub.crypto.factory.Algorithms;
import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class CodingAuditTest {
    private HistoryManager historyManager;

    @BeforeMethod
    public void setUp() {
        historyManager = Mockito.mock(HistoryManager.class);
    }

    @Test(expectedExceptions = EmptyHistoryException.class)
    public void should_throwException_when_history_in_findMostPopularCodec_is_empty() {
        Mockito.when(historyManager.getAllHistoryRecords()).thenReturn(new ArrayList<>());
        new CodingAudit(historyManager).findMostPopularCodec(CodecUsecase.ENCODING);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void test_countCodingsByDate_with_null_argument_is_success() {
        new CodingAudit(historyManager).countCodingsByDate(null);
    }

    @Test
    public void test_countCodingsByDate_is_success() {
        LocalDate today = LocalDate.now();
        LocalDate yesterday = today.minusDays(1);
        LocalDate tomorrow = today.plusDays(1);

        List<HistoryRecord> historyRecords = new ArrayList<>();
        historyRecords.add(new HistoryRecord("today", Algorithms.CAESAR, MenuOperation.ENCODE, today));
        historyRecords.add(new HistoryRecord("today2", Algorithms.CAESAR, MenuOperation.ENCODE, today));
        historyRecords.add(new HistoryRecord("yesterday", Algorithms.CAESAR, MenuOperation.ENCODE, yesterday));
        historyRecords.add(new HistoryRecord("tomorrow", Algorithms.CAESAR, MenuOperation.ENCODE, tomorrow));
        historyRecords.add(new HistoryRecord("tomorrow2", Algorithms.CAESAR, MenuOperation.ENCODE, tomorrow));
        historyRecords.add(new HistoryRecord("tomorrow3", Algorithms.CAESAR, MenuOperation.ENCODE, tomorrow));

        Mockito.when(historyManager.getAllHistoryRecords()).thenReturn(historyRecords);

        Map<LocalDate, Long> map = new CodingAudit(historyManager).countCodingsByDate(CodecUsecase.ENCODING);

        long todayQ = map.get(today);
        long yesterdayQ = map.get(yesterday);
        long tomorrowQ = map.get(tomorrow);

        assertEquals(2L, todayQ);
        assertEquals(1L, yesterdayQ);
        assertEquals(3L, tomorrowQ);
    }

    @Test
    public void test_countCodingsByDate_with_empty_history_is_success() {
        new CodingAudit(historyManager).countCodingsByDate(CodecUsecase.ENCODING);
    }


    @Test
    public void test_countEncodingInputs_with_empty_history_is_success() {
        Mockito.when(historyManager.getAllHistoryRecords()).thenReturn(new ArrayList<>());
        new CodingAudit(historyManager).countEncodingInputs();
    }

    @Test
    public void test_countEncodingInputs_is_success() {
        LocalDate today = LocalDate.now();

        List<HistoryRecord> historyRecords = new ArrayList<>();
        historyRecords.add(new HistoryRecord("first", Algorithms.CAESAR, MenuOperation.ENCODE, today));
        historyRecords.add(new HistoryRecord("first", Algorithms.CAESAR, MenuOperation.ENCODE, today));
        historyRecords.add(new HistoryRecord("second", Algorithms.CAESAR, MenuOperation.ENCODE, today));
        historyRecords.add(new HistoryRecord("today", Algorithms.CAESAR, MenuOperation.ENCODE, today));
        historyRecords.add(new HistoryRecord("today", Algorithms.CAESAR, MenuOperation.ENCODE, today));
        historyRecords.add(new HistoryRecord("today", Algorithms.CAESAR, MenuOperation.ENCODE, today));

        Mockito.when(historyManager.getAllHistoryRecords()).thenReturn(historyRecords);

        Map<String, Long> map = new CodingAudit(historyManager).countEncodingInputs();

        long twoMathes = map.get("first");
        long oneMatch = map.get("second");
        long theMostPopular = map.get("today");

        assertEquals(1L, oneMatch);
        assertEquals(2L, twoMathes);
        assertEquals(3L, theMostPopular);
    }

    @Test
    public void test_countEncodingInputs_with_inputs_with_spaces_is_success() {
        LocalDate today = LocalDate.now();

        List<HistoryRecord> historyRecords = new ArrayList<>();
        historyRecords.add(new HistoryRecord(" first  ", Algorithms.CAESAR, MenuOperation.ENCODE, today));
        historyRecords.add(new HistoryRecord("   first ", Algorithms.CAESAR, MenuOperation.ENCODE, today));
        historyRecords.add(new HistoryRecord("  second  today  ", Algorithms.CAESAR, MenuOperation.ENCODE, today));
        historyRecords.add(new HistoryRecord("  today  ", Algorithms.CAESAR, MenuOperation.ENCODE, today));
        historyRecords.add(new HistoryRecord(" today  ", Algorithms.CAESAR, MenuOperation.ENCODE, today));
        historyRecords.add(new HistoryRecord("  today ", Algorithms.CAESAR, MenuOperation.ENCODE, today));
        Mockito.when(historyManager.getAllHistoryRecords()).thenReturn(historyRecords);

        Map<String, Long> map = new CodingAudit(historyManager).countEncodingInputs();

        long twoMathes = map.get("first");
        long oneMatch = map.get("second");
        long theMostPopular = map.get("today");

        assertEquals(1L, oneMatch);
        assertEquals(2L, twoMathes);
        assertEquals(4L, theMostPopular);
    }

    @Test
    public void should_throwException_when_countEncodingInputs_not_count_spaces() {
        LocalDate today = LocalDate.now();

        List<HistoryRecord> historyRecords = new ArrayList<>();
        historyRecords.add(new HistoryRecord("        ", Algorithms.CAESAR, MenuOperation.ENCODE, today));
        Mockito.when(historyManager.getAllHistoryRecords()).thenReturn(historyRecords);

        Map<String, Long> map = new CodingAudit(historyManager).countEncodingInputs();

        Long qSpaces = map.get(" ");

        assertNull(qSpaces);
    }


    @Test(expectedExceptions = IllegalArgumentException.class)
    public void test_findMostPopularCodec_with_null_argument_is_success() {

        List<HistoryRecord> historyRecords = new ArrayList<>();
        historyRecords.add(new HistoryRecord("test", Algorithms.CAESAR, MenuOperation.ENCODE, LocalDate.now()));
        Mockito.when(historyManager.getAllHistoryRecords()).thenReturn(historyRecords);

        new CodingAudit(historyManager).findMostPopularCodec(null);
    }

    @Test
    public void test_findMostPopularCodec_with_encode_usecase_is_success() {
        LocalDate today = LocalDate.now();

        List<HistoryRecord> historyRecords = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            historyRecords.add(new HistoryRecord("text", Algorithms.CAESAR, MenuOperation.ENCODE, today));
        }
        for (int i = 0; i < 5; i++) {
            historyRecords.add(new HistoryRecord("text", Algorithms.MORSE, MenuOperation.ENCODE, today));
        }
        for (int i = 0; i < 7; i++) {
            historyRecords.add(new HistoryRecord("text", Algorithms.VIGENERE, MenuOperation.ENCODE, today));
        }
        for (int i = 0; i < 5; i++) {
            historyRecords.add(new HistoryRecord("text", Algorithms.VIGENERE_OVER_CAESAR, MenuOperation.ENCODE, today));
        }
        Mockito.when(historyManager.getAllHistoryRecords()).thenReturn(historyRecords);

        Algorithms algorithm = new CodingAudit(historyManager).findMostPopularCodec(CodecUsecase.ENCODING);

        assertEquals(Algorithms.CAESAR, algorithm);
    }

    @Test
    public void test_findMostPopularCodec_with_decode_usecase_is_success() {
        LocalDate today = LocalDate.now();

        List<HistoryRecord> historyRecords = new ArrayList<>();
        historyRecords.add(new HistoryRecord("text", Algorithms.CAESAR, MenuOperation.DECODE, today));
        historyRecords.add(new HistoryRecord("text", Algorithms.CAESAR, MenuOperation.DECODE, today));
        historyRecords.add(new HistoryRecord("text", Algorithms.CAESAR, MenuOperation.DECODE, today));
        historyRecords.add(new HistoryRecord("text", Algorithms.MORSE, MenuOperation.DECODE, today));
        historyRecords.add(new HistoryRecord("text", Algorithms.MORSE, MenuOperation.DECODE, today));
        Mockito.when(historyManager.getAllHistoryRecords()).thenReturn(historyRecords);

        Algorithms algorithm = new CodingAudit(historyManager).findMostPopularCodec(CodecUsecase.DECODING);

        assertEquals(Algorithms.CAESAR, algorithm);
    }
}