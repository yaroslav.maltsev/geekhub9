package org.geekhub.crypto;

import org.geekhub.crypto.factory.Algorithms;
import org.geekhub.crypto.exceptions.EmptyHistoryException;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.geekhub.crypto.CodecUsecase.DECODING;
import static org.geekhub.crypto.CodecUsecase.ENCODING;
import static org.geekhub.crypto.MenuOperation.DECODE;
import static org.geekhub.crypto.MenuOperation.ENCODE;

@Service
public class CodingAudit {
    private final HistoryManager historyManager;
    private static final String SPACE_STRING = " ";

    public CodingAudit(HistoryManager historyManager) {
        this.historyManager = historyManager;
    }

    /**
     * Find words repetitions across all encoding inputs in descending order.
     *
     * @return Map<Word, OccurrenceCount>
     */
    public Map<String, Long> countEncodingInputs() {
        List<HistoryRecord> preparedHistory = prepareUsecaseRecords(
                ENCODING,
                historyManager.getAllHistoryRecords()
        );

        if(preparedHistory.isEmpty()) {
            new HashMap<>();
        }

        return preparedHistory.stream()
                .map(elem -> splitInputToWords(elem.getInput()))
                .flatMap(Collection::stream)
                .collect(
                        Collectors.groupingBy(
                                elem -> elem,
                                HashMap::new,
                                Collectors.counting()
                        )
                );
    }

    private Collection<String> splitInputToWords(String input) {
        return Arrays.stream(input.split(SPACE_STRING))
                .filter(elem -> !elem.isBlank())
                .collect(Collectors.toList());
    }

    /**
     * Count operations done per day.
     *
     * @param usecase - type of coding operation: encoding/decoding
     * @return -  Map<Date, OperationsCount>
     */
    public Map<LocalDate, Long> countCodingsByDate(CodecUsecase usecase) {
        validateUsecase(usecase);

        List<HistoryRecord> records = prepareUsecaseRecords(
                usecase,
                historyManager.getAllHistoryRecords()
        );

        return groupBy(records, HistoryRecord::getDate);
    }

    /**
     * Find the algorithm used the most times for usecase.
     *
     * @param usecase - type of coding operation: encoding/decoding
     * @return - top used algorithm
     */
    public Algorithms findMostPopularCodec(CodecUsecase usecase) {
        validateUsecase(usecase);
        if (historyManager.getAllHistoryRecords().isEmpty()) {
            throw new EmptyHistoryException("History must be not empty!");
        }

        List<HistoryRecord> records = prepareUsecaseRecords(
                usecase,
                historyManager.getAllHistoryRecords()
        );

        Map<Algorithms, Long> algorithmToCount = groupBy(records, HistoryRecord::getAlgorithm);


        return getKeyForMaxValue(algorithmToCount);
    }


    private <K> Map<K, Long> groupBy(List<HistoryRecord> records, Function<HistoryRecord, K> mapper) {
        return records
                .stream()
                .collect(
                        Collectors.groupingBy(
                                mapper,
                                HashMap::new,
                                Collectors.counting()
                        )
                );
    }

    private static Algorithms getKeyForMaxValue(Map<Algorithms, Long> algorithmToCount) {
        return algorithmToCount.entrySet()
                .stream()
                .max(Map.Entry.comparingByValue())
                .orElseThrow()
                .getKey();
    }

    private List<HistoryRecord> prepareUsecaseRecords(CodecUsecase usecase, List<HistoryRecord> records) {
        return records
                .stream()
                .filter(record -> isUsecaseTheSameAsOperation(usecase, record))
                .collect(Collectors.toList());
    }

    private static boolean isUsecaseTheSameAsOperation(CodecUsecase usecase, HistoryRecord record) {
        MenuOperation operation = record.getOperation();
        boolean isEncoding = usecase == ENCODING && operation == ENCODE;
        boolean isDecoding = usecase == DECODING && operation == DECODE;
        return isDecoding || isEncoding;
    }

    private static void validateUsecase(CodecUsecase usecase) {
        if (usecase == null) {
            throw new IllegalArgumentException("Usecase must be not null!");
        }
    }


}