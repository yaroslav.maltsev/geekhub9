package org.geekhub.crypto;

public enum CodecUsecase {
    ENCODING,
    DECODING
}
