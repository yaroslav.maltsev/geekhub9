package org.geekhub.crypto;

import org.flywaydb.core.Flyway;
import org.geekhub.crypto.config.DatabaseConfig;
import org.geekhub.crypto.factory.Algorithms;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.List;

import static org.testng.Assert.*;

@ContextConfiguration(classes = {DatabaseConfig.class})
@JdbcTest
public class HistoryDaoTest extends AbstractTestNGSpringContextTests  {
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;
    private HistoryDao historyDao;

    @BeforeMethod
    public void setUp() {
        historyDao = new HistoryDao(jdbcTemplate);
        historyDao.clearHistory();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_in_addToHistory_when_historyRecord_is_null() {
        historyDao.addToHistory(null);
    }

    @Test
    public void when_element_is_not_null_add_success() {
        HistoryRecord historyRecord = new HistoryRecord("test", Algorithms.CAESAR, MenuOperation.ENCODE, LocalDate.now());
        historyDao.addToHistory(historyRecord);

        assertEquals(historyDao.getAllHistoryRecords().size(), 1);
    }

    @Test
    public void test_multiply_adding_success() {
        HistoryRecord historyRecord = new HistoryRecord("test", Algorithms.CAESAR, MenuOperation.ENCODE, LocalDate.now());
        historyDao.addToHistory(historyRecord);
        historyDao.addToHistory(historyRecord);

        assertEquals(historyDao.getAllHistoryRecords().size(), 2);
    }


    @Test
    public void delete_empty_history_is_success() {
        assertEquals(historyDao.getAllHistoryRecords(), List.of());
        historyDao.deleteLastElementInHistory();
    }

    @Test
    public void delete_sole_element_in_history_is_success() {
        historyDao.addToHistory(new HistoryRecord("input", Algorithms.CAESAR, MenuOperation.ENCODE, LocalDate.now()));
        assertNotEquals(historyDao.getAllHistoryRecords(), List.of());

        historyDao.deleteLastElementInHistory();
        assertEquals(historyDao.getAllHistoryRecords(), List.of());
    }

    @Test
    public void delete_last_not_sole_element_in_history_is_success() {
        HistoryRecord historyRecord = new HistoryRecord("input", Algorithms.CAESAR, MenuOperation.ENCODE, LocalDate.now());

        historyDao.addToHistory(historyRecord);
        historyDao.addToHistory(historyRecord);

        historyDao.deleteLastElementInHistory();
        assertEquals(historyDao.getAllHistoryRecords().size(), 1);
    }


    @Test
    public void clear_empty_history_is_success() {
        historyDao.clearHistory();

        assertEquals(historyDao.getAllHistoryRecords(), List.of());
    }

    @Test
    public void clear_not_empty_history_is_success() {
        historyDao.addToHistory(new HistoryRecord("input", Algorithms.CAESAR, MenuOperation.ENCODE, LocalDate.now()));
        historyDao.addToHistory(new HistoryRecord("input", Algorithms.CAESAR, MenuOperation.ENCODE, LocalDate.now()));
        historyDao.addToHistory(new HistoryRecord("input", Algorithms.CAESAR, MenuOperation.ENCODE, LocalDate.now()));

        historyDao.clearHistory();

        assertEquals(historyDao.getAllHistoryRecords(), List.of());
    }


    @Test
    public void getHistoryMethod_should_return_empty_list() {
        List<HistoryRecord> history = historyDao.getAllHistoryRecords();
        assertEquals(history, List.of());
    }

    @Test
    public void getHistoryMethod_should_return_not_null() {
        List<HistoryRecord> history = historyDao.getAllHistoryRecords();
        assertNotNull(history);
    }

    @Test
    public void test_gettingHistory_is_success() {
        HistoryRecord record = new HistoryRecord("input", Algorithms.CAESAR, MenuOperation.ENCODE, LocalDate.now());
        historyDao.addToHistory(record);
        historyDao.addToHistory(record);
        historyDao.addToHistory(record);

        List<HistoryRecord> historyRecords = historyDao.getAllHistoryRecords();

        assertEquals(historyRecords.size(), 3);
    }
}