package org.geekhub.crypto;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;

public class HistoryManagerTest {
    private HistoryDao historyDao = mock(HistoryDao.class);
    private HistoryManager historyManager;
    @BeforeMethod
    public void setUp() {
        historyManager = new HistoryManager(historyDao);
    }

    @Test
    public void test_addToHistory_is_success() {
        HistoryRecord historyRecord = mock(HistoryRecord.class);
        historyManager.addToHistory(historyRecord);
        verify(historyDao, times(1)).addToHistory(historyRecord);
    }

    @Test
    public void test_deleteLastElementInHistory_is_success() {
        historyManager.deleteLastElementInHistory();
        verify(historyDao, times(1)).deleteLastElementInHistory();
    }

    @Test
    public void test_clearHistory_is_success() {
        historyManager.clearHistory();
        verify(historyDao, times(1)).clearHistory();
    }

    @Test
    public void test_getAllHistoryRecords_is_success() {
        historyManager.getAllHistoryRecords();
        verify(historyDao, times(1)).getAllHistoryRecords();
    }
}