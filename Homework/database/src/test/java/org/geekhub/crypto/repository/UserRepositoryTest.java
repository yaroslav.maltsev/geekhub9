package org.geekhub.crypto.repository;

import org.geekhub.crypto.config.DatabaseConfig;
import org.geekhub.crypto.persistance.CustomUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@ContextConfiguration(classes = {DatabaseConfig.class})
@JdbcTest
public class UserRepositoryTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;
    private UserRepository userRepository;

    @BeforeMethod
    public void setUp() {
        userRepository = new UserRepository(jdbcTemplate);
        userRepository.clear();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_in_save_method_when_user_is_null() {
        userRepository.save(null);
    }

    @Test
    public void when_element_is_not_null_add_success() {
        CustomUser customUser = new CustomUser();
        customUser.setId(1);
        String username = "username";
        customUser.setUsername(username);
        String password = "password";
        customUser.setPassword(password);
        String role = "ROLE_USER";
        customUser.setRole(role);
        userRepository.save(customUser);

        CustomUser user = userRepository.findByUsername(username).orElseThrow();
        assertEquals(user.getUsername(), username);
        assertEquals(user.getPassword(), password);
        assertEquals(user.getRole(), role);
    }

    @Test
    public void test_multiply_adding_success() {
        CustomUser customUser = new CustomUser();
        customUser.setId(1);
        customUser.setUsername("username");
        customUser.setPassword("password");
        customUser.setRole("ROLE_USER");

        CustomUser secondCustomUser = new CustomUser();
        secondCustomUser.setId(2);
        secondCustomUser.setUsername("username2");
        secondCustomUser.setPassword("password2");
        secondCustomUser.setRole("ROLE_USER");

        userRepository.save(customUser);
        userRepository.save(secondCustomUser);

        CustomUser user = userRepository.findByUsername("username").get();
        assertEquals(user.getId(), customUser.getId());
        assertEquals(user.getUsername(), customUser.getUsername());
        assertEquals(user.getPassword(), customUser.getPassword());
        assertEquals(user.getRole(), customUser.getRole());

        CustomUser secondUser = userRepository.findByUsername("username2").get();
        assertEquals(secondUser.getId(), secondCustomUser.getId());
        assertEquals(secondUser.getUsername(), secondCustomUser.getUsername());
        assertEquals(secondUser.getPassword(), secondCustomUser.getPassword());
        assertEquals(secondUser.getRole(), secondCustomUser.getRole());
    }
}