create table if not exists public.history (
      id        serial        primary key,
      date      date          not null,
      operation varchar(100)  not null,
      algorithm varchar(100)  not null,
      input     varchar(1000) not null
)