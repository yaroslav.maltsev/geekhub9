package org.geekhub.crypto.persistance;

import java.util.Objects;

public class CustomUser {
    private Integer id;
    private String username;
    private String password;
    private String role;

    public void setId(Integer id) {
        if (Objects.isNull(id)) {
            throw new IllegalArgumentException("Id must be not null!");
        }
        this.id = id;
    }

    public void setUsername(String username) {
        if (Objects.isNull(username)) {
            throw new IllegalArgumentException("Username must be not null!");
        }
        this.username = username;
    }

    public void setPassword(String password) {
        if (Objects.isNull(password)) {
            throw new IllegalArgumentException("Password must be not null!");
        }
        this.password = password;
    }

    public void setRole(String role) {
        if (Objects.isNull(role)) {
            throw new IllegalArgumentException("Role must be not null!");
        }
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }
}
