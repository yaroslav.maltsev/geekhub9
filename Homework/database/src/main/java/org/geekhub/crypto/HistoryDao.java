package org.geekhub.crypto;

import org.geekhub.crypto.factory.Algorithms;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;

@Repository
public class HistoryDao {
    private NamedParameterJdbcTemplate jdbcTemplate;

    public HistoryDao(
            NamedParameterJdbcTemplate jdbcTemplate
    ) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void addToHistory(HistoryRecord historyRecord) {
        if (historyRecord == null) {
            throw new IllegalArgumentException("History record must be not null!");
        }

        jdbcTemplate.update(
                "INSERT into history(date, operation, algorithm, input) values(:date, :operation, :algorithm, :input)",
                new MapSqlParameterSource(
                        Map.ofEntries(
                                Map.entry("date", Date.valueOf(historyRecord.getDate())),
                                Map.entry("operation", historyRecord.getOperation().name()),
                                Map.entry("algorithm", historyRecord.getAlgorithm().name()),
                                Map.entry("input", historyRecord.getInput())
                        )
                )
        );
    }

    public void deleteLastElementInHistory() {
        jdbcTemplate.update(
                "DELETE FROM history WHERE id = (SELECT id FROM history ORDER BY id DESC LIMIT 1)",
                new MapSqlParameterSource()
        );
    }

    public void clearHistory() {
        jdbcTemplate.update(
                "DELETE FROM history",
                new MapSqlParameterSource()
        );
    }

    public List<HistoryRecord> getAllHistoryRecords() {
        return jdbcTemplate.query(
                "SELECT date, operation, algorithm, input FROM history",
                (rs, rowNum) -> getHistoryRecordFromResultSet(rs)
        );
    }

    private HistoryRecord getHistoryRecordFromResultSet(ResultSet resultSet) throws SQLException {
        LocalDate date = convertDateToLocalDate(resultSet.getDate("date"));

        String algorithmText = resultSet.getString("algorithm");
        String operationText = resultSet.getString("operation");

        Algorithms algorithms = Algorithms.valueOf(algorithmText);
        MenuOperation operation = MenuOperation.valueOf(operationText);

        String input = resultSet.getString("input");

        return new HistoryRecord(input, algorithms, operation, date);
    }

    private static LocalDate convertDateToLocalDate(Date dateToConvert) {
        return Instant.ofEpochMilli(dateToConvert.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }
}