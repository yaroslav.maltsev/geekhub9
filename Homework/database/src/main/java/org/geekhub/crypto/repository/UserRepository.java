package org.geekhub.crypto.repository;


import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.geekhub.crypto.persistance.CustomUser;

import java.util.Map;
import java.util.Optional;

@Repository
public class UserRepository {
    private NamedParameterJdbcTemplate jdbcTemplate;

    public UserRepository(
            NamedParameterJdbcTemplate jdbcTemplate
    ) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Optional<CustomUser> findByUsername(String username) {
        CustomUser customUser = jdbcTemplate.queryForObject(
                "SELECT id, username, password, role FROM custom_users WHERE username = :username",
                new MapSqlParameterSource("username", username),
                new BeanPropertyRowMapper<>(CustomUser.class)
        );
        return Optional.ofNullable(customUser);
    }

    public void save(CustomUser customUser) {
        if (customUser == null) {
            throw new IllegalArgumentException("User must be not null!");
        }
        jdbcTemplate.update(
                "INSERT INTO custom_users (username, password, role) values (:username, :password, :role)",
                new MapSqlParameterSource(
                        Map.ofEntries(
                            Map.entry("username", customUser.getUsername()),
                            Map.entry("password", customUser.getPassword()),
                            Map.entry("role", customUser.getRole())
                    )
                )
        );
    }

    public void clear() {
        jdbcTemplate.update(
                "DELETE FROM custom_users",
                new MapSqlParameterSource()
        );
    }
}
