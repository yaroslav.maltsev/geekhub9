package org.geekhub.crypto.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class DatabaseConfig {
    @Bean
    public DataSource devDataSource(
            @Value("${app.database.host}") String url,
            @Value("${app.database.login}") String login,
            @Value("${app.database.password}") String password,
            @Value("${app.database.driver}") String driver
    ) {
        final HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(driver);
        hikariConfig.setJdbcUrl(url);
        hikariConfig.setUsername(login);
        hikariConfig.setPassword(password);
        return new HikariDataSource(hikariConfig);
    }

    @Profile("prod")
    @Bean
    public DataSource prodDataSource(
            @Value("${app.database.host}") String url,
            @Value("${app.database.login}") String login,
            @Value("${app.database.password}") String password,
            @Value("${app.database.driver}") String driver
    ) {
        final HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(driver);
        hikariConfig.setJdbcUrl(url);
        hikariConfig.setUsername(login);
        hikariConfig.setPassword(password);
        return new HikariDataSource(hikariConfig);
    }

    @Bean
    public NamedParameterJdbcTemplate template(DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }
}