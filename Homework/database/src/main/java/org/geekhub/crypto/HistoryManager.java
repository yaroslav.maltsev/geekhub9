package org.geekhub.crypto;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HistoryManager {
    private HistoryDao historyDao;

    public HistoryManager(HistoryDao historyDao) {
        this.historyDao = historyDao;
    }

    public void addToHistory(HistoryRecord historyRecord) {
        if (historyRecord == null) {
            throw new IllegalArgumentException("History record must be not null!");
        }

        historyDao.addToHistory(historyRecord);
    }

    public void deleteLastElementInHistory() {
        historyDao.deleteLastElementInHistory();
    }

    public void clearHistory() {
        historyDao.clearHistory();
    }

    public List<HistoryRecord> getAllHistoryRecords() {
        return historyDao.getAllHistoryRecords();
    }
}
