package org.geekhub.crypto;

import org.geekhub.crypto.factory.Algorithms;

import java.time.LocalDate;

public class HistoryRecord {
    private final Algorithms algorithm;
    private final MenuOperation operation;
    private final LocalDate date;
    private final String input;

    public HistoryRecord(String input, Algorithms algorithm, MenuOperation operation, LocalDate date) {
        if (input == null || algorithm == null || operation == null || date == null) {
            throw new IllegalArgumentException("Fields mustn't be null!");
        } else if ("".equals(input)) {
            throw new IllegalArgumentException("Input mustn't be empty");
        }
        this.algorithm = algorithm;
        this.operation = operation;
        this.input = input;
        this.date = date;
    }

    public String getInput() {
        return input;
    }

    public LocalDate getDate() {
        return date;
    }

    public MenuOperation getOperation() {
        return operation;
    }

    public Algorithms getAlgorithm() {
        return algorithm;
    }
}
