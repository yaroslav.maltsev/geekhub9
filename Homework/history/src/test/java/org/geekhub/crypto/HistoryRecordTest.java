package org.geekhub.crypto;

import org.geekhub.crypto.factory.Algorithms;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;

import static org.testng.Assert.*;

public class HistoryRecordTest {
    private HistoryRecord historyRecord;

    @BeforeMethod
    public void setUp() {
        historyRecord = null;
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_arguments_in_constructor_are_null() {
        historyRecord = new HistoryRecord(null, null, null, null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_is_empty() {
        historyRecord = new HistoryRecord("", Algorithms.CAESAR, MenuOperation.ENCODE, LocalDate.now());
    }

    @Test
    public void test_creating_is_success() {
        historyRecord = new HistoryRecord("in", Algorithms.CAESAR, MenuOperation.ENCODE, LocalDate.now());
    }

    @Test
    public void test_getters_is_success() {
        LocalDate nowDate = LocalDate.now();
        historyRecord = new HistoryRecord("in", Algorithms.CAESAR, MenuOperation.ENCODE, nowDate);
        assertEquals("in", historyRecord.getInput());
        assertEquals(Algorithms.CAESAR, historyRecord.getAlgorithm());
        assertEquals(MenuOperation.ENCODE, historyRecord.getOperation());
        assertEquals(nowDate, historyRecord.getDate());
    }
}