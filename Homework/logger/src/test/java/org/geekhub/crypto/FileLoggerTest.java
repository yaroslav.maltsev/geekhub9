package org.geekhub.crypto;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class FileLoggerTest {
    private FileLogger fileLogger;

    @BeforeMethod
    public void setUp() {
        fileLogger = new FileLogger();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_argument_in_error_is_null() {
        fileLogger.error(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_argument_in_warn_is_null() {
        fileLogger.warn(null);
    }
}