package org.geekhub.crypto;

import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CompositeLoggerTest {
    private Logger logger;

    @BeforeMethod
    public void setUp() {
        logger = new CompositeLogger(
                Mockito.mock(LoggerRegister.class)
        );
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_argument_in_error_is_null() {
        logger.error(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_argument_in_warn_is_null() {
        logger.warn(null);
    }
}