package org.geekhub.crypto;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ConsoleLoggerTest {
    private ConsoleLogger consoleLogger;

    @BeforeMethod
    public void setUp() {
        consoleLogger = new ConsoleLogger();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_argument_in_error_is_null() {
        consoleLogger.error(null);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_argument_in_warn_is_null() {
        consoleLogger.warn(null);
    }
}