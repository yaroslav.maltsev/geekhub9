package org.geekhub.crypto;

class CompositeLogger implements Logger {
    private Logger[] loggers;

    CompositeLogger(LoggerRegister loggerRegister) {
        loggers = loggerRegister.getRegisteredLoggers();
    }
    @Override
    public void warn(String message) {
        checkMessageOnNull(message);
        for (Logger logger : loggers) {
            logger.warn(message);
        }
    }

    @Override
    public void error(Exception e) {
        checkExceptionOnNull(e);
        for (Logger logger : loggers) {
            logger.error(e);
        }
    }

    @Override
    public Loggers getSupportedEnum() {
        return null;
    }

    private void checkMessageOnNull(String message) {
        if (message == null) {
            throw new IllegalArgumentException("Message must be not null!");
        }
    }

    private void checkExceptionOnNull(Exception e) {
        if (e == null) {
            throw new IllegalArgumentException("Exception must be not null!");
        }
    }
}
