package org.geekhub.crypto;

public interface Logger {
    void warn(String message);

    void error(Exception e);

    Loggers getSupportedEnum();
}
