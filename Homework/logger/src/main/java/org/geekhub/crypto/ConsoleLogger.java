package org.geekhub.crypto;

import org.springframework.stereotype.Component;

import java.time.LocalDateTime;


@Component
public class ConsoleLogger implements Logger {
    @Override
    public void warn(String message) {
        checkMessageOnNull(message);
        LocalDateTime dateTime = LocalDateTime.now();
        String errorMessage = dateTime + ":   WARNING    " + message;
        System.out.println(errorMessage);
    }

    @Override
    public void error(Exception e) {
        checkExceptionOnNull(e);
        System.out.println(LocalDateTime.now() + ":  ");
        System.out.println(e.getMessage());
        System.out.println(e.getCause());
    }

    @Override
    public Loggers getSupportedEnum() {
        return Loggers.CONSOLE;
    }

    private void checkMessageOnNull(String message) {
        if (message == null) {
            throw new IllegalArgumentException("Message must be not null!");
        }
    }

    private void checkExceptionOnNull(Exception e) {
        if (e == null) {
            throw new IllegalArgumentException("Exception must be not null!");
        }
    }
}
