package org.geekhub.crypto;

import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;

@Component
public class FileLogger implements Logger {
    private static final String LOG_FILE = "logs.txt";

    @Override
    public void warn(String message) {
        checkMessageOnNull(message);
        LocalDateTime dateTime = LocalDateTime.now();
        String errorMessage = dateTime + ":   WARNING    " + message;
        writeInFile(errorMessage);
    }

    @Override
    public void error(Exception e) {
        checkExceptionOnNull(e);
        LocalDateTime dateTime = LocalDateTime.now();
        String errorMessage = dateTime + ":   " + Arrays.toString(e.getStackTrace()) + "[Cause] : " + e.getCause();
        writeInFile(errorMessage);
    }

    @Override
    public Loggers getSupportedEnum() {
        return Loggers.FILE;
    }

    private void writeInFile(String text) {
        try (
                FileWriter writer = new FileWriter(LOG_FILE, true)
        ) {
            writer.write(text + "\n");
        } catch (IOException e) {
            throw new IllegalArgumentException("No such a file: " + LOG_FILE, e);
        }
    }

    private void checkMessageOnNull(String message) {
        if (message == null) {
            throw new IllegalArgumentException("Message must be not null!");
        }
    }

    private void checkExceptionOnNull(Exception e) {
        if (e == null) {
            throw new IllegalArgumentException("Exception must be not null!");
        }
    }
}
