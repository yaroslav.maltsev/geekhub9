package org.geekhub.crypto;

import org.springframework.stereotype.Component;

@Component
public class LoggerFactory {
    private Logger logger;

    public LoggerFactory(
            LoggerRegister loggerRegister
    ) {
        this.logger = new CompositeLogger(loggerRegister);
    }

    public Logger getLogger() {
        return logger;
    }
}