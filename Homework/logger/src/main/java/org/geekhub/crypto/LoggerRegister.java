package org.geekhub.crypto;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class LoggerRegister {
    private Logger[] loggers;
    private Map<Loggers, Logger> enumLoggerMap;

    LoggerRegister(@Value("${logger}") String settedLoggers, Logger[] allLoggers) {
        this.enumLoggerMap = getEnumLoggerMap(allLoggers);
        this.loggers = getLoggersFromProperties(settedLoggers);
    }

    public Logger[] getRegisteredLoggers() {
        return loggers;
    }

    private Map<Loggers, Logger> getEnumLoggerMap(Logger[] allLoggers) {
        Map<Loggers, Logger> map = new HashMap<>();
        for (Logger logger : allLoggers) {
            map.put(logger.getSupportedEnum(), logger);
        }
        return map;
    }

    private Logger[] getLoggersFromProperties(String loggersFromProperties) {
        String[] loggersNamesList = loggersFromProperties.split(",");
        int length = loggersNamesList.length;
        Logger[] loggersInstances = new Logger[length];

        for (int i = 0; i < length; i++) {
            Loggers loggerEnum = getEnumIfExist(loggersNamesList[i]);
            Logger logger = enumLoggerMap.get(loggerEnum);
            loggersInstances[i] = logger;
        }

        return loggersInstances;
    }

    private Loggers getEnumIfExist(String enumName) {
        try {
            return Loggers.valueOf(enumName);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("No such enum in loggerEnumList: " + enumName, e);
        }
    }
}
