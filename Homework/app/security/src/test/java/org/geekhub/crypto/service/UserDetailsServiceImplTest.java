package org.geekhub.crypto.service;

import org.geekhub.crypto.persistance.CustomUser;
import org.geekhub.crypto.repository.UserRepository;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.testng.Assert.assertEquals;

public class UserDetailsServiceImplTest {

    private UserDetailsService userDetailsService;
    private UserRepository userRepository = mock(UserRepository.class);

    @BeforeMethod
    public void setUp() {
        userDetailsService = new UserDetailsServiceImpl(userRepository);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_username_is_null() {
        userDetailsService.loadUserByUsername(null);
    }

    @Test
    public void test_loadByUsername_is_success() {
        CustomUser customUser = new CustomUser();
        customUser.setId(1);
        customUser.setUsername("username");
        customUser.setPassword("password");
        customUser.setRole("ROLE_USER");

        when(userRepository.findByUsername(anyString()))
                .thenReturn(Optional.of(customUser));

        UserDetails userDetails = userDetailsService.loadUserByUsername("username");
        assertEquals(userDetails.getUsername(), customUser.getUsername());
        assertEquals(userDetails.getPassword(), customUser.getPassword());
        assertEquals(
                userDetails.getAuthorities(),
                Collections.singletonList(new SimpleGrantedAuthority(customUser.getRole()))
        );
    }
}