package org.geekhub.crypto;


import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.geekhub.crypto.persistance.CustomUser;
import org.geekhub.crypto.repository.UserRepository;

@Component
public class UsersInitializer implements ApplicationRunner {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UsersInitializer(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(ApplicationArguments args) {
        CustomUser customUser = new CustomUser();
        customUser.setUsername("user");
        customUser.setPassword(passwordEncoder.encode("user"));
        customUser.setRole("ROLE_USER");

        CustomUser customAdmin = new CustomUser();
        customAdmin.setUsername("admin");
        customAdmin.setPassword(passwordEncoder.encode("admin"));
        customAdmin.setRole("ROLE_ADMIN");

        userRepository.clear();

        userRepository.save(customUser);
        userRepository.save(customAdmin);
    }
}
