package org.geekhub.crypto.service;


import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.geekhub.crypto.persistance.CustomUser;
import org.geekhub.crypto.repository.UserRepository;

import java.util.Collections;
import java.util.Objects;
import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;

    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        if (Objects.isNull(username)) {
            throw new IllegalArgumentException("Username must be not null!");
        }
        final Optional<CustomUser> optionalUser = userRepository.findByUsername(username);
        if (optionalUser.isPresent()) {
            final CustomUser customUser = optionalUser.get();
            return new User(
                    customUser.getUsername(), customUser.getPassword(), Collections.singletonList(new SimpleGrantedAuthority(customUser.getRole()))
            );
        } else {
            throw new UsernameNotFoundException("Incorrect login or password.");
        }
    }
}
