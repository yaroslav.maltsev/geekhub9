package org.geekhub.crypto.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String ROLE_ADMIN = "ROLE_ADMIN";
    private static final String ROLE_USER = "ROLE_USER";

    private final UserDetailsService userDetailsService;
    private final PasswordEncoder passwordEncoder;

    public WebSecurityConfig(UserDetailsService userDetailsService, PasswordEncoder passwordEncoder) {
        this.userDetailsService = userDetailsService;
        this.passwordEncoder = passwordEncoder;
    }

    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/admin/**").hasAuthority(ROLE_ADMIN)
                .antMatchers("/analytics/**").hasAuthority(ROLE_ADMIN)
                .antMatchers("/crypto/deleteLast").hasAuthority(ROLE_ADMIN)
                .antMatchers("/crypto/clear").hasAuthority(ROLE_ADMIN)

                .antMatchers("/api/analytics/**").hasAuthority(ROLE_ADMIN)
                .antMatchers("/api/history/clear").hasAuthority(ROLE_ADMIN)
                .antMatchers("/api/history/deleteLast").hasAuthority(ROLE_ADMIN)


                .antMatchers("/api/history/**").hasAuthority(ROLE_USER)

                .antMatchers("/crypto/**").hasAuthority(ROLE_USER)
                .antMatchers("/user/**").hasAuthority(ROLE_USER)

                .antMatchers("/login").permitAll()
                .antMatchers("/**").permitAll()

        .and()
            .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/auth")
                .usernameParameter("username")
                .passwordParameter("password")
            .permitAll()
        .and()
                .logout()
                    .logoutUrl("/logout")
                    .logoutSuccessUrl("/")
                .permitAll()
        .and()
            .rememberMe()
                .useSecureCookie(true)
                .rememberMeCookieName("RememberMe")
                .key("RememberMeToken")
                .tokenValiditySeconds(86400);
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }

    @Bean
    public RoleHierarchy roleHierarchy() {
        RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();

        roleHierarchy.setHierarchy(
                ROLE_ADMIN + " > " + ROLE_USER
        );

        return roleHierarchy;
    }
}