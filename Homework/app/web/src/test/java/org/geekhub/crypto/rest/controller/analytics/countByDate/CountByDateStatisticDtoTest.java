package org.geekhub.crypto.rest.controller.analytics.countByDate;

import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.*;

public class CountByDateStatisticDtoTest {
    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_argument_is_null() {
        new CountByDateStatisticDto(null);
    }

    @Test
    public void test_converting_map_in_map_with_custom_entry_is_success() {
        LocalDate now = LocalDate.now();
        LocalDate tomorrow = now.plusDays(1);
        LocalDate yesterday = now.minusDays(1);
        Map<LocalDate, Long> countedByDateMap = Map.ofEntries(
                Map.entry(now, 1L),
                Map.entry(tomorrow, 2L),
                Map.entry(yesterday, 3L)
        );
        CountByDateStatisticDto countByDateStatisticDto = new CountByDateStatisticDto(countedByDateMap);
        List<CountByDateStatisticEntry> listOfEntries = countByDateStatisticDto.getListOfEntries();
        List<CountByDateStatisticEntry> expected = List.of(
                new CountByDateStatisticEntry(now, 1L),
                new CountByDateStatisticEntry(tomorrow, 2L),
                new CountByDateStatisticEntry(yesterday, 3L)
        );
        assertEquals(listOfEntries.size(), expected.size());
    }
}