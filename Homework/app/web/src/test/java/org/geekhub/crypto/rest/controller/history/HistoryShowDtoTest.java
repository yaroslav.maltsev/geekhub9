package org.geekhub.crypto.rest.controller.history;

import org.geekhub.crypto.HistoryRecord;
import org.geekhub.crypto.MenuOperation;
import org.geekhub.crypto.factory.Algorithms;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.List;

import static org.testng.Assert.*;

public class HistoryShowDtoTest {
    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_argument_is_null() {
        new HistoryShowDto(null);
    }

    @Test
    public void test_getHistoryRecords_is_success() {
        LocalDate now = LocalDate.now();
        HistoryRecord historyRecord = new HistoryRecord("input", Algorithms.CAESAR, MenuOperation.ENCODE, now);
        List<HistoryRecord> historyRecordList = List.of(
                historyRecord,
                historyRecord,
                historyRecord
        );
        HistoryShowDto historyShowDto = new HistoryShowDto(historyRecordList);
        List<HistoryRecord> showDtoHistoryRecords = historyShowDto.getHistoryRecords();
        assertEquals(showDtoHistoryRecords, historyRecordList);
    }
}