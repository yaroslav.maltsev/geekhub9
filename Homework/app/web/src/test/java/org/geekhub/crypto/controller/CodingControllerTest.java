package org.geekhub.crypto.controller;

import org.geekhub.crypto.HistoryManager;
import org.geekhub.crypto.LoggerFactory;
import org.geekhub.crypto.factory.DecodersFactory;
import org.geekhub.crypto.factory.EncodersFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;

import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = CodingController.class)
public class CodingControllerTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private EncodersFactory encodersFactory;
    @MockBean
    private DecodersFactory decodersFactory;
    @MockBean
    private HistoryManager historyManager;
    @MockBean
    private LoggerFactory lf;
    @MockBean
    private UserDetailsService userDetailsServiceImpl;
    @MockBean
    private PasswordEncoder passwordEncoder;

    @Test
    public void test_get_codecs_when_action_is_decoding_is_success() throws Exception {
        mockMvc.perform(
                get("/codecs")
                .param("action", "decoding")
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("CAESAR")))
                .andExpect(content().string(containsString("MORSE")))
                .andExpect(content().string(containsString("VIGENERE")))
                .andExpect(content().string(containsString("VIGENERE_OVER_CAESAR")))
                .andExpect(content().string(containsString("TRANSLATOR")));
    }

    @Test
    public void test_get_codecs_when_action_is_encoding_is_success() throws Exception {
        mockMvc.perform(
                get("/codecs")
                        .param("action", "encoding")
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("CAESAR")))
                .andExpect(content().string(containsString("MORSE")))
                .andExpect(content().string(containsString("VIGENERE")))
                .andExpect(content().string(containsString("VIGENERE_OVER_CAESAR")))
                .andExpect(content().string(containsString("TRANSLATOR")));
    }

    @Test
    public void test_get_answer_is_success() throws Exception {
        mockMvc.perform(
                get("/answer")
                        .param("result", "testAnswer")
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("testAnswer")));
    }
}