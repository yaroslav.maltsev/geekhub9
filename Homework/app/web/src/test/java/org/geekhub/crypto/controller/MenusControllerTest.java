package org.geekhub.crypto.controller;

import org.geekhub.crypto.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;

import static org.hamcrest.core.StringContains.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = MenusController.class)
public class MenusControllerTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private LoggerFactory lf;
    @MockBean
    private UserDetailsService userDetailsServiceImpl;
    @MockBean
    private PasswordEncoder passwordEncoder;

    @Test
    public void test_guest_menu_is_success() throws Exception{
        mockMvc.perform(get("/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("<a href=\"codecs?action=decoding\">DECODING</a>")))
                .andExpect(content().string(containsString("<a href=\"codecs?action=encoding\">ENCODING</a>")));
    }

    @Test
    public void test_user_menu_is_success() throws Exception{
        mockMvc.perform(get("/user/menu").with(user("user")))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("<a href=\"/codecs?action=decoding\">DECODING</a>")))
                .andExpect(content().string(containsString("<a href=\"/codecs?action=encoding\">ENCODING</a>")))
                .andExpect(content().string(containsString("<a href=\"/history\">HISTORY</a>")));
    }

    @Test
    public void test_admin_menu_is_success() throws Exception{
        mockMvc.perform(
                get("/admin/menu")
                        .with(user("admin").roles("ADMIN"))
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("<a href=\"/codecs?action=decoding\">DECODING</a>")))
                .andExpect(content().string(containsString("<a href=\"/codecs?action=encoding\">ENCODING</a>")))
                .andExpect(content().string(containsString("<a href=\"/history\">HISTORY</a>")))
                .andExpect(content().string(containsString("<a href=\"/analytics\">ANALYTICS</a>")));
    }
}