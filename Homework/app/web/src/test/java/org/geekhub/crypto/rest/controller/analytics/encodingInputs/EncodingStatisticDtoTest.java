package org.geekhub.crypto.rest.controller.analytics.encodingInputs;

import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

import static org.testng.Assert.assertEquals;

public class EncodingStatisticDtoTest {
    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_argument_is_null() {
        new EncodingStatisticDto(null);
    }

    @Test
    public void test_converting_map_in_map_with_custom_entry_is_success() {
        String first = "first";
        String second = "second";
        String third = "third";
        Map<String, Long> countedEncodingInputsMap = Map.ofEntries(
                Map.entry(first, 1L),
                Map.entry(second, 2L),
                Map.entry(third, 3L)
        );
        EncodingStatisticDto encodingStatisticDto = new EncodingStatisticDto(countedEncodingInputsMap);
        List<EncodingStatisticEntry> listOfEntries = encodingStatisticDto.getListOfEntries();
        List<EncodingStatisticEntry> expected = List.of(
                new EncodingStatisticEntry(first, 1L),
                new EncodingStatisticEntry(second, 2L),
                new EncodingStatisticEntry(third, 3L)
        );
        assertEquals(listOfEntries.size(), expected.size());
    }
}