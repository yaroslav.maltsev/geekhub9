package org.geekhub.crypto.rest.controller.coding;

import org.geekhub.crypto.HistoryManager;
import org.geekhub.crypto.LoggerFactory;
import org.geekhub.crypto.factory.DecodersFactory;
import org.geekhub.crypto.factory.EncodersFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = RestCodingController.class)
public class RestCodingControllerTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private HistoryManager historyManager = mock(HistoryManager.class);
    @MockBean
    private EncodersFactory encodersFactory;
    @MockBean
    private DecodersFactory decodersFactory;
    @MockBean
    private LoggerFactory lf;
    @MockBean
    private UserDetailsService userDetailsServiceImpl;
    @MockBean
    private PasswordEncoder passwordEncoder;
}