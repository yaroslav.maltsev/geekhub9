package org.geekhub.crypto.rest.controller.history;

import org.geekhub.crypto.HistoryManager;
import org.geekhub.crypto.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = RestHistoryController.class)
public class RestHistoryControllerTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HistoryManager historyManager = mock(HistoryManager.class);
    @MockBean
    private LoggerFactory lf;
    @MockBean
    private UserDetailsService userDetailsServiceImpl;
    @MockBean
    private PasswordEncoder passwordEncoder;

    @Test
    public void test_showHistory_is_success() throws Exception {
        mockMvc.perform(
                get("/api/history/show/")
                .with(user("user"))
                .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$['historyRecords']").value(new ArrayList<>()));
        verify(historyManager, times(0)).getAllHistoryRecords();
    }

    @Test
    public void test_post_deleteLastElementInHistory_is_success() throws Exception {
        mockMvc.perform(
                post("/api/history/deleteLast/")
                        .with(user("admin").roles("ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());
        verify(historyManager, times(0)).deleteLastElementInHistory();
    }

    @Test
    public void test_post_clearHistory_is_success() throws Exception {
        mockMvc.perform(
                post("/api/history/clear/")
                        .with(user("admin").roles("ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());
        verify(historyManager, times(0)).clearHistory();
    }
}