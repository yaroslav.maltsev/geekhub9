package org.geekhub.crypto.controller;

import org.geekhub.crypto.CodingAudit;
import org.geekhub.crypto.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;

import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = AnalyticsController.class)
public class AnalyticsControllerTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CodingAudit codingAudit;
    @MockBean
    private LoggerFactory lf;
    @MockBean
    private UserDetailsService userDetailsServiceImpl;
    @MockBean
    private PasswordEncoder passwordEncoder;

    @Test
    public void test_get_analytics_is_success() throws Exception {
        mockMvc.perform(
                get("/analytics/")
                        .with(user("admin").roles("ADMIN"))
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("<a href=\"countEncodingInputs\">COUNT_ENCODING_INPUTS</a>")))
                .andExpect(content().string(containsString("<a href=\"usecases?action=POPULAR_CODEC\">POPULAR_CODEC</a>")))
                .andExpect(content().string(containsString("<a href=\"usecases?action=SORT_BY_DATE\">SORT_BY_DATE</a>")));
    }

    @Test
    public void test_get_countEncodingInputs_is_success() throws Exception {
        mockMvc.perform(
                get("/analytics/countEncodingInputs")
                        .with(user("admin").roles("ADMIN"))
        )
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void test_get_usecases_is_success() throws Exception {
        mockMvc.perform(
                get("/analytics/usecases")
                        .with(user("admin").roles("ADMIN"))
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("<input type=\"radio\" name=\"usecase\" value=\"ENCODING\">ENCODING")))
                .andExpect(content().string(containsString("<input type=\"radio\" name=\"usecase\" value=\"DECODING\">DECODING")));
    }

    @Test
    public void test_post_encoding_usecase_when_action_is_popular_codec_is_success() throws Exception {
        mockMvc.perform(
                post("/analytics/usecases")
                        .param("action", "POPULAR_CODEC")
                        .param("usecase", "ENCODING")
                        .with(user("admin").roles("ADMIN"))
        )
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void test_post_decoding_usecase_when_action_is_popular_codec_is_success() throws Exception {
        mockMvc.perform(
                post("/analytics/usecases")
                        .param("action", "POPULAR_CODEC")
                        .param("usecase", "DECODING")
                        .with(user("admin").roles("ADMIN"))
        )
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void test_post_encoding_usecase_when_action_is_sort_by_date_is_success() throws Exception {
        mockMvc.perform(
                post("/analytics/usecases")
                        .param("action", "SORT_BY_DATE")
                        .param("usecase", "ENCODING")
                        .with(user("admin").roles("ADMIN"))
        )
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void test_post_decoding_usecase_when_action_is_sort_by_date_is_success() throws Exception {
        mockMvc.perform(
                post("/analytics/usecases")
                        .param("action", "SORT_BY_DATE")
                        .param("usecase", "DECODING")
                        .with(user("admin").roles("ADMIN"))
        )
                .andDo(print())
                .andExpect(status().isOk());
    }
}