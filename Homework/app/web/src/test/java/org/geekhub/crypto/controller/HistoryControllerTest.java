package org.geekhub.crypto.controller;

import org.geekhub.crypto.HistoryManager;
import org.geekhub.crypto.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;

import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = HistoryController.class)
public class HistoryControllerTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private HistoryManager historyManager;
    @MockBean
    private LoggerFactory lf;
    @MockBean
    private UserDetailsService userDetailsServiceImpl;
    @MockBean
    private PasswordEncoder passwordEncoder;

    @Test
    public void test_user_history_menu_is_success() throws Exception {
        mockMvc.perform(
                get("/history/")
                        .with(user("user"))
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("<input type=\"radio\" name=\"history-action\" value=\"SHOW\">SHOW")));
    }

    @Test
    public void test_admin_history_menu_is_success() throws Exception {
        mockMvc.perform(
                get("/history/")
                        .with(user("user").roles("ADMIN"))
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("<input type=\"radio\" name=\"history-action\" value=\"SHOW\">SHOW")))
                .andExpect(content().string(containsString("<input type=\"radio\" name=\"history-action\" value=\"DELETE_LAST_ELEMENT\">DELETE_LAST_ELEMENT")))
                .andExpect(content().string(containsString("<input type=\"radio\" name=\"history-action\" value=\"CLEAR\">CLEAR")));
    }

    @Test
    public void test_post_show_history_action_is_success() throws Exception {
        mockMvc.perform(
                post("/history/")
                .param("history-action", "SHOW")
        )
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/history/show"));
    }

    @Test
    public void test_post_delete_history_action_is_success() throws Exception {
        mockMvc.perform(
                post("/history/")
                        .param("history-action", "DELETE_LAST_ELEMENT")
        )
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/history/deleteLast"));
    }

    @Test
    public void test_post_clear_history_action_is_success() throws Exception {
        mockMvc.perform(
                post("/history/")
                        .param("history-action", "CLEAR")
        )
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/history/clear"));
    }

    @Test
    public void test_get_show_is_success() throws Exception {
        mockMvc.perform(
                get("/history/show")
        )
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void test_get_deleteLast_is_success() throws Exception {
        mockMvc.perform(
                get("/history/deleteLast")
        )
                .andDo(print())
                .andExpect(redirectedUrl("/crypto"));
    }

    @Test
    public void test_get_clear_is_success() throws Exception {
        mockMvc.perform(
                get("/history/clear")
        )
                .andDo(print())
                .andExpect(redirectedUrl("/crypto"));
    }

}