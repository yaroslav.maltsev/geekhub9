package org.geekhub.crypto.rest.controller.analytics;

import org.geekhub.crypto.CodecUsecase;
import org.geekhub.crypto.CodingAudit;
import org.geekhub.crypto.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = RestAnalyticsController.class)
public class RestAnalyticsControllerTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CodingAudit codingAudit = mock(CodingAudit.class);
    @MockBean
    private LoggerFactory lf;
    @MockBean
    private UserDetailsService userDetailsServiceImpl;
    @MockBean
    private PasswordEncoder passwordEncoder;

    @Test
    public void test_getCountEncodingInputs_is_success() throws Exception {
        mockMvc.perform(
                get("/api/analytics/countEncodingInputs")
                        .with(user("admin").roles("ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$['listOfEntries']").value(new ArrayList<>()));
        verify(codingAudit, times(0)).countEncodingInputs();
    }

    @Test
    public void test_getPopularCodec_with_encoding_usecase_is_success() throws Exception {
        mockMvc.perform(
                get("/api/analytics/popularCodec")
                        .param("usecase", "encoding")
                        .with(user("admin").roles("ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());
        verify(codingAudit, times(0)).findMostPopularCodec(CodecUsecase.ENCODING);
    }

    @Test
    public void test_getPopularCodec_with_decoding_usecase_is_success() throws Exception {
        mockMvc.perform(
                get("/api/analytics/popularCodec")
                        .param("usecase", "decoding")
                        .with(user("admin").roles("ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk());
        verify(codingAudit, times(0)).findMostPopularCodec(CodecUsecase.DECODING);
    }

    @Test
    public void test_getCountByDate_with_encoding_usecase_is_success() throws Exception {
        mockMvc.perform(
                get("/api/analytics/countByDate")
                        .param("usecase", "encoding")
                        .with(user("admin").roles("ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$['listOfEntries']").value(new ArrayList<>()));
        verify(codingAudit, times(0)).countCodingsByDate(CodecUsecase.ENCODING);
    }

    @Test
    public void test_getCountByDate_with_decoding_usecase_is_success() throws Exception {
        mockMvc.perform(
                get("/api/analytics/countByDate")
                        .param("usecase", "decoding")
                        .with(user("admin").roles("ADMIN"))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$['listOfEntries']").value(new ArrayList<>()));
        verify(codingAudit, times(0)).countCodingsByDate(CodecUsecase.DECODING);
    }
}