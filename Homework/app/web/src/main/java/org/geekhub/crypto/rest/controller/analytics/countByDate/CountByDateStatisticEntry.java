package org.geekhub.crypto.rest.controller.analytics.countByDate;

import java.time.LocalDate;

public class CountByDateStatisticEntry {

    private LocalDate date;
    private long count;

    public CountByDateStatisticEntry(LocalDate date, long count) {
        this.date = date;
        this.count = count;
    }

    public LocalDate getDate() {
        return date;
    }

    public long getCount() {
        return count;
    }
}


