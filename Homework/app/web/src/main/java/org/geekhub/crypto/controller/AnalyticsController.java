package org.geekhub.crypto.controller;

import org.geekhub.crypto.AnalyticsActions;
import org.geekhub.crypto.CodecUsecase;
import org.geekhub.crypto.CodingAudit;
import org.geekhub.crypto.factory.Algorithms;
import org.geekhub.crypto.AnalyticsActionsEnumConverter;
import org.geekhub.crypto.CodecUsecaseEnumConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.Map;

@RequestMapping(path = "/analytics")
@Controller
public class AnalyticsController {
    private CodingAudit codingAudit;

    public AnalyticsController(CodingAudit codingAudit) {
        this.codingAudit = codingAudit;
    }

    @GetMapping("/")
    public String getAnalytics(Model model) {
        model.addAttribute("analyticActions", AnalyticsActions.values());
        return "analytics/analytics";
    }

    @GetMapping("/countEncodingInputs")
    public String getCountEncodingInputs(Model model) {
        Map<String, Long> encodingInputsMap = codingAudit.countEncodingInputs();
        model.addAttribute("encodingInputsMap", encodingInputsMap);
        return "analytics/countEncodingInputs";
    }


    @GetMapping("/usecases")
    public String getUsecases(HttpServletRequest request, Model model) {
        model.addAttribute("usecases", CodecUsecase.values());
        model.addAttribute("analyticsAction", request.getParameter("action"));
        return "analytics/usecases";
    }

    @PostMapping("/usecases")
    public String getAnalyticResult(HttpServletRequest request, Model model) {
        String action = request.getParameter("action");
        String usecaseParam = request.getParameter("usecase");

        CodecUsecase usecase = new CodecUsecaseEnumConverter().convert(usecaseParam);
        AnalyticsActions analyticsActions = new AnalyticsActionsEnumConverter().convert(action);

        if (analyticsActions == AnalyticsActions.POPULAR_CODEC) {
            Algorithms mostPopularCodec = codingAudit.findMostPopularCodec(usecase);
            model.addAttribute("mostPopularCodec", mostPopularCodec);
            return "analytics/mostPopularCodec";
        } else {
            Map<LocalDate, Long> sortedByDateMap = codingAudit.countCodingsByDate(usecase);
            model.addAttribute("sortedByDateMap", sortedByDateMap);
            return "analytics/sortByDate";
        }
    }
}
