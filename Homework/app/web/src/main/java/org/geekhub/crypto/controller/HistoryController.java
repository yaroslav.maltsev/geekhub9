package org.geekhub.crypto.controller;


import org.geekhub.crypto.HistoryActions;
import org.geekhub.crypto.HistoryManager;
import org.geekhub.crypto.HistoryRecord;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

@RequestMapping("/history")
@Controller
public class HistoryController {

    private HistoryManager historyManager;

    public HistoryController(HistoryManager historyManager) {
        this.historyManager = historyManager;
    }

    @GetMapping("/")
    public String getAllHistoryActions(Model model, Authentication authentication) {
        boolean isAdmin = hasRole("ROLE_ADMIN", authentication);
        if (isAdmin) {
            model.addAttribute("historyActions", HistoryActions.values());
        } else {
            HistoryActions[] showAction = new HistoryActions[] {HistoryActions.SHOW};
            model.addAttribute("historyActions", showAction);
        }

        return "history/historyActions";
    }

    @PostMapping("/")
    public void executeHistoryAction(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String historyActionParameter = request.getParameter("history-action").toUpperCase();
        HistoryActions historyAction;
        try {
            historyAction = HistoryActions.valueOf(historyActionParameter);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Wrong action parameter: " + historyActionParameter, e);
        }


        if (historyAction == HistoryActions.SHOW) {
            response.sendRedirect("/history/show");
        } else if (historyAction == HistoryActions.CLEAR) {
            response.sendRedirect("/history/clear");
        } else if (historyAction == HistoryActions.DELETE_LAST_ELEMENT) {
            response.sendRedirect("/history/deleteLast");
        } else {
            throw new UnsupportedOperationException("No such operation: " + historyActionParameter);
        }
    }

    @GetMapping("/show")
    public String showAllHistoryRecords(Model model) {
        List<HistoryRecord> historyRecordList = historyManager.getAllHistoryRecords();
        model.addAttribute("historyList", historyRecordList);
        return "history/showHistory";
    }

    @GetMapping("/deleteLast")
    public void deleteLastElementInHistory(HttpServletResponse response) throws IOException {
        historyManager.deleteLastElementInHistory();
        response.sendRedirect("/crypto");
    }
    @GetMapping("/clear")
    public void clearHistory(HttpServletResponse response) throws IOException {
        historyManager.clearHistory();
        response.sendRedirect("/crypto");
    }

    private boolean hasRole(String role, Authentication authentication) {
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        return authorities
                .stream()
                .anyMatch(
                        r -> r.getAuthority().equals(role)
                );
    }
}