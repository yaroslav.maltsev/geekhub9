package org.geekhub.crypto.rest.controller.analytics.countByDate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class CountByDateStatisticDto {
    private List<CountByDateStatisticEntry> listOfEntries;

    public CountByDateStatisticDto(Map<LocalDate, Long> countedByDateMap) {
        if (Objects.isNull(countedByDateMap)) {
            throw new IllegalArgumentException("CountedByDate map must be not null!");
        }
        listOfEntries = convertMapInEntryList(countedByDateMap);
    }

    public List<CountByDateStatisticEntry> getListOfEntries() {
        return listOfEntries;
    }

    private List<CountByDateStatisticEntry> convertMapInEntryList(Map<LocalDate, Long> countedByDateMap) {
        List<CountByDateStatisticEntry> entryList = new ArrayList<>();
        for (Map.Entry<LocalDate, Long> entry : countedByDateMap.entrySet()) {
            CountByDateStatisticEntry countByDateStatisticEntry = getCountByDateStatisticEntry(entry);
            entryList.add(countByDateStatisticEntry);
        }
        return entryList;
    }

    private CountByDateStatisticEntry getCountByDateStatisticEntry(Map.Entry<LocalDate, Long> mapEntry) {
        return new CountByDateStatisticEntry(mapEntry.getKey(), mapEntry.getValue());
    }
}
