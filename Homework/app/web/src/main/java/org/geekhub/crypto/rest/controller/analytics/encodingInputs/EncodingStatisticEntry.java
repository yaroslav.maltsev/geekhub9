package org.geekhub.crypto.rest.controller.analytics.encodingInputs;

public class EncodingStatisticEntry {
    private String input;
    private long number;

    public EncodingStatisticEntry(String input, long number) {
        this.input = input;
        this.number = number;
    }

    public String getInput() {
        return input;
    }

    public long getNumber() {
        return number;
    }
}
