package org.geekhub.crypto.rest.controller.history;

import org.geekhub.crypto.HistoryManager;
import org.geekhub.crypto.HistoryRecord;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/api/history")
@RestController
public class RestHistoryController {

    private HistoryManager historyManager;

    public RestHistoryController(HistoryManager historyManager) {
        this.historyManager = historyManager;
    }

    @GetMapping("/show")
    public HistoryShowDto showAllHistoryRecords(){
        List<HistoryRecord> allHistoryRecords = historyManager.getAllHistoryRecords();
        return new HistoryShowDto(allHistoryRecords);
    }

    @PostMapping("/deleteLast")
    public void deleteLastElementInHistory(){
        historyManager.deleteLastElementInHistory();
    }

    @PostMapping("/clear")
    public void clearHistory(){
        historyManager.clearHistory();
    }
}