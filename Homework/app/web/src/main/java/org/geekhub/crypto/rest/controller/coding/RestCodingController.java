package org.geekhub.crypto.rest.controller.coding;

import org.geekhub.crypto.Decoder;
import org.geekhub.crypto.Encoder;
import org.geekhub.crypto.factory.Algorithms;
import org.geekhub.crypto.factory.DecodersFactory;
import org.geekhub.crypto.factory.EncodersFactory;
import org.geekhub.crypto.AlgorithmsEnumConverter;
import org.geekhub.crypto.HistoryManager;
import org.geekhub.crypto.HistoryRecord;
import org.geekhub.crypto.MenuOperation;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RequestMapping("/api/coding")
@RestController
public class RestCodingController {
    private HistoryManager historyManager;
    private EncodersFactory encodersFactory;
    private DecodersFactory decodersFactory;

    public RestCodingController(
            HistoryManager historyManager,
            EncodersFactory encodersFactory,
            DecodersFactory decodersFactory
    ) {
        this.historyManager = historyManager;
        this.encodersFactory = encodersFactory;
        this.decodersFactory = decodersFactory;
    }

    @GetMapping("/encoding")
    @ResponseBody
    public String getEncodedMessage(
            @RequestParam String codec,
            @RequestParam String message
    ) {
        Algorithms algorithms = new AlgorithmsEnumConverter().convert(codec);

        HistoryRecord historyRecord = new HistoryRecord(message, algorithms, MenuOperation.ENCODE, LocalDate.now());
        historyManager.addToHistory(historyRecord);

        Encoder encoder = encodersFactory.getEncoder(algorithms);
        return encoder.encode(message);
    }

    @GetMapping("/decoding")
    @ResponseBody
    public String getDecodedMessage(
            @RequestParam String codec,
            @RequestParam String message
    ) {
        Algorithms algorithms = new AlgorithmsEnumConverter().convert(codec);

        HistoryRecord historyRecord = new HistoryRecord(message, algorithms, MenuOperation.DECODE, LocalDate.now());
        historyManager.addToHistory(historyRecord);

        Decoder decoder = decodersFactory.getDecoder(algorithms);
        return decoder.decode(message);
    }
}
