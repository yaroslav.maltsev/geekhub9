package org.geekhub.crypto.controller;

import org.geekhub.crypto.MenuActions;
import org.geekhub.crypto.Decoder;
import org.geekhub.crypto.Encoder;
import org.geekhub.crypto.factory.Algorithms;
import org.geekhub.crypto.factory.DecodersFactory;
import org.geekhub.crypto.factory.EncodersFactory;
import org.geekhub.crypto.AlgorithmsEnumConverter;
import org.geekhub.crypto.MenuActionsEnumConverter;
import org.geekhub.crypto.HistoryManager;
import org.geekhub.crypto.HistoryRecord;
import org.geekhub.crypto.MenuOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Objects;

@Controller
public class CodingController {
    private EncodersFactory encodersFactory;
    private DecodersFactory decodersFactory;
    private HistoryManager historyManager;

    public CodingController(EncodersFactory encodersFactory, DecodersFactory decodersFactory, HistoryManager historyManager) {
        this.encodersFactory = encodersFactory;
        this.decodersFactory = decodersFactory;
        this.historyManager = historyManager;
    }

    @GetMapping("/codecs")
    public String showCodecs(HttpServletRequest request, Model model) {
        String actionParameterValue = request.getParameter("action");
        model.addAttribute("codecs", Algorithms.values());
        model.addAttribute("actionParameter", actionParameterValue);
        return "codecs";
    }

    @PostMapping("/codecs")
    public void getCodingAnswer(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String text = request.getParameter("text");
        String action = request.getParameter("action");
        String codec = request.getParameter("codec");

        if (Objects.isNull(text) || Objects.isNull(action) || Objects.isNull(codec)) {
            throw new IllegalArgumentException("Parameters should be not null!");
        }

        MenuActions codingAction = new MenuActionsEnumConverter().convert(action);
        Algorithms algorithms = new AlgorithmsEnumConverter().convert(codec);

        MenuOperation menuOperation;
        String result;
        if (codingAction == MenuActions.ENCODING) {
            Encoder encoder = encodersFactory.getEncoder(algorithms);
            result = encoder.encode(text);
            menuOperation = MenuOperation.ENCODE;
        } else {
            Decoder decoder = decodersFactory.getDecoder(algorithms);
            result = decoder.decode(text);
            menuOperation = MenuOperation.DECODE;
        }
        HistoryRecord historyRecord = new HistoryRecord(text, algorithms, menuOperation, LocalDate.now());
        historyManager.addToHistory(historyRecord);
        response.sendRedirect("/answer?result=" + result);
    }

    @GetMapping("/answer")
    public String showAnswer(HttpServletRequest request, Model model) {
        String result = request.getParameter("result");
        model.addAttribute("result", result);
        return "answer";
    }
}