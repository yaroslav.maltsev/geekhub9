package org.geekhub.crypto;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableWebSecurity
@EnableConfigurationProperties(DictionaryConfig.class)
@EnableCaching
@EnableScheduling
@PropertySource(
        {
                "classpath:codec/caesar.properties",
                "classpath:codec/morse.properties",
                "classpath:codec/vigenere.properties",
                "classpath:codec/vocabulary.properties",
                "classpath:logger.properties"
        }
)
public class WebApplication {
    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }
}