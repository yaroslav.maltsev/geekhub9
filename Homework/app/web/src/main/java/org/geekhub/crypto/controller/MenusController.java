package org.geekhub.crypto.controller;

import org.geekhub.crypto.MenuActions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MenusController {

    @GetMapping("/admin/menu")
    public String getAdminMenu(Model model) {
        MenuActions[] adminMenuActions = new MenuActions[] {
                MenuActions.DECODING,
                MenuActions.ENCODING,
                MenuActions.HISTORY,
                MenuActions.ANALYTICS,
        };
        model.addAttribute("allMenuActions", adminMenuActions);
        return "menu/adminMenu";
    }

    @GetMapping("/")
    public String getGuestMenu(Model model){

        MenuActions[] menuActions = new MenuActions[] {
                MenuActions.DECODING,
                MenuActions.ENCODING
        };
        model.addAttribute("menuActions", menuActions);
        return "menu/guestMenu";
    }

    @GetMapping("/user/menu")
    public String getUserMenu(Model model){
        MenuActions[] menuActions = new MenuActions[] {
                MenuActions.DECODING,
                MenuActions.ENCODING,
                MenuActions.HISTORY
        };
        model.addAttribute("menuActions", menuActions);
        return "menu/userMenu";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }
}
