package org.geekhub.crypto.rest.advice;

public class ErrorDto {
    private final String description;

    public ErrorDto(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}