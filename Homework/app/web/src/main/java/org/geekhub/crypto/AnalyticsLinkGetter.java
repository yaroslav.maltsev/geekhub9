package org.geekhub.crypto;


public class AnalyticsLinkGetter {
    private AnalyticsLinkGetter(){}

    public static String getLink(AnalyticsActions enumeration) {
        validateEnum(enumeration);
        switch (enumeration) {
            case POPULAR_CODEC:
            case SORT_BY_DATE:
                return "usecases?action=" + enumeration.name();
            case COUNT_ENCODING_INPUTS:
                return "countEncodingInputs";
            default:
                throw new IllegalArgumentException("No link for " + enumeration);
        }
    }

    private static void validateEnum(AnalyticsActions enumeration) {
        if (enumeration == null) {
            throw new IllegalArgumentException("enum must be not null!");
        }
    }
}
