package org.geekhub.crypto.rest.controller.analytics.encodingInputs;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class EncodingStatisticDto {
    private List<EncodingStatisticEntry> listOfEntries;

    public EncodingStatisticDto(Map<String, Long> countedEncodingInputsMap) {
        if (Objects.isNull(countedEncodingInputsMap)) {
            throw new IllegalArgumentException("CountedEncodingInputsMap must be not null!");
        }
        listOfEntries = convertMapInEntryList(countedEncodingInputsMap);
    }

    public List<EncodingStatisticEntry> getListOfEntries() {
        return listOfEntries;
    }

    private List<EncodingStatisticEntry> convertMapInEntryList(Map<String, Long> countedEncodingInputsMap) {
        List<EncodingStatisticEntry> entryList = new ArrayList<>();
        for (Map.Entry<String, Long> entry : countedEncodingInputsMap.entrySet()) {
            EncodingStatisticEntry encodingStatisticEntry = getEncodingStatisticEntry(entry);
            entryList.add(encodingStatisticEntry);
        }
        return entryList;
    }

    private EncodingStatisticEntry getEncodingStatisticEntry(Map.Entry<String, Long> mapEntry) {
        return new EncodingStatisticEntry(mapEntry.getKey(), mapEntry.getValue());
    }
}
