package org.geekhub.crypto;

public class MainMenuLinkGetter {
    private MainMenuLinkGetter() {}
    public static String getLink(MenuActions enumeration) {
        validateEnum(enumeration);
        if (enumeration == MenuActions.ENCODING || enumeration == MenuActions.DECODING) {
            return "codecs?action=" + enumeration.name().toLowerCase();
        } else {
            return enumeration.name().toLowerCase();
        }
    }

    private static void validateEnum(MenuActions enumeration) {
        if (enumeration == null) {
            throw new IllegalArgumentException("enum must be not null!");
        }
    }
}
