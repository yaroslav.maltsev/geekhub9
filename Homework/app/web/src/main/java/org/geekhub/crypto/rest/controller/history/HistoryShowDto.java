package org.geekhub.crypto.rest.controller.history;

import org.geekhub.crypto.HistoryRecord;

import java.util.List;
import java.util.Objects;

public class HistoryShowDto {
    private List<HistoryRecord> historyRecords;

    public HistoryShowDto(List<HistoryRecord> historyRecords) {
        if (Objects.isNull(historyRecords)) {
            throw new IllegalArgumentException("HistoryRecords should be not null!");
        }
        this.historyRecords = historyRecords;
    }

    public List<HistoryRecord> getHistoryRecords() {
        return historyRecords;
    }
}
