package org.geekhub.crypto.interceptor;


import org.geekhub.crypto.Logger;
import org.geekhub.crypto.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.Objects;

@Component
public class ControllerLogInterceptor extends HandlerInterceptorAdapter {
    private final Logger logger;

    public ControllerLogInterceptor(LoggerFactory loggerFactory) {
        this.logger = loggerFactory.getLogger();
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if (handler instanceof HandlerMethod) {
            logger.warn(
                    "[" + LocalDateTime.now() + "]: [" + ((HandlerMethod) handler).getBeanType() + "]"
                            + "   -   START:    " + request.getMethod() + " " + request.getRequestURI()
            );
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        if (handler instanceof HandlerMethod) {
            if (Objects.nonNull(ex)) {
                logger.warn(
                        "[" + LocalDateTime.now() + "]: ["
                        + ((HandlerMethod) handler).getBeanType() + "]   -   ERROR:    "
                        + request.getMethod() + " " + request.getRequestURI() + ": " + ex.toString()
                );
            } else {
                logger.warn(
                        "[" + LocalDateTime.now() + "]: ["
                        + ((HandlerMethod) handler).getBeanType() + "]   -   COMPLETE: "
                        + request.getMethod() + " " + request.getRequestURI()
                );
            }
        }
    }
}
