package org.geekhub.crypto.rest.advice;

import org.geekhub.crypto.exceptions.CodecUnsupportedException;
import org.geekhub.crypto.exceptions.EmptyHistoryException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class ExceptionAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CodecUnsupportedException.class)
    @ResponseStatus(HttpStatus.NOT_IMPLEMENTED)
    public ErrorDto handleCodecUnsupportedException(CodecUnsupportedException e) {
        return new ErrorDto("No such codec: " + e.getCodec()) ;
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ErrorDto handleUsernameNotFoundException() {
        return new ErrorDto("User with this username not found");
    }

    @ExceptionHandler(UnsupportedOperationException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorDto handleUnsupportedOperationException() {
        return new ErrorDto( "Wrong action parameter");
    }

    @ExceptionHandler(EmptyHistoryException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handleEmptyHistoryException() {
        return new ErrorDto( "History for analytic must be not empty");
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorDto handleIllegalArgumentException(IllegalArgumentException e) {
        return new ErrorDto(e.getMessage());
    }
}
