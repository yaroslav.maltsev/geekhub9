package org.geekhub.crypto.rest.controller.analytics;

import org.geekhub.crypto.CodecUsecase;
import org.geekhub.crypto.CodingAudit;
import org.geekhub.crypto.factory.Algorithms;
import org.geekhub.crypto.CodecUsecaseEnumConverter;
import org.springframework.web.bind.annotation.*;
import org.geekhub.crypto.rest.controller.analytics.countByDate.CountByDateStatisticDto;
import org.geekhub.crypto.rest.controller.analytics.encodingInputs.EncodingStatisticDto;

import java.time.LocalDate;
import java.util.Map;

@RequestMapping("/api/analytics")
@RestController
public class RestAnalyticsController {
    private final CodecUsecaseEnumConverter codecUsecaseEnumConverter = new CodecUsecaseEnumConverter();

    private CodingAudit codingAudit;

    public RestAnalyticsController(CodingAudit codingAudit) {
        this.codingAudit = codingAudit;
    }

    @GetMapping("/countEncodingInputs")
    public EncodingStatisticDto getCountEncodingInputs() {
        Map<String, Long> encodingInputs = codingAudit.countEncodingInputs();
        return new EncodingStatisticDto(encodingInputs);
    }

    @GetMapping("/popularCodec")
    @ResponseBody
    public Algorithms getPopularCodec(@RequestParam String usecase) {
        CodecUsecase codecUsecase = codecUsecaseEnumConverter.convert(usecase);
        return codingAudit.findMostPopularCodec(codecUsecase);
    }

    @GetMapping("/countByDate")
    @ResponseBody
    public CountByDateStatisticDto getCountByDate(@RequestParam String usecase) {
        CodecUsecase codecUsecase = codecUsecaseEnumConverter.convert(usecase);
        Map<LocalDate, Long> codingsByDate = codingAudit.countCodingsByDate(codecUsecase);
        return new CountByDateStatisticDto(codingsByDate);
    }
}
