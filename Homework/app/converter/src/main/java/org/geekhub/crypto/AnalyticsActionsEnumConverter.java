package org.geekhub.crypto;

import org.springframework.core.convert.converter.Converter;

public class AnalyticsActionsEnumConverter implements Converter<String, AnalyticsActions> {
    @Override
    public AnalyticsActions convert(String source) {
        try {
            return AnalyticsActions.valueOf(source.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("AnalyticsAction with name \"" + source + "\" not exist", e);
        }
    }
}
