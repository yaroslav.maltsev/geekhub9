package org.geekhub.crypto;

import org.springframework.core.convert.converter.Converter;

public class MenuActionsEnumConverter implements Converter<String, MenuActions> {
    @Override
    public MenuActions convert(String source) {
        try {
            return MenuActions.valueOf(source.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("MenuAction with name \"" + source + "\" not exist", e);
        }
    }
}
