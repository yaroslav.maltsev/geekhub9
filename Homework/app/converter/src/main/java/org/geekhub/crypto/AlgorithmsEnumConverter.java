package org.geekhub.crypto;

import org.geekhub.crypto.factory.Algorithms;
import org.springframework.core.convert.converter.Converter;

public class AlgorithmsEnumConverter implements Converter<String, Algorithms> {
    @Override
    public Algorithms convert(String source) {
        try {
            return Algorithms.valueOf(source.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Algorithm with name \"" + source + "\" not exist", e);
        }
    }
}
