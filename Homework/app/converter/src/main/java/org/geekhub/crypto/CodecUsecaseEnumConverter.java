package org.geekhub.crypto;

import org.springframework.core.convert.converter.Converter;

public class CodecUsecaseEnumConverter implements Converter<String, CodecUsecase> {
    @Override
    public CodecUsecase convert(String source) {
        try {
            return CodecUsecase.valueOf(source.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("CodecUsecase with name \"" + source + "\" not exist", e);
        }
    }
}
