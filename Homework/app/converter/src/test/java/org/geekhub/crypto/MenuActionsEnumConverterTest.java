package org.geekhub.crypto;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class MenuActionsEnumConverterTest {
    private MenuActionsEnumConverter menuActionsEnumConverter;

    @BeforeMethod
    public void setUp() {
        menuActionsEnumConverter = new MenuActionsEnumConverter();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_is_empty() {
        menuActionsEnumConverter.convert("");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_is_blank() {
        menuActionsEnumConverter.convert("   ");
    }

    @Test
    public void test_convert_with_lowercase_input_is_success() {
        MenuActions menuActions = menuActionsEnumConverter.convert("analytics");
        assertEquals(menuActions, MenuActions.ANALYTICS);
    }

    @Test
    public void test_convert_with_uppercase_input_is_success() {
        MenuActions menuActions = menuActionsEnumConverter.convert("HISTORY");
        assertEquals(menuActions, MenuActions.HISTORY);
    }

    @Test
    public void test_convert_with_input_in_different_cases_is_success() {
        MenuActions menuActions = menuActionsEnumConverter.convert("DeCoDiNG");
        assertEquals(menuActions, MenuActions.DECODING);
    }

    @Test
    public void check_if_all_algorithm_are_included_is_success() {
        MenuActions[] allEnumValues = MenuActions.values();
        for (MenuActions menuActions : allEnumValues) {
            assertEquals(menuActionsEnumConverter.convert(menuActions.name()), menuActions);
        }
    }
}