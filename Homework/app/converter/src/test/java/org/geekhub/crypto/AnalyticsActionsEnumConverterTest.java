package org.geekhub.crypto;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class AnalyticsActionsEnumConverterTest {
    private AnalyticsActionsEnumConverter analyticsActionsEnumConverter;
    @BeforeMethod
    public void setUp() {
        analyticsActionsEnumConverter = new AnalyticsActionsEnumConverter();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_is_empty() {
        analyticsActionsEnumConverter.convert("");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_is_blank() {
        analyticsActionsEnumConverter.convert("   ");
    }

    @Test
    public void test_convert_with_lowercase_input_is_success() {
        AnalyticsActions analyticsActions = analyticsActionsEnumConverter.convert("popular_codec");
        assertEquals(analyticsActions, AnalyticsActions.POPULAR_CODEC);
    }

    @Test
    public void test_convert_with_uppercase_input_is_success() {
        AnalyticsActions analyticsActions = analyticsActionsEnumConverter.convert("SORT_BY_DATE");
        assertEquals(analyticsActions, AnalyticsActions.SORT_BY_DATE);
    }

    @Test
    public void test_convert_with_input_in_different_cases_is_success() {
        AnalyticsActions analyticsActions = analyticsActionsEnumConverter.convert("COuNT_ENCOdiNG_INpUTS");
        assertEquals(analyticsActions, AnalyticsActions.COUNT_ENCODING_INPUTS);
    }

    @Test
    public void check_if_all_algorithm_are_included_is_success() {
        AnalyticsActions[] allEnumValues = AnalyticsActions.values();
        for (AnalyticsActions analyticsActions : allEnumValues) {
            assertEquals(analyticsActionsEnumConverter.convert(analyticsActions.name()), analyticsActions);
        }
    }
}