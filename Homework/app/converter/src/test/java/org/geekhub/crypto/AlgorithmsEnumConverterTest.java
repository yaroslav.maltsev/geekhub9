package org.geekhub.crypto;

import org.geekhub.crypto.factory.Algorithms;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class AlgorithmsEnumConverterTest {
    private AlgorithmsEnumConverter algorithmsEnumConverter;
    @BeforeMethod
    public void setUp() {
        algorithmsEnumConverter = new AlgorithmsEnumConverter();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_is_empty() {
        algorithmsEnumConverter.convert("");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_is_blank() {
        algorithmsEnumConverter.convert("   ");
    }

    @Test
    public void test_convert_with_lowercase_input_is_success() {
        Algorithms algorithms = algorithmsEnumConverter.convert("caesar");
        assertEquals(algorithms, Algorithms.CAESAR);
    }

    @Test
    public void test_convert_with_uppercase_input_is_success() {
        Algorithms algorithms = algorithmsEnumConverter.convert("morse");
        assertEquals(algorithms, Algorithms.MORSE);
    }

    @Test
    public void test_convert_with_input_in_different_cases_is_success() {
        Algorithms algorithms = algorithmsEnumConverter.convert("ViGeNeRe");
        assertEquals(algorithms, Algorithms.VIGENERE);
    }

    @Test
    public void check_if_all_algorithm_are_included_is_success() {
        Algorithms[] allEnumValues = Algorithms.values();
        for (Algorithms algorithms : allEnumValues) {
            assertEquals(algorithmsEnumConverter.convert(algorithms.name()), algorithms);
        }
    }
}