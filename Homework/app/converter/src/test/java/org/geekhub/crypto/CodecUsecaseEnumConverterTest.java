package org.geekhub.crypto;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CodecUsecaseEnumConverterTest {
    private CodecUsecaseEnumConverter codecUsecaseEnumConverter;
    @BeforeMethod
    public void setUp() {
        codecUsecaseEnumConverter = new CodecUsecaseEnumConverter();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_is_empty() {
        codecUsecaseEnumConverter.convert("");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void should_throwException_when_input_is_blank() {
        codecUsecaseEnumConverter.convert("   ");
    }

    @Test
    public void test_convert_with_lowercase_input_is_success() {
        CodecUsecase codecUsecase = codecUsecaseEnumConverter.convert("decoding");
        assertEquals(codecUsecase, CodecUsecase.DECODING);
    }

    @Test
    public void test_convert_with_uppercase_input_is_success() {
        CodecUsecase codecUsecase = codecUsecaseEnumConverter.convert("ENCODING");
        assertEquals(codecUsecase, CodecUsecase.ENCODING);
    }

    @Test
    public void test_convert_with_input_in_different_cases_is_success() {
        CodecUsecase codecUsecase = codecUsecaseEnumConverter.convert("DeCODiNG");
        assertEquals(codecUsecase, CodecUsecase.DECODING);
    }

    @Test
    public void check_if_all_algorithm_are_included_is_success() {
        CodecUsecase[] allEnumValues = CodecUsecase.values();
        for (CodecUsecase codecUsecase : allEnumValues) {
            assertEquals(codecUsecaseEnumConverter.convert(codecUsecase.name()), codecUsecase);
        }
    }
}