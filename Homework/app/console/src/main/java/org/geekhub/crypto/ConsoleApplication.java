package org.geekhub.crypto;

import org.geekhub.crypto.factory.Algorithms;
import org.geekhub.crypto.factory.DecodersFactory;
import org.geekhub.crypto.factory.EncodersFactory;
import org.geekhub.crypto.exceptions.EmptyHistoryException;
import org.geekhub.crypto.exceptions.IllegalInputException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.UnaryOperator;

import static org.geekhub.crypto.ConsoleMaster.input;

@Service
public class ConsoleApplication {
    private Logger logger;
    private HistoryManager historyManager;
    private ActionOperation actionOperation;
    private EncodersFactory encodersFactory;
    private DecodersFactory decodersFactory;
    private CodingAudit codingAudit;
    private AuthenticationManager authenticationManager;

    public ConsoleApplication(
            LoggerFactory loggerFactory,
            HistoryManager historyManager,
            CodingAudit codingAudit,
            ActionOperation actionOperation,
            EncodersFactory encodersFactory,
            DecodersFactory decodersFactory,
            AuthenticationManager authenticationManager

    ) {
        this.logger = loggerFactory.getLogger();
        this.historyManager = historyManager;
        this.codingAudit = codingAudit;
        this.actionOperation = actionOperation;
        this.encodersFactory = encodersFactory;
        this.decodersFactory = decodersFactory;
        this.authenticationManager = authenticationManager;
    }

    private boolean isApplicationRunning = false;

    public void start() {
        isApplicationRunning = true;
        while (isApplicationRunning) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            MenuActions[] allowedMenuActionsForCurrentUser = getAllowedMenuActions(authentication);
            MenuActions menuAnswer = actionOperation.inputVariantFromPrintedList(allowedMenuActionsForCurrentUser);
            if (menuAnswer == MenuActions.DECODING || menuAnswer == MenuActions.ENCODING) {
                executeMessageCoding(menuAnswer);
            } else if (menuAnswer == MenuActions.ANALYTICS) {
                executeAnalyticsSection();
            } else if (menuAnswer == MenuActions.HISTORY) {
                HistoryActions[] allowedHistoryActionsForCurrentUser = getAllowedHistoryActions(authentication);
                HistoryActions historyAction = actionOperation
                        .inputVariantFromPrintedList(allowedHistoryActionsForCurrentUser);
                executeHistorySection(historyAction);
            } else if (menuAnswer == MenuActions.EXIT){
                this.stop();
            } else if (menuAnswer == MenuActions.LOGIN) {
                login();
            } else if (menuAnswer == MenuActions.LOGOUT) {
                logout();
            }
        }
    }

    public void stop() {
        isApplicationRunning = false;
    }

    private MenuActions[] getAllowedMenuActions(Authentication authentication) {
        MenuActions[] guestMenuActions = {
                MenuActions.DECODING,
                MenuActions.ENCODING,
                MenuActions.LOGIN,
                MenuActions.EXIT
        };

        if (authentication == null) {
            return guestMenuActions;
        } else if (hasRole("ROLE_ADMIN", authentication)) {
            return new MenuActions[] {
                    MenuActions.DECODING,
                    MenuActions.ENCODING,
                    MenuActions.HISTORY,
                    MenuActions.ANALYTICS,
                    MenuActions.LOGOUT,
                    MenuActions.EXIT
            };
        } else if (hasRole("ROLE_USER", authentication)) {
            return new MenuActions[] {
                    MenuActions.DECODING,
                    MenuActions.ENCODING,
                    MenuActions.HISTORY,
                    MenuActions.LOGOUT,
                    MenuActions.EXIT
            };
        } else {
            return guestMenuActions;
        }
    }

    private void executeMessageCoding(MenuActions menuAnswer) {
        System.out.println("Choose algorithm: ");
        Algorithms algorithm = actionOperation.inputVariantFromPrintedList(Algorithms.values());

        MenuOperation operation;
        UnaryOperator<String> func;

        if (menuAnswer == MenuActions.DECODING) {
            operation = MenuOperation.DECODE;
            Decoder decoder = decodersFactory.getDecoder(algorithm);
            func = decoder::decode;
        } else {
            operation = MenuOperation.ENCODE;
            Encoder encoder = encodersFactory.getEncoder(algorithm);
            func = encoder::encode;
        }

        String codedText = null;

        System.out.println("Input a text: ");
        String input = getNotBlankInput();

        while (codedText == null) {
            try {
                codedText = func.apply(input);
            } catch (IllegalInputException e) {
                logger.warn(e.getMessage());
                System.out.println("Try again: ");
                input = getNotBlankInput();
            }
        }

        System.out.println(codedText);

        HistoryRecord historyRecord = new HistoryRecord(input, algorithm, operation, LocalDate.now());
        historyManager.addToHistory(historyRecord);
    }

    private void executeAnalyticsSection() {
        AnalyticsActions analyticsAction = actionOperation.inputVariantFromPrintedList(AnalyticsActions.values());
        if (analyticsAction == AnalyticsActions.SORT_BY_DATE) {
            System.out.println("Choose usecase: ");
            CodecUsecase usecase = actionOperation.inputVariantFromPrintedList(CodecUsecase.values());
            printMap(codingAudit.countCodingsByDate(usecase));
        } else if (analyticsAction == AnalyticsActions.POPULAR_CODEC) {
            System.out.println("Choose usecase: ");
            CodecUsecase usecase = actionOperation.inputVariantFromPrintedList(CodecUsecase.values());
            Algorithms mostPopularAlgorithm;
            try {
                mostPopularAlgorithm = codingAudit.findMostPopularCodec(usecase);
                System.out.println(mostPopularAlgorithm.name());
            } catch (EmptyHistoryException e) {
                logger.warn(e.getMessage());
            }
        } else if (analyticsAction == AnalyticsActions.COUNT_ENCODING_INPUTS) {
            printMap(codingAudit.countEncodingInputs());
        }
    }

    private void executeHistorySection(HistoryActions historyAction) {
        if (historyAction == HistoryActions.SHOW) {
            List<HistoryRecord> list = historyManager.getAllHistoryRecords();
            System.out.println("History(" + list.size() + "): ");
            list.forEach(
                    e -> System.out.println(
                            "\t" + e.getDate() + ":" + e.getOperation() + "|" + e.getAlgorithm() + "," + e.getInput()
                    )
            );

        } else if (historyAction == HistoryActions.DELETE_LAST_ELEMENT) {
            historyManager.deleteLastElementInHistory();
        } else if (historyAction == HistoryActions.CLEAR) {
            historyManager.clearHistory();
        }
    }

    private HistoryActions[] getAllowedHistoryActions(Authentication authentication) {
        if (hasRole("ROLE_ADMIN", authentication)) {
            return HistoryActions.values();
        } else {
            return new HistoryActions[] {
                    HistoryActions.SHOW
            };
        }
    }

    private void login() {
        System.out.println("Username: ");
        String username = getNotBlankInput();

        System.out.println("Password: ");
        String password = getNotBlankInput();

        boolean isAuthenticationSuccess = authenticate(username, password);
        if (!isAuthenticationSuccess) {
            logger.warn("Login failed! Try again: ");
            login();
        }
    }

    private void logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    private boolean authenticate(String principal, String credentials) {
        if (principal == null || credentials == null) {
            return false;
        }
        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(principal, credentials);
            Authentication authenticationToken = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            return true;
        } catch (BadCredentialsException e) {
            return false;
        }
    }

    private String getNotBlankInput() {
        String input = input();
        while (input.isBlank()) {
            logger.warn("Input must be not blank! Try again: ");
            input = input();
        }
        return input;
    }

    private <K, T> void printMap(Map<K, T> map) {
        map.forEach((key, value) -> System.out.println(key + " : " + value));
    }

    private boolean hasRole(String role, Authentication authentication) {
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        return authorities
                .stream()
                .anyMatch(
                        r -> r.getAuthority().equals(role)
                );
    }
}
