package org.geekhub.crypto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@SpringBootApplication(scanBasePackages = {
        "org.geekhub.crypto"
})
@EnableGlobalMethodSecurity(securedEnabled = true)
public class Main {
    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(WebApplication.class, args);
        ConsoleApplication consoleApplication = applicationContext.getBean(ConsoleApplication.class);
        consoleApplication.start();
    }
}
