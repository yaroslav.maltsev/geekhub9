package org.geekhub.crypto;

import java.util.Scanner;

public class ConsoleMaster {
    private ConsoleMaster() {
    }

    public static <E> void printEnumArrayElementsWithIterator(E[] listOfActions) {
        int iterator = 0;
        for (E elem : listOfActions) {
            System.out.println(++iterator + "." + elem);
        }
    }

    public static String input() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
