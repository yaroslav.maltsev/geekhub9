package org.geekhub.crypto;

public enum HistoryActions {
    SHOW, DELETE_LAST_ELEMENT, CLEAR
}
