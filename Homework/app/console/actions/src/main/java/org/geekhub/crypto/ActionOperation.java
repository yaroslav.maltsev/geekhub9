package org.geekhub.crypto;

import org.geekhub.crypto.exceptions.IllegalInputException;
import org.geekhub.crypto.exceptions.OperationUnsupportedException;
import org.springframework.stereotype.Service;

import static org.geekhub.crypto.ConsoleMaster.input;

@Service
public class ActionOperation {
    private Logger logger;
    public ActionOperation(LoggerFactory loggerFactory) {
        logger = loggerFactory.getLogger();
    }

    public <E extends Enum<E>> E inputVariantFromPrintedList(E[] enumClassWithActions) {
        checkEnumArrayOnNull(enumClassWithActions);
        ConsoleMaster.printEnumArrayElementsWithIterator(enumClassWithActions);
        return checkInput(enumClassWithActions);
    }

    private <E extends Enum<E>> E checkInput(E[] listOfActions) {
        E command = null;
        boolean isCommandCorrect = false;
        while (!isCommandCorrect) {
            String inputVariant = input();
            try {
                command = determineCommand(listOfActions, inputVariant);
                isCommandCorrect = true;
            } catch (IllegalInputException | OperationUnsupportedException e) {
                logger.warn(e.getMessage());
            }
        }

        return command;
    }

    private <E extends Enum<E>> E determineCommand(E[] listOfActions, String input) {
        Integer enteredNumber = getIndexFromString(input);
        if (enteredNumber == null) {
            return getActionFromList(listOfActions, input);
        }
        boolean isIndexToBig = enteredNumber > listOfActions.length;
        boolean isIndexToSmall = enteredNumber <= 0;
        if (isIndexToBig || isIndexToSmall) {
            throw new IllegalInputException("Index is wrong", input);
        } else {
            return listOfActions[enteredNumber - 1];
        }
    }

    private Integer getIndexFromString(String text) {
        try {
            return Integer.parseInt(text);
        } catch (Exception e) {
            return null;
        }
    }

    private <E extends Enum<E>> E getActionFromList(E[] listOfActions, String input) {
        for (E action : listOfActions) {
            if (action.name().equalsIgnoreCase(input)) {
                return action;
            }
        }
        throw new OperationUnsupportedException("No such action: " + input);
    }

    private <E extends Enum<E>> void checkEnumArrayOnNull(E[] array) {
        if (array == null) {
            throw new IllegalArgumentException("Class must be not null");
        }
    }
}
