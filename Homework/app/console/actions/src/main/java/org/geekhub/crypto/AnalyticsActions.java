package org.geekhub.crypto;

public enum AnalyticsActions {
    COUNT_ENCODING_INPUTS, POPULAR_CODEC, SORT_BY_DATE
}
