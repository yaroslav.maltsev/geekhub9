package org.geekhub.crypto;

public enum MenuActions {
    DECODING, ENCODING, ANALYTICS, HISTORY, EXIT, LOGIN, LOGOUT
}
